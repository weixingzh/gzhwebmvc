Jquery UI build date: 2017/10/22

Interactions: Draggable, Resizable, Sortable

Theme: Base

CSS Scope is not specified.

The jquery-ui.structure.css file provides the structural CSS rules (such as margins and positioning), and the jquery-ui.theme.css file contains the theming rules (such as backgrounds, colors, and fonts). 
This setup allows you to easily change themes by swapping in new jquery-ui.theme.css files. You can also build a theme from scratch by building on top of jquery-ui.structure.css. 
The jquery-ui.css file contains both jquery-ui.structure.css and jquery-ui.theme.css