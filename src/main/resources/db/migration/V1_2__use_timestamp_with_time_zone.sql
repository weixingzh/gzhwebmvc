-- *********************************************************************
-- Update Database Script
-- *********************************************************************
-- Change Log: ../build/dbChangeLog.xml
-- Ran at: 17-11-19 下午10:44
-- Against: gzhtest@jdbc:postgresql://localhost/gzhtestdb?currentSchema=gzhtest
-- Liquibase version: 3.5.3
-- *********************************************************************

-- Changeset ../build/dbChangeLog.xml::1511102680102-1::CJU (generated)
ALTER TABLE gzhtest.gzh_company ALTER COLUMN access_token_expires TYPE TIMESTAMP WITHOUT TIME ZONE USING (access_token_expires::TIMESTAMP WITH TIME ZONE);

-- Changeset ../build/dbChangeLog.xml::1511102680102-2::CJU (generated)
ALTER TABLE gzhtest.chanjet_user ALTER COLUMN chanjet_cia_token_get_last_time TYPE TIMESTAMP WITHOUT TIME ZONE USING (chanjet_cia_token_get_last_time::TIMESTAMP WITH TIME ZONE);

-- Changeset ../build/dbChangeLog.xml::1511102680102-3::CJU (generated)
ALTER TABLE gzhtest.task_time ALTER COLUMN end_time TYPE TIMESTAMP WITHOUT TIME ZONE USING (end_time::TIMESTAMP WITH TIME ZONE);

-- Changeset ../build/dbChangeLog.xml::1511102680102-4::CJU (generated)
ALTER TABLE gzhtest.feedback ALTER COLUMN last_status_change TYPE TIMESTAMP WITHOUT TIME ZONE USING (last_status_change::TIMESTAMP WITH TIME ZONE);

-- Changeset ../build/dbChangeLog.xml::1511102680102-5::CJU (generated)
ALTER TABLE gzhtest.gzh_task ALTER COLUMN last_status_change TYPE TIMESTAMP WITHOUT TIME ZONE USING (last_status_change::TIMESTAMP WITH TIME ZONE);

-- Changeset ../build/dbChangeLog.xml::1511102680102-6::CJU (generated)
ALTER TABLE gzhtest.gzh_task_step ALTER COLUMN last_status_change TYPE TIMESTAMP WITHOUT TIME ZONE USING (last_status_change::TIMESTAMP WITH TIME ZONE);

-- Changeset ../build/dbChangeLog.xml::1511102680102-7::CJU (generated)
ALTER TABLE gzhtest.gzh_user ALTER COLUMN last_status_change TYPE TIMESTAMP WITHOUT TIME ZONE USING (last_status_change::TIMESTAMP WITH TIME ZONE);

-- Changeset ../build/dbChangeLog.xml::1511102680102-8::CJU (generated)
ALTER TABLE gzhtest.orders ALTER COLUMN pay_status_confirm_time TYPE TIMESTAMP WITHOUT TIME ZONE USING (pay_status_confirm_time::TIMESTAMP WITH TIME ZONE);

-- Changeset ../build/dbChangeLog.xml::1511102680102-9::CJU (generated)
ALTER TABLE gzhtest.orders ALTER COLUMN pay_time TYPE TIMESTAMP WITHOUT TIME ZONE USING (pay_time::TIMESTAMP WITH TIME ZONE);

-- Changeset ../build/dbChangeLog.xml::1511102680102-10::CJU (generated)
ALTER TABLE gzhtest.orders ALTER COLUMN qr_code_expire_time TYPE TIMESTAMP WITHOUT TIME ZONE USING (qr_code_expire_time::TIMESTAMP WITH TIME ZONE);

-- Changeset ../build/dbChangeLog.xml::1511102680102-11::CJU (generated)
ALTER TABLE gzhtest.task_time ALTER COLUMN start_time TYPE TIMESTAMP WITHOUT TIME ZONE USING (start_time::TIMESTAMP WITH TIME ZONE);

-- Changeset ../build/dbChangeLog.xml::1511102680102-12::CJU (generated)
ALTER TABLE gzhtest.referal_cash_out ALTER COLUMN status_change_time TYPE TIMESTAMP WITHOUT TIME ZONE USING (status_change_time::TIMESTAMP WITH TIME ZONE);

-- Changeset ../build/dbChangeLog.xml::1511102680102-13::CJU (generated)
ALTER TABLE gzhtest.feedback ALTER COLUMN submit_time TYPE TIMESTAMP WITHOUT TIME ZONE USING (submit_time::TIMESTAMP WITH TIME ZONE);

-- Changeset ../build/dbChangeLog.xml::1511102680102-14::CJU (generated)
ALTER TABLE gzhtest.orders ALTER COLUMN submit_time TYPE TIMESTAMP WITHOUT TIME ZONE USING (submit_time::TIMESTAMP WITH TIME ZONE);

-- Changeset ../build/dbChangeLog.xml::1511102680102-15::CJU (generated)
ALTER TABLE gzhtest.referal_cash_out ALTER COLUMN submit_time TYPE TIMESTAMP WITHOUT TIME ZONE USING (submit_time::TIMESTAMP WITH TIME ZONE);

-- Changeset ../build/dbChangeLog.xml::1511102680102-16::CJU (generated)
ALTER TABLE gzhtest.bill ALTER COLUMN time_stamp TYPE TIMESTAMP WITHOUT TIME ZONE USING (time_stamp::TIMESTAMP WITH TIME ZONE);

-- Changeset ../build/dbChangeLog.xml::1511102680102-17::CJU (generated)
ALTER TABLE gzhtest.billing_status_change ALTER COLUMN time_stamp TYPE TIMESTAMP WITHOUT TIME ZONE USING (time_stamp::TIMESTAMP WITH TIME ZONE);

-- Changeset ../build/dbChangeLog.xml::1511102680102-18::CJU (generated)
ALTER TABLE gzhtest.referal_transaction ALTER COLUMN time_stamp TYPE TIMESTAMP WITHOUT TIME ZONE USING (time_stamp::TIMESTAMP WITH TIME ZONE);

-- Changeset ../build/dbChangeLog.xml::1511102680102-19::CJU (generated)
ALTER TABLE gzhtest.transaction ALTER COLUMN time_stamp TYPE TIMESTAMP WITHOUT TIME ZONE USING (time_stamp::TIMESTAMP WITH TIME ZONE);

-- Changeset ../build/dbChangeLog.xml::1511102680102-20::CJU (generated)
ALTER TABLE gzhtest.global_controller_exception ALTER COLUMN timestamp TYPE TIMESTAMP WITHOUT TIME ZONE USING (timestamp::TIMESTAMP WITH TIME ZONE);

-- Changeset ../build/dbChangeLog.xml::1511102680102-21::CJU (generated)
ALTER TABLE gzhtest.global_error ALTER COLUMN timestamp TYPE TIMESTAMP WITHOUT TIME ZONE USING (timestamp::TIMESTAMP WITH TIME ZONE);

