-- *********************************************************************
-- Update Database Script
-- *********************************************************************
-- Change Log: ../build/dbChangeLog.xml
-- Ran at: 17-11-8 上午11:43
-- Against: gzhtest@jdbc:postgresql://localhost/gzhtestdb?currentSchema=gzhtest
-- Liquibase version: 3.5.3
-- *********************************************************************

CREATE TABLE task_time (id BIGSERIAL NOT NULL, duration VARCHAR(255), end_time TIMESTAMP WITHOUT TIME ZONE, item_count INT NOT NULL, note VARCHAR(255), start_time TIMESTAMP WITHOUT TIME ZONE, task_name VARCHAR(255), CONSTRAINT task_time_pkey PRIMARY KEY (id));

ALTER TABLE gzh_contact ADD email VARCHAR(255);

ALTER TABLE gzh_user ADD email VARCHAR(255);

ALTER TABLE gzh_task ADD note TEXT;

ALTER TABLE chanjet_client_company ADD close_period VARCHAR(255);

ALTER TABLE chanjet_client_company ADD current_period VARCHAR(255);

ALTER TABLE chanjet_client_company ADD sync_start_period VARCHAR(255);