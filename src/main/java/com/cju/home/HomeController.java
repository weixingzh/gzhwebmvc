package com.cju.home;

import java.io.IOException;
import java.time.OffsetDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Locale;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import com.cju.file.Image;
import com.cju.file.ImageRepository;
import com.cju.file.ImageService;
import com.cju.file.ImageSourceType;
import com.cju.gzhOps.GzhCompany;
import com.cju.security.RepositoryQueryFilter;
import com.cju.user.User;

@Controller
public class HomeController {
    private Logger log = LoggerFactory.getLogger(HomeController.class);
    
    @Autowired
    private FeedbackRepository feedbackRepository;
    
    @Autowired
    private ImageService imageService;
    
    @Autowired 
    RepositoryQueryFilter repoQueryFilter;
    
    @Autowired
    private MessageSource messageSource;
    
    @GetMapping(value={"/", "/pub/home", "/pub/home/about"})
    public String home() {
         return "home/index";
    }
    
    @GetMapping(value={"/gzhOps"})
    public String gzhOpsHome() {
        return "home/index";
    }
    
    @GetMapping(value={"/pub/home/usage"})
    public String usage() {
         return "home/usage";
    }
    
    @GetMapping(value={"/pub/home/serviceTerms"})
    public String serviceTerms() {
        return "home/serviceTerms";
    }
    
    @GetMapping(value={"/pub/home/pricing"})
    public String pricing() {
        return "home/pricing";
    }

    //Anyone can submit a feedback
    //If needed, we can add authentication requirement @PreAuthorize("isAuthenticated()")
    @GetMapping("/pub/home/feedback/create")
    public String getFeedbackCreateForm() {  
        return "home/feedbackCreate";
    }
    
    //Anyone can submit a feedback
    //If needed, we can add authentication requirement @PreAuthorize("isAuthenticated()")
    //This is Non-Ajax request. Response with a view.
    @PostMapping("/pub/home/feedback/create")
    public String submitFeedbackCreateFrom(Feedback feedback, @RequestParam("file") MultipartFile file, Model model) throws IOException {
        log.info("submitFeedbackCreateFrom feedback=" + feedback);
        log.info("submitFeedbackCreateFrom file=" + file);
        
        long size = file.getSize();
        if (size > 0) {
            String fileName = file.getOriginalFilename();
            byte[] inImageData = file.getBytes();
            Image returnedImg = imageService.saveImage(fileName, inImageData, ImageSourceType.ANONYMOUS_UPLOADED, null);
            feedback.setImgId(returnedImg.getId());
        }

        feedback.setStatus(FeedbackStatus.NOT_STARTED);
        OffsetDateTime lastStatusChange = OffsetDateTime.now();
        feedback.setSubmitTime(lastStatusChange);
        Feedback returnedFeedback = feedbackRepository.save(feedback);
        long feedbackId = returnedFeedback.getId();
        String message = messageSource.getMessage("gzh.feedback.save.success", new Object[] {feedbackId}, Locale.SIMPLIFIED_CHINESE);
        model.addAttribute("message", message);
        return "gzhOps/gzhOpsMessage";
    }
    
    @GetMapping(value={"/pub/home/feedback/list"})
    public String getFeedbackList(Pageable pageable, Model model) {
        Page<Feedback> feedbackPage = feedbackRepository.findAll(pageable);
        model.addAttribute("feedbackPage", feedbackPage);
        log.info("getFeedbackList feedbackPage.size=" + feedbackPage.getSize() + ", feedbackPage.Number " + feedbackPage.getNumber());

        return "home/feedbackList";
    }
    
    @GetMapping(value={"/pub/home/feedback/details/{id}"})
    public String getFeedbackDetails(@AuthenticationPrincipal User user, @PathVariable("id") long id, Model model) {
        Feedback feedback = feedbackRepository.findOne(id);
        model.addAttribute("feedback", feedback);
        log.info("getFeedbackDetails feedback=" + feedback);

        String userRoles = "";
        if (user != null)
           userRoles = user.getRoles();
        model.addAttribute("userRoles", userRoles);
        return "home/feedbackDetails";
    }
    
    //REST API
    //REST API does not response with a view. It responses with just data.
    
    //For now, anyone can update the feedback. The client side javascript restricts what can be updated based on user role.
    //If needed, we can add authentication requirement @PreAuthorize("isAuthenticated()")
    //WebAdmin can update all the fields. Others can only update the reply field.
    @PostMapping("/pub/api/v1/feedback")
    public @ResponseBody String postFeedbackEditApi(@AuthenticationPrincipal User user, @RequestBody Feedback feedback) {
        log.info("postFeedbackEditApi feedback=" + feedback);
        Feedback currFeedback = feedbackRepository.findOne(feedback.getId());
        if (currFeedback.getStatus() != feedback.getStatus()) {
            currFeedback.setStatus(feedback.getStatus());
            OffsetDateTime lastStatusChange = OffsetDateTime.now();
            currFeedback.setLastStatusChange(lastStatusChange);
        }
        currFeedback.setNote(feedback.getNote());
        if (feedback.getReply() != "") {
            DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
            String offsetDateTime = OffsetDateTime.now().format(dateTimeFormatter);
            String userName = messageSource.getMessage("gzh.user.anonymous", null, Locale.SIMPLIFIED_CHINESE);
            if(user != null)
                userName = user.getUsername();
            String reply =  messageSource.getMessage("gzh.feedback.reply", null, Locale.SIMPLIFIED_CHINESE);
            String replyWithTime = offsetDateTime.concat(" " + userName + " ").concat(reply+"\n").concat(feedback.getReply());
            String newDescription = currFeedback.getDescription() + "\n" + replyWithTime;
            currFeedback.setDescription(newDescription);
        }
        Feedback returnedFeedback = feedbackRepository.save(currFeedback);
        
        long feedbackId = returnedFeedback.getId();
        String message = messageSource.getMessage("gzh.feedback.save.success", new Object[] {feedbackId}, Locale.SIMPLIFIED_CHINESE);
        return message;
    }
}