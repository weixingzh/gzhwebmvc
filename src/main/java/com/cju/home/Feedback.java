package com.cju.home;

import java.time.OffsetDateTime;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

@Entity
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id", scope = Feedback.class)
@Table(name = "Feedback")
public class Feedback {
    @Transient
    final int DESCRIPTION_SIZE = 100 * 1024;
    
	@Transient
	private Logger log = LoggerFactory.getLogger(Feedback.class);

	//AUTO strategy uses the global number generator to generate a primary key for every new entity object
	//IDENTIFY strategy uses a separate identity generator per type hierarchy, so generated values are unique only per type hierarchy.
	//SEQUENCE strategy generates an automatic value as soon as a new entity object is persisted (i.e. before commit)
	//@SequenceGenerator(name="seq", initialValue=1, allocationSize=100)
	//@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="seq")
    @Id    
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    
    private String title;
    @Column(length=DESCRIPTION_SIZE)
    private String description;
    @Embedded
    private Contact contact = new Contact();
    private long imgId;
    @Column(columnDefinition= "TIMESTAMP WITH TIME ZONE")
    private OffsetDateTime submitTime;
    @Enumerated(EnumType.STRING)
    private FeedbackStatus status;
    @Column(columnDefinition= "TIMESTAMP WITH TIME ZONE")
    private OffsetDateTime lastStatusChange;
    @Column(length=DESCRIPTION_SIZE)
    private String note;
    @Transient
    private String reply;
    private long bugTrackId;
    
    public Feedback() {}
    public Feedback(long id) {
    	this.id = id;
    }

    public void setId(long id) {
    	this.id = id;
    }
    public long getId() {
    	return id;
    }

    public void setTitle(String title) {
        this.title = title;
    }
    public String getTitle() {
        return title;
    }
    
    public void setDescription(String description) {
        this.description = description;
    }
    public String getDescription() {
        return description;
    }
    
    public void setContact(Contact contact) {
        this.contact = contact;
    }
    public Contact getContact() {
        return contact;
    }
    
    public void setImgId(long imgId) {
        this.imgId = imgId;
    }
    public long getImgId() {
        return imgId;
    }
    
    public void setSubmitTime(OffsetDateTime submitTime) {
        this.submitTime = submitTime;
    }
    public OffsetDateTime getSubmitTime() {
        return submitTime;
    }
    
    public void setStatus(FeedbackStatus status) {
        this.status = status;
    }
    public FeedbackStatus getStatus() {
        return status;
    }
    
    public void setLastStatusChange(OffsetDateTime lastStatusChange) {
        this.lastStatusChange = lastStatusChange;
    }
    public OffsetDateTime getLastStatusChange() {
        return lastStatusChange;
    }
    
    public void setNote(String note) {
        this.note = note;
    }
    public String getNote() {
        return note;
    }
    
    public void setReply(String reply) {
        this.reply = reply;
    }
    public String getReply() {
        return reply;
    }
    
    public void setBugTrackId(long bugTrackId) {
        this.bugTrackId = bugTrackId;
    }
    public long getBugTrackId() {
        return bugTrackId;
    }
    
    @Override
    public String toString() {
        return String.format("Feedback[id=%d,  title='%s', description='%s']", id,  title, description);
    }
}