package com.cju.home;

import com.fasterxml.jackson.annotation.JsonProperty;

public enum FeedbackStatus {
    @JsonProperty("未开始") NOT_STARTED("NotStarted"), @JsonProperty("调查中") INVESTIGATE("Investigate"),  @JsonProperty("问题-已解决") ISSUE_RESOLVED("Issue-Resolved"),
    @JsonProperty("非问题-已关闭") NOT_ISSUE_CLOSED("NotIssue-Closed");
    
    private String feedbackStatus;
    FeedbackStatus(String feedbackStatus) {
        this.feedbackStatus = feedbackStatus;
    }
    
    @Override
    public String toString() {
        switch (feedbackStatus) {
            case "NotStarted": 
                return "未开始";
            case "Investigate":
                return "调查中";
            case "Issue-Resolved":
                return "问题-已解决";
            case "NotIssue-Closed":
                return "非问题-已关闭";
            default: 
                return "未开始";
        }
    }
}
