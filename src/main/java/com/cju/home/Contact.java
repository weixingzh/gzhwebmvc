package com.cju.home;

import javax.persistence.Embeddable;

@Embeddable
public class Contact {

    private String userName;
	private String name;
    private String email;
    private String mobile;
    
    public Contact() {}

    public void setUserName(String userName) {
        this.userName = userName;
    }
    public String getUserName() {
        return userName;
    }
    
    public void setName(String name) {
        this.name = name;
    }
    public String getName() {
        return name;
    }
    
    public void setEmail(String email) {
        this.email = email;
    }
    public String getEmail() {
        return email;
    }
    
    public void setMobile(String mobile) {
        this.mobile = mobile;
    }
    public String getMobile() {
        return mobile;
    }
    
    @Override
    public String toString() {
        return String.format("Contact[name='%s', userName=%s, email='%s']", name, userName, email);
    }
}