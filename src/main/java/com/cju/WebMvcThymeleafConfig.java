package com.cju;

import java.util.Collections;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.ResourceBundleMessageSource;
import org.springframework.web.servlet.ViewResolver;
import org.thymeleaf.dialect.springdata.SpringDataDialect;
import org.thymeleaf.extras.java8time.dialect.Java8TimeDialect;
import org.thymeleaf.extras.springsecurity4.dialect.SpringSecurityDialect;
import org.thymeleaf.spring4.SpringTemplateEngine;
import org.thymeleaf.spring4.templateresolver.SpringResourceTemplateResolver;
import org.thymeleaf.spring4.view.ThymeleafViewResolver;
import org.thymeleaf.templatemode.TemplateMode;
import org.thymeleaf.templateresolver.ClassLoaderTemplateResolver;
import org.thymeleaf.templateresolver.ITemplateResolver;
import nz.net.ultraq.thymeleaf.LayoutDialect;

//spring-boot-starter-thymeleaf is based on older Thymeleaf 2.x version
//Here we configure SpringBoot to use Theymeleaf 3.
@Configuration
public class WebMvcThymeleafConfig implements ApplicationContextAware {
	
    @Autowired
    private MessageSource messageSource;

    @Value("${thymeleaf.templates.cache: false}")
    String thymeleafCache;

	private ApplicationContext applicationContext;

    public void setApplicationContext(ApplicationContext applicationContext) {
        this.applicationContext = applicationContext;
    }
    
    @Bean
    public ITemplateResolver templateResolver() {
        SpringResourceTemplateResolver templateResolver = new SpringResourceTemplateResolver();
        templateResolver.setApplicationContext(applicationContext);
        templateResolver.setPrefix("classpath:/templates/");
        templateResolver.setSuffix(".html");
        templateResolver.setTemplateMode("HTML");
        templateResolver.setCharacterEncoding("UTF-8");

        if (thymeleafCache.equals("true")){
            templateResolver.setCacheable(true);
        } else {
            templateResolver.setCacheable(false);
        }

        return templateResolver;
    }

    @Bean
    public SpringTemplateEngine templateEngine() {
        SpringTemplateEngine templateEngine = new SpringTemplateEngine();
        //SpringTemplateEngine already has SpringStandardDialect() in it.
        
        templateEngine.setEnableSpringELCompiler(true);
        templateEngine.setTemplateResolver(templateResolver());
        templateEngine.setMessageSource(messageSource);
        templateEngine.addDialect(new LayoutDialect());
	    //This will introduce the sd namespace, and the new attribute processors that you to use in your pages: 
	    //   pagination, pagination-sort, pagination-summary, pagination-url, page-object, pagination-qualifier and page-size-selector.
        templateEngine.addDialect(new SpringDataDialect());
        templateEngine.addDialect(new SpringSecurityDialect());
        templateEngine.addDialect(new Java8TimeDialect());

        return templateEngine;
    }

    @Bean
    public ViewResolver viewResolver() {
        ThymeleafViewResolver viewResolver = new ThymeleafViewResolver();
        viewResolver.setTemplateEngine(templateEngine());
        return viewResolver;
    }
    
    //Email template engine
    @Bean
    public SpringTemplateEngine emailTemplateEngine() {
        final SpringTemplateEngine templateEngine = new SpringTemplateEngine();
        
        // Resolver for HTML emails (except the editable one)
        templateEngine.setTemplateResolver(templateResolver());
        
        // Message source, internationalization specific to emails
        templateEngine.setTemplateEngineMessageSource(messageSource);
        
        return templateEngine;
    }
}
