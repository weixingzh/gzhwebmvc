package com.cju;

import java.util.Locale;
import javax.servlet.Filter;
import org.apache.catalina.Context;
import org.apache.catalina.filters.RequestDumperFilter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.embedded.ConfigurableEmbeddedServletContainer;
import org.springframework.boot.context.embedded.EmbeddedServletContainerCustomizer;
import org.springframework.boot.context.embedded.tomcat.TomcatContextCustomizer;
import org.springframework.boot.context.embedded.tomcat.TomcatEmbeddedServletContainerFactory;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.context.support.ResourceBundleMessageSource;
import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.i18n.SessionLocaleResolver;

import com.cju.user.UserDetailsServiceImpl;
import com.fasterxml.jackson.databind.Module;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;

import ch.qos.logback.access.tomcat.LogbackValve;

//To initialize beans used in the application。
//weiXinService is autowired in WebMvcConfig.java and it requires messageSource.
//If we put the messageSource bean in WebMvcConfig.java as well, we will get an error when running the application: 
//Error creating bean with name 'messageSource': Requested bean is currently in creation: Is there an unresolvable circular reference?
//Maybe this is due to the execution order of @Autowired and @Bean annotation in class with @Configuration.

//To declare a bean, simply annotate a method with the @Bean annotation. 
//When JavaConfig encounters such a method, it will execute that method and register the return value as a bean within a BeanFactory.

@Configuration
public class WebMvcBeansConfig {
    private static final Logger log = LoggerFactory.getLogger(WebMvcBeansConfig.class);

    @Autowired
    UserDetailsServiceImpl userDetailsServiceImpl;
    
    
    // Customize DaoAuthenticationProvider
    // setHideUserNotFoundExceptions(false) enables DaoAuthenticationProvider to throw UsernameNotFoundException when username is not found.
    // Without this, DaoAuthenticationProvider throws BadCredentialsException when username is not found.
    @Bean
    public AuthenticationProvider daoAuthenticationProvider() {
        DaoAuthenticationProvider daoAuthenticationProvider = new DaoAuthenticationProvider();
        daoAuthenticationProvider.setHideUserNotFoundExceptions(false) ;
        daoAuthenticationProvider.setUserDetailsService(userDetailsServiceImpl);
        daoAuthenticationProvider.setPasswordEncoder(new BCryptPasswordEncoder());
        return daoAuthenticationProvider;
    }
    
    // When a request comes in, the DispatcherServlet looks for a locale resolver, 
    // and if it finds one it tries to use it to set the locale. Using the RequestContext.getLocale() method, 
    // you can always retrieve the locale that was resolved by the locale resolver.
    @Bean
    public LocaleResolver localeResolver() {
        SessionLocaleResolver localeResolver = new SessionLocaleResolver();
        localeResolver.setDefaultLocale(new Locale("zh", "CN")); 
        return localeResolver;
    }
    
    // When an ApplicationContext is loaded, it automatically searches for a MessageSource bean defined in the context. 
    // The bean must have the name messageSource. If such a bean is found, all calls to the preceding methods are delegated to the message source. 
    // If no message source is found, the ApplicationContext attempts to find a parent containing a bean with the same name.
    @Bean
    public ResourceBundleMessageSource messageSource() {
         ResourceBundleMessageSource source = new ResourceBundleMessageSource();
         //source.setBundleClassLoader(classLoader);
         // name of the resource bundle 
         source.setBasenames("i18n/message");  
         source.setDefaultEncoding("UTF-8");
         //Set whether to use the message code as default message instead of throwing a NoSuchMessageException.
         source.setUseCodeAsDefaultMessage(true);
         return source;
    }
    
    //To register an additional Jackson module. Spring Boot auto detects all Module @Bean. 
    //Since Jackson 2.6.0 the "old" JSR310Module is deprecated and replaced by JavaTimeModule
    //Note that as of 2.6, java8TimeModule does NOT support auto-registration, because of existence of legacy version, JSR310Module. 
    //With Jackson 2.8.9 which is used in this application, javaTimeModule is registered with the default objectMapper.
    @Bean
    public Module java8TimeModule() {
        JavaTimeModule javaTimeModule = new JavaTimeModule();
        return javaTimeModule;
    }
    
    //If you want to replace the default ObjectMapper completely, define a @Bean of that type and mark it as @Primary.
    
    //Defining a @Bean of type Jackson2ObjectMapperBuilder will allow you to customize both default ObjectMapper and XmlMapper
    //Jackson2ObjectMapperBuilder.configure(ObjectMapper objectMapper) will configure an existing ObjectMapper instance with this builder's settings. 
    //I guess the configure method of the new Jackson2ObjectMapperBuilder bean will be called by Springboot.
    //    @Bean
    //    public Jackson2ObjectMapperBuilder objectMapperBuilder() {
    //        Jackson2ObjectMapperBuilder builder = new Jackson2ObjectMapperBuilder();
    //        builder.serializationInclusion(JsonInclude.Include.NON_NULL);
    //        return builder;
    //    }
    //
    //You can also apply the @Autowire annotation to methods with arbitrary names and/or multiple arguments
    //It means the method will be called and arguments will be autowired
    //    @Autowired(required = true)
    //    public void configeJackson(ObjectMapper objectMapper, JavaTimeModule javaTimeModule) {
    //        objectMapper.registerModule(javaTimeModule);
    //    }
    
    //    This object mapper has a problem. It does not serialize the object identifier when testing gzhUser
    //    There is a SpringBoot ObjectMapper instance that can be autowired and works fine.
    //    Will remove this objectMapper. 
    //    @Bean
    //    public ObjectMapper objectMapper() {
    //        ObjectMapper objectMapper = new ObjectMapper();
    //        //register the data type support offered by JavaTimeModule into objectmapper object
    //        //objectMapper.findAndRegisterModules();
    //        return objectMapper;
    //    }

    //All @Scheduled methods share a single thread by default. This override the default behavior.
    //A simpler alternative, the ThreadPoolTaskScheduler, can be used whenever external thread management is not a requirement. 
    //Internally, it delegates to a ScheduledExecutorService instance. 
    //ThreadPoolTaskScheduler actually implements Spring’s TaskExecutor interface as well, 
    //so that a single instance can be used for asynchronous execution as soon as possible as well as scheduled, and potentially recurring, executions.
    @Bean
    public ThreadPoolTaskScheduler threadPoolTaskScheduler(){
        ThreadPoolTaskScheduler threadPoolTaskScheduler = new ThreadPoolTaskScheduler();
        //No method to set the queue size. Maybe the queue is unbounded
        threadPoolTaskScheduler.setPoolSize(10);
        threadPoolTaskScheduler.setThreadNamePrefix("ThreadPoolTaskScheduler");
        return threadPoolTaskScheduler;
    }
    
    //  Logging level for RequestDumperFilter is configured in logback-spring.xml. 
    //  Logging level for RequestDumperFilter does not work if it is configured in application.properties.
    //  There is no control for the format.
    //  RequestDumperFilter will dump http headers for every http request and response.
    //  However, it does not support logging http body. And it only dump to console.
    @Bean
    @Profile("dev-consoledump-http-request-header")
    public FilterRegistrationBean requestDumperFilter() {
        FilterRegistrationBean registration = new FilterRegistrationBean();
        Filter requestDumperFilter = new RequestDumperFilter();
        registration.setFilter(requestDumperFilter);
        registration.addUrlPatterns("/*");
        return registration;
    }

    
    //Valves are tomcat-specific. Filters are standard and their behaviour is defined by the specification. 
    //In most cases you would want a filter.
    //Valves are to be used only for functionalities that requrie access to native-tomcat APIs. Which is rather rare.
    
    // To log the full http request and response, we need the Tomcat LogbackValve, together with the TeeFilter and
    // logback-access.xml config file.
    // The logback-access log file is logs/logback-access.log.
    // This is different from ****Tomcat access**** log file, which is my-tomcat/access_log.yyyy-mm-dd.log and 
    // configured by server.tomcat.accesslog.enabled=true in application-dev.properties.
    
    // In order to diagnose bugs in a web-application, it is often handy to capture the client's request as well as the server's response. 
    // The TeeFilter was designed precisely for this purpose. It should be noted that TeeFilter is a regular servlet filter.
    // Once TeeFilter is installed, the PatternLayout converters fullRequest and fullResponse in logback-access.xml will output 
    // the full contents of the request and respectively the response.
    @Bean(name = "TeeFilter")
    @Profile("dev-log-http-fullrequest")
    public Filter teeFilter() {
        return new ch.qos.logback.access.servlet.TeeFilter();
    }
    
    //By default, LogbackValve looks for a configuration file called logback-access.xml.
    //The ch.qos.logback.access.tomcat.LogbackValve class extends Tomcat's ValveBase class. 
    //Valves are usually associated together to form a processing pipeline.
    @Bean
    @Profile("dev-log-http-fullrequest")
    @ConditionalOnProperty(name = "embedded.tomcat.logback.access.config.file")
    public EmbeddedServletContainerCustomizer containerCustomizer(
        final @Value("${embedded.tomcat.logback.access.config.file:}") String fileName) {
        return new EmbeddedServletContainerCustomizer() {
            @Override
            public void customize(ConfigurableEmbeddedServletContainer container) {
                if (container instanceof TomcatEmbeddedServletContainerFactory) {
                    TomcatEmbeddedServletContainerFactory tomcatContainer = (TomcatEmbeddedServletContainerFactory) container;
                    tomcatContainer.addContextCustomizers(new TomcatContextCustomizer() {
                        @Override
                        public void customize(Context context) {
                            log.info("Customizing TomcatEmbeddedServletContainerFactory to add logbackValve");
                            LogbackValve logbackValve = new LogbackValve();
                            logbackValve.setFilename(fileName);
                            context.getPipeline().addValve(logbackValve);
                        }
                    });
                }
            }
        };
    }
}
