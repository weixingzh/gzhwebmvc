package com.cju;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.time.OffsetDateTime;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.annotation.PostConstruct;
import javax.servlet.Filter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.catalina.Context;
import org.apache.catalina.filters.RequestDumperFilter;
import org.apache.http.HttpStatus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.embedded.ConfigurableEmbeddedServletContainer;
import org.springframework.boot.context.embedded.EmbeddedServletContainerCustomizer;
import org.springframework.boot.context.embedded.tomcat.TomcatContextCustomizer;
import org.springframework.boot.context.embedded.tomcat.TomcatEmbeddedServletContainerFactory;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.DependsOn;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.Profile;
import org.springframework.context.support.ResourceBundleMessageSource;
import org.springframework.http.CacheControl;
import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;
import org.springframework.security.web.method.annotation.AuthenticationPrincipalArgumentResolver;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;
import org.springframework.web.servlet.i18n.LocaleChangeInterceptor;
import org.springframework.web.servlet.i18n.SessionLocaleResolver;
import org.springframework.web.servlet.resource.VersionResourceResolver;
import com.cju.gzh.WeiXinService;
import com.cju.gzh.WeixinAccessTokenAndOpenId;
import com.cju.gzhOps.GzhCompany;
import com.cju.gzhOps.GzhCompanyPaymentStatus;
import com.cju.gzhOps.GzhCompanyRepository;
import com.cju.gzhOps.GzhCompanyServiceRunningStatus;
import com.fasterxml.jackson.databind.Module;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;

import ch.qos.logback.access.tomcat.LogbackValve;

//URL Namespace are divided as the following:
//http://hostname/gzh/ is for receiving weixin user TEXT and EVENT messages, relayed by weixin server.
//http://hostname/gzh/company/{companyId}/* is for receiving weixin user URL requests sent directly from weixin user.
//http://hostname/gzhOps is for backend operations for gzh company admins
//http://hostname/ops is for admin of the whole website.

//If you want to keep Spring Boot MVC features, and you just want to add additional MVC configuration (interceptors, formatters, view controllers etc.)
//you can add your own @Configuration class of type WebMvcConfigurerAdapter, but without @EnableWebMvc.
//If you opt-out of the Spring Boot default MVC configuration by providing your own @EnableWebMvc configuration, 
//then you can take control completely and do everything manually using getMessageConverters from WebMvcConfigurationSupport.

// In case when there is no custom controller logic, we can use addViewControllers() method in our JavaConfig 
// that will map URLs to views and hence there is no need to create actual controller class.

// Guess the /logout functionality is provided by spring-boot-starter-security automatically.
// And upon successfully logging out it will redirect the user to "login-page?logout". 
// login-page is configured in WebSecurityConfig.java

//Do not put @EnableWebMvc here as it will opt out the Spring Boot default MVC configuration.
@Configuration
@EnableAspectJAutoProxy
public class WebMvcConfig extends WebMvcConfigurerAdapter  {
	private static final Logger log = LoggerFactory.getLogger(WebMvcConfig.class);
	
	@Value("${client.cachePeriod}")
    private int cachePeriod;
	
	@Value("${server.cacheResources}")
    private Boolean cacheResources;
	
	@Value("${gzh.weixin.oauth2.host}")
	private String weixinOAuth2Host;
	
	@Autowired
	GzhCompanyRepository companyRepository;
	
	@Autowired
	private WeiXinService weiXinService;
	
    @Autowired
    private MessageSource messageSource;
	
    @Override
    public void addViewControllers(ViewControllerRegistry registry) {
        //If a postrequest mapping is set in a controller, then the addViewController().setViewName() configured here is overridden.
        //registry.addViewController("/").setViewName("index");
    }
    
    //This enables us to use @AuthenticationPrinciple in Controller Methods
    @Override
    public void addArgumentResolvers(List<HandlerMethodArgumentResolver> argumentResolvers) {
        argumentResolvers.add( new AuthenticationPrincipalArgumentResolver());
    }
    
    //ContentVersionStrategy is a good default choice to use except in cases where it cannot be used (e.g. with JavaScript module loaders). 
    //You can configure different version strategies against different patterns as shown below. 
    //Keep in mind also that computing content-based versions is expensive and therefore resource chain caching should be enabled in production.
    //resourceChain(cacheResources) whether to cache the result of resource resolution; 
    //setting this to "true" is recommended for production (and "false" for development, especially when applying a version strategy)
    
    
    //How to generate versioned URLs
    //With a ResourceUrlProvider a resource URL (e.g. /javascript/test.js) can be converted to a versioned URL (e.g. /javascript/test-69ea0cf3b5941340f06ea65583193168.js). 
    //A ResourceUrlProvider bean with the id mvcResourceUrlProvider is automatically declared with the MVC configuration.
    //In case you are using Thymeleaf as template engine, you can access the ResourceUrlProvider bean directly from templates using the @bean syntax.
    
    //TBD: need a way to config for dev profile.
    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
    	log.info("addResourceHandlers: cachePeriod=" + cachePeriod + ", cacheResources=" + cacheResources);
        registry.addResourceHandler("/**")
                .addResourceLocations("classpath:/static/")
                .setCacheControl(CacheControl.maxAge(cachePeriod, TimeUnit.SECONDS).cachePublic())
                .resourceChain(cacheResources).addResolver(new VersionResourceResolver().addContentVersionStrategy("/**"));
    }
    
    // AntPathMatcher
    // ? matches one character
    // * matches zero or more characters
    // ** matches zero or more 'directories' in a path
    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(localeChangeInterceptor());
        registry.addInterceptor(authorizationInterceptor()).addPathPatterns("/gzh/company/{companyId}/**");
    }
    
    //This interceptor will look for a request parameter named lang and will use its value to determine which locale to switch to.
    //@Bean ??? Do not need to make this as a bean as this is registered in addInterceptors() 
    public LocaleChangeInterceptor localeChangeInterceptor() {
        LocaleChangeInterceptor lci = new LocaleChangeInterceptor();
        lci.setParamName("lang");
        return lci;
    }
    
	//When a user sends an EventMsg from weixin client gzh UI, the http request will go through weixin server. Thus this http request will not share 
    //the same HttpSession with those http requests sent from user's weixin client directly to the gzh server. 
    //In these EventMsg http request, the FromUserName in the httpbody in the openId, which is unique for the pair of user and the gzh. 
    //Thus we do not need to intercept these EventMsg http requests.
    
	//For those http requests coming directly from user weixin client by click a URL either within or outside the gzh UI, the URL may not contain user's identity
    //unless the URL is somehow generated by the gzh server with user's identify.
    //For these http requests that we do need to know the user's identity (for example, to check a user's service progress), we will need to intercept the http request here
    //and use OAuth2 to get user's openId and then pass the http request for further processing. 
    //Only intercept request URLs in the format http://hostname/gzh/company/{companuId}/*, see addInterceptors() below.
    //@Bean ??? Do not need to make this as a bean as this is registered in addInterceptors() 
    public HandlerInterceptorAdapter authorizationInterceptor() {
    	HandlerInterceptorAdapter handlerInterceptor = new HandlerInterceptorAdapter () {
    		
    		private GzhCompany getCompanyFromURI(String uri) {
    			long companyId = -1;
				Pattern p = Pattern.compile("/gzh/company/(\\d+)/");
				Matcher m = p.matcher(uri);
				if (m.find())
				{
				  companyId = Long.parseLong(m.group(1));
				}
    			return companyRepository.findOne(companyId);
    		}
    		
    		private long getCompanyIdFromURI(String uri) {
                long companyId = -1;
                Pattern p = Pattern.compile("/gzh/company/(\\d+)/");
                Matcher m = p.matcher(uri);
                if (m.find())
                  companyId = Long.parseLong(m.group(1));
                return companyId;
    		}
    		
    		private void startOAuth2ForOpenId(HttpServletRequest request, HttpServletResponse response) throws IOException {
    		    //How do we get the appId for the user's gzh ?
                //The gzhCompany id must be in the request path. We get appId from gzhCompany.
                GzhCompany company = getCompanyFromURI(request.getRequestURI());
                
                if (company == null) {
                    log.error("HandlerInterceptorAdapter startOAuth2ForOpenId(), company not found. RequestURI=" + request.getRequestURI() 
                            + "An HttpStatus.SC_INTERNAL_SERVER_ERROR will be sent back to weixin user. OAuth2 process will not start.");
                    String message = messageSource.getMessage("gzh.gzhcompany.not.found", null, Locale.SIMPLIFIED_CHINESE);
                    response.sendError(HttpStatus.SC_INTERNAL_SERVER_ERROR, message);
                    //Since error code is set in response, the request will be forwarded to /error url and handled by GlobalErrorController.
                    return;
                }
                else if (company.getServiceRunningStatus() != GzhCompanyServiceRunningStatus.RUNNING)
                {
                    log.info("HandlerInterceptorAdapter startOAuth2ForOpenId, company not in service RUNNING state. RequestURI=" + request.getRequestURI()
                           + "An HttpStatus.SC_UNAUTHORIZED will be sent back to weixin user. OAuth2 process will not start.");
                    String message = messageSource.getMessage("gzh.gzhcompany.not.running", new Object[] {company.getServiceRunningStatus()}, Locale.SIMPLIFIED_CHINESE);
                    response.sendError(HttpStatus.SC_UNAUTHORIZED, message);
                    return;
                }
                
                String appID = company.getAppId();
                
                //Always use snsapi_userinfo SCOPE. 
                //If the user is already a subscriber to the gzh, weixin will make it silent and does not show the authorization UI to user.
                //If the user is not a subscriber, weixin server will show the authorization UI to user. User in this case may deny or grant. 
                //In case the user grants, we can further get user's details info when needed since we used the snsapi_userinfo SCOPE.
                String weixinOAuth2Url = weixinOAuth2Host + "/connect/oauth2/authorize?appid=" + appID
                    + "&redirect_uri=" + request.getRequestURL()
                    + "&response_type=code&scope=snsapi_userinfo&state=returningWeiXinCode&connect_redirect=1#wechat_redirect";
                
                log.info("HandlerInterceptorAdapter startOAuth2ForOpenId() sending response to weixin user to start the OAuth2 process. redirectUrl=" + weixinOAuth2Url);
                //let's redirect to weixin authorization page.
                response.sendRedirect(weixinOAuth2Url);
    		}
    		 
    	    //preHandle returns True if the execution chain should proceed with the next interceptor or the handler itself.
    	    //Else, DispatcherServlet assumes that this interceptor has already dealt with the response itself.
    		@Override
    		public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
    		    String remoteHost = request.getRemoteHost();
    		    String requestUrl = request.getRequestURL().toString();
    		    String queryString = request.getQueryString();
    		    String xRealIp = request.getHeader("X-Real-IP");
    		    String xForwardedFor = request.getHeader("X-Forwarded-For");
    		    log.info("HandlerInterceptorAdapter preHandle() intercepting a request" +
    		             ", requestUrl= "+ requestUrl +
    		             ", queryString= "+ queryString +
    		             ", X-Real-IP= "+ xRealIp + 
    		             ", X-Forwarded-For= " + xForwardedFor + 
    		             ", remoteHost= " + remoteHost);

    		    //request.getRemoteHost() returns the fully qualified name of the client or the last proxy that sent the request.
    			//On AWS elastic beanstalk, nginx is configured on EC2 to forward http traffic received on port 80 to local webserver on port 5000.
    			//As a result, all the http request coming to webserver have remoteHost of localhost or 127.0.0.1
    			//However, nginx on AWS Beanstalk add X-Real-IP and X-Forwarded-For header.
    		    //X-Forwarded-For: client, proxy1, proxy2
    		    //X-Read-IP is retrieved from X-Forwarded-For list by nginx following some rules and configurations.
    		    if (xForwardedFor != null) {
    		        //This is not a local test case. 
    		        //Do nothing, continue
    		    }
    		    else if (remoteHost.contains("0:0:0:0:0:0:0:1") || 
    			    remoteHost.contains("127.0.0.1") ||
    			    remoteHost.contains("localhost")) {
    			    //request coming from localhost for testing purpose.
    			    //We can not do weixin OAuth on local browser
    			    //hard code an openId and continue. 
    			    HttpSession localHostSession = request.getSession(true);
    			    //This is the openId for charlesyju subscribe to charlesyju test gzh. Its gzhUser nickname is charlesyju.
                    String openId = "oN2eB1Z-VSvptcBk1p1I8_01bdOo";
    			    localHostSession.setAttribute("openId", openId);
    			    //Do not need to cache gzhCompanyId as this is a test from local browser.
    			    //gzhCompanyId cached in http session is used to handle the case where a gzhUser subscribes to two gzhCompany and switch the Gzh in one http session.
    			    //localHostSession.setAttribute("gzhCompanyId", company.getId());
                    log.info("HandlerInterceptorAdapter preHandle() requestURL= " + requestUrl +
    			             "This is a request from localhost to test gzh as gzhUser1.  Continue further processing　by gzh controller.");
                    //Continue further processing in controller.
                    return true;
    			}
    			
    		    //When a user click on a VIEW link from weixin client, some times we receive more than one http requests for the user action.
    			//getSession(true) will create a session if a session is not associated with the request.
    			HttpSession session = request.getSession(true);
    			if (session.getAttribute("openId") == null)
    			{
    				log.info("HandlerInterceptorAdapter preHandle() openId is not set in session.");
    				String state = request.getParameter("state");
    				if (state == null || !state.equals("returningWeiXinCode") )
    				{
    					//This is the first time a user without an associated HttpSession reaches our server via a VIEW type button.
        				//We need to get the CODE from WeiXin using OAuth2 by sending redirect response to user's  WeiXin client.
    					//The user's WeiXin client will follow the redirect to GET the weixinOAuth2Url.
    					//WeiXin Server will reply back to user's WeiXin client with a redirect to the user's original request URL configured 
    					//for the VIEW type button the user clicked in the first place. The reply will have CODE and STATE parameters.
    	                log.info("HandlerInterceptorAdapter preHandle() receives a request from a weixin user without an associated HttpSession." 
    	                        + "Calling startOAuth2ForOpenId()");
    				    startOAuth2ForOpenId(request, response);
    				    
        				//Complete the http processing. Do not go further. WeiXin will get back to us with code.
        				return false;
    				}
    				else
    				{
    					//There is a state parameter equals to "returningWeiXinCode"
						//This is the case when WeiXin response to our OAuth2 request for CODE.
						String code = request.getParameter("code");
						if (code.equals("authdeny")) 
						{
							//This can only happen when the user does not subscribe to the gzh, but click on a link to the gzh somewhere.
							//If the user is already a subscriber, the authorization process is silent(user does not see the authorization page) 
							//even we set the SCOPE to be snsapi_userinfo. And weiXin will always return the authorization code.
							log.info("HandlerInterceptorAdapter preHandle() receives follow up request from WeiXin server for OAuth2 process with code=authdeny. requestURL=" + requestUrl);
							//Stop processing further.
							return false;
						}
						else{	
						    log.info("HandlerInterceptorAdapter preHandle() receives follow up request from WeiXin server for OAuth2 process with code=" + code 
						           + ". requestURL=" + requestUrl);
							//Now we have the CODE. We need to use this CODE to get access_token and customer openId.
							//code will be different for every authorization call. It can only used once, and expires in 5 minutes.
							
							//company should exists and be ACTIVE here
	    					GzhCompany company = getCompanyFromURI(request.getRequestURI());
	    					String appID = company.getAppId();
	    					String appSecret = company.getAppSecret();

	    					log.info("HandlerInterceptorAdapter preHandle() request weixin to get user accessToken and openId using the code=" + code
	    					       + " as the next stop in the OAuth2 process.");
							WeixinAccessTokenAndOpenId accessTokenAndOpenId = weiXinService.getUserAccessTokenAndOpenId(appID, appSecret, code);
							if (accessTokenAndOpenId.getOpenId() == null) {
							    //Error in getting the openId, stop further processing. Let user retry.
							    //It happens a lot that weixin server sends two http requests from two different IP when a user click on a url.
							    //We will then try to get openId twice. But weixin server will return the same code.
							    //When we use the code to get openId for the second time, weixin returns error.
							    log.info("HandlerInterceptorAdapter preHandle(): Error in getting openId. Stop further processing."
							           + " errmsg=" + accessTokenAndOpenId.getErrMsg());
							    return false;
							}
							else {
    							//Now we have the openId. 
    							//The accessToken for the gzhUser is not saved and not used.
    							//set the openId attribute in httpSession.
    							session.setAttribute("openId", accessTokenAndOpenId.getOpenId());
    							session.setAttribute("gzhCompanyId", company.getId());
    							log.info("HandlerInterceptorAdapter preHandle(): receives user access token and openId from weixin server."
    							       + " OAuth2 process completed. Set openId in httpSession: " + accessTokenAndOpenId.getOpenId()
    							       + " Continue further processing the requestUrl=" + requestUrl + " in controller.");
    							//Continue further processing in controller.
    							return true;
							}
						}
    				}
    			}
    			else
    			{   // openId is already set in the httpRequest. 
    				long companyIdFromURI = getCompanyIdFromURI(request.getRequestURI());
    				long companyIdFromSession = Long.valueOf(session.getAttribute("gzhCompanyId").toString());
    				if (companyIdFromURI == companyIdFromSession) {
    				    //Pass the request through for further normal processing.
    				    log.info("HandlerInterceptorAdapter preHandle(), openId is already set in session. companyIdFromURI matches companyIdFromSession."
    				           + " Pass to controller for further processing." 
    				           + " openId=" + session.getAttribute("openId").toString()
    				           + ", gzhCompanyId in URL = " + companyIdFromURI
    				           + ", gzhCompanyId cached in http session = " + companyIdFromSession);
    				    return true;
    				}
    				else {
    				    //This is a rare case where a weixin user subscribes to two GZH in our system.
    				    //When the weixin user access to one GZH first and then access another GZH, our httpSession will cache the openId 
    				    //for the first GZH. We need to clear the cached openId and gzhCompanyId and get the openId again.
                        log.info("HandlerInterceptorAdapter preHandle(), openId is already set in session. But companyIdFromURI does not matches companyIdFromSession."
                               + "Clear openId and gzhCompanyId cached in http session. Start OAuth2 for openId again."
                               + "openId=" + session.getAttribute("openId").toString()
                               + "gzhCompanyId in URL = " + companyIdFromURI
                               + "gzhCompanyId cached in http session = " + companyIdFromSession);
    				    session.removeAttribute("openId");
    				    session.removeAttribute("gzhCompanyId");
    				    startOAuth2ForOpenId(request, response);
    				    //Do not go further. WeiXin will get back to us with code.
    				    return false;
    				}
    			}
    		}
    	};

        return handlerInterceptor;
    }
    
}
