package com.cju.testdata;

import java.time.LocalDate;
import java.time.OffsetDateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.context.annotation.Profile;
import org.springframework.core.annotation.Order;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;

import com.cju.billing.Account;
import com.cju.billing.AccountRepository;
import com.cju.billing.BillingService;
import com.cju.chanjet.ChanjetClientCompany;
import com.cju.chanjet.ChanjetClientCompanyRepository;
import com.cju.chanjet.ChanjetUser;
import com.cju.chanjet.ChanjetUserRepository;
import com.cju.gzhOps.GzhClientCompany;
import com.cju.gzhOps.GzhClientCompanyRepository;
import com.cju.gzhOps.GzhCompany;
import com.cju.gzhOps.GzhCompanyBillingStatus;
import com.cju.gzhOps.GzhCompanyMenuType;
import com.cju.gzhOps.GzhCompanyPaymentStatus;
import com.cju.gzhOps.GzhCompanyRepository;
import com.cju.gzhOps.GzhCompanyServiceType;
import com.cju.gzhOps.GzhCompanyServiceRunningStatus;
import com.cju.gzhOps.GzhUser;
import com.cju.gzhOps.GzhUserRepository;
import com.cju.gzhOps.GzhUserStatus;
import com.cju.marketing.Referal;
import com.cju.marketing.ReferalAccount;
import com.cju.marketing.ReferalAccountRepository;
import com.cju.marketing.ReferalRepository;
import com.cju.marketing.ReferalService;
import com.cju.marketing.ReferalStatus;
import com.cju.user.User;
import com.cju.user.UserRepository;

//ApplicationRunner will run once SpringApplication has started.
//CommandLineRunner has access to command line args.
//ApplicationRunner has access to ApplicationArguments passed to SpringApplication.run(WebMvcApplication.class, args) in WebMvcApplication.java

//Only initialize the test database in dev-insert-data profile.
@Profile("dev-insert-data")
//Make sure this runs before GzhStartup
//GzhStartUp has Order(3)
@Order(1)
@Component
public class WebMvcTestDatabaseInit implements ApplicationRunner{
    private static final Logger log = LoggerFactory.getLogger(WebMvcTestDatabaseInit.class);
      
    @Autowired
    GzhCompanyRepository companyRepository;
    
    @Autowired
    AccountRepository accountRepository;
      
    @Autowired
    GzhClientCompanyRepository gzhClientCompanyRepository;
    
    @Autowired
    ChanjetClientCompanyRepository chanjetClientCompanyRepository;
      
    @Autowired
    GzhUserRepository gzhUserRepository;
      
    @Autowired
    UserRepository userRepository;
    
    @Autowired
    ReferalRepository referalRepository;
    
    @Autowired
    ReferalAccountRepository referalAccountRepository;
    
    @Autowired
    ReferalService referalService;
    
    @Autowired
    ChanjetUserRepository chanjetUserRepository;
      
    private String pw123 = encodeUserPassword("123");
      
    private String encodeUserPassword(String password) {
        BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
        return passwordEncoder.encode(password);
    }
  
    // A user can either owns or admin a gzhCompany. 
    // A user also can be associated with a chanjetUser to download reports for all the gzhClientCompany.
    // A gzhUser is associated with a gzhClientCompany.
    // A gzhClientCompany is served by a gzhCompany.
    private void initGzhCompany1() {
        //user1 is owner of gzhCompany1.
        //user1 also admins gzhCompany1
        
        User user1 = new User();
        long idBefore = user1.getId();
        log.info("initGzhCompany1, id of user1 before user1 is saved to userRepository = " + idBefore);
        user1.setUserName("user1");
        user1.setPassword(pw123);
        user1.setEmail("user1.test@gandongtech.com");
        user1.setEnabled(true);
        userRepository.save(user1);
        long idAfter = user1.getId();
        log.info("initGzhCompany1, id of user1 before user1 is saved to userRepository = " + idAfter);
        
        ChanjetUser chanjetUser1 = new ChanjetUser();
        chanjetUser1.setChanjetUserId(201688L);
        chanjetUser1.setChanjetUserName("15916201201");
        chanjetUser1.setChanjetPassword("Ab123456");
        chanjetUser1.setChanjetCiaToken("6ed505cb-41ef-4058-abc1-fe717ea6a1ba");
        chanjetUser1.setUser(user1);
        chanjetUserRepository.save(chanjetUser1);
        
        user1.setChanjetUser(chanjetUser1);
        userRepository.save(user1);

        //user2 admins gzhCompany1
        User user2 = new User();
        user2.setUserName("user2");
        user2.setPassword(pw123);
        user2.setEmail("user2.test@gandongtech.com");
        user2.setEnabled(true);
        userRepository.save(user2);
        
        ChanjetUser chanjetUser2 = new ChanjetUser();
        chanjetUser2.setChanjetUserId(201892L);
        chanjetUser2.setChanjetUserName("15015943775");
        chanjetUser2.setChanjetPassword("Ab123456");
        chanjetUser2.setChanjetCiaToken("414fab62-0ff0-45b1-ae4f-7c98777e8236");
        chanjetUser2.setUser(user2);
        chanjetUserRepository.save(chanjetUser2);

        user2.setChanjetUser(chanjetUser2);
        userRepository.save(user2);
        
        GzhCompany gzhCompany1 = new GzhCompany("charlesyjuweixin测试公众号", "wx654b0a6018713ffb", "e69ee6123ed24bb5199607498846e694", "gh_e922cb3a8a34");
        gzhCompany1.setServiceType(GzhCompanyServiceType.GSSWDL_A);
        gzhCompany1.setPaymentStatus(GzhCompanyPaymentStatus.NO_DEBT);
        gzhCompany1.setBillingStatus(GzhCompanyBillingStatus.TRIAL);
        gzhCompany1.setTrialStartDate(LocalDate.parse("2017-01-31"));
        gzhCompany1.setActiveBillingStartDate(LocalDate.parse("2017-01-31").plusMonths(1));
        gzhCompany1.setServiceRunningStatus(GzhCompanyServiceRunningStatus.NOT_STARTED);
        gzhCompany1.setGzhMenuType(GzhCompanyMenuType.SYSTEM_DEFAULT);
        //OneToOne gzhCompany1 ownedBy user1
        gzhCompany1.setOwnerUser(user1);
        //create an Account for the GzhCompany
        Account account1 = new Account();
        account1.setInternalAccountOwnerName(user1.getUserName());
        accountRepository.save(account1);
        gzhCompany1.setAccount(account1);
        //CASCADE for GzhCompany is set to None. Which means when company is saved, user will not be saved again.
        companyRepository.save(gzhCompany1);
        
        account1.setGzhCompany(gzhCompany1);
        accountRepository.save(account1);
        
        //OneToOne user1 owns gzhCompany1
        user1.setOwnedGzhCompany(gzhCompany1);
        //ManyToOne user1 admins gzhCompany1
        user1.setAdminedGzhCompany(gzhCompany1);
        userRepository.save(user1);
        
        GzhCompany gzhCompany2 = new GzhCompany("Fake GzhCompany 2", "wx222222222", "appsecret222", "gh_gzhid222");
        gzhCompany2.setServiceType(GzhCompanyServiceType.GSSWDL_A);
        gzhCompany2.setPaymentStatus(GzhCompanyPaymentStatus.NO_DEBT);
        gzhCompany2.setBillingStatus(GzhCompanyBillingStatus.TRIAL);
        gzhCompany2.setTrialStartDate(LocalDate.parse("2017-01-15"));
        gzhCompany2.setActiveBillingStartDate(LocalDate.parse("2017-01-15").plusMonths(1));
        gzhCompany2.setServiceRunningStatus(GzhCompanyServiceRunningStatus.NOT_STARTED);
        gzhCompany2.setGzhMenuType(GzhCompanyMenuType.SYSTEM_DEFAULT);
        //OneToOne gzhCompany2 ownedBy user2
        gzhCompany2.setOwnerUser(user2);
        //create an Account for the GzhCompany
        Account account2 = new Account();
        account2.setInternalAccountOwnerName(user2.getUserName());
        accountRepository.save(account2);
        gzhCompany2.setAccount(account2);
        //CASCADE for GzhCompany is set to None. Which means when company is saved, user will not be saved again.
        companyRepository.save(gzhCompany2);
        
        account2.setGzhCompany(gzhCompany2);
        accountRepository.save(account2);
        
        //OneToOne user2 owns gzhCompany2
        user2.setOwnedGzhCompany(gzhCompany2);
        //ManyToOne user2 admins gzhCompany2
        user2.setAdminedGzhCompany(gzhCompany2);
        userRepository.save(user2);
        
        // save a couple of gzhUsers for gzhCompany1
        GzhUser charlesyjuweixinGzhUser = new GzhUser();
        charlesyjuweixinGzhUser.setOpenId("oN2eB1Z-VSvptcBk1p1I8_01bdOo");
        charlesyjuweixinGzhUser.setGzhCompanyByGzhId("gh_e922cb3a8a34");
        charlesyjuweixinGzhUser.setNickName("charlesyju");
        charlesyjuweixinGzhUser.setName("鞠禹");
        gzhUserRepository.save(charlesyjuweixinGzhUser);
        
        log.info("Adding a few gzhUsers for gzhCompany1.");
        for(int i=1; i < 51; i++) {
            String name = "gzhUser" + Integer.toString(i);
            String nickName = "gzhUserNickName" + Integer.toString(i);
            GzhUser gzhUser = new GzhUser(name);
            gzhUser.setNickName(nickName);
            gzhUser.setGzhCompanyByGzhId("gh_e922cb3a8a34");
            gzhUserRepository.save(gzhUser);
        }
        
        //gzhClientCompany1 for gzhCompany1
        ChanjetClientCompany chanjetClientCompany1 = new ChanjetClientCompany();
        chanjetClientCompany1.setAccId(null);
        chanjetClientCompany1.setName("chanjetClientCompany1");
        chanjetClientCompanyRepository.save(chanjetClientCompany1);
        
        GzhClientCompany gzhClientCompany1 = new GzhClientCompany();
        gzhClientCompany1.setChanjetClientCompany(chanjetClientCompany1);
        //ManyToOne gzhClientCompany1 => gzhCompany1 
        gzhClientCompany1.setGzhCompany(gzhCompany1);
        gzhClientCompanyRepository.save(gzhClientCompany1);

        //ManyToMany (gzhUser1, gzhUser11) associated to gzhClientCompany1
        //gzhClientCompany1 is the owning side of ManyToMany relationship. 
        gzhClientCompany1.addGzhUser(charlesyjuweixinGzhUser);
        GzhUser gzhUser11 = gzhUserRepository.findOne(11L);
        gzhClientCompany1.addGzhUser(gzhUser11);
        gzhClientCompany1.setFirstImportDate(LocalDate.parse("2016-12-25"));
        gzhClientCompanyRepository.save(gzhClientCompany1);

        //gzhClientCompany2 for gzhCompany1
        ChanjetClientCompany chanjetClientCompany2 = new ChanjetClientCompany();
        chanjetClientCompany2.setAccId(null);
        chanjetClientCompany2.setName("chanjetClientCompany2");
        chanjetClientCompanyRepository.save(chanjetClientCompany2);
        
        GzhClientCompany gzhClientCompany2 = new GzhClientCompany();
        gzhClientCompany2.setChanjetClientCompany(chanjetClientCompany2);
        //ManyToOne gzhClientCompany2 => gzhCompany1 
        gzhClientCompany2.setGzhCompany(gzhCompany1);
        gzhClientCompanyRepository.save(gzhClientCompany2);
        
        //gzhUser2 is associated with gzhClientCompany2
        //gzhClientCompany2 is the owning side of ManyToMany relationship. 
        GzhUser gzhUser2 = gzhUserRepository.findOne(2L);
        gzhClientCompany2.addGzhUser(gzhUser2);
        gzhClientCompany2.setFirstImportDate(LocalDate.parse("2017-01-16"));
        gzhClientCompanyRepository.save(gzhClientCompany2);
        
        //gzhClientCompany3 for gzhCompany1
        //gzhClientCompany3 is not associated to a ghzUser
        ChanjetClientCompany chanjetClientCompany3 = new ChanjetClientCompany();
        chanjetClientCompany3.setAccId(null);
        chanjetClientCompany3.setName("chanjetClientCompany3");
        chanjetClientCompanyRepository.save(chanjetClientCompany3);
        
        GzhClientCompany gzhClientCompany3 = new GzhClientCompany();
        gzhClientCompany3.setChanjetClientCompany(chanjetClientCompany3);
        //Maintain the owning side (with FK) of ManyToOne relationship. 
        gzhClientCompany3.setGzhCompany(gzhCompany1);
        gzhClientCompany3.setFirstImportDate(LocalDate.parse("2017-02-25"));
        gzhClientCompanyRepository.save(gzhClientCompany3);
  }
  
  private void initGzhCompany51() {
        //user51 is owner of gzhCompany51.
        //user51 does not have associated chanjetUser
        User user51 = new User();
        user51.setUserName("user51");
        user51.setPassword(pw123);
        user51.setEmail("user51@company51.com");
        user51.setEnabled(true);
        userRepository.save(user51);
        
        //user52 admins gzhCompany51
        //user52 does not have associated chanjetUser
        User user52 = new User();
        user52.setUserName("user52");
        user52.setPassword(pw123);
        user52.setEmail("user52@company51.com");
        user52.setEnabled(true);
        userRepository.save(user52);
        
        //user53 owns gzhCompany53
        //user53 does not have associated chanjetUser
        User user53 = new User();
        user53.setUserName("user53");
        user53.setPassword(pw123);
        user53.setEmail("user53@company51.com");
        user53.setEnabled(true);
        userRepository.save(user53);
        
        GzhCompany gzhCompany51 = new GzhCompany("yu_ju2001测试公众号", "wx7350abaa08c820f5", "66de80c52d6456cec16feef45f5eca6d", "gh_ebffba110aa1");
        gzhCompany51.setServiceType(GzhCompanyServiceType.GDTECH);
        gzhCompany51.setPaymentStatus(GzhCompanyPaymentStatus.NO_DEBT);
        gzhCompany51.setBillingStatus(GzhCompanyBillingStatus.TRIAL_NOT_STARTED);
        gzhCompany51.setServiceRunningStatus(GzhCompanyServiceRunningStatus.NOT_STARTED);
        gzhCompany51.setServiceRunningStatus(GzhCompanyServiceRunningStatus.NOT_STARTED);
        gzhCompany51.setGzhMenuType(GzhCompanyMenuType.SYSTEM_DEFAULT);
        //gzhCompany51.setBillingStatus(GzhCompanyBillingStatus.TRIAL);
        //gzhCompany51.setTrialStartDate(LocalDate.parse("2017-01-15"));
        //gzhCompany51.setActiveBillingStartDate(LocalDate.parse("2017-01-15").plusMonths(1));
        //OneToOne gzhCompany51 ownedBy user51
        
        gzhCompany51.setOwnerUser(user51);
        //create an Account for the GzhCompany
        Account account51 = new Account();
        account51.setInternalAccountOwnerName(user51.getUserName());
        accountRepository.save(account51);
        gzhCompany51.setAccount(account51);
        companyRepository.save(gzhCompany51);
        
        account51.setGzhCompany(gzhCompany51);
        accountRepository.save(account51);
        
        //OneToOne user51 owns gzhCompany51
        user51.setOwnedGzhCompany(gzhCompany51);
        //ManyToOne user51 admins gzhCompany51
        user51.setAdminedGzhCompany(gzhCompany51);
        userRepository.save(user51);
        
        GzhCompany gzhCompany52 = new GzhCompany("Fake GzhCompany 52", "wx52525252", "apikey52525252", "gh_ghzid52");;
        gzhCompany52.setServiceType(GzhCompanyServiceType.GSSWDL_A);
        gzhCompany52.setPaymentStatus(GzhCompanyPaymentStatus.NO_DEBT);
        gzhCompany52.setBillingStatus(GzhCompanyBillingStatus.TRIAL);
        gzhCompany52.setTrialStartDate(LocalDate.parse("2017-01-15"));
        gzhCompany52.setActiveBillingStartDate(LocalDate.parse("2017-01-15").plusMonths(1));
        gzhCompany52.setServiceRunningStatus(GzhCompanyServiceRunningStatus.NOT_STARTED);
        gzhCompany52.setGzhMenuType(GzhCompanyMenuType.SYSTEM_DEFAULT);
        
        //OneToOne gzhCompany52 ownedBy user52
        gzhCompany52.setOwnerUser(user52);
        //create an Account for the GzhCompany
        Account account52 = new Account();
        account52.setInternalAccountOwnerName(user52.getUserName());
        accountRepository.save(account52);
        gzhCompany52.setAccount(account52);
        companyRepository.save(gzhCompany52);
        
        account52.setGzhCompany(gzhCompany52);
        accountRepository.save(account52);
        
        //OneToOne user52 owns gzhCompany52
        user52.setOwnedGzhCompany(gzhCompany52);
        //ManyToOne user52 admins gzhCompany52
        user52.setAdminedGzhCompany(gzhCompany52);
        userRepository.save(user52);

        GzhCompany gzhCompany53 = new GzhCompany("Fake GzhCompany 53", "wx53535353", "apikey535353", "gh_ghzid53");;
        gzhCompany53.setServiceType(GzhCompanyServiceType.GSSWDL_A);
        gzhCompany53.setPaymentStatus(GzhCompanyPaymentStatus.NO_DEBT);
        gzhCompany53.setBillingStatus(GzhCompanyBillingStatus.TRIAL);
        gzhCompany53.setTrialStartDate(LocalDate.parse("2017-01-15"));
        gzhCompany53.setActiveBillingStartDate(LocalDate.parse("2017-01-15").plusMonths(1));
        gzhCompany53.setServiceRunningStatus(GzhCompanyServiceRunningStatus.NOT_STARTED);
        gzhCompany53.setGzhMenuType(GzhCompanyMenuType.SYSTEM_DEFAULT);
        
        //OneToOne gzhCompany52 ownedBy user52
        gzhCompany53.setOwnerUser(user53);
        //create an Account for the GzhCompany
        Account account53 = new Account();
        account53.setInternalAccountOwnerName(user53.getUserName());
        accountRepository.save(account53);
        gzhCompany53.setAccount(account53);
        companyRepository.save(gzhCompany53);
        
        account53.setGzhCompany(gzhCompany53);
        accountRepository.save(account53);
        
        //OneToOne user53 owns gzhCompany53
        user53.setOwnedGzhCompany(gzhCompany53);
        //ManyToOne user53 admins gzhCompany53
        user53.setAdminedGzhCompany(gzhCompany53);
        userRepository.save(user53);
        
        //gzhUser51
        GzhUser yu_ju2001GzhUser = new GzhUser();
        yu_ju2001GzhUser.setOpenId("olrli1bueeEdDGJJBPEINhXk_Gaw");
        yu_ju2001GzhUser.setGzhCompanyByGzhId("gh_ebffba110aa1");
        yu_ju2001GzhUser.setName("yu_ju2001");
        yu_ju2001GzhUser.setNickName("鞠禹");
        yu_ju2001GzhUser.setStatus(GzhUserStatus.SUBSCRIBE);
        yu_ju2001GzhUser.setLastStatusChange(OffsetDateTime.now());
        gzhUserRepository.save(yu_ju2001GzhUser);
        
        log.info("Adding a few gzhUsers for gzhCompany51.");
        for(int i=51; i < 61; i++) {
            String name = "gzhUser" + Integer.toString(i);
            GzhUser gzhUser = new GzhUser(name);
            gzhUser.setGzhCompanyByGzhId("gh_ebffba110aa1");
            gzhUserRepository.save(gzhUser);
        }
        
        //gzhClientCompany51 for gzhCompany51
        ChanjetClientCompany chanjetClientCompany51 = new ChanjetClientCompany();
        chanjetClientCompany51.setAccId(null);
        chanjetClientCompany51.setName("chanjetClientCompany51");
        chanjetClientCompanyRepository.save(chanjetClientCompany51);
        
        GzhClientCompany gzhClientCompany51 = new GzhClientCompany();
        gzhClientCompany51.setChanjetClientCompany(chanjetClientCompany51);
        //ManyToOne gzhClientCompany51 => gzhCompany51
        gzhClientCompany51.setGzhCompany(gzhCompany51);
        gzhClientCompanyRepository.save(gzhClientCompany51);

        //ManyToMany gzhUser51 is associated with gzhClientCompany51
        //gzhClientCompany51 is the owning side of ManyToMany relationship. 
        gzhClientCompany51.addGzhUser(yu_ju2001GzhUser);
        gzhClientCompany51.setFirstImportDate(LocalDate.parse("2017-02-28"));
        gzhClientCompanyRepository.save(gzhClientCompany51);
    }
  
    @Override
    public void run(ApplicationArguments applicationArguments) throws Exception {
        initGzhCompany1();
        initGzhCompany51();
        
        //Setup referal
        User user1 = userRepository.findOne(1L);
        User user2 = userRepository.findOne(2L);
        User user51 = userRepository.findOne(3L);
        User user52 = userRepository.findOne(4L);
        User user53 = userRepository.findOne(5L);
        
        int parentReferalNum = 0;
        //user1 has no parent referal.
        referalService.createReferalForUser(user1, parentReferalNum);
        
        parentReferalNum = user1.getReferal().getReferalNum();
        referalService.createReferalForUser(user2, parentReferalNum);
        
        parentReferalNum = user2.getReferal().getReferalNum();
        referalService.createReferalForUser(user51, parentReferalNum);
        referalService.createReferalForUser(user52, parentReferalNum);
        
        parentReferalNum = user51.getReferal().getReferalNum();
        referalService.createReferalForUser(user53, parentReferalNum);
        
        Referal referal1 = user1.getReferal();
        referal1.setName("user1RealName");
        referalService.updateReferalStatus(referal1, ReferalStatus.ACTIVE, "Testing");
        
        ReferalAccount referalAccount1 = referal1.getReferalAccount();
        referalAccount1.setBalance(600.0);
        referalAccountRepository.save(referalAccount1); 
        
        //create admin if not exists
        User admin = userRepository.findOneByUserName("admin");
        if (admin == null) {
            admin = new User();
            admin.setUserName("admin");
            admin.setPassword(pw123);
            admin.setEmail("admin@gandongtech.com");
            admin.setRoles("ROLE_USER, ROLE_WEBADMIN, ROLE_ACTUATOR");
            admin.setEnabled(true);
            userRepository.save(admin);
            referalService.createReferalForUser(admin, 0);
        }
    }
}
