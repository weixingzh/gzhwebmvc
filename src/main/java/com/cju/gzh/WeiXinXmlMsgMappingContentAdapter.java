package com.cju.gzh;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.bind.annotation.adapters.XmlAdapter;
import javax.xml.namespace.QName;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

//This will map one XML element <MsgTag> msgValue </MsgTag> into one WeiXinXmlMsgMappingContent
public class WeiXinXmlMsgMappingContentAdapter extends XmlAdapter<Element, WeiXinXmlMsgMappingContent>{
	private Logger log = LoggerFactory.getLogger(WeiXinXmlMsgMappingContentAdapter.class);
	private JAXBContext jaxbContext;
	private DocumentBuilder documentBuilder;
	
	public WeiXinXmlMsgMappingContentAdapter() throws JAXBException, ParserConfigurationException {
        jaxbContext = JAXBContext.newInstance(WeiXinXmlMsgMappingContent.class);
        documentBuilder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
    }
	
    @Override
    public Element marshal(WeiXinXmlMsgMappingContent weiXinXmlMsgContent) throws Exception {
    	//log.info("marshal() is called for " + weiXinXmlMsgContent.toString());
 
        // 1. Build the JAXBElement to wrap the instance of weiXinXmlMsgContent.
        QName qName = new QName(weiXinXmlMsgContent.getMsgTag());
        String value = weiXinXmlMsgContent.getMsgValue();
        
        //qName - Java binding of xml element tag name
        //declaredType - Java binding of xml element declaration's type
        //scope - Java binding of scope of xml element declaration. Passing null is the same as passing GlobalScope.class
        //value - Java instance representing xml element's value.
        
        //The type for the value is a generic Object. 
        //The value may be a String or a Number or something else depending on the WeiXin tag.
        //But in weiXinXmlMsgContent, we use String type to represent them in an uniformed way.
        //So we can avoid JAXBElement raw type and use JAXBElement<String> here.
        JAXBElement<String> jaxbElement = new JAXBElement<String>(qName, String.class, value);
 
        // 2.  Marshal the JAXBElement to a DOM element.
        Document document = documentBuilder.newDocument();
        Marshaller marshaller = jaxbContext.createMarshaller();
        marshaller.marshal(jaxbElement, document);
        Element element = document.getDocumentElement();

        return element;
    }

    
	//Element is a DOM element.
    @Override
    public WeiXinXmlMsgMappingContent unmarshal(Element element) throws Exception {
        Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
        
        //Here the element is something like: 
        //     <content> This is a test text </content> or
        //     <msgId> 64bit integer </msgId>
        //The type of the value really depends on the tag.
        
        //element's localName is already set to content.
        //We need to unmarshal the element's child "This is a test text" into a String.
        //So integer or float will be unmarshalled into string.
        JAXBElement<String> jaxbElement = unmarshaller.unmarshal(element, String.class);

        //jaxbElement.getValue() returns Object
        WeiXinXmlMsgMappingContent weiXinXmlMsgContent = new WeiXinXmlMsgMappingContent(element.getLocalName(), jaxbElement.getValue());
        
    	//log.info("unmarshal() result: " + weiXinXmlMsgContent.toString());
    	
        return weiXinXmlMsgContent;
    }
 
}
