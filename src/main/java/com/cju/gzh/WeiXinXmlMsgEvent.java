package com.cju.gzh;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Component;
import com.cju.gzhOps.GzhCompany;
import com.cju.gzhOps.GzhCompanyRepository;
import com.cju.gzhOps.GzhUserRepository;
import com.cju.gzhOps.GzhUserService;

@Component
public class WeiXinXmlMsgEvent {
	private Logger log = LoggerFactory.getLogger(WeiXinXmlMsgEvent.class);

	@Autowired
	private WeiXinXmlMsgAutoReplyService msgAutoReplyService;
	
	@Autowired
	GzhUserRepository gzhUserRepository;
	
	@Autowired
	GzhUserService gzhUserService;
	
    @Autowired 
    private GzhCompanyRepository gzhCompanyRepository; 
	
    @Autowired
    private MessageSource messageSource;

    public WeiXinXmlMsgEvent() {
    }
	
	public WeiXinXmlMsgMapping processXmlMsg(WeiXinXmlMsgMapping xmlMsg) {
		WeiXinXmlMsgMapping responseMsg = new WeiXinXmlMsgMapping();
	    String fromUserName = xmlMsg.getFromUserName();
	    String toUserName = xmlMsg.getToUserName();
		
		responseMsg.setToUserName(fromUserName);
		responseMsg.setFromUserName(toUserName);
		//The time in seconds since epoch
		responseMsg.setCreateTime(new Date().getTime());
		
		//contentList for EventMsg should contain "Event" and others depending on the event
		//The value for Event can be subscribe, SCAN, LOCATION, CLICK
		Map<String, String> contentMap = new HashMap<String, String>();
		for(WeiXinXmlMsgMappingContent item: xmlMsg.getMsgContent()) {
			contentMap.put(item.getMsgTag(), (String)item.getMsgValue());
		}
		log.info("Content list in receive msg: " + contentMap.toString());
		
		List<WeiXinXmlMsgMappingContent> msgContent = new ArrayList<WeiXinXmlMsgMappingContent>();
		String openId = null;
		switch((String)contentMap.get("Event")) {
		case "CLICK" : 
			//CLICK event happens when a subscriber click on a menu item of CLICK type. It has a EventKey. 
			//By default, reply with a text msg back.
			responseMsg.setMsgType("text");
			String replyStr = msgAutoReplyService.getAutoReplyMsg(fromUserName, toUserName, contentMap.get("EventKey"));
			//set the text content. Content can not exceed 2048 characters
			//WeiXin needs it to be "Content", not "content"
			msgContent.add(new WeiXinXmlMsgMappingContent("Content", replyStr));
			responseMsg.setMsgContent(msgContent);
			break;
		case "VIEW" : 
			//When a user click on a VIEW menu, weixin will go directly to the URL setup for the view without going through WeiXin server.
			//WeiXin is not suppose to send a VIEW Event to application server, but WeiXin does send such an event.
			//VIEW event has a MenuId, EventKey to be the URL,
            //Just reply the same XML message back.
			//If responseMsg's content is set with a List<WeiXinXmlMsgMappingContent>, WeiXinXmlMsgMappingContentAdapter.marshal() will be called.
			//Otherwise, WeiXinXmlMsgMappingContentAdapter.marshal() will not be called.
			break;
		case "subscribe" : 
			log.info("processXmlMsg subscribe");
			//subscribe event happens when a user subscribe to gzh. There is no further parameters.
            openId = xmlMsg.getFromUserName();
            GzhCompany gzhCompany = gzhCompanyRepository.findOneByGzhId(xmlMsg.getToUserName());
            gzhUserService.gzhUserSubscribe(openId, gzhCompany);
		    
			//By default, reply with a text msg back.
			responseMsg.setMsgType("text");
			replyStr = messageSource.getMessage("gzh.weixin.subscribe.welcome", null, Locale.SIMPLIFIED_CHINESE);
			//set the text content. Content can not exceed 2048 characters
			//WeiXin needs it to be "Content", not "content"
			msgContent.add(new WeiXinXmlMsgMappingContent("Content", replyStr));
			responseMsg.setMsgContent(msgContent);
			break;
	     case "unsubscribe" : 
	            log.info("processXmlMsg unsubscribe.");
	            openId = xmlMsg.getFromUserName();
	            gzhUserService.gzhUserUnsubscribe(openId);
	            
	            //By default, reply with a text msg back.
	            responseMsg.setMsgType("text");
	            replyStr = messageSource.getMessage("gzh.weixin.unsubscribe.bye", null, Locale.SIMPLIFIED_CHINESE);
	            //set the text content. Content can not exceed 2048 characters
	            //WeiXin needs it to be "Content", not "content"
	            msgContent.add(new WeiXinXmlMsgMappingContent("Content", replyStr));
	            responseMsg.setMsgContent(msgContent);
	            break;
		default: 
            //Just reply the same XML message back.
		}

		return responseMsg;
	}
}