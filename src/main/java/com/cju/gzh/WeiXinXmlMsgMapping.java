package com.cju.gzh;

import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAnyElement;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

//By default, the following HttpMessageConverters instances are pre-enabled:
//
//    ByteArrayHttpMessageConverter – converts byte arrays
//    StringHttpMessageConverter – converts Strings
//    ResourceHttpMessageConverter – converts org.springframework.core.io.Resource for any type of octet stream
//    SourceHttpMessageConverter – converts javax.xml.transform.Source
//    FormHttpMessageConverter – converts form data to/from a MultiValueMap<String, String>.
//    Jaxb2RootElementHttpMessageConverter – converts Java objects to/from XML (added only if JAXB2 is present on the classpath)
//    MappingJackson2HttpMessageConverter – converts JSON (added only if Jackson 2 is present on the classpath)
//    MappingJacksonHttpMessageConverter – converts JSON (added only if Jackson is present on the classpath)
//    AtomFeedHttpMessageConverter – converts Atom feeds (added only if Rome is present on the classpath)
//    RssChannelHttpMessageConverter – converts RSS feeds (added only if Rome is present on the classpath)

//Java Architecture for XML Binding (JAXB) allows Java developers to map Java classes to XML representations. 
//JAXB provides two main features: 
//   the ability to marshal Java objects into XML and the inverse, i.e. to unmarshal XML back into Java objects.

//This is for mapping of weixin xml msg 
//Due to the way weixin xml msg is organized, we have to use List<WeiXinXmlMsgContent> to capture the tags varies 
//according to different msgType.

//By default JAXB impls treat public fields and properties as mapped. 
//You can put the annotation on the get or set method and it will be treated the same. 
//If you want to annotate the field you should specify @XmlAccessorType(XAccessType.FIELD) on the class.
@XmlRootElement(name="xml")
@XmlType (propOrder={"toUserName","fromUserName","createTime", "msgType", "msgContent"})
@XmlAccessorType(XmlAccessType.FIELD)
public class WeiXinXmlMsgMapping {
	@XmlElement(name = "ToUserName")
	private String toUserName;
	@XmlElement(name = "FromUserName")
	private String fromUserName;
	@XmlElement(name = "CreateTime")
	private long   createTime;
	@XmlElement(name = "MsgType")
	private String msgType;
	
	//@XmlTransient is mutually exclusive with all other JAXB defined annotations.
	//@XmlTransient
	//MsgId is only present in text msg.
	//private int    msgId;
	
	//This is list of all the type specific XML elements 
	//For msgType of text, this list will have one item for the <content> </content> pair in the XML msg.
    @XmlAnyElement(lax=true)
	private List<WeiXinXmlMsgMappingContent> msgContent;
	
	public WeiXinXmlMsgMapping() {
	}
	
	public String getToUserName() {
	    return toUserName;
	}
	public void setToUserName(String toUserName) {
	    this.toUserName = toUserName;
	}


	public String getFromUserName() {
	    return fromUserName;
	}
	public void setFromUserName(String fromUserName) {
	    this.fromUserName = fromUserName;
	}
	

	public long getCreateTime() {
	    return createTime;
	}
	public void setCreateTime(long createTime) {
	    this.createTime = createTime;
	}


	public String getMsgType() {
	    return msgType;
	}
	public void setMsgType(String msgType) {
	    this.msgType = msgType;
	}
	
	//type specific XML tags in the XML msg.
    public List<WeiXinXmlMsgMappingContent> getMsgContent() {
        return msgContent;
    }
    public void setMsgContent(List<WeiXinXmlMsgMappingContent> msgContent) {
        this.msgContent = msgContent;
    }
	
    
    @Override
    public String toString() {
        return  "ToUserName=" + toUserName +
                ", FromUserName=" + fromUserName +
                ", CreateTime=" + createTime;
    }
    

}
