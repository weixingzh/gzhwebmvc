package com.cju.gzh;

import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.time.LocalDateTime;
import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.Locale;
import java.util.Map;
import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletRequest;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.apache.commons.codec.digest.DigestUtils;
import org.w3c.dom.CDATASection;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import com.cju.gzh.WeiXinXmlMsgText;
import com.cju.gzhOps.GzhCompany;
import com.cju.gzhOps.GzhCompanyRepository;
import com.cju.gzhOps.GzhCompanyServiceRunningStatus;
import com.cju.gzhOps.GzhUserService;
import com.cju.order.Order;
import com.cju.order.OrderRepository;
import com.cju.order.OrderService;
import com.cju.security.RepositoryQueryFilter;

//About weixin openId
//For each pair of weixin user and weixin gzh, there is a uniqe weixin user openId, even though the user does not even subscribe to the gzh.
//If a user did not subscribe to a gzh
//    1) He can not go inside the gzh UI and click anything.
//    2) But he can click a link to the gzh server outside the gzh UI within weixin client. The gzh server can then use OAuth2 to get user authorization 
//       to get his openId and accesstoken.
//If a weixin user subscribes to a gzh
//    3) He can go inside the gzh UI and send a TEXT or CLICK message, the message will go through weixin server which will include FromUserName in the 
//       http body in XML format. This FromUserName is the same openId.
//    4) He can go inside the gzh UI and click a VIEW type button or a URL link. The http request will go directly to the gzh server without going through weixin server.
//       In this case, either the link is a customized link contains user's identity, or the gzh server can use OAuth2 to get user openId.

//If a weixin user subscribes to a gzh
//    5)The user's openId together with the gzh's access token is enough to get user's details info.
//If a weixin user did not subscribe to a gzh
//    6)If we use snsapi_base as SCOPE in step 2, we can get the user's openId. But we can not get user's detailed info.
//    7)If we use snsapi_userinfo as as SCOPE in step 2 (which will bring up authorization UI on user's weixin UI), the user's openId together with the user's specific 
//      access token can get user's details info.

//在微信公众平台， 在设置->公众号设置页面下，可以在功能设置里配置业务域名，JS接口安全域名，网页授权域名，这些域名要配置成gzh.gandongtech.com。测试可以填写charlesyju.imwork.net
//WeiXin will do a get on http://gzh.gandongtech.com/MP_verify_FP1L6lUuS87P6rtA.txt for verification.
//Different GZH will have a different MP_verify_xxx.txt

//在微信公众平台， 在开发 ->基本配置页面下，在服务器配置下，服务器地址要配置成  http://gzh.gandongtech.com/gzh。测试可以填写http://charlesyju.imwork.net/gzh
//WeiXin will do a http GET to http://charlesyju-gzh-platform/gzh to validate token when a user is setting up GZH.
//WeiXin will do http POST to http://charlesyju-gzh-platform/gzh when a user send a message to WeiXin or trigger an event
@Controller
public class GzhController {
    private Logger log = LoggerFactory.getLogger(GzhController.class);
    
 //   final private int REFERER_COOKIE_EXPIRE_SECONDS = 30 * 24 * 3600;
    
    @Value("${gzh.weixin.validate.token}")
    private String validateToken;
    
    @Autowired
    private WeiXinXmlMsgText textMsg;
    
    @Autowired
    private WeiXinXmlMsgEvent eventMsg;
    
    @Autowired 
    private GzhCompanyRepository gzhCompanyRepository; 
    
    @Autowired 
    RepositoryQueryFilter repoQueryFilter;
    
    @Autowired
    private MessageSource messageSource;
    
    @Autowired
    private WeiXinService weiXinService;
    
    @Autowired
    GzhUserService gzhUserService;
    
    @Autowired
    private OrderRepository orderRepository;
    
    @Autowired
    private OrderService orderService;
    
    private DocumentBuilder w3cDomBuilder;
    private Transformer transformer;
    
    @PostConstruct
    public void init() throws ParserConfigurationException, TransformerConfigurationException {
        w3cDomBuilder = DocumentBuilderFactory.newInstance().newDocumentBuilder();        
        TransformerFactory tf = TransformerFactory.newInstance();
        transformer = tf.newTransformer();
    }
    
    //URLs for weixin initiated http requests -- Section Start
    
    //Use regex to set the request mapping path with changing filename and a .txt file extenstion.
    //Http request to http://gzh.gandongtech.com/MP_verify_FP1L6lUuS87P6rtA.txt will be processed here 
    //rather than served as a static file.
    //This is to make weixin MP verification happy.
    @GetMapping("/MP_verify_{filename:.+}.txt")
    public @ResponseBody String getMpVerifyFile(HttpServletRequest request, @PathVariable("filename") String filename) {
        log.info("getMpVerifyFile requestUrl=" + request.getRequestURL());
        //The content for MP_verify_FP1L6lUuS87P6rtA.txt is only one line, which is FP1L6lUuS87P6rtA
        return filename;
    }
    
    //Weixin send a GET to url setup for receiving messages when validates the gzh developer mode.
    //Not sure Weixin will send request to this url after the validation is passed.
    @GetMapping("/gzh")
    public  String validateToken(HttpServletRequest request, @RequestParam Map<String, String> params, Model model ) {
        StringBuffer requestUrl = request.getRequestURL();
        String queryString = request.getQueryString();
        log.info("validateToken requestUrl=" + requestUrl);
        log.info("validateToken queryString=" + queryString);
        
        String timeStamp = params.get("timestamp");
        String nonce = params.get("nonce");
        String echoStr = params.get("echostr");
        
        if (timeStamp != null && nonce != null) {
            //This is the case of WeiXin validating token.
            String paramArray[] = {validateToken, timeStamp, nonce };
            Arrays.sort(paramArray);
            StringBuilder strBuilder = new StringBuilder();
            for (int i = 0; i < paramArray.length; i++) {
               strBuilder.append(paramArray[i]);
            }
            String paramStr = strBuilder.toString();
            log.info("validateToken() paramStr: " + paramStr);
            String digest = DigestUtils.sha1Hex(paramStr);
            String sig = params.get("signature");
            
            // Java == compares reference
            if (sig.equals(digest))    {
                log.info("validateToken: signature match digest. return echostr: " + params.get("echostr"));
                model.addAttribute("echoStr", echoStr);
            } 
            else {
                model.addAttribute("echoStr", "failed validating token!");
            }
               
            return "gzh/gzhWeiXinValidateToken";
        }
        else {
            //Normal http request.
            return "home/index";
        }
    }
    
    //On weixin gzh platform, the message-receiving-interface url is setup as http://charlesyju-gzh-platform/gzh 
    
    //We need to turn off csrf protection on the POST to this URI as this request is initiated by weixin.
    //csrf protection for /gzh is turned off in WebMvcSecurityConfig
    //csrf protection is done by this server to add the hidden csrf data to a form shown to a user.
    
    //When a user send a message to WeiXin or trigger an event, WeiXin will post the message to the development server (this SpringBoot Application).
    //A user can trigger an event by select a menu item of "CLICK" type, or by subscribe to the gzh, or by scan the two dimensional code.
    //In these cases, we need to reply with a text/xml media type with XML body. We can not reply back with a web page.
    //When a user click on a menu of type "VIEW", the user will be shown a webpage. We need to reply with a webpaage in these cases.
    //Here we only process the post XML messages. Weixin sends in text/xml media type.
    @PostMapping(value="/gzh", consumes = {MediaType.TEXT_XML_VALUE, MediaType.APPLICATION_XML_VALUE}, produces = MediaType.TEXT_XML_VALUE)
    @ResponseBody
    public WeiXinXmlMsgMapping handleXmlPostMsg(HttpServletRequest request, @RequestBody WeiXinXmlMsgMapping xmlMsg) {
        log.info("handleXmlPostMsg() receives a post request. requestUrl= " + request.getRequestURL().toString() +
                 ", XML requestBody unmarshalled = " + xmlMsg.toString());
        String gzhId = xmlMsg.getToUserName();
        GzhCompany gzhCompany = gzhCompanyRepository.findOneByGzhId(gzhId);
        GzhCompanyServiceRunningStatus serviceRunningStatus = gzhCompany.getServiceRunningStatus();
        log.info("handleXmlPostMsg: gzhCompany name=" + gzhCompany.getName() + ", serviceRunningStatus=" + serviceRunningStatus);
        
        WeiXinXmlMsgMapping  responseXml = new WeiXinXmlMsgMapping();
        if (!serviceRunningStatus.equals(GzhCompanyServiceRunningStatus.RUNNING)) {
            //Do not process further, return empty responseXml
            return responseXml;
        }
        //For any type of a received msg, we may choose to respond with any type of msg.
        switch (xmlMsg.getMsgType()) {
        case "text":
            responseXml = textMsg.processXmlMsg(xmlMsg); 
            
            break;
        case "event":
            responseXml = eventMsg.processXmlMsg(xmlMsg);
            break;
        default:
            responseXml.setMsgType("text");
        }
        log.info("handleXmlPostMsg: responding with " + responseXml.toString());
        return(responseXml);
    }
    
    //We need to turn off csrf protection on the POST to this URI as this request is initiated by weixin.
    //csrf protection for /gzh/wxpay/** is turned off in WebMvcSecurityConfig
    @PostMapping(value="/gzh/wxpay/payNotify", consumes = {MediaType.TEXT_XML_VALUE, MediaType.APPLICATION_XML_VALUE}, produces = MediaType.TEXT_XML_VALUE)
    @ResponseBody
    public String handleWeiXinPayNotifyPost(@RequestBody String xmlMsg) throws IOException, TransformerException {
        
        String retCode = "";
        String retMsg = "";
        boolean continueParsingXml = true;
        
        log.info("handleWeiXinPayNotifyPost received pay notify, incoming xmlMsg=" + xmlMsg);
        
        Document document = null;
        try {
            document = w3cDomBuilder.parse(new InputSource(new StringReader(xmlMsg)));
        } 
        catch (SAXException e) {
            e.printStackTrace();
            retCode = "FAIL";
            retMsg = messageSource.getMessage("gzh.weixin.notify.params.error", null, Locale.SIMPLIFIED_CHINESE);
            continueParsingXml = false;
        }

        Element root = null;
        //check signature
        if (continueParsingXml) {
            root = document.getDocumentElement();
            String sign = root.getElementsByTagName("sign").item(0).getTextContent();
            String calculatedSign = weiXinService.calculateWeiXinPayOrderSign(document);
            //log.info("calculatedSign=" + calculatedSign);
            if (!calculatedSign.equals(sign)) {
                retCode = "FAIL";
                retMsg = messageSource.getMessage("gzh.weixin.sign.error", null, Locale.SIMPLIFIED_CHINESE);
                continueParsingXml = false;
            }
        }
        
        //signature is correct, check result_code
        if (continueParsingXml) {
            String resultCode = root.getElementsByTagName("result_code").item(0).getTextContent();
            if (!resultCode.equals("SUCCESS")) {
                
                //Not used in this function
                //    Node errCodeNode = root.getElementsByTagName("err_code").item(0);
                //    if (errCodeNode != null) {
                //        String errCode = errCodeNode.getTextContent();
                //    }
                //    Node errCodeDesNode = root.getElementsByTagName("err_code_des").item(0);
                //    if (errCodeDesNode != null) {
                //        String errCodeDes = errCodeDesNode.getTextContent();
                //    }
                
                //user pay failed. But there is no trade_state field as in the pay order status query API. 
                //We do not update the half open order here since the user may try again and succeed later.
                //If user did not pay successfully in the end, we can manually or schedule task to query the status 
                //after the qrCodeUrl expires and update the pay order status.
                
                //Also, in this case, weixin does not give further info to tell which pay order failed.
                //So we do not know which pay order to update even if we want to.
                log.info("handleWeiXinPayNotifyPost got a notification for failed pay. xmlMsg=" + xmlMsg);
                
                //However, we still need to response to weixin saying we received and parsed the FAIL notification successfully
                //so that weixin will not keep sending the same FAIL notification for upto 9 times within 3600 sec.
                retCode = "SUCCESS";
                retMsg = "OK";
                continueParsingXml = false;
            }
        }
        
        //result_code is SUCCESS, continue parsing
        if (continueParsingXml) {
            //Not used in this function
            //    String appId = root.getElementsByTagName("appid").item(0).getTextContent();
            //    String attach = root.getElementsByTagName("attach").item(0).getTextContent();
            //    String mchId = root.getElementsByTagName("mch_id").item(0).getTextContent();
            //    String nonceStr = root.getElementsByTagName("nonce_str").item(0).getTextContent();
            
            String outTradeNo = root.getElementsByTagName("out_trade_no").item(0).getTextContent();
            String timeEnd = root.getElementsByTagName("time_end").item(0).getTextContent();
            String txId = root.getElementsByTagName("transaction_id").item(0).getTextContent();
            String openId = root.getElementsByTagName("openid").item(0).getTextContent();
           
            int orderNum = Integer.valueOf(outTradeNo);
            //timeEnd has the format of "20140903131540". It does not have zone info and it is assumed China time.
            LocalDateTime localPayTime = LocalDateTime.parse(timeEnd, DateTimeFormatter.ofPattern("yyyyMMddHHmmss"));
            //China ZoneOffset is UTC+08:00
            OffsetDateTime payTime = localPayTime.atOffset(ZoneOffset.ofHours(8));
        
            Order order = orderRepository.findOneByOrderNum(orderNum);
            if (order == null) {
                log.info("handleWeiXinPayNotifyPost Got a SUCCESS pay notification with a not found orderNum " + orderNum);
            }
            else {
                //user pay success, update the half open order with the weixin pay information
                orderService.updateOrderWithSuccessWeiXinPayInfo(order, payTime, txId, openId);
            }
            
            retCode = "SUCCESS";
            retMsg = "OK";
        }

        //Response to tell weixin 
        Document doc = w3cDomBuilder.newDocument();
        Element xmlElm = doc.createElement("xml");
        doc.appendChild(xmlElm);
        
        Element retCodeElm = doc.createElement("return_code");
        xmlElm.appendChild(retCodeElm);
        CDATASection retCodeCdata = doc.createCDATASection(retCode);
        retCodeElm.appendChild(retCodeCdata);
        
        Element retMsgElm = doc.createElement("return_msg");
        xmlElm.appendChild(retMsgElm);
        CDATASection retMsgCdata = doc.createCDATASection(retMsg);
        retMsgElm.appendChild(retMsgCdata);

        // below code to remove XML declaration
        transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
        transformer.setOutputProperty(OutputKeys.INDENT, "yes");
        StringWriter writer = new StringWriter();
        transformer.transform(new DOMSource(doc), new StreamResult(writer));
        String response = writer.getBuffer().toString();
        log.info("handleWeiXinPayNotifyPost response=" + response);
        
        //payNotify comes from WeiXin server, we need to response to WeiXin Server with XML body to tell WeiXin server that we received the notify.
        return(response);
    }
    
    @PostMapping(value="/gzh/wxpay/referalVerifyNotify", consumes = {MediaType.TEXT_XML_VALUE, MediaType.APPLICATION_XML_VALUE}, produces = MediaType.TEXT_XML_VALUE)
    @ResponseBody
    public String handleWeiXinReferalVerifyNotifyPost(@RequestBody String xmlMsg) throws IOException, TransformerException {
        
        String retCode = "";
        String retMsg = "";
        boolean continueParsingXml = true;
        
        log.info("handleWeiXinReferalVerifyNotifyPost received pay notify, incoming xmlMsg=" + xmlMsg);
        
        Document document = null;
        try {
            document = w3cDomBuilder.parse(new InputSource(new StringReader(xmlMsg)));
        } 
        catch (SAXException e) {
            e.printStackTrace();
            retCode = "FAIL";
            retMsg = messageSource.getMessage("gzh.weixin.notify.params.error", null, Locale.SIMPLIFIED_CHINESE);
            continueParsingXml = false;
        }
        
        Element root = null;
        //check signature
        if (continueParsingXml) {
            root = document.getDocumentElement();
            String sign = root.getElementsByTagName("sign").item(0).getTextContent();
            String calculatedSign = weiXinService.calculateWeiXinPayOrderSign(document);
            //log.info("calculatedSign=" + calculatedSign);
            if (!calculatedSign.equals(sign)) {
                retCode = "FAIL";
                retMsg = messageSource.getMessage("gzh.weixin.sign.error", null, Locale.SIMPLIFIED_CHINESE);
                continueParsingXml = false;
            }
        }
        
        //signature is correct, check result_code
        if (continueParsingXml) {
            String resultCode = root.getElementsByTagName("result_code").item(0).getTextContent();
            if (!resultCode.equals("SUCCESS")) {
                
                //user pay failed. But there is no trade_state field as in the pay order status query API. 
                //We do not update the half open order here since the user may try again and succeed later.
                //If user did not pay successfully in the end, we can manually or schedule task to query the status 
                //after the qrCodeUrl expires and update the pay order status.
                
                //Also, in this case, weixin does not give further info to tell which pay order failed.
                //So we do not know which pay order to update even if we want to.
                log.info("handleWeiXinReferalVerifyNotifyPost got a notification for failed pay. xmlMsg=" + xmlMsg);
                
                //However, we still need to response to weixin saying we received and parsed the FAIL notification successfully
                //so that weixin will not keep sending the same FAIL notification for upto 9 times within 3600 sec.
                retCode = "SUCCESS";
                retMsg = "OK";
                continueParsingXml = false;
            }
        }
        
        //result_code is SUCCESS, continue parsing
        if (continueParsingXml) {
            String outTradeNo = root.getElementsByTagName("out_trade_no").item(0).getTextContent();
            String timeEnd = root.getElementsByTagName("time_end").item(0).getTextContent();
            String txId = root.getElementsByTagName("transaction_id").item(0).getTextContent();
            String openId = root.getElementsByTagName("openid").item(0).getTextContent();
            
            int orderNum = Integer.valueOf(outTradeNo);
            //timeEnd has the format of "20140903131540". It does not have zone info and it is assumed China time.
            LocalDateTime localPayTime = LocalDateTime.parse(timeEnd, DateTimeFormatter.ofPattern("yyyyMMddHHmmss"));
            //China ZoneOffset is UTC+08:00
            OffsetDateTime payTime = localPayTime.atOffset(ZoneOffset.ofHours(8));
            
            Order order = orderRepository.findOneByOrderNum(orderNum);
            if (order == null) {
                log.info("handleWeiXinReferalVerifyNotifyPost Got a SUCCESS pay notification with a not found orderNum " + orderNum);
            }
            else {
                //user pay success, update the half open order with the weixin pay information
                orderService.updateOrderWithSuccessWeiXinPayInfo(order, payTime, txId, openId);
            }
            
            retCode = "SUCCESS";
            retMsg = "OK";
        }
        
        //Response to tell weixin 
        Document doc = w3cDomBuilder.newDocument();
        Element xmlElm = doc.createElement("xml");
        doc.appendChild(xmlElm);
        
        Element retCodeElm = doc.createElement("return_code");
        xmlElm.appendChild(retCodeElm);
        CDATASection retCodeCdata = doc.createCDATASection(retCode);
        retCodeElm.appendChild(retCodeCdata);
        
        Element retMsgElm = doc.createElement("return_msg");
        xmlElm.appendChild(retMsgElm);
        CDATASection retMsgCdata = doc.createCDATASection(retMsg);
        retMsgElm.appendChild(retMsgCdata);
        
        // below code to remove XML declaration
        transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
        transformer.setOutputProperty(OutputKeys.INDENT, "yes");
        StringWriter writer = new StringWriter();
        transformer.transform(new DOMSource(doc), new StreamResult(writer));
        String response = writer.getBuffer().toString();
        log.info("handleWeiXinReferalVerifyNotifyPost response=" + response);
        
        //payNotify comes from WeiXin server, we need to response to WeiXin Server with XML body to tell WeiXin server that we received the notify.
        return(response);
    }
    //URLs for weixin initiated http requests -- Section End
}
