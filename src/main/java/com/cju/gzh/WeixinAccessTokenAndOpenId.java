package com.cju.gzh;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

//This represents an accessToken and openId of a weixin user subscribing to a particular gzh.
//openId is unique for a pair of weixin user and gzh
//The accessToken is not used. 
//The AccessToken for the GzhCompany is used along with the openId to get gzhUser info.
@JsonIgnoreProperties(ignoreUnknown = true)
public class WeixinAccessTokenAndOpenId {
	private String accessToken;
	private int exipresIn;
	private String refreshToken;
	private String openId;
	private String scope;
	private String errCode;
	private String errMsg;
	
	public WeixinAccessTokenAndOpenId() {
	}
	
	@JsonProperty("access_token")
	public String getAccessToken() {
	    return accessToken;
	}
    public void setAccessToken(String access_token) {
        this.accessToken = access_token;
    }
    
    @JsonProperty("expires_in")
    public int getExipresIn() {
	    return exipresIn;
	}
    public void setExipresIn(int exipres_in ) {
        this.exipresIn = exipres_in;
    }
    
	@JsonProperty("refresh_token")
	public String getRefreshToken() {
	    return refreshToken;
	}
    public void setRefreshToken(String refreshToken) {
        this.refreshToken = refreshToken;
    }
    
	@JsonProperty("openid")
	public String getOpenId() {
	    return openId;
	}
    public void setOpenId(String openId) {
        this.openId = openId;
    }
    
	@JsonProperty("scope")
	public String getScope() {
	    return scope;
	}
    public void setScope(String scope) {
        this.scope = scope;
    }
    
    @JsonProperty("errcode")
	public String getErrCode() {
	    return errCode;
	}
    public void setErrCode(String errcode) {
        this.errCode = errcode;
    }
    
    @JsonProperty("errmsg")
	public String getErrMsg() {
	    return errMsg;
	}
	
    public void setErrMsg(String errmsg) {
        this.errMsg = errmsg;
    }
    
    @Override
    public String toString() {
        return "{" +
                "access_token=" + accessToken +
                ", exipres_in=" + exipresIn  +
                "}";
    }
}
