package com.cju.gzh;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class WeiXinUser {
	private int    subscribe;
	private String openId;
	private String nickName;
	private int    sex;
	private String language;
	private String city;
	private String province;
	private String country;
	private String headimgurl;
	private int    subscribe_time;
	
	public WeiXinUser() {
	}
	
	@JsonProperty("subscribe")
	public int getSubscribe() {
	    return subscribe;
	}
    public void setSubscribe(int subscribe) {
        this.subscribe = subscribe;
    }
    
	@JsonProperty("openid")
	public String getOpenId() {
	    return openId;
	}
    public void setOpenId(String openId) {
        this.openId = openId;
    }
    
	@JsonProperty("nickname")
	public String getNickName() {
	    return nickName;
	}
    public void setNickName(String nickName) {
        this.nickName = nickName;
    }
    
	@JsonProperty("sex")
	public int getSex() {
	    return sex;
	}
    public void setSex(int sex) {
        this.sex = sex;
    }
    
	@JsonProperty("language")
	public String getLanguage() {
	    return language;
	}
    public void setLanguage(String language) {
        this.language = language;
    }
    
	@JsonProperty("city")
	public String getCity() {
	    return city;
	}
    public void setCity(String city) {
        this.city = city;
    }
    
	@JsonProperty("province")
	public String getProvince() {
	    return province;
	}
    public void setProvince(String province) {
        this.province = province;
    }
    
	@JsonProperty("country")
	public String getCountry() {
	    return country;
	}
    public void setCountry(String country) {
        this.country = country;
    }
    
	@JsonProperty("headimgurl")
	public String getHeadimgurl() {
	    return headimgurl;
	}
    public void setHeadimgurl(String headimgurl) {
        this.headimgurl = headimgurl;
    }
    
	@JsonProperty("subscribe_time")
	public int getSubscribe_time() {
	    return subscribe_time;
	}
    public void setSubscribe_time(int subscribe_time) {
        this.subscribe_time = subscribe_time;
    }
    
    @Override
    public String toString() {
        return "{" +
                "nickName=" + nickName +
                ", openId=" + openId  +
                "}";
    }
}
