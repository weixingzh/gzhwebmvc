package com.cju.gzh;

import java.io.IOException;
import java.nio.charset.Charset;
import java.util.Locale;
import java.util.Map;
import java.util.Optional;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.json.JsonParser;
import org.springframework.boot.json.JsonParserFactory;
import org.springframework.context.MessageSource;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Component;
import org.springframework.util.StreamUtils;

import com.cju.gzhOps.GzhAutoReply;
import com.cju.gzhOps.GzhAutoReplyRepository;
import com.cju.gzhOps.GzhCompany;
import com.cju.gzhOps.GzhCompanyRepository;
import com.cju.security.RepositoryQueryFilter;
import com.querydsl.core.types.Predicate;

@Component
public class WeiXinXmlMsgAutoReplyService {
	private Logger log = LoggerFactory.getLogger(WeiXinXmlMsgAutoReplyService.class);

    @Autowired
    GzhAutoReplyRepository gzhAutoReplyRepository;
    
    @Autowired 
    RepositoryQueryFilter repoQueryFilter;
    
    @Autowired
    private MessageSource messageSource;
    
    public WeiXinXmlMsgAutoReplyService() {
    }
    
    //Get reply message from DB.
    public String getAutoReplyMsg(String fromUserName, String toUserName, String key) {
        log.info("getAutoReplyMsg, toUserName=" + toUserName);
        
        String defaultKey = messageSource.getMessage("gzh.auto.reply.default.key", null, Locale.SIMPLIFIED_CHINESE);
        
        Predicate predicate = repoQueryFilter.filterGzhAutoReplyByGzhIdAndKey(toUserName, key);
        GzhAutoReply gzhAutoReply = gzhAutoReplyRepository.findOne(predicate);
        
        if (gzhAutoReply == null) {
            predicate = repoQueryFilter.filterGzhAutoReplyByGzhIdAndKey(toUserName, defaultKey);
            gzhAutoReply = gzhAutoReplyRepository.findOne(predicate);
        }
        
        if (gzhAutoReply == null) {
            predicate = repoQueryFilter.filterGzhAutoReplyByPublicAndKey(key);
            gzhAutoReply = gzhAutoReplyRepository.findOne(predicate);
        }
        
        if (gzhAutoReply == null) {
            predicate = repoQueryFilter.filterGzhAutoReplyByPublicAndKey(defaultKey);
            gzhAutoReply = gzhAutoReplyRepository.findOne(predicate);
        }
        
        if (gzhAutoReply == null) {
            //Nothing in the system matches and we do not have the default one.
            //This happens if we did not load the public autoReply.json in the system.
            String message = messageSource.getMessage("gzh.auto.reply.default.message", null, Locale.SIMPLIFIED_CHINESE);
            return message;
        }
        
        //Found a match in the database
        String replyStr = gzhAutoReply.getMessage();
        log.info("getAutoReplyMsg replyStr =" + replyStr);
        return replyStr;
    }
    
}
