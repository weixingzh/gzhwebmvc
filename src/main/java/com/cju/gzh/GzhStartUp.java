package com.cju.gzh;

import java.io.IOException;
import java.io.InputStream;
import java.net.URISyntaxException;
import java.nio.charset.Charset;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;

import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.core.annotation.Order;
import org.springframework.core.io.Resource;
import org.springframework.orm.jpa.EntityManagerHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;
import org.springframework.transaction.support.TransactionSynchronizationManager;
import org.springframework.util.StreamUtils;
import com.amazonaws.SdkClientException;
import com.cju.file.ImageService;
import com.cju.file.ImageSourceType;
import com.cju.gzhOps.GzhAutoReply;
import com.cju.gzhOps.GzhAutoReplyRepository;
import com.cju.gzhOps.GzhAutoReplyType;
import com.cju.gzhOps.GzhCompanyRepository;
import com.cju.gzhOps.GzhHtmlPage;
import com.cju.gzhOps.GzhHtmlPageLinkType;
import com.cju.gzhOps.GzhHtmlPageRepository;
import com.cju.gzhOps.GzhHtmlPageType;
import com.cju.gzhOps.GzhSystemLink;
import com.cju.gzhOps.GzhSystemLinkRepository;
import com.cju.gzhOps.GzhTask;
import com.cju.gzhOps.GzhTaskRepository;
import com.cju.gzhOps.GzhTaskStep;
import com.cju.gzhOps.GzhTaskStepRepository;
import com.cju.marketing.ReferalService;
import com.cju.security.RepositoryQueryFilter;
import com.cju.user.User;
import com.cju.user.UserRepository;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.querydsl.core.types.Predicate;

//ApplicationRunner will run once SpringApplication has started.
//ApplicationArguments are the arguments passed to SpringApplication.run(WebMvcApplication.class, args) in WebMvcApplication.java
//@PropertySource("classpath:/gzh/gzh.properties")
@Order(3)
@Component
public class GzhStartUp implements ApplicationRunner {
	private Logger log = LoggerFactory.getLogger(GzhStartUp.class);
    
	@Value("${app.image.clear}")
	private Boolean gzhImageClear;
	
	@Value("${app.image.load}")
	private Boolean gzhImageLoad;
	
	@Value("${app.htmlpage.template.clear}")
	private Boolean gzhHtmlPageTemplateClear;
	
	@Value("${app.htmlpage.template.load}")
	private Boolean gzhHtmlPageTemplateLoad;
	
	@Value("${app.task.template.clear}")
	private Boolean gzhTaskTemplateClear;
	
	@Value("${app.task.template.load}")
	private Boolean gzhTaskTemplateLoad;
	
	@Value("${app.auto.reply.clear}")
	private Boolean gzhAutoReplyClear;
	
	@Value("${app.auto.reply.load}")
	private Boolean gzhAutoReplyLoad;
	
	@Value("${app.system.link.clear}")
	private Boolean gzhSystemLinkClear;
	
	@Value("${app.system.link.load}")
	private Boolean gzhSystemLinkLoad;
    
	@Value(value = "classpath:/gzhOps/serverProvidedImages.json")
	private Resource serverProvidedImagesJson;
	
	@Value(value = "classpath:/gzhOps/htmlPageTemplate.json")
	private Resource htmlPageTemplateJson;
	
    @Value(value = "classpath:/gzhOps/taskTemplate.json")
    private Resource taskTemplateJson;
    
    @Value(value = "classpath:/gzhOps/systemLink.json")
    private Resource systemLinkJson;
    
    @Value(value = "classpath:/gzh/autoReply.json")
    private Resource autoReplyJson;
    
    @Value("${gzh.weixin.api.host}")
	private String weixinApiHost;
    
    @Value("${app.superuser.password}")
    private String superUserPassword;
	
	@Autowired
	UserRepository userRepository;
	
	@Autowired
	GzhCompanyRepository companyRepository;
	
    @Autowired
    GzhTaskRepository gzhTaskRepository;
    
    @Autowired
    GzhTaskStepRepository gzhTaskStepRepository;
    
    @Autowired
    ImageService imageService;
    
    @Autowired
    GzhHtmlPageRepository gzhHtmlPageRepository;
    
    @Autowired
    GzhAutoReplyRepository gzhAutoReplyRepository;
    
    @Autowired
    GzhSystemLinkRepository gzhSystemLinkRepository;
    
    @Autowired
    ReferalService referalService;
    
    @Autowired 
    RepositoryQueryFilter repoQueryFilter;
    
    @Autowired
    private ObjectMapper objectMapper;
    
    //Inject the entityManagerFactory, only one in the whole application
    @PersistenceUnit
    private EntityManagerFactory entityManagerFactory;
    
    @PostConstruct
    public void init() {
        if(!TransactionSynchronizationManager.hasResource(entityManagerFactory)) {
            log.info("GzhStartUp init(), binding entityManager to thread!");
            EntityManager entityManager = entityManagerFactory.createEntityManager();
            TransactionSynchronizationManager.bindResource(entityManagerFactory, new EntityManagerHolder(entityManager));
        };
    }
    
    private void cleanUp() {
        if(TransactionSynchronizationManager.hasResource(entityManagerFactory)) {
            log.info("GzhStartUp cleanUp(), Unbind entityManager from thread!");
            TransactionSynchronizationManager.unbindResource(entityManagerFactory);
        };
    }
    
	@Override
    public void run(ApplicationArguments applicationArguments) {
	    if (gzhImageClear) {
	        log.info("run(), clear server provided images into DB.");
	        clearGzhImages();
	    }
	    if (gzhImageLoad) {
	        log.info("run(), load server provided images into DB.");
	        try {
                loadGzhImages();
            } catch (URISyntaxException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (SdkClientException e) {
                //Some times when running on home PC, com.amazonaws.SdkClientException is thrown due to java.net.UnknownHostException
                //which is caused by DNS not able to resolve gzhwebmvc-test.s3.cn-north-1.amazonaws.com.cn
                //Note that java.net.UnknownHostException is a subclass of IOException and can be caught by loadGzhImages(). 
                //But aws wraps java.net.UnknownHostException inside com.amazonaws.SdkClientException, 
                //which inherits from java.lang.RuntimeException and thus not caught by loadGzhImages().
                
                //Catch the exception here so that run() returns normally. 
                //Otherwise, the exception is thrown up levels and the application does not start.
                log.info("GzhStartUp loadGzhImages() throws com.amazonaws.SdkClientException");
                e.printStackTrace();
            }
	    }
	    
	    if (gzhTaskTemplateClear) {
	        log.info("run(), clear gzhTask Public Template.");
	        clearGzhTaskPublicTemplates();
	    }
	    if (gzhTaskTemplateLoad) {
	        log.info("run(), load gzhTask Public Template.");
	        loadGzhTaskPublicTemplates();
	    }
	    
	    if (gzhHtmlPageTemplateClear) {
	        log.info("run(), clear gzhHtmlPageTemplate.");
	        clearGzhHtmlPageTemplates();
	    }
	    if (gzhHtmlPageTemplateLoad) {
	        log.info("run(), load gzhHtmlPageTemplate.");
	        try {
                loadGzhHtmlPageTemplates();
            } catch (URISyntaxException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
	    }
	    
        if (gzhAutoReplyClear) {
            log.info("run(), clear gzhAutoReply.");
            clearGzhAutoReply();
        }
        if (gzhAutoReplyLoad) {
            log.info("run(), load gzhAutoReply.");
            loadGzhAutoReply();
        }
        
        if (gzhSystemLinkClear) {
            log.info("run(), clear gzhSystemLink.");
            clearGzhSystemLink();
        }
        if (gzhSystemLinkLoad) {
            log.info("run(), load gzhSystemLink.");
            loadGzhSystemLink();
        }
        
        BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
        
        //create superuser if not exists
        User superUser = userRepository.findOneByUserName("superuser");
        if (superUser == null) {
            superUser = new User();
            superUser.setUserName("superuser");
            superUser.setPassword(passwordEncoder.encode(superUserPassword));
            superUser.setEmail("superuser@gandongtech.com");
            superUser.setRoles("ROLE_USER, ROLE_SUPERUSER, ROLE_WEBADMIN");
            superUser.setEnabled(true);
            userRepository.save(superUser);
            referalService.createReferalForUser(superUser, 0);
        }
        
        String pw123 = passwordEncoder.encode("123");
        
        //create support if not exists
        User support = userRepository.findOneByUserName("support");
        if (support == null) {
            support = new User();
            support.setUserName("support");
            support.setPassword(pw123);
            support.setEmail("support@gandongtech.com");
            support.setRoles("ROLE_USER");
            support.setEnabled(true);
            userRepository.save(support);
            referalService.createReferalForUser(support, 0);
        }
        
        //create test if not exists
        User test = userRepository.findOneByUserName("test");
        if (test == null) {
            test = new User();
            test.setUserName("test");
            test.setPassword(pw123);
            test.setEmail("test@gandongtech.com");
            test.setRoles("ROLE_USER");
            test.setEnabled(true);
            userRepository.save(test);
            referalService.createReferalForUser(test, 0);
        }
        
        cleanUp();
    }
	
	//Hibernate does require the same EntityManager to be available in order to lazily load objects.
	//With spring.jpa.open-in-view=true, SpringBoot will register an OpenEntityManagerInViewInterceptor, 
	//which registers an EntityManager to the current thread, so you will have the same EntityManager until the web request is finished.
	
	//However, clearGzhTaskTemplates() is not called inside a web request. And the EntityManager will be closed after method call.
	//So if steps is lazy loaded, gzhTaskTemplate.getSteps() will throw the famous 
	//org.hibernate.LazyInitializationException: failed to lazily initialize a collection of role: com.cju.gzhOps.GzhTask.steps, 
	//could not initialize proxy - no Session
	//Note: Hibernate Session is the implementation of EntityManager. Also called Persistent Context.
	
	//To overcome the problem, bind an entityManger to the local thread in @PostConstruct
	private void clearGzhTaskPublicTemplates() {
	    Predicate predicate = repoQueryFilter.filterGzhTaskPublicTemplate();
        List<GzhTask> gzhTaskTemplateList = gzhTaskRepository.findAll(predicate);
        for (GzhTask gzhTaskTemplate: gzhTaskTemplateList) {
            //delete steps first. Task can not be deleted before steps as step has reference to task.
            List<GzhTaskStep> steps = gzhTaskTemplate.getSteps();
            for (GzhTaskStep step: steps)
               gzhTaskStepRepository.delete(step);
            
            gzhTaskRepository.delete(gzhTaskTemplate);
        }
	}
	
	private void loadGzhTaskPublicTemplates() {
        //read JSON like DOM Parser
        JsonNode rootNode = null;
        try {
            String taskTemplateJsonStr=StreamUtils.copyToString(taskTemplateJson.getInputStream(), Charset.forName("UTF-8"));
            rootNode = objectMapper.readTree(taskTemplateJsonStr);
            for( JsonNode task: rootNode) {
                GzhTask taskFromJson = objectMapper.treeToValue(task, GzhTask.class);
                //Add task template to database
                GzhTask savedTask = gzhTaskRepository.save(taskFromJson);
                for (GzhTaskStep taskStep: taskFromJson.getSteps()) {
                    //savedTask has id 
                    taskStep.setGzhTask(savedTask);
                    gzhTaskStepRepository.save(taskStep);
                }
            }
            log.info("loadGzhTaskTemplates() rootNode=" + rootNode);
        } catch (JsonProcessingException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
	}
	
	private void clearGzhImages() {
	    imageService.removeServerProvidedImageEntries();
	}
	
    private void loadGzhImages() throws URISyntaxException {
        
        try {
            String serverProvidedImagesJsonStr = StreamUtils.copyToString(serverProvidedImagesJson.getInputStream(), Charset.forName("UTF-8"));
            JsonNode rootNode = objectMapper.readTree(serverProvidedImagesJsonStr);
            log.info("loadGzhImages() rootNote=" + rootNode);
            
            //The following works in Eclipse IDE, but does not work when running a jar.
            //URL fileUrl = this.getClass().getResource(fileFullName);
            //File file = new File(fileUrl.toURI());
            //it is not a file! When you run from the ide you don't have any error, because you don't run a jar file. 
            //In the IDE classes and resources are extracted on the file system.
            //The URI is not hierarchical occurs because the URI for a resource within a jar file 
            //is going to look like something like this: file:/example.jar!/file.txt.
            
            //rootNode is an array 
            for (JsonNode imageNode: rootNode) {
                String fileName = imageNode.path("fileName").asText();
                String fileFullName = "/static/pub/images/" + fileName;
                InputStream fileInputStream = this.getClass().getResourceAsStream(fileFullName);
                byte[] inImageData = IOUtils.toByteArray(fileInputStream);
                imageService.saveImage(fileName, inImageData, ImageSourceType.SERVER_PROVIDED, null);
            }
        } catch (JsonProcessingException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
	
    private void clearGzhHtmlPageTemplates() {
        Predicate predicate = repoQueryFilter.filterGzhHtmlPageByType(GzhHtmlPageType.TEMPLATE);
        Iterable<GzhHtmlPage> gzhHtmlPageList = gzhHtmlPageRepository.findAll(predicate);
        for (GzhHtmlPage gzhHtmlPage: gzhHtmlPageList) {
            //log.info("clearGzhHtmlPageTemplates deleting gzhHtmlPage=" + gzhHtmlPage);
            gzhHtmlPageRepository.delete(gzhHtmlPage);
        }
    }
    
    private void loadGzhHtmlPageTemplates() throws URISyntaxException {
        ObjectMapper objectMapper = new ObjectMapper();
        //read JSON like DOM Parser
        try {
            String htmlPageTemplateJsonStr=StreamUtils.copyToString(htmlPageTemplateJson.getInputStream(), Charset.forName("UTF-8"));
            JsonNode rootNode = objectMapper.readTree(htmlPageTemplateJsonStr);
            log.info("loadGzhHtmlPageTemplates() rootNote=" + rootNode);
            
            //rootNode is an array 
            for (JsonNode htmlPageNode: rootNode) {
                String title = htmlPageNode.path("title").asText();
                String fileName = htmlPageNode.path("fileName").asText();
                InputStream fileInputStream = this.getClass().getResourceAsStream(fileName);
                String bodyHtml = IOUtils.toString(fileInputStream, Charset.forName("UTF-8"));
                //log.info("bodyHtml" + bodyHtml);
                
                GzhHtmlPage gzhHtmlPage = new GzhHtmlPage();
                gzhHtmlPage.setTitle(title);
                gzhHtmlPage.setPageType(GzhHtmlPageType.TEMPLATE);
                gzhHtmlPage.setLinkType(GzhHtmlPageLinkType.LOCAL_HTML);
                gzhHtmlPage.setLocalHtml(bodyHtml);
                
                gzhHtmlPageRepository.save(gzhHtmlPage);
            }
        } catch (JsonProcessingException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
	
    private void clearGzhAutoReply() {
        Predicate predicate = repoQueryFilter.filterGzhAutoReplyByPublic();
        List<GzhAutoReply> gzhAutoReplyList = gzhAutoReplyRepository.findAll(predicate);
        for (GzhAutoReply gzhAutoReply: gzhAutoReplyList) {
            log.info("clearGzhAutoReply deleting gzhAutoReply=" + gzhAutoReply);
            gzhAutoReplyRepository.delete(gzhAutoReply);
        }
    }
    
    private void loadGzhAutoReply() {
        
        //read JSON like DOM Parser
        JsonNode rootNode = null;
        try {
            String autoReplyJsonStr=StreamUtils.copyToString(autoReplyJson.getInputStream(), Charset.forName("UTF-8"));
            rootNode = objectMapper.readTree(autoReplyJsonStr);
            for( JsonNode autoReply: rootNode) {
                GzhAutoReply autoReplyFromJson = objectMapper.treeToValue(autoReply, GzhAutoReply.class);
                autoReplyFromJson.setType(GzhAutoReplyType.PUBLIC);
                Predicate predicate = repoQueryFilter.filterGzhAutoReplyByPublicAndKey(autoReplyFromJson.getKey());
                GzhAutoReply gzhAutoReply = gzhAutoReplyRepository.findOne(predicate);
                if (gzhAutoReply == null) {
                    gzhAutoReplyRepository.save(autoReplyFromJson);
                }
                else {
                    gzhAutoReply.setMessage(autoReplyFromJson.getMessage());
                    gzhAutoReplyRepository.save(gzhAutoReply);
                }
            }
            log.info("loadGzhAutoReply() rootNode=" + rootNode);
        } catch (JsonProcessingException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
    
    private void clearGzhSystemLink() {
        List<GzhSystemLink> gzhSystemLinkList = gzhSystemLinkRepository.findAll();
        for (GzhSystemLink gzhSystemLink: gzhSystemLinkList) {
            log.info("clearGzhSystemLink deleting gzhSystemLink=" + gzhSystemLink);
            gzhSystemLinkRepository.delete(gzhSystemLink);
        }
    }
    
    private void loadGzhSystemLink() { 
        
        //read JSON like DOM Parser
        JsonNode rootNode = null;
        try {
            String systemLinkJsonStr=StreamUtils.copyToString(systemLinkJson.getInputStream(), Charset.forName("UTF-8"));
            rootNode = objectMapper.readTree(systemLinkJsonStr);
            for( JsonNode systemLink: rootNode) {
                GzhSystemLink systemLinkFromJson = objectMapper.treeToValue(systemLink, GzhSystemLink.class);
                GzhSystemLink gzhSystemLink = gzhSystemLinkRepository.findOneByName(systemLinkFromJson.getName());
                if (gzhSystemLink == null) {
                    gzhSystemLinkRepository.save(systemLinkFromJson);
                }
                else {
                    gzhSystemLink.setUrl(systemLinkFromJson.getUrl());
                    gzhSystemLink.setDescription(systemLinkFromJson.getDescription());
                }
            }
            log.info("loadGzhSystemLink() rootNode=" + rootNode);
        } catch (JsonProcessingException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
}