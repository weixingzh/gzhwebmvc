package com.cju.gzh;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

//This represents a gzh accessToken for a gzhCompany.
//It is retrieved from weixin by GzhCompany appId and appSecrete
@JsonIgnoreProperties(ignoreUnknown = true)
public class WeixinAccessToken {
	private String accessToken;
	private int expiresIn;
	private String errCode;
	private String errMsg;
	
	public WeixinAccessToken() {
	}
	
	@JsonProperty("access_token")
	public String getAccessToken() {
	    return accessToken;
	}
	
    public void setAccessToken(String access_token) {
        this.accessToken = access_token;
    }
    
    @JsonProperty("expires_in")
    public int getExpiresIn() {
	    return expiresIn;
	}
	
    public void setExpiresIn(int expiresIn ) {
        this.expiresIn = expiresIn;
    }
    
    @JsonProperty("errcode")
	public String getErrCode() {
	    return errCode;
	}
	
    public void setErrCode(String errcode) {
        this.errCode = errcode;
    }
    
    @JsonProperty("errmsg")
	public String getErrMsg() {
	    return errMsg;
	}
	
    public void setErrMsg(String errmsg) {
        this.errMsg = errmsg;
    }
    
    @Override
    public String toString() {
        return "{" +
                "errcode=" + errCode +
                "errmsg=" + errMsg +
                "access_token=" + accessToken +
                ", expires_in=" + expiresIn  +
                "}";
    }
}
