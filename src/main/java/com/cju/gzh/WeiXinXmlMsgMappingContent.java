package com.cju.gzh;

import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

//Use an adapter that implements XmlAdapter for custom marshaling.
//The msgValue may be of type String, integer or float. 
//We use the String representation to align with the XML msg.
//When we need to use the msgValue in the program, we need to convert it to proper type based on the msgTag.
@XmlJavaTypeAdapter(WeiXinXmlMsgMappingContentAdapter.class)
class WeiXinXmlMsgMappingContent {
	private String msgTag;
	private String msgValue;
	
	public WeiXinXmlMsgMappingContent() {
	}
	
	public WeiXinXmlMsgMappingContent(String msgTag, String msgValue) {
		this.msgTag = msgTag;
		this.msgValue = msgValue;
	}
	
	public String getMsgTag() {
	    return msgTag;
	}
	public void setMsgTag(String msgTag) {
	    this.msgTag = msgTag;
	}
	
    public String getMsgValue() {
        return msgValue;
    }
 
    public void setMsgValue(String msgValue) {
        this.msgValue = msgValue;
    }
    
    @Override
    public String toString() {
        return "{" +
                "msgTag=" + msgTag +
                ", msgValue=" + msgValue  +
                "}";
    }
}