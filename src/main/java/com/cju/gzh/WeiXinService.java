package com.cju.gzh;

import java.io.IOException;
import java.io.StringWriter;
import java.nio.charset.Charset;
import java.time.OffsetDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import javax.annotation.PostConstruct;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.TransformerFactoryConfigurationError;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.json.JsonParser;
import org.springframework.boot.json.JsonParserFactory;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.stereotype.Service;
import org.springframework.util.DigestUtils;
import org.springframework.web.client.RestTemplate;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import com.cju.gzhOps.GzhCompany;
import com.cju.gzhOps.GzhCompanyService;
import com.cju.order.Order;
import com.cju.util.ResultCode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.rholder.retry.RetryException;
import com.github.rholder.retry.Retryer;
import com.github.rholder.retry.RetryerBuilder;
import com.github.rholder.retry.StopStrategies;
import com.google.common.base.Predicates;

// WeiXin Gzh AccessToken and WeiXin User AccessToken may expire. We are not caching the access tokens anywhere for now.
// Gzh AccessToken is only used in createGzhMenu() in GzhStartUp
// WeiXin User AccessToken is not used for now. 
// getAndCacheUserAccessTokenAndOpenId() is called in preHandle() in WebMvcConfig to get and cache weixin user openId in httpSession.
@Service
public class WeiXinService {
	private Logger log = LoggerFactory.getLogger(WeiXinService.class);
	
    @Value(value = "classpath:/gzh/gsswdl_A_menu.json")
    private Resource gsswdl_A_menuJson;

    @Value(value = "classpath:/gzh/gdtech_menu.json")
    private Resource gdTech_menuJson;
	
    @Value("${gzh.weixin.menu.host}")
    private String weixinMenuHost;
    
	@Value("${gzh.weixin.api.host}")
	private String weixinApiHost;
	
	@Value("${gzh.weixin.oauth2.host}")
	private String weixinOAuth2Host;
	
	//The appId of the gzh from which weixin pay account is applied 
	@Value("${gzh.weixin.gandong.appId}")
	private String weixinGanDongAppId;
	
	@Value("${gzh.weixin.gandong.merchant.id}")
	private String weixinGanDongMerchantId;
	
	@Value("${gzh.weixin.gandong.merchant.apiKey}")
	private String weixinGanDongMerchantApiKey;
    
	@Autowired 
	private GzhCompanyService gzhCompanyService;
	
    @Autowired
    private ObjectMapper objectMapper;
    
    private DocumentBuilder w3cDomBuilder;
    private Transformer transformer;
    
    @PostConstruct
    public void init() throws ParserConfigurationException, TransformerConfigurationException, TransformerFactoryConfigurationError {
        w3cDomBuilder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
        transformer = TransformerFactory.newInstance().newTransformer();
        transformer.setOutputProperty(OutputKeys.INDENT, "yes");
        transformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
    }
    
	//private WeixinAccessToken accessToken = new WeixinAccessToken();
	
	//This is to get gzh specific access token. Each gzh will have a different access token.
	//TBD. Need to cache the accessToken somewhere.
	public WeixinAccessToken getGzhCompanyAccessToken(String appId, String appSecret) {
        String url = weixinApiHost + "/cgi-bin/token?grant_type={CREDENTIAL}&appid={APPID}&secret={APPSECRET}";	        
        //weixin requires the value to be "client_credential"
        String clientCredential = "client_credential";
		
		RestTemplate restTemplate = new RestTemplate();
		restTemplate.getMessageConverters().add(0, new StringHttpMessageConverter(Charset.forName("UTF-8")));
		
		Map<String, String> vars = new HashMap<String, String>();
		vars.put("APPID", appId);
		vars.put("APPSECRET", appSecret);
		vars.put("CREDENTIAL", clientCredential);
		
		log.info("getAndSetAccessToken() is called. weixinApiHost= " + weixinApiHost + " vars=" + vars.toString());
		
		Callable<WeixinAccessToken> callable = new Callable<WeixinAccessToken>() {
		    public WeixinAccessToken call() throws Exception {
		    	WeixinAccessToken result = restTemplate.getForObject(url, WeixinAccessToken.class, vars);
				log.info("accessToken=" + result.toString());
		        return result; 
		    }
		};

		Retryer<WeixinAccessToken> retryer = RetryerBuilder.<WeixinAccessToken>newBuilder()
		        .retryIfResult(Predicates.<WeixinAccessToken>isNull())
		        .retryIfExceptionOfType(IOException.class)
		        .retryIfRuntimeException()
		        .withStopStrategy(StopStrategies.stopAfterAttempt(3))
		        .build();
		
		WeixinAccessToken result = new WeixinAccessToken();
		
		try {
			result = retryer.call(callable);
		} catch (RetryException e) {
		    log.error("RetryException when trying to get accessToken" + ExceptionUtils.getStackTrace(e));
		} catch (ExecutionException e) {
			log.error("ExecutionException when trying to get accessToken" + ExceptionUtils.getStackTrace(e));
		}
		
		return result;
	}
	
	//An gzh accessToken identifies a gzh in WeiXin
	public ResultCode createGzhMenu(GzhCompany gzhCompany, String accessToken) throws IOException {
		RestTemplate restTemplate = new RestTemplate();
		restTemplate.getMessageConverters().add(0, new StringHttpMessageConverter(Charset.forName("UTF-8")));
		
		Map<String, String> vars = new HashMap<String, String>();
		//getInputStream may throw an IOException
		String menuJsonStr= gzhCompanyService.getGzhMenuJson(gzhCompany);
		String createMenuUrl = weixinApiHost + "/cgi-bin/menu/create?access_token={accessToken}";	
		vars.put("accessToken", accessToken);
		log.info("createGzhMenu createMenuUrl:" + createMenuUrl);
		log.info("createGzhMenu menuJsonStr:" + menuJsonStr);
		
		ResultCode resultCode = new ResultCode();
		
		Callable<Boolean> callable = new Callable<Boolean>() {
		    public Boolean call() throws Exception {
		    	HttpHeaders headers = new HttpHeaders();
		    	headers.setContentType(MediaType.APPLICATION_JSON_UTF8);
		    	HttpEntity<String> entity = new HttpEntity<String>(menuJsonStr ,headers);
		    	
		    	//weixin responses with application/json type.
				String resultStr = restTemplate.postForObject(createMenuUrl, entity, String.class, vars);			
				JsonParser jsonParser = JsonParserFactory.getJsonParser();
				Map<String, Object> resultMap = jsonParser.parseMap(resultStr);
				//check the result
				if ((int)resultMap.get("errcode") == 0)
				{
					log.info("gzhStartUp menu.json create menu success with result:" + resultStr);
					resultCode.setCode(ResultCode.CODE.OK);
					return true; 
				}
				else 
				{
					log.error("gzhStartUp menu.json create menu result:" + resultStr);
					resultCode.setCode(ResultCode.CODE.ERR);
					resultCode.setMessage((String)resultMap.get("errmsg"));
					return false;
				}
		    }
		};

		Retryer<Boolean> retryer = RetryerBuilder.<Boolean>newBuilder()
		        .retryIfResult(Predicates.<Boolean>equalTo(false))
		        .retryIfExceptionOfType(IOException.class)
		        .retryIfRuntimeException()
		        .withStopStrategy(StopStrategies.stopAfterAttempt(3))
		        .build();
		try {
		    retryer.call(callable);
		} catch (RetryException e) {
		    log.error("Exception when trying to create gzh menu." + ExceptionUtils.getStackTrace(e));
		} catch (ExecutionException e) {
			log.error("Exception when trying to create gzh menu." + ExceptionUtils.getStackTrace(e));
		}
		
		return resultCode;
	}

	// A gzh subscriber has a different openId for different gzh.
	// Here accessToken is the gzhCompany accessToken, not weixin user's accessToken
	public WeiXinUser loadUserByOpenIdAndAccessToken(String openId, String accessToken){
	    //Here we need to call weixin api to get user info.
        String url = weixinApiHost + "/cgi-bin/user/info?access_token={ACCESS_TOKEN}&openid={OPENID}&lang=zh_CN";	 
		
		RestTemplate restTemplate = new RestTemplate();
		restTemplate.getMessageConverters().add(0, new StringHttpMessageConverter(Charset.forName("UTF-8")));
		
		Map<String, String> vars = new HashMap<String, String>();
		vars.put("ACCESS_TOKEN", accessToken);
		vars.put("OPENID", openId);
		//log.info("loadUserByOpenIdAndAccessToken() is called. weixinApiHost= " + weixinApiHost + " vars=" + vars.toString());
		
		WeiXinUser result = new WeiXinUser();
		
		Callable<WeiXinUser> callable = new Callable<WeiXinUser>() {
		    public WeiXinUser call() throws Exception {
		    	//log.info("loadUserByOpenIdAndAccessToken() url= " + url + " vars=" + vars.toString());
		    	WeiXinUser user = restTemplate.getForObject(url, WeiXinUser.class, vars);		
				log.info("loadUserByOpenIdAndAccessToken: url= " + url + " result="+ user);
		        return user; 
		    }
		};

		Retryer<WeiXinUser> retryer = RetryerBuilder.<WeiXinUser>newBuilder()
		        .retryIfResult(Predicates.<WeiXinUser>isNull())
		        .retryIfExceptionOfType(IOException.class)
		        .retryIfRuntimeException()
		        .withStopStrategy(StopStrategies.stopAfterAttempt(3))
		        .build();
		try {
		    result = retryer.call(callable);
		} catch (RetryException e) {
		    log.error("RetryException in loadUserByOpenIdAndAccessToken()." + ExceptionUtils.getStackTrace(e));
		} catch (ExecutionException e) {
			log.error("Exception in loadUserByOpenIdAndAccessToken()." + ExceptionUtils.getStackTrace(e));
		}
		
		return result;
    }
	
    //This is to get a access token for a specific gzh subscriber and his openId.
	//This is not a gzh's accessToken
	public WeixinAccessTokenAndOpenId getUserAccessTokenAndOpenId(String appId, String appSecret, String code) {
		String url = weixinApiHost 
				+ "/sns/oauth2/access_token?appid={APPID}&secret={SECRET}&code={CODE}&" 
				+ "grant_type=authorization_code";

		RestTemplate restTemplate = new RestTemplate();
		restTemplate.getMessageConverters().add(0, new StringHttpMessageConverter(Charset.forName("UTF-8")));
		
		Map<String, String> vars = new HashMap<String, String>();
		vars.put("APPID", appId);
		vars.put("SECRET", appSecret);
		vars.put("CODE", code);

		log.info("getUserAccessTokenAndOpenId() is called. url= " + url + " vars=" + vars.toString());
		Callable<WeixinAccessTokenAndOpenId> callable = new Callable<WeixinAccessTokenAndOpenId>() {
		    public WeixinAccessTokenAndOpenId call() throws Exception {
		    	//For this API, weixin response with media type text/plain, but the http body is really a JSON.
		    	//MappingJackson2HttpMessageConverter will only convert application/json.
		    	//StringHttpMessageConverter will only conver text/plain to String.
		    	//There is no HttpMessageConverter to convert text/plain into JSON.
		    	String jsonStr = restTemplate.getForObject(url, String.class, vars);
				log.info("getUserAccessTokenAndOpenId() accessTokenAndOpenId=" + jsonStr.toString());
				//result is a JSON String. Parse it to WeixinAccessTokenAndOpenId.
				WeixinAccessTokenAndOpenId result = objectMapper.readValue(jsonStr, WeixinAccessTokenAndOpenId.class);
		        return result; 
		    }
		};

		Retryer<WeixinAccessTokenAndOpenId> retryer = RetryerBuilder.<WeixinAccessTokenAndOpenId>newBuilder()
		        .retryIfResult(Predicates.<WeixinAccessTokenAndOpenId>isNull())
		        .retryIfExceptionOfType(IOException.class)
		        .retryIfRuntimeException()
		        .withStopStrategy(StopStrategies.stopAfterAttempt(3))
		        .build();
		
		WeixinAccessTokenAndOpenId result = new WeixinAccessTokenAndOpenId();
		
		try {
			result = retryer.call(callable);
		} catch (RetryException e) {
		    log.error("Exception when trying to get accessTokenAndOpenId" + ExceptionUtils.getStackTrace(e));
		} catch (ExecutionException e) {
			log.error("Exception when trying to get accessTokenAndOpenId" + ExceptionUtils.getStackTrace(e));
		}
		return result;
	}
	
	public String calculateWeiXinPayOrderSign(Document doc) {
	    TreeMap<String, String> values = new TreeMap<String, String>();
	    
	    //Element is a subtype of Node
	    //The return character between xml tags are parsed into Node with nodeName #text and nodeType=3 in the Document
	    Element rootElm = doc.getDocumentElement();
	    NodeList nodeList = rootElm.getChildNodes();
	    Node node;
	    
        for (int i = 0; i < nodeList.getLength(); i++) {
            node = nodeList.item(i);
            if (node.getNodeType() == Node.ELEMENT_NODE && !node.getTextContent().equals("") && !node.getNodeName().equals("sign")) {
                    values.put(node.getNodeName(), node.getTextContent());
                    
                    //    log.info("calculateWeiXinPayOrderSign, nodeName=" + node.getNodeName());
                    //    log.info("calculateWeiXinPayOrderSign, nodeValue=" + node.getTextContent());
                    //    byte[] nodeValueBytes = node.getTextContent().getBytes();
                    //    log.info("calculateWeiXinPayOrderSign, nodeValueAsHex=" + Hex.encodeHexString(nodeValueBytes));
            }
        }
        
        StringBuffer stringA = new StringBuffer();
        values.forEach((k,v) -> {
            stringA.append(k).append("=").append(v).append("&");
        });
        
        stringA.append("key=").append(weixinGanDongMerchantApiKey);
        
        String strToSign = stringA.toString();
        //log.info("calculateWeiXinPayOrderSign, strToSign=" + strToSign);
        
        String sign = DigestUtils.md5DigestAsHex(strToSign.getBytes()).toUpperCase();

        return sign;
	}
	
	public String generatePayOrder(Order order)  {
	    String generatePayOrderUrl="https://api.mch.weixin.qq.com/pay/unifiedorder";

	    Document doc = w3cDomBuilder.newDocument();
        Element rootElement = doc.createElement("xml");
        doc.appendChild(rootElement);

        Element appIdElm = doc.createElement("appid");
        appIdElm.appendChild(doc.createTextNode(weixinGanDongAppId));
        rootElement.appendChild(appIdElm);
        
        //merchant Id from weixin
        Element mchIdElm = doc.createElement("mch_id");
        mchIdElm.appendChild(doc.createTextNode(weixinGanDongMerchantId));
        rootElement.appendChild(mchIdElm);
        
        String nonce = String.valueOf(OffsetDateTime.now().toEpochSecond());
        Element nonceElm = doc.createElement("nonce_str");
        nonceElm.appendChild(doc.createTextNode(nonce));
        rootElement.appendChild(nonceElm);
        
        //optional, self defined additional info
        String attach = order.getNote();
        Element attachElm = doc.createElement("attach");
        attachElm.appendChild(doc.createTextNode(attach));
        rootElement.appendChild(attachElm);

        String orderNum = String.valueOf(order.getOrderNum());
        Element orderElm = doc.createElement("out_trade_no");
        orderElm.appendChild(doc.createTextNode(orderNum));
        rootElement.appendChild(orderElm);
   
        //money unit used in weixin is fen
        int amount = (int) (order.getAmount() * 100);
        Element amountElm = doc.createElement("total_fee");
        amountElm.appendChild(doc.createTextNode(Integer.toString(amount)));
        rootElement.appendChild(amountElm);
    
        Element ipElm = doc.createElement("spbill_create_ip");
        ipElm.appendChild(doc.createTextNode("127.0.0.1"));
        rootElement.appendChild(ipElm);

        //This is optional
        //weixin pay time_start and time_expire come from order
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyyMMddHHmmss");
        OffsetDateTime submitTime = order.getSubmitTime();
        String timeStart = submitTime.format(formatter);
        Element timeStartElm = doc.createElement("time_start");
        timeStartElm.appendChild(doc.createTextNode(timeStart));
        rootElement.appendChild(timeStartElm);
       
        OffsetDateTime qrCodeExpireTime = order.getQrCodeExpireTime();
        String timeExpire = qrCodeExpireTime.format(formatter);
        Element timeExpireElm = doc.createElement("time_expire");
        timeExpireElm.appendChild(doc.createTextNode(timeExpire));
        rootElement.appendChild(timeExpireElm);
        
        //The url space /gzh is public accessible. In addition, CSRF protection for POST is turned off for /gzh
        String notifyUrl = "http://" + weixinMenuHost + "/gzh/wxpay/payNotify";
        Element notifyUrlElm = doc.createElement("notify_url");
        notifyUrlElm.appendChild(doc.createTextNode(notifyUrl));
        rootElement.appendChild(notifyUrlElm);
        
        Element tradeTypeElm = doc.createElement("trade_type");
        tradeTypeElm.appendChild(doc.createTextNode("NATIVE"));
        rootElement.appendChild(tradeTypeElm);
        
        String productBrief = order.getProductBrief();
        Element productElm = doc.createElement("body");
        productElm.appendChild(doc.createTextNode(productBrief));
        rootElement.appendChild(productElm);
        
        //Required when trade_type is NATIVE
        String productId = String.valueOf(order.getProductId());
        Element productIdElm = doc.createElement("product_id");
        productIdElm.appendChild(doc.createTextNode(productId));
        rootElement.appendChild(productIdElm);
        
        String sign = calculateWeiXinPayOrderSign(doc);
        Element signElm = doc.createElement("sign");
        signElm.appendChild(doc.createTextNode(sign));
        rootElement.appendChild(signElm);
        
        DOMSource domSource = new DOMSource(doc);
        StringWriter writer = new StringWriter();
        StreamResult streamResult = new StreamResult(writer);
        
        try {
            transformer.transform(domSource, streamResult);
        } catch (TransformerException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        String xmlMsg = writer.toString();
        log.info("generatePayOrder, XML body in pay order generation request to weixin server: \n" + xmlMsg);
        
        //Call weixin API and get response xmlMsg
        RestTemplate restTemplate = new RestTemplate();
        restTemplate.getMessageConverters().add(0, new StringHttpMessageConverter(Charset.forName("UTF-8")));
        
        Map<String, String> vars = new HashMap<String, String>();
        HttpHeaders headers = new HttpHeaders();
        Charset utf8 = Charset.forName("UTF-8");
        MediaType mediaType = new MediaType("application", "xml", utf8);
        headers.setContentType(mediaType);
        HttpEntity<String> entity = new HttpEntity<String>(xmlMsg ,headers);
        
        //OrderService.generateWeiXinPayOrder() is the caller and it will parse the WeiXin response xmlMsg
        //weixin responses with xml body.
        String xmlFromWeiXin = restTemplate.postForObject(generatePayOrderUrl, entity, String.class, vars); 
        log.info("generatePayOrder, XML body in returned response to pay order generation from weixin server: \n" + xmlFromWeiXin);
        
        //For testing
        //    xmlFromWeiXin = "<xml>" +
        //             "<return_code><![CDATA[SUCCESS]]></return_code>" +
        //             "<return_msg><![CDATA[OK]]></return_msg>" +
        //             "<appid><![CDATA[wx2421b1c4370ec43b]]></appid>" +
        //             "<mch_id><![CDATA[10000100]]></mch_id>" +
        //             "<nonce_str><![CDATA[IITRi8Iabbblz1Jc]]></nonce_str>" +
        //             "<openid><![CDATA[oUpF8uMuAJO_M2pxb1Q9zNjWeS6o]]></openid>" +
        //             "<sign><![CDATA[7921E432F65EB8ED0CE9755F0E86D72F]]></sign>" +
        //             "<result_code><![CDATA[SUCCESS]]></result_code>" +
        //             "<prepay_id><![CDATA[wx201411101639507cbf6ffd8b0779950874]]></prepay_id>" +
        //             "<trade_type><![CDATA[JSAPI]]></trade_type>" +
        //             "<code_url><![CDATA[http://baidu.com]]></code_url>" +
        //             "</xml>";
        
        return xmlFromWeiXin;
	}
	
    public String generateReferalVerifyOrder(Order order) {
        String generatePayOrderUrl="https://api.mch.weixin.qq.com/pay/unifiedorder";

        Document doc = w3cDomBuilder.newDocument();
        Element rootElement = doc.createElement("xml");
        doc.appendChild(rootElement);

        Element appIdElm = doc.createElement("appid");
        appIdElm.appendChild(doc.createTextNode(weixinGanDongAppId));
        rootElement.appendChild(appIdElm);
        
        //merchant Id from weixin
        Element mchIdElm = doc.createElement("mch_id");
        mchIdElm.appendChild(doc.createTextNode(weixinGanDongMerchantId));
        rootElement.appendChild(mchIdElm);
        
        String nonce = String.valueOf(OffsetDateTime.now().toEpochSecond());
        Element nonceElm = doc.createElement("nonce_str");
        nonceElm.appendChild(doc.createTextNode(nonce));
        rootElement.appendChild(nonceElm);

        String orderNum = String.valueOf(order.getOrderNum());
        Element orderElm = doc.createElement("out_trade_no");
        orderElm.appendChild(doc.createTextNode(orderNum));
        rootElement.appendChild(orderElm);
   
        //money unit used in weixin is fen
        int amount = (int) (order.getAmount() * 100);
        Element amountElm = doc.createElement("total_fee");
        amountElm.appendChild(doc.createTextNode(Integer.toString(amount)));
        rootElement.appendChild(amountElm);
    
        Element ipElm = doc.createElement("spbill_create_ip");
        ipElm.appendChild(doc.createTextNode("127.0.0.1"));
        rootElement.appendChild(ipElm);

        //This is optional
        //time_start is set. No expire time is set for the qrCode
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyyMMddHHmmss");
        OffsetDateTime submitTime = order.getSubmitTime();
        String timeStart = submitTime.format(formatter);
        Element timeStartElm = doc.createElement("time_start");
        timeStartElm.appendChild(doc.createTextNode(timeStart));
        rootElement.appendChild(timeStartElm);
       
        //The url space /gzh is public accessible. In addition, CSRF protection for POST is turned off for /gzh
        String notifyUrl = "http://" + weixinMenuHost + "/gzh/wxpay/referalVerifyNotify";
        Element notifyUrlElm = doc.createElement("notify_url");
        notifyUrlElm.appendChild(doc.createTextNode(notifyUrl));
        rootElement.appendChild(notifyUrlElm);
        
        Element tradeTypeElm = doc.createElement("trade_type");
        tradeTypeElm.appendChild(doc.createTextNode("NATIVE"));
        rootElement.appendChild(tradeTypeElm);
        
        String productBrief = order.getProductBrief();
        Element productElm = doc.createElement("body");
        productElm.appendChild(doc.createTextNode(productBrief));
        rootElement.appendChild(productElm);
        
        String productId = String.valueOf(order.getProductId());
        Element productIdElm = doc.createElement("product_id");
        productIdElm.appendChild(doc.createTextNode(productId));
        rootElement.appendChild(productIdElm);
        
        String sign = calculateWeiXinPayOrderSign(doc);
        Element signElm = doc.createElement("sign");
        signElm.appendChild(doc.createTextNode(sign));
        rootElement.appendChild(signElm);
        
        DOMSource domSource = new DOMSource(doc);
        StringWriter writer = new StringWriter();
        StreamResult streamResult = new StreamResult(writer);
        
        try {
            transformer.transform(domSource, streamResult);
        } catch (TransformerException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        String xmlMsg = writer.toString();
        log.info("generateReferalVerifyOrder, XML body in pay order generation request to weixin server: \n" + xmlMsg);
        
        //Call weixin API and get response xmlMsg
        RestTemplate restTemplate = new RestTemplate();
        restTemplate.getMessageConverters().add(0, new StringHttpMessageConverter(Charset.forName("UTF-8")));
        
        Map<String, String> vars = new HashMap<String, String>();
        HttpHeaders headers = new HttpHeaders();
        Charset utf8 = Charset.forName("UTF-8");
        MediaType mediaType = new MediaType("application", "xml", utf8);
        headers.setContentType(mediaType);
        HttpEntity<String> entity = new HttpEntity<String>(xmlMsg ,headers);
        
        //OrderService.generateWeiXinPayOrder() is the caller and it will parse the WeiXin response xmlMsg
        //weixin responses with xml body.
        String xmlFromWeiXin = restTemplate.postForObject(generatePayOrderUrl, entity, String.class, vars); 
        log.info("generateReferalVerifyOrder, XML body in returned response to pay order generation from weixin server: \n" + xmlFromWeiXin);
        
        return xmlFromWeiXin;
    }
	   
	public String queryPayOrderStatus(Order order) throws TransformerException  {
	    String queryPayOrderUrl="https://api.mch.weixin.qq.com/pay/orderquery";

        Document doc = w3cDomBuilder.newDocument();
        Element rootElement = doc.createElement("xml");
        doc.appendChild(rootElement);

        Element appIdElm = doc.createElement("appid");
        appIdElm.appendChild(doc.createTextNode(weixinGanDongAppId));
        rootElement.appendChild(appIdElm);
        
        //merchant Id from weixin
        Element mchIdElm = doc.createElement("mch_id");
        mchIdElm.appendChild(doc.createTextNode(weixinGanDongMerchantId));
        rootElement.appendChild(mchIdElm);
        
        String orderNum = String.valueOf(order.getOrderNum());
        Element orderElm = doc.createElement("out_trade_no");
        orderElm.appendChild(doc.createTextNode(orderNum));
        rootElement.appendChild(orderElm);
        
        String nonce = String.valueOf(OffsetDateTime.now().toEpochSecond());
        Element nonceElm = doc.createElement("nonce_str");
        nonceElm.appendChild(doc.createTextNode(nonce));
        rootElement.appendChild(nonceElm);
        
        String sign = calculateWeiXinPayOrderSign(doc);
        Element signElm = doc.createElement("sign");
        signElm.appendChild(doc.createTextNode(sign));
        rootElement.appendChild(signElm);
        
        DOMSource domSource = new DOMSource(doc);
        StringWriter writer = new StringWriter();
        StreamResult streamResult = new StreamResult(writer);
        transformer.transform(domSource, streamResult);
        String xmlMsg = writer.toString();
        log.info("queryPayOrderStatus, XML body in pay order query request to weixin server: \n" + xmlMsg);

        //Call weixin API and get response xmlMsg
        RestTemplate restTemplate = new RestTemplate();
        //Need to add utf8 message converter to restTemplate. Otherwise, the xmlFromWeiXin will contain garbage for trade_state_des.
        restTemplate.getMessageConverters().add(0, new StringHttpMessageConverter(Charset.forName("UTF-8")));
        
        Map<String, String> vars = new HashMap<String, String>();
        HttpHeaders headers = new HttpHeaders();
        Charset utf8 = Charset.forName("UTF-8");
        MediaType mediaType = new MediaType("application", "xml", utf8);
        headers.setContentType(mediaType);
        HttpEntity<String> entity = new HttpEntity<String>(xmlMsg ,headers);
        
        //OrderService.generateWeiXinPayOrder() is the caller and it will parse the WeiXin response xmlMsg
        //weixin responses with xml body.
        String xmlFromWeiXin = restTemplate.postForObject(queryPayOrderUrl, entity, String.class, vars); 
        log.info("queryPayOrderStatus, XML body in returned response to pay order status query from weixin server: \n" + xmlFromWeiXin);
        
        //For testing
        //    xmlFromWeiXin =  "<xml>" +
        //                       "<return_code><![CDATA[SUCCESS]]></return_code>" +
        //                       "<return_msg><![CDATA[OK]]></return_msg>" +
        //                       "<appid><![CDATA[wx2421b1c4370ec43b]]></appid>" +
        //                       "<mch_id><![CDATA[10000100]]></mch_id>" +
        //                       "<device_info><![CDATA[1000]]></device_info>" +
        //                       "<nonce_str><![CDATA[TN55wO9Pba5yENl8]]></nonce_str>" +
        //                       "<sign><![CDATA[BDF0099C15FF7BC6B1585FBB110AB635]]></sign>" +
        //                       "<result_code><![CDATA[SUCCESS]]></result_code>" +
        //                       "<openid><![CDATA[oUpF8uN95-Ptaags6E_roPHg7AG0]]></openid>" +
        //                       "<is_subscribe><![CDATA[Y]]></is_subscribe>" +
        //                       "<trade_type><![CDATA[MICROPAY]]></trade_type>" +
        //                       "<bank_type><![CDATA[CCB_DEBIT]]></bank_type>" +
        //                       "<total_fee>1</total_fee>" +
        //                       "<fee_type><![CDATA[CNY]]></fee_type>" +
        //                       "<transaction_id><![CDATA[1008450740201411110005820873]]></transaction_id>" +
        //                       "<out_trade_no><![CDATA[1415757673]]></out_trade_no>" +
        //                       "<attach><![CDATA[订单额外描述]]></attach>" +
        //                       "<time_end><![CDATA[20141111170043]]></time_end>" +
        //                       "<trade_state><![CDATA[SUCCESS]]></trade_state>" +
        //                       "</xml>";
	    return xmlFromWeiXin;
	}
}
