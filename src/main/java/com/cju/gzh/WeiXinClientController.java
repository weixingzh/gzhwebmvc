package com.cju.gzh;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.time.OffsetDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.querydsl.QSort;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cju.chanjet.ChanjetClientCompany;
import com.cju.gzhOps.GzhClientCompany;
import com.cju.gzhOps.GzhClientCompanyReport;
import com.cju.gzhOps.GzhClientCompanyReportRepository;
import com.cju.gzhOps.GzhClientCompanyReportType;
import com.cju.gzhOps.GzhClientCompanyRepository;
import com.cju.gzhOps.GzhCompany;
import com.cju.gzhOps.GzhHtmlPage;
import com.cju.gzhOps.GzhHtmlPageLinkType;
import com.cju.gzhOps.GzhHtmlPageRepository;
import com.cju.gzhOps.GzhHtmlPageTitle;
import com.cju.gzhOps.GzhCompanyRepository;
import com.cju.gzhOps.GzhContact;
import com.cju.gzhOps.GzhContactRepository;
import com.cju.gzhOps.GzhTask;
import com.cju.gzhOps.GzhTaskRepository;
import com.cju.gzhOps.GzhTaskStep;
import com.cju.gzhOps.GzhTaskStepRepository;
import com.cju.gzhOps.GzhTaskType;
import com.cju.gzhOps.GzhUser;
import com.cju.gzhOps.GzhUserRepository;
import com.cju.gzhOps.GzhUserService;
import com.cju.security.RepositoryQueryFilter;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.querydsl.core.types.Predicate;

@Controller
public class WeiXinClientController {
    private Logger log = LoggerFactory.getLogger(WeiXinClientController.class);
    
    @Autowired 
    private GzhClientCompanyReportRepository gzhClientCompanyReportRepository; 
    
    @Autowired 
    private GzhClientCompanyRepository gzhClientCompanyRepository; 
    
    @Autowired 
    private GzhCompanyRepository gzhCompanyRepository; 
    
    @Autowired 
    private GzhUserRepository gzhUserRepository; 
    
    @Autowired 
    private GzhTaskRepository gzhTaskRepository; 
    
    @Autowired 
    private GzhTaskStepRepository gzhTaskStepRepository; 
    
    @Autowired 
    private GzhHtmlPageRepository gzhHtmlPageRepository; 
    
    @Autowired 
    private GzhContactRepository gzhContactRepository; 
    
    @Autowired 
    RepositoryQueryFilter repoQueryFilter;
    
    @Autowired
    private MessageSource messageSource;
    
    @Autowired
    GzhUserService gzhUserService;
    
    @Autowired
    private ObjectMapper objectMapper;
    
    //URLs starts with /pub/gzh/, http requests initiated from weixin client
    
    //If the IntroPage is an external link, the request will be sent to the external link directly from weixin client.
    //Only when IntroPage is a local html file, the request is sent here.
    //Anyone can access this request.
    //Query parameter: pageTitle=IntroPage
    @GetMapping("/pub/gzh/company/{id}/localHtml")
    public String getGzhLocalHtml(HttpServletRequest request, @PathVariable("id") long id, @RequestParam Map<String, String> params, Model model)  {
        String htmlBody = null;
        log.info("getGzhLocalHtml() receives a request. requestUrl=" + request.getRequestURL().toString());
        //Get the html page for the GzhCompany with the htmlPageType
        GzhHtmlPageTitle pageTitle = GzhHtmlPageTitle.fromString(params.get("pageTitle"));
        Predicate predicate = repoQueryFilter.filterGzhHtmlPageByCompanyIdAndTitle(id, pageTitle);
        GzhHtmlPage gzhHtmlPage = gzhHtmlPageRepository.findOne(predicate);
        
        if (gzhHtmlPage == null) {
            GzhCompany gzhCompany = gzhCompanyRepository.findOne(id);
            String message = messageSource.getMessage("gzh.gzhcompany.htmlpage.no.local.html", 
                    new Object[] {gzhCompany.getName(), pageTitle.toString()}, Locale.SIMPLIFIED_CHINESE);
            model.addAttribute("message", message);
            return "gzh/gzhWeiXinMessage";
        }
        
        if (gzhHtmlPage.getLinkType() == GzhHtmlPageLinkType.EXTERNAL_LINK) {
            return "redirect:" + gzhHtmlPage.getExternalLink();
        }

        // To return localHtml page
        htmlBody = gzhHtmlPage.getLocalHtml(); 
        model.addAttribute("pageTitle", pageTitle.toString());
        model.addAttribute("htmlBody", htmlBody);
        return "gzh/gzhWeiXinHtmlPageWithoutLayout";
    }
    
    //we do not need gzhCompanyId in this method
    //For htmlPage templage, gzhCompany is null.
    //For url consistency, we still use the url prefix /pub/gzh/company
    @GetMapping("/pub/gzh/company/localHtml/{htmlPageId}")
    public String getGzhLocalHtmlByHtmlPageId(@PathVariable("htmlPageId") long htmlPageId, Model model)  {
        
        GzhHtmlPage gzhHtmlPage = gzhHtmlPageRepository.findOne(htmlPageId);
        if (gzhHtmlPage == null) {
            String message = messageSource.getMessage("gzh.weixin.htmlpage.not.found", new Object[] {htmlPageId}, Locale.SIMPLIFIED_CHINESE);
            model.addAttribute("message", message);
            return "gzh/gzhWeiXinMessage";
        }
        
        if (gzhHtmlPage.getLinkType() == GzhHtmlPageLinkType.EXTERNAL_LINK) {
            return "redirect:" + gzhHtmlPage.getExternalLink();
        }
        
        String htmlBody = gzhHtmlPage.getLocalHtml(); 
        String title = gzhHtmlPage.getTitle();
        model.addAttribute("pageTitle", title);
        model.addAttribute("htmlBody", htmlBody);
        return "gzh/gzhWeiXinHtmlPageWithoutLayout";
    }
    
    //For url consistency, we still use the url prefix /pub/gzh/company
    //query parameter: fileName=weixinHtmlPageTemplate.html
    @GetMapping("/pub/gzh/company/localHtml")
    public String getGzhLocalHtmlByFileName(@RequestParam Map<String, String> params, Model model) throws IOException  {
        String fileName = params.get("fileName");
        fileName = "/templates/weixin/" + fileName;
        InputStream fileInputStream = this.getClass().getResourceAsStream(fileName);
        if (fileInputStream == null) {
            String message = messageSource.getMessage("gzh.weixin.htmlpage.not.found", new Object[] {fileName}, Locale.SIMPLIFIED_CHINESE);
            model.addAttribute("message", message);
            return "gzh/gzhWeiXinMessage";
        }
        String htmlBody = IOUtils.toString(fileInputStream, Charset.forName("UTF-8"));
        //log.info("htmlBody" + htmlBody);
        
        String title = fileName;
        model.addAttribute("pageTitle", title);
        model.addAttribute("htmlBody", htmlBody);
        return "gzh/gzhWeiXinHtmlPageWithoutLayout";
    }
    
    @GetMapping("/pub/gzh/company/localHtml/collectFileName")
    public String getGzhLocalHtmlCollectFileNameForm() throws IOException  {
        return "gzh/gzhWeiXinHtmlPageCollectFileName";
    }
    
    @GetMapping("/pub/gzh/company/{id}/contact/list")
    public String getGzhContactList(@PathVariable("id") long id,  Model model)  {
        Predicate predicate = repoQueryFilter.filterGzhContactByGzhCompanyId(id);
        List<GzhContact> gzhContactList = gzhContactRepository.findAll(predicate);
        
        if(gzhContactList.isEmpty()) {
            String message = messageSource.getMessage("gzh.weixin.contact.empty", null, Locale.SIMPLIFIED_CHINESE);
            model.addAttribute("message", message);
            return "gzh/gzhWeiXinMessage";
        }
        model.addAttribute("gzhContactList", gzhContactList);
        return "gzh/gzhWeiXinContactList";
    }
    
    // URLs starts with /gzh/company/{companyId}, http request initiated from weixin clients
    
    //All the methods starts with /gzh/company/{companyId}/ will need to have openId set in HttpSession.
    //companyId is gzhCompanyId.
    //Even though we do not use gzhCompanyId in those methods mapping to /gzh/company/*, we still need it in the url path, 
    //so that the authorizationInterceptor can match the URL pattern and intercept it to set the openId in the httpSession.
    //The openId is set in the authorizationInterceptor via OAuth2.
    
    @GetMapping(value="/gzh/company/{companyId}/service/request")
    public String getGzhWeiXinServiceRequest(HttpSession httpSession, @PathVariable("companyId") long companyId, Model model) {
        //Now we have the openId in httpSession setup by the preHandle of authorizationInterceptor
        String openId = (String) httpSession.getAttribute("openId");
        if (openId == null) {
            //This should not happen if a user access the link from weixin client.
            //However, someone may access the link directly from a browser if he is able to guess the link. 
            //In this case, returns here for security.
            String message = messageSource.getMessage("gzh.openId.not.found.in.http", null, Locale.SIMPLIFIED_CHINESE);
            model.addAttribute("message", message);
            return "gzh/gzhWeiXinMessage";
        }
        
        //Need to make sure gzhUser exists in database. If not, add it right away.
        GzhUser gzhUser = gzhUserService.addGzhUserIfNotExist(openId, companyId);
        if (gzhUser.getMobile() == null || gzhUser.getMobile().equals("")) {
            String message = messageSource.getMessage("gzh.weixin.mobile.bind.first", null, Locale.SIMPLIFIED_CHINESE);
            model.addAttribute("message", message);
            return "gzh/gzhWeiXinMessage";
        }
        model.addAttribute("companyId", companyId);
        return ("gzh/gzhWeiXinServiceRequest");
    }
    
    @GetMapping(value="/gzh/company/{companyId}/selfInfo")
    public String getGzhWeiXinSelfInfo(HttpSession httpSession, @PathVariable("companyId") long companyId, Model model) {
        //Now we have the openId in httpSession setup by the preHandle of authorizationInterceptor
        String openId = (String) httpSession.getAttribute("openId");
        if (openId == null) {
            //This should not happen if a user access the link from weixin client.
            //However, someone may access the link directly from a browser if he is able to guess the link. 
            //In this case, returns here for security.
            String message = messageSource.getMessage("gzh.openId.not.found.in.http", null, Locale.SIMPLIFIED_CHINESE);
            model.addAttribute("message", message);
            return "gzh/gzhWeiXinMessage";
        }
        
        //Need to make sure gzhUser exists in database. If not, add it right away.
        GzhUser gzhUser = gzhUserService.addGzhUserIfNotExist(openId, companyId);
        model.addAttribute("gzhUser", gzhUser);
        model.addAttribute("companyId", companyId);
        return "gzh/gzhWeiXinSelfInfo";
    }
    
    //Ajax post to update selfInfo
    @PostMapping(value="/gzh/company/{companyId}/api/v1/selfInfo")
    public @ResponseBody String postGzhWeiXinSelfInfoApi(HttpSession httpSession, @PathVariable("companyId") long companyId, @RequestBody JsonNode json, Model model) {
        //Now we have the openId in httpSession setup by the preHandle of authorizationInterceptor
        String openId = (String) httpSession.getAttribute("openId");
        if (openId == null) {
            //This should not happen if a user access the link from weixin client.
            //However, someone may access the link directly from a browser if he is able to guess the link. 
            //In this case, returns here for security.
            String message = messageSource.getMessage("gzh.openId.not.found.in.http", null, Locale.SIMPLIFIED_CHINESE);
            model.addAttribute("message", message);
            return "gzh/gzhWeiXinMessage";
        }
        
        //Need to make sure gzhUser exists in database. If not, add it right away.
        GzhUser gzhUser = gzhUserService.addGzhUserIfNotExist(openId, companyId);
        String name = json.path("name").asText();
        String mobile = json.path("mobile").asText();
        String weixin = json.path("weixin").asText();
        String email = json.path("email").asText();
        gzhUser.setName(name);
        gzhUser.setMobile(mobile);
        gzhUser.setWeixin(weixin);
        gzhUser.setEmail(email);
        model.addAttribute("gzhUser", gzhUser);
        gzhUserRepository.save(gzhUser);
        
        String updateSuccess = messageSource.getMessage("gzh.weixin.selfinfo.update.success", null, Locale.SIMPLIFIED_CHINESE);
        model.addAttribute("updateSuccess", updateSuccess);
        return updateSuccess;
    }
    
    @GetMapping(value="/gzh/company/{companyId}/task/template/list")
    public @ResponseBody String getGzhWeiXinTaskTemplateListApi(HttpSession httpSession, @PathVariable("companyId") long companyId, HttpServletResponse response) {
        //Now we have the openId in httpSession setup by the preHandle of authorizationInterceptor
        String openId = (String) httpSession.getAttribute("openId");
        if (openId == null) {
            //This should not happen if a user access the link from weixin client.
            //However, someone may access the link directly from a browser if he is able to guess the link. 
            //In this case, returns here for security.
            String message = messageSource.getMessage("gzh.openId.not.found.in.http", null, Locale.SIMPLIFIED_CHINESE);
            response.setStatus(HttpStatus.BAD_REQUEST.value());
            return message;
        }
        
        //QueryDSL has a bug to generate correct SQL for publicPredicate.or(privatePredicate)
        //It generate something like the following with cross join 
        //select ... from GzhTask cross join GzhUser 
        //    where gzhTask.gzhUser.id == gzhUser.id AND (gzhTaskTemplateType == PUBLIC OR gzhTask.gzhUser.gzhCompanyId == gzhUser.gzhCompany.id)
        //Since all the public templates does not have gzhUser, they will not be in the result set.
        Predicate publicPredicate = repoQueryFilter.filterGzhTaskPublicTemplate();
        List<GzhTask> gzhTaskpublicTemplateList = gzhTaskRepository.findAll(publicPredicate);
        log.info("getGzhWeiXinTaskTemplateListApi gzhTaskPublicTemplateList=" + gzhTaskpublicTemplateList);
        
        Predicate privatePredicate = repoQueryFilter.filterGzhTaskPrivateTemplateByGzhCompanyId(companyId);
        List<GzhTask> gzhTaskPrivateTemplateList = gzhTaskRepository.findAll(privatePredicate);
        log.info("getGzhWeiXinTaskTemplateListApi gzhTaskPrivateTemplateList=" + gzhTaskPrivateTemplateList);
        
        List<GzhTask> gzhTaskTemplateList = new ArrayList<GzhTask>();
        gzhTaskTemplateList.addAll(gzhTaskpublicTemplateList);
        gzhTaskTemplateList.addAll(gzhTaskPrivateTemplateList);
        
        try {
            String gzhTaskTemplateListJson = objectMapper.writeValueAsString(gzhTaskTemplateList);
            return  gzhTaskTemplateListJson;
        } catch (JsonProcessingException e) {
            response.setStatus(HttpStatus.BAD_REQUEST.value());
            return e.getLocalizedMessage();
        }
    }
    
    @GetMapping(value="/gzh/company/{companyId}/task/list")
    public String getGzhWeiXinTaskList(HttpSession httpSession, @PathVariable("companyId") long companyId, Model model, Pageable pageable) {
        //Now we have the openId in httpSession setup by the preHandle of authorizationInterceptor
        String openId = (String) httpSession.getAttribute("openId");
        if (openId == null) {
            //This should not happen if a user access the link from weixin client.
            //However, someone may access the link directly from a browser if he is able to guess the link. 
            //In this case, returns here for security.
            String message = messageSource.getMessage("gzh.openId.not.found.in.http", null, Locale.SIMPLIFIED_CHINESE);
            model.addAttribute("message", message);
            return "gzh/gzhWeiXinMessage";
        }
        
        //get the task list page.
        Predicate predicate = repoQueryFilter.filterGzhTaskByOpenId(openId);
        QSort sortByLastStatusChangeDesc = repoQueryFilter.sortGzhTaskByLastStatusChangeDesc();
        Iterable<GzhTask> gzhTaskItr = gzhTaskRepository.findAll(predicate, sortByLastStatusChangeDesc);
        List<GzhTask> gzhTaskList = new ArrayList<GzhTask>();
        for (GzhTask gzhTask: gzhTaskItr) {
            gzhTaskList.add(gzhTask);
        }
        
        if (gzhTaskList.size() == 0) {
            String message = messageSource.getMessage("gzh.weixin.task.not.found", null, Locale.SIMPLIFIED_CHINESE);
            model.addAttribute("message", message);
            return "gzh/gzhWeiXinMessage";
        }
        
        //Create a page
        PageImpl<GzhTask> gzhTaskListPage = new PageImpl<GzhTask>(gzhTaskList, pageable, gzhTaskList.size());
        
        model.addAttribute("gzhTaskListPage", gzhTaskListPage);
        model.addAttribute("gzhCompanyId", companyId);
        return ("gzh/gzhWeiXinTaskList");
    }
    
    @GetMapping(value="/gzh/company/{companyId}/task/{taskId}/details")
    public String getGzhWeiXinTaskDetails(HttpSession httpSession, @PathVariable("taskId") long taskId,  Model model) {
        //Now we have the openId in httpSession setup by the preHandle of authorizationInterceptor
        String openId = (String) httpSession.getAttribute("openId");
        if (openId == null) {
            //This should not happen if a user access the link from weixin client.
            //However, someone may access the link directly from a browser if he is able to guess the link. 
            //In this case, returns here for security.
            String message = messageSource.getMessage("gzh.openId.not.found.in.http", null, Locale.SIMPLIFIED_CHINESE);
            model.addAttribute("message", message);
            return "gzh/gzhWeiXinMessage";
        }
        
        //get the task list page.
        GzhTask gzhTask = gzhTaskRepository.findOne(taskId);
        model.addAttribute("gzhTask", gzhTask);
        return ("gzh/gzhWeiXinTaskDetails");
    }

    @PostMapping(value="/gzh/company/{companyId}/task/create")
    public @ResponseBody String postGzhWeiXinTaskCreateApi(HttpSession httpSession, @PathVariable("companyId") long companyId, 
        @RequestBody GzhTask gzhTask, HttpServletResponse response) {
        
        //Now we have the openId in httpSession setup by the preHandle of authorizationInterceptor
        String openId = (String) httpSession.getAttribute("openId");
        if (openId == null) {
            //This should not happen if a user access the link from weixin client.
            //However, someone may access the link directly from a browser if he is able to guess the link. 
            //In this case, returns here for security.
            String message = messageSource.getMessage("gzh.openId.not.found.in.http", null, Locale.SIMPLIFIED_CHINESE);
            response.setStatus(HttpStatus.BAD_REQUEST.value());
            return message;
        }
        
        //Need to make sure gzhUser exists in database. If not, add it right away.
        GzhUser gzhUser = gzhUserService.addGzhUserIfNotExist(openId, companyId);
        
        OffsetDateTime lastStatusChange = OffsetDateTime.now();
        gzhTask.setType(GzhTaskType.CUSTOMER_TASK);
        gzhTask.setLastStatusChange(lastStatusChange);
        gzhTask.setGzhUser(gzhUser);
        gzhTaskRepository.save(gzhTask);
        
        for (GzhTaskStep step: gzhTask.getSteps()) {
            //clear the Id
            step.setId(0);
            step.setGzhTask(gzhTask);
            step.setLastStatusChange(lastStatusChange);
            gzhTaskStepRepository.save(step);
        }
        
        try {
            String gzhTaskJson = objectMapper.writeValueAsString(gzhTask);
            return  gzhTaskJson;
        } catch (JsonProcessingException e) {
            response.setStatus(HttpStatus.BAD_REQUEST.value());
            return e.getLocalizedMessage();
        }
    }
    
    @PostMapping(value="/gzh/company/{companyId}/api/v1/task/update")
    public @ResponseBody String postGzhWeiXinTaskUpdateApi(HttpSession httpSession, @PathVariable("companyId") long companyId, 
        @RequestBody JsonNode json, HttpServletResponse response) {
        
        //Now we have the openId in httpSession setup by the preHandle of authorizationInterceptor
        String openId = (String) httpSession.getAttribute("openId");
        if (openId == null) {
            //This should not happen if a user access the link from weixin client.
            //However, someone may access the link directly from a browser if he is able to guess the link. 
            //In this case, returns here for security.
            String message = messageSource.getMessage("gzh.openId.not.found.in.http", null, Locale.SIMPLIFIED_CHINESE);
            response.setStatus(HttpStatus.BAD_REQUEST.value());
            return message;
        }
        
        long taskId = json.path("taskId").asLong();
        GzhTask gzhTask = gzhTaskRepository.findOne(taskId);
        
        String addedNote = json.path("addedNote").asText();
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        String offsetDateTime = OffsetDateTime.now().format(dateTimeFormatter);
        String gzhUserNickName = gzhTask.getGzhUser().getNickName();
        String addedNoteWithTime = "[".concat(offsetDateTime).concat(" " + gzhUserNickName + "] ").concat(addedNote);
        String newTaskNote = gzhTask.getNote() + "\n" + addedNoteWithTime;
         
        gzhTask.setNote(newTaskNote);
        gzhTaskRepository.save(gzhTask);
        
        return "Success";
    }
    
    //A gzhUser may have more than one gzhClientCompany.
    //companyId is gzhCompanyId. Not used in the function. But it is needed in the url to get the openId in authorizationInterceptor
    //We can not use @PreAuthorize() here since the gzh weixin user does not login in our system.
    //We check openId in httpSession for security protection.
    @GetMapping(value="/gzh/company/{companyId}/clientCompany/list")
    public String getGzhWeiXinClientCompanyList(HttpSession httpSession, @PathVariable("companyId") long companyId, Model model, Pageable pageable) {
        
        //Now we have the openId in httpSession setup by the preHandle of authorizationInterceptor
        String openId = (String) httpSession.getAttribute("openId");
        log.info("getGzhWeiXinClientCompanyList openId=" + openId);
        if (openId == null) {
            //This should not happen if a user access the link from weixin client.
            //However, someone may access the link directly from a browser if he is able to guess the link. 
            //In this case, returns here for security.
            String message = messageSource.getMessage("gzh.openId.not.found.in.http", null, Locale.SIMPLIFIED_CHINESE);
            model.addAttribute("message", message);
            return "gzh/gzhWeiXinMessage";
        }
        
        //filterGzhClientCompanyByOpenId() does not try to find gzhUser by openId. 
        //So we do not need to make sure that the gzhUser exists in the database.
        Predicate predicate = repoQueryFilter.filterGzhClientCompanyByOpenId(openId);
        Page<GzhClientCompany> gzhClientCompanyPage = gzhClientCompanyRepository.findAll(predicate, pageable);
        log.info("getGzhClientCompanyReportList: gzhClientCompanyPage: " + gzhClientCompanyPage.toString());
        
        if (gzhClientCompanyPage.getContent().size() == 0) {
            String message = messageSource.getMessage("gzh.clientcompany.not.bound", null, Locale.SIMPLIFIED_CHINESE);
            model.addAttribute("message", message);
            return "gzh/gzhWeiXinMessage";
        }
        else {
            // A gzhUser has multiple gzhClientCompany
            model.addAttribute("gzhClientCompanyPage", gzhClientCompanyPage);
            return "gzh/gzhWeiXinClientCompanyList";
        }
    }
    
    @GetMapping(value="/gzh/company/{companyId}/clientCompany/{clientCompanyId}/report/list")
    public String getGzhWeiXinClientCompanyReportList(HttpSession httpSession, @PathVariable("clientCompanyId") long clientCompanyId, 
        @RequestParam Map<String, String> params, Model model, Pageable pageable) {
        
        //Now we have the openId in httpSession setup by the preHandle of authorizationInterceptor
        String openId = (String) httpSession.getAttribute("openId");
        if (openId == null) {
            //This should not happen if a user access the link from weixin client.
            //However, someone may access the link directly from a browser if he is able to guess the link. 
            //In this case, returns here for security.
            String message = messageSource.getMessage("gzh.openId.not.found.in.http", null, Locale.SIMPLIFIED_CHINESE);
            model.addAttribute("message", message);
            return "gzh/gzhWeiXinMessage";
        }
        
        log.info("getGzhWeiXinClientCompanyReportList clientCompanyId=", clientCompanyId);
        //get the report list for the specific gzhClientCompany
        Predicate reportPredicate = repoQueryFilter.filterGzhClientCompanyReportByClientCompanyId(Long.valueOf(clientCompanyId));
        Page<GzhClientCompanyReport> gzhClientCompanyReportPage = gzhClientCompanyReportRepository.findAll(reportPredicate, pageable);
        float nrOfPages = gzhClientCompanyReportPage.getTotalPages();
        model.addAttribute("maxPages", nrOfPages);
        GzhClientCompany gzhClientCompany = gzhClientCompanyRepository.findOne(Long.valueOf(clientCompanyId));
        model.addAttribute("gzhClientCompany", gzhClientCompany);
        model.addAttribute("gzhClientCompanyReportPage", gzhClientCompanyReportPage);
        return "gzh/gzhWeiXinClientCompanyReportList";
    }
    
    //query parameters are gzhClientCompanyId=gzhClientCompanyId&period=period&reportType=reportType
    @GetMapping("/gzh/company/{companyId}/report/details")
    public String getGzhWeiXinClientCompanyReportDetails(HttpSession httpSession, @RequestParam Map<String, String> params, Model model) {
        
        //Now we have the openId in httpSession setup by the preHandle of authorizationInterceptor
        String openId = (String) httpSession.getAttribute("openId");
        
        if (openId == null) {
            String message = messageSource.getMessage("gzh.openId.not.found.in.http",
                    null, Locale.SIMPLIFIED_CHINESE);
            model.addAttribute("message", message);
            return "gzh/gzhWeiXinMessage";
        }
        
        String gzhClientCompanyId = params.get("gzhClientCompanyId");
        String period = params.get("period");
        GzhClientCompanyReportType reportType = GzhClientCompanyReportType.fromString(params.get("reportType"));
        GzhClientCompany gzhClientCompany = gzhClientCompanyRepository.findOne(Long.valueOf(gzhClientCompanyId));
        long bookId = gzhClientCompany.getChanjetClientCompany().getBookId();
        log.info("getGzhClientCompanyReportDetails() bookId=" + bookId + ", peroid=" + period + ", reportType=" + reportType); 
        Predicate predicate = repoQueryFilter.filterGzhClientCompanyReportByBookIdAndPeriodAndReportType(bookId, period, reportType);
        GzhClientCompanyReport gzhClientCompanyReport = gzhClientCompanyReportRepository.findOne(predicate);
        model.addAttribute("gzhClientCompanyReport", gzhClientCompanyReport);
        String view="";
        switch (reportType) {
        case ASSET: 
            view = "gzh/gzhWeiXinClientCompanyReportAssetDetails";
            break;
        case INCOME:
            view = "gzh/gzhWeiXinClientCompanyReportIncomeDetails";
            break;
        case CASH:
            view = "gzh/gzhWeiXinClientCompanyReportCashDetails";
            break;
        }
        return view;
    }
}
