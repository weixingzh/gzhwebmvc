package com.cju.gzh;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class WeiXinXmlMsgText {
	private Logger log = LoggerFactory.getLogger(WeiXinXmlMsgText.class);
    
	@Autowired
	private WeiXinXmlMsgAutoReplyService msgAutoReplyService;

    public WeiXinXmlMsgText() {
    }
    
    // We can get the gzhUser from the fromUserName in the XML Msg.
    // We can then get the gzhCompany specific response.
	public WeiXinXmlMsgMapping processXmlMsg(WeiXinXmlMsgMapping xmlMsg) {
		WeiXinXmlMsgMapping responseMsg = new WeiXinXmlMsgMapping();
		String fromUserName = xmlMsg.getFromUserName();
		String toUserName = xmlMsg.getToUserName();
		
		responseMsg.setToUserName(fromUserName);
		responseMsg.setFromUserName(toUserName);
		//The time in seconds since epoch
		responseMsg.setCreateTime(new Date().getTime());
		//Do not set the msgId in the reply msg.
		
		//contentList for TextMsg should contain "Content" and "MsgId"
		Map<String, String> contentMap = new HashMap<String, String>();
		for(WeiXinXmlMsgMappingContent item: xmlMsg.getMsgContent()) {
			contentMap.put(item.getMsgTag(), item.getMsgValue());
		}
		log.info("Content list in receive msg: " + contentMap.toString());
		
		//By default, reply with a text msg back
		responseMsg.setMsgType("text");
		
		//set the text content. Content can not exceed 2048 characters
		String replyStr = msgAutoReplyService.getAutoReplyMsg(fromUserName, toUserName, contentMap.get("Content"));
		
		List<WeiXinXmlMsgMappingContent> msgContent = new ArrayList<WeiXinXmlMsgMappingContent>();
		//WeiXin needs it to be "Content", not "content"
		msgContent.add(new WeiXinXmlMsgMappingContent("Content", replyStr));
		responseMsg.setMsgContent(msgContent);
		return responseMsg;
	}
}