package com.cju.stats;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class TaskTimeController {
    private Logger log = LoggerFactory.getLogger(TaskTimeController.class);
    
    @Autowired 
    private TaskTimeRepository taskTimeRepository; 

    
    @PreAuthorize("isWebAdmin()")
    @GetMapping("/gzhOps/stats/taskTime/list")
    public String getGzhTaskTimeList(Pageable pageable, Model model) {
        Page<TaskTime> taskTimePage = taskTimeRepository.findAll(pageable);
        model.addAttribute("taskTimePage", taskTimePage);
        
        return "stats/taskTimeList";
    }
    
}
