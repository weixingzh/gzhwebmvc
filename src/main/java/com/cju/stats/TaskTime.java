package com.cju.stats;

import java.time.OffsetDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

@Entity
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id", scope = TaskTime.class)
@Table(name = "TaskTime")
public class TaskTime {
    @Id    
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    
    private String taskName;
    private int itemCount;
    @Column(columnDefinition= "TIMESTAMP WITH TIME ZONE")
    private OffsetDateTime startTime;
    @Column(columnDefinition= "TIMESTAMP WITH TIME ZONE")
    private OffsetDateTime endTime;
    private String duration;
    private String note;
    
    public void setId(long id) {
        this.id = id;
    }
    public long getId() {
        return id;
    }

    public void setTaskName(String taskName) {
        this.taskName = taskName;
    }
    public String getTaskName() {
        return taskName;
    }
    
    public void setItemCount(int itemCount) {
        this.itemCount = itemCount;
    }
    public int getItemCount() {
        return itemCount;
    }
    
    public void setStartTime(OffsetDateTime startTime) {
        this.startTime = startTime;
    }
    public OffsetDateTime getStartTime() {
        return startTime;
    }
    
    public void setEndTime(OffsetDateTime endTime) {
        this.endTime = endTime;
    }
    public OffsetDateTime getEndTime() {
        return endTime;
    }
    
    public void setDuration(String duration) {
        this.duration = duration;
    }
    public String getDuration() {
        return duration;
    }
    
    public void setNote(String note) {
        this.note = note;
    }
    public String getNote() {
        return note;
    }
}