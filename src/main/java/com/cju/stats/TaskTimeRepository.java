package com.cju.stats;

import java.util.List;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;
import com.querydsl.core.types.Predicate;

@Repository
public interface TaskTimeRepository 
    extends PagingAndSortingRepository<TaskTime, Long>, QueryDslPredicateExecutor<TaskTime> {
    List<TaskTime> findAll(Predicate predicate);
    TaskTime findById(long id);
}