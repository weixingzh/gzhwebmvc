package com.cju.util;

public class ResultCode {
	public static enum CODE {
		OK,
		ERR_DB_ENTRY_EXISTS,
		ERR
	};
	private CODE code;
	private String message = "Default Message";
	
	public ResultCode() {}
	public ResultCode(CODE code) {
		this.code = code;
	}
	public ResultCode(CODE code, String message) {
		this.code = code;
		this.message = message;
	}
	public CODE getCode() {
		return code;
	}
	
	public void setCode(CODE code) {
		this.code = code;
	}
	
	public String getMessage() {
		return message;
	}
	
	public void setMessage(String errorMessage) {
		this.message = errorMessage;
	}
}

