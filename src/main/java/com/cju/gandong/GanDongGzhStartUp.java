package com.cju.gandong;

import java.util.Locale;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Profile;
import org.springframework.core.annotation.Order;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;
import com.cju.billing.Account;
import com.cju.billing.AccountRepository;
import com.cju.gzhOps.GzhCompany;
import com.cju.gzhOps.GzhCompanyBillingStatus;
import com.cju.gzhOps.GzhCompanyMenuType;
import com.cju.gzhOps.GzhCompanyPaymentStatus;
import com.cju.gzhOps.GzhCompanyRepository;
import com.cju.gzhOps.GzhCompanyServiceRunningStatus;
import com.cju.gzhOps.GzhCompanyServiceType;
import com.cju.marketing.ReferalService;
import com.cju.user.User;
import com.cju.user.UserRepository;

//Order(1) WebMvcTestDatabaseInit.java
//Order(3) GzhStartUp.java

//Only create this in prod environment. 
//Do not create this in dev environment as we may accidently start the gzh which will cause our real gandongtech Gzh to fail.
@Profile("prod-aws-BeiJing")
@Order(5)
@Component
public class GanDongGzhStartUp implements ApplicationRunner {
    private Logger log = LoggerFactory.getLogger(GanDongGzhStartUp.class);
            
    @Value("${gzh.weixin.gandong.appId}")
    private String ganDongAppId;
    
    @Value("${gzh.weixin.gandong.appSecret}")
    private String ganDongAppSecret;
    
    @Value("${gzh.weixin.gandong.gzhId}")
    private String ganDongGzhId;
    
    @Autowired
    UserRepository userRepository;
    
    @Autowired
    ReferalService referalService;
    
    @Autowired
    GzhCompanyRepository gzhCompanyRepository;
    
    @Autowired
    AccountRepository accountRepository;
    
    @Autowired
    private MessageSource messageSource;
    
    @Override
    public void run(ApplicationArguments applicationArguments) throws Exception {
        BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
        String pw123 = passwordEncoder.encode("123");
        
        //create gandonggzh if not exists
        User gandonggzh = userRepository.findOneByUserName("gandonggzh");
        if (gandonggzh == null) {
            gandonggzh = new User();
            gandonggzh.setUserName("gandonggzh");
            gandonggzh.setPassword(pw123);
            gandonggzh.setEmail("gzh@gandongtech.com");
            gandonggzh.setRoles("ROLE_USER");
            gandonggzh.setEnabled(true);
            userRepository.save(gandonggzh);
            referalService.createReferalForUser(gandonggzh, 0);
        }
        
        //create GzhCompany 
        String  ganDongTech = messageSource.getMessage("gzh.gandongtech.name", null, Locale.SIMPLIFIED_CHINESE);
        GzhCompany gzhCompany = gzhCompanyRepository.findOneByGzhId(ganDongGzhId);
        if (gzhCompany == null) {
            gzhCompany = new GzhCompany(ganDongTech, ganDongAppId, ganDongAppSecret, ganDongGzhId);
            
            gzhCompany.setServiceType(GzhCompanyServiceType.GDTECH);
            gzhCompany.setPaymentStatus(GzhCompanyPaymentStatus.NO_DEBT);
            gzhCompany.setBillingStatus(GzhCompanyBillingStatus.TRIAL_NOT_STARTED);
            gzhCompany.setServiceRunningStatus(GzhCompanyServiceRunningStatus.NOT_STARTED);
            gzhCompany.setGzhMenuType(GzhCompanyMenuType.SYSTEM_DEFAULT);
            gzhCompany.setOwnerUser(gandonggzh);
            //CASCADE for GzhCompany is set to None. Which means when company is saved, user will not be saved again.
            gzhCompanyRepository.save(gzhCompany);
            
            //create an Account for the GzhCompany
            Account account = new Account();
            account.setInternalAccountOwnerName(gandonggzh.getUserName());
            account.setGzhCompany(gzhCompany);
            accountRepository.save(account);
            
            gzhCompany.setAccount(account);
            gzhCompanyRepository.save(gzhCompany);
            
            gandonggzh.setOwnedGzhCompany(gzhCompany);
            gandonggzh.setAdminedGzhCompany(gzhCompany);
            userRepository.save(gandonggzh);
        }
    }
}
