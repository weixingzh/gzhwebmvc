package com.cju;

import java.util.Map;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.SchedulingConfigurer;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;
import org.springframework.scheduling.config.ScheduledTaskRegistrar;
import org.springframework.scheduling.support.CronTrigger;

//@Configuration classes themselves are instantiated and managed as individual Spring beans.

//Optional interface to be implemented by @Configuration classes annotated with @EnableScheduling. 
//Typically used for setting a specific TaskScheduler bean to be used when executing scheduled tasks or for registering scheduled tasks in a programmatic fashion 
//as opposed to the declarative approach of using the @Scheduled annotation. 
//This may be necessary when implementing Trigger-based tasks, which are not supported by the @Scheduled annotation.

@Configuration
@EnableScheduling
public class WebMvcTaskSchedulingConfig implements SchedulingConfigurer {

    @Autowired
    private ThreadPoolTaskScheduler threadPoolTaskScheduler;
    
    @Autowired
    private WebMvcTask webMvcTask;
    
    //@Scheduled(cron=". . .")
    //    The pattern is a list of six single space-separated fields: representing second, minute, hour, day, month, weekday.
    //    "0 0 * * * *" = the top of every hour of every day.
    //    "*/10 * * * * *" = every ten seconds.
    //    "0 0 8-10 * * *" = 8, 9 and 10 o'clock of every day.
    //    "0 0 6,19 * * *" = 6:00 AM and 7:00 PM every day.
    //    "0 0/30 8-10 * * *" = 8:00, 8:30, 9:00, 9:30, 10:00 and 10:30 every day.
    //    "0 0 9-17 * * MON-FRI" = on the hour nine-to-five weekdays
    //    "0 0 0 25 12 ?" = every Christmas Day at midnight
    
    //Billing Schedule
    //At 0:30 in the morning every day of a month, check to update the billingStatus from TRIAL to ACTIVE.
    //At 2:00 in the morning day 1 of a month, generate bills for all GzhCompany that has started trial.
    //At 2:00 in the morning day 11 of month, check to update IN_DEBT payment status
    //At 2:00 in the morning day 21 of month, check to update OVER_DEBT payment status
    
    //Import chanjet report schedule
    //At 2:00 in the morning day 5, 15, 25 of a month, import chanjet report of last month
    
    //Referal fee schedule
    //At 1:30 in the morning every day of a month, check to update the referalFee Accumulating Status from NOT_STARTED to ACCUMULATING.
    //At 2:00 in the morning day 2 of a month, generate accumulated referal fee.
    //At 3:00 in the morning day 2 of a month, update referal fee accounting completed status
    
    @Override
    public void configureTasks(ScheduledTaskRegistrar taskRegistrar) {
        taskRegistrar.setScheduler(threadPoolTaskScheduler);
        
        //Billing related shceduled tasks
        //0:30 of every day
        taskRegistrar.addTriggerTask(
            new Runnable() {
                public void run() {
                    webMvcTask.updateBillingStatusFromTrialToActive();
                }
            },
            new CronTrigger("0 30 0 * * *")
        );
        
        //2:00 of day 1 of every month
        taskRegistrar.addTriggerTask(
            new Runnable() {
                public void run() {
                    webMvcTask.generateBillForAllGzhCompany();
                }
            },
            new CronTrigger("0 0 2 * * *")
        );
        
        //2:00 of day 11 of every month
        taskRegistrar.addTriggerTask(
            new Runnable() {
                public void run() {
                    webMvcTask.updateGzhCompanyInDebt();
                }
            },
            new CronTrigger("0 0 2 11 * *")
        );
        
        //2:00 of day 21 of every month
        taskRegistrar.addTriggerTask(
            new Runnable() {
                public void run() {
                    webMvcTask.updateGzhCompanyOverDebt();
                }
            },
            new CronTrigger("0 0 2 21 * *")
        );
        
        //Referal fee scheduled tasks
        //1:30 of every day
        taskRegistrar.addTriggerTask(
            new Runnable() {
                public void run() {
                    webMvcTask.updateReferalFeeStatus();
                }
            },
            new CronTrigger("0 30 1 * * *")
        );
        
        //2:00 of day 2 of every month
        taskRegistrar.addTriggerTask(
            new Runnable() {
                public void run() {
                    webMvcTask.generateAccumulatedReferalFee();
                }
            },
            new CronTrigger("0 0 2 2 * *")
        );
        
        //3:00 of day 2 of every month
        taskRegistrar.addTriggerTask(
            new Runnable() {
                public void run() {
                    webMvcTask.updateReferalFeeAccountingCompletedStatus();
                }
            },
            new CronTrigger("0 0 3 2 * *")
        );
        
        //GzhClienCompanyReport related scheduled tasks
        //2:00 of day 16, 20 of every month
        taskRegistrar.addTriggerTask(
            new Runnable() {
                public void run() {
                    webMvcTask.setupTasksToUpdateGzhClientComanyCurrentPeriod(webMvcTask);
                }
            },
            new CronTrigger("0 0 2 16,20 * *")
        );
        //3:00 of day 16 of every month
        taskRegistrar.addTriggerTask(
            new Runnable() {
                public void run() {
                    webMvcTask.setupTasksToSyncGzhClientComanyReport(webMvcTask);
                }
            },
            new CronTrigger("0 0 3 16,20 * *")
        );
    }
    
    //We use the ThreadPoolTaskScheduler created in WebMvcBeansConfig.java as the scheduler and executor. 
    //Internally, it delegates to a ScheduledExecutorService instance. 
    //ThreadPoolTaskScheduler actually implements Spring’s TaskExecutor interface as well, 
    
    //An ExecutorService that can schedule commands to run after a given delay, or to execute periodically.
    //ScheduledExecutorService is an interface (a contract) and ScheduledThreadPoolExecutor implements that interface.
    //    @Bean(destroyMethod="shutdown")
    //    public ScheduledExecutorService scheduledExecutorService() {
    //        //corePoolSize = 10, the number of threads to keep in the pool, even if they are idle
    //        return Executors.newScheduledThreadPool(10);
    //    }
}
