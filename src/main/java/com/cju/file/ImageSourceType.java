package com.cju.file;

import com.fasterxml.jackson.annotation.JsonProperty;

public enum ImageSourceType {
    @JsonProperty("网站自有") SERVER_PROVIDED("ServerProvided"), 
    @JsonProperty("用户上传") USER_UPLOADED("UserUpLoaded"),
    @JsonProperty("匿名上传") ANONYMOUS_UPLOADED("AnonymousUpLoaded"),
    @JsonProperty("未知来源") UNKNOWN("Unknown");
    private String imageSourceType;
    private ImageSourceType(String imageSourceType) {
        this.imageSourceType = imageSourceType;
    }
    
    public static ImageSourceType fromString(String str) {
        for (ImageSourceType type: ImageSourceType.values()) {
            if (type.imageSourceType.equals(str)) {
                return type;
            }
        }
        //Default return
        return SERVER_PROVIDED;
    }
    
    //used in gzhHtmlPageDetails.html
    public String getValue() {
        return imageSourceType;
    }
    
    @Override
    public String toString() {
        switch (imageSourceType) {
            case "ServerProvided": 
                return "网站自有";
            case "UserUpLoaded": 
                return "用户上传";
            case "AnonymousUpLoaded": 
                return "用户上传";
            default: 
                return "未知来源";
        }
        
    }
}
