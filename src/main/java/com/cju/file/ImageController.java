package com.cju.file;

import java.io.IOException;
import java.net.URISyntaxException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import com.cju.gzhOps.GzhCompany;
import com.cju.security.RepositoryQueryFilter;
import com.cju.user.User;

@Controller
public class ImageController {
    private Logger log = LoggerFactory.getLogger(ImageController.class);
    
    @Autowired
    private ImageRepository imageRepository;
    
    @Autowired
    private ImageService imageService;
    
    @Autowired 
    RepositoryQueryFilter repoQueryFilter;
    
    @PreAuthorize("isAuthenticated()")
    @GetMapping("/gzhOps/image/list")
    public String getImageList(Pageable pageable, Model model) {
        Page<Image> imagePage = imageRepository.findAll(pageable);
        
        model.addAttribute("imagePage", imagePage);
        return "image/imageList";
    }
    
    //REST API
    @GetMapping("/pub/image/digest/{digest}")
    public ResponseEntity<byte[]> getImageByDigest(@PathVariable("digest") String digest, Model model) throws URISyntaxException, IOException {  
        Image img = imageRepository.findOneByDigest(digest);
        byte[] imageData = imageService.getImage(img);
        
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.IMAGE_JPEG);
        ResponseEntity<byte[]> responseEntity = new ResponseEntity<>(imageData, headers, HttpStatus.OK);
        return responseEntity;
    }
    
    @GetMapping("/pub/image/thumbnail/{id}")
    public ResponseEntity<byte[]> getImageThumbNail(@PathVariable("id") long id, Model model) {  
        Image img = imageRepository.findOne(id);
        
        byte[] thumbNailData = imageService.getImageThumbNail(img);
        
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.IMAGE_JPEG);
        ResponseEntity<byte[]> responseEntity = new ResponseEntity<>(thumbNailData, headers, HttpStatus.OK);
        return responseEntity;
    }

    //Used in GzhHtmlPageEmptyEditor to upload images.
    @PreAuthorize("isAuthenticated()")
    @PostMapping("/gzhOps/api/v1/image/upload")
    public @ResponseBody String postImageUploadApi(@AuthenticationPrincipal User user, @RequestParam("file") MultipartFile file) throws IOException  {
        log.info("postImageUploadApi file=" + file);
        
        String fileName = file.getOriginalFilename();
        byte[] inImageData = file.getBytes();
        GzhCompany gzhCompany = user.getAdminedGzhCompany();
        Image returnedImg = imageService.saveImage(fileName, inImageData, ImageSourceType.USER_UPLOADED, gzhCompany);
        String imgUrl = "/pub/image/digest/" +  returnedImg.getDigest();
        
        return imgUrl;
    }
 }
