package com.cju.file;

import java.io.IOException;
import java.net.URISyntaxException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@Controller
public class QrImageController {
    private Logger log = LoggerFactory.getLogger(QrImageController.class);
    
    @Autowired
    private QrImageRepository qrImageRepository;
    
    @PreAuthorize("isWebAdmin()")
    @GetMapping("/gzhOps/qrImage/list")
    public String getQrImageList(Pageable pageable, Model model) {
        Page<QrImage> qrImagePage = qrImageRepository.findAll(pageable);
        
        model.addAttribute("qrImagePage", qrImagePage);
        return "image/qrImageList";
    }
    
    //REST API
    @PreAuthorize("isWebAdmin() || isGzhAdminForGzhCompanyRechargeOrderQrImage(#id) || isReferalOwnerForReferalVerifyOrderQrImage(#id) || "
                + "isReferalOwnerForReferalLinkQrImage(#id)")
    @GetMapping("/gzhOps/qrImage/{id}")
    public ResponseEntity<byte[]> getQrImage(@PathVariable("id") long id, Model model) throws URISyntaxException, IOException {  
        QrImage img = qrImageRepository.findOne(id);
        byte[] data = new byte[0];
        
        data = img.getData();
        
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.IMAGE_JPEG);
        ResponseEntity<byte[]> responseEntity = new ResponseEntity<>(data, headers, HttpStatus.OK);
        return responseEntity;
    }
 }
