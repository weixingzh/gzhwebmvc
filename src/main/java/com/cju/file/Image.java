package com.cju.file;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.cju.gzhOps.GzhCompany;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

@Entity
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id", scope = Image.class)
@Table(name = "Image")
public class Image {
    @Transient
    final int FILE_SIZE = 2*1024 * 1024;
    
    @Transient
    private Logger log = LoggerFactory.getLogger(Image.class);

    //AUTO strategy uses the global number generator to generate a primary key for every new entity object
    //IDENTIFY strategy uses a separate identity generator per type hierarchy, so generated values are unique only per type hierarchy.
    //SEQUENCE strategy generates an automatic value as soon as a new entity object is persisted (i.e. before commit)
    //@SequenceGenerator(name="seq", initialValue=1, allocationSize=100)
    //@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="seq")
    @Id    
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    
    //There is a DB entry for Server_Provided images. 
    //The DB keeps all the data for Server_Provided images except for the data file.
    //But the data files for Server_Provided images are stored in static/pub/images/
    @Enumerated(EnumType.STRING)
    private ImageSourceType imageSourceType;
    private String fileName;
    private long size;
    private String digest;
    
    //PostgreSQL provides two distinct ways to store binary data. 
    //Binary data can be stored in a table using the data type bytea 
    //or by using the Large Object feature which stores the binary data in a separate table in a special format 
    //and refers to that table by storing a value of type oid in your table.

    //The bytea data type is not well suited for storing very large amounts of binary data. 
    //While a column of type bytea can hold up to 1 GB of binary data, it would require a huge amount of memory to process such a large value. 
    //The Large Object method for storing binary data is better suited to storing very large values, but it has its own limitations. 
    //Specifically deleting a row that contains a Large Object reference does not delete the Large Object. 
    //Deleting the Large Object is a separate operation that needs to be performed. 
    //Large Objects also have some security issues since anyone connected to the database can view and/or modify any Large Object, 
    //even if they don't have permissions to view/update the row containing the Large Object reference.
    //@Lob will make postgreSql to use Large Object
    //Without @Lob, bytea will be used to store binary data.
    //    @Basic(fetch = FetchType.LAZY)
    //    @Column(length=FILE_SIZE)
    //    private byte[] data;
    
    //To lazy fetch entity attributes, you can either use bytecode enhancement or subentities. 
    //Although bytecode instrumentation allows you to use only one entity per table, subentities are more flexible and can even deliver better performance 
    //since they don’t involve an interceptor call whenever reading an entity attribute.
    //To use bytecode build time enhancement, org.hibernate:hibernate-gradle-plugin is needed in the buildscripts block in build.gradle.
    //The bytecode build time enhancement happens during commnad line gradlew build, but not within Eclipse.
    //Moreover, all the entity class are instrumented and will incur performance over head.
    //Therefore, bytecode instrumentation is removed and not used.
    //    @Basic(fetch = FetchType.LAZY)
    //    @Column(length=FILE_SIZE)
    //    private byte[] thumbNailData;
    
    @JsonIgnore
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="gzhCompanyId", foreignKey = @ForeignKey(name = "Image_FK_GzhCompanyId"))
    private GzhCompany gzhCompany;
    
    public Image() {}
    public Image(long id) {
        this.id = id;
    }

    public void setId(long id) {
        this.id = id;
    }
    public long getId() {
        return id;
    }

    public void setImageSourceType(ImageSourceType imageSourceType) {
        this.imageSourceType = imageSourceType;
    }
    public ImageSourceType getImageSourceType() {
        return imageSourceType;
    }
    
    public void setFileName(String fileName) {
        this.fileName = fileName;
    }
    public String getFileName() {
        return fileName;
    }
    public void setSize(long size) {
        this.size = size;
    }
    public long getSize() {
        return size;
    }
    
    public void setDigest(String digest) {
        this.digest = digest;
    }
    public String getDigest() {
        return digest;
    }
    
    public void setGzhCompany(GzhCompany gzhCompany) {
        this.gzhCompany = gzhCompany;
    }
    public GzhCompany getGzhCompany() {
        return gzhCompany;
    }
}
