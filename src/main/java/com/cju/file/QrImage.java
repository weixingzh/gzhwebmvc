package com.cju.file;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.cju.gzhOps.GzhCompany;
import com.cju.gzhOps.GzhHtmlPageType;
import com.cju.home.Feedback;
import com.cju.order.Order;
import com.cju.user.User;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

@Entity
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id", scope = QrImage.class)
@Table(name = "QrImage")
public class QrImage {
    @Transient
    private Logger log = LoggerFactory.getLogger(QrImage.class);

    //AUTO strategy uses the global number generator to generate a primary key for every new entity object
    //IDENTIFY strategy uses a separate identity generator per type hierarchy, so generated values are unique only per type hierarchy.
    //SEQUENCE strategy generates an automatic value as soon as a new entity object is persisted (i.e. before commit)
    //@SequenceGenerator(name="seq", initialValue=1, allocationSize=100)
    //@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="seq")
    @Id    
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
       
    //PostgreSQL provides two distinct ways to store binary data. 
    //Binary data can be stored in a table using the data type bytea 
    //or by using the Large Object feature which stores the binary data in a separate table in a special format 
    //and refers to that table by storing a value of type oid in your table.

    //The bytea data type is not well suited for storing very large amounts of binary data. 
    //While a column of type bytea can hold up to 1 GB of binary data, it would require a huge amount of memory to process such a large value. 
    //The Large Object method for storing binary data is better suited to storing very large values, but it has its own limitations. 
    //Specifically deleting a row that contains a Large Object reference does not delete the Large Object. 
    //Deleting the Large Object is a separate operation that needs to be performed. 
    //Large Objects also have some security issues since anyone connected to the database can view and/or modify any Large Object, 
    //even if they don't have permissions to view/update the row containing the Large Object reference.
    //@Lob will make postgreSql to use Large Object
    //Without @Lob, bytea will be used to store binary data.
    private byte[] data;
    
    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name="orderId", foreignKey = @ForeignKey(name = "QrImage_FK_OrderId"))
    private Order order;
    
    public QrImage() {}
    public QrImage(long id) {
        this.id = id;
    }

    public void setId(long id) {
        this.id = id;
    }
    public long getId() {
        return id;
    }
   
    public void setData(byte[] data) {
        this.data = data;
    }
    public byte[] getData() {
        return data;
    }
    
    public void setOrder(Order order) {
        this.order = order;
    }
    public Order getOrder() {
        return order;
    }
}
