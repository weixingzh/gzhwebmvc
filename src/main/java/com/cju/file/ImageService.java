package com.cju.file;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Iterator;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.imageio.ImageIO;
import javax.imageio.ImageReader;
import javax.imageio.stream.ImageInputStream;
import javax.xml.bind.DatatypeConverter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import com.cju.aws.S3Service;
import com.cju.gzhOps.GzhCompany;
import com.cju.security.RepositoryQueryFilter;
import com.querydsl.core.types.Predicate;

@Service
public class ImageService {
    private Logger log = LoggerFactory.getLogger(ImageService.class);
    
    @Value("${aws.s3.bucket.name}")
    private String s3BucketName;
    
    @Autowired 
    private ImageRepository imageRepository; 
    
    @Autowired 
    RepositoryQueryFilter repoQueryFilter;
    
    @Autowired
    private S3Service s3Service;
    
    private MessageDigest md5;
    
    @PostConstruct
    public void init() throws NoSuchAlgorithmException {
        md5 = MessageDigest.getInstance("MD5");
    }
    
    private byte[] getThumbNailImage(byte[] imageInByte) throws IOException {
        byte[] imageOutByte = new byte[0] ;
        
        InputStream is = new ByteArrayInputStream(imageInByte);
        //BufferedImage bImage = ImageIO.read(is);
        //java.awt.Image javaImage = bImage.getScaledInstance(100, 100, BufferedImage.SCALE_SMOOTH);
 
        ImageInputStream iis = ImageIO.createImageInputStream(is);
        Iterator<ImageReader> iter = ImageIO.getImageReaders(iis);
        if (iter.hasNext()) {
            ImageReader reader = (ImageReader)iter.next();
            reader.setInput(iis);
            String formatName = reader.getFormatName();
            
            //iis may have multiple images. In our case, only one image.
            BufferedImage bImage = reader.read(0);
            java.awt.Image javaImage = bImage.getScaledInstance(100, 100, BufferedImage.SCALE_SMOOTH);
            BufferedImage thumbNailImage = new BufferedImage(100, 100, BufferedImage.TYPE_INT_RGB);
            
            Graphics2D g = thumbNailImage.createGraphics();
            g.setColor(Color.WHITE);
            g.fillRect(0, 0, thumbNailImage.getWidth(), thumbNailImage.getHeight());
            thumbNailImage.createGraphics().drawImage(javaImage, 0, 0, null);
            g.dispose();
            
            // convert BufferedImage to byte array
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            ImageIO.write(thumbNailImage, formatName, baos);
            baos.flush();
            imageOutByte = baos.toByteArray();
            baos.close();
        }
        
        return imageOutByte;
    }

    public Image saveImage(String fileName, byte[] inImageData, ImageSourceType imageSourceType, GzhCompany gzhCompany) throws IOException {
        
        byte[] digestByte = md5.digest(inImageData);
        String digest = DatatypeConverter.printHexBinary(digestByte);
        
        Predicate predicate;
        log.info("saveImage(), image digest=" + digest);
        if (gzhCompany == null) {
            predicate = repoQueryFilter.filterImageByNullGzhCompanyAndDigest(digest);
        }
        else {
            predicate = repoQueryFilter.filterImageByGzhCompanyIdAndDigest(gzhCompany.getId(), digest);
        }
        //In concurrent race condition, there may be multiple same images uploaded.
        List<Image> currImageList = imageRepository.findAll(predicate);
        if (currImageList.size() > 0) {
            //The same image is already in DB
            return currImageList.get(0);
        }
        
        Image img = new Image();
        img.setImageSourceType(imageSourceType);
        img.setGzhCompany(gzhCompany);
        img.setFileName(fileName);
        long size = inImageData.length;
        img.setSize(size);
        img.setDigest(digest);
        
        String key = "image/unknown/" + digest;
        String thumbNailkey = "image/thumbNail/unknown/" + digest;
        if (imageSourceType == ImageSourceType.SERVER_PROVIDED) {
            key = "image/public/" + digest;
            thumbNailkey = "image/public/thumbNail/" + digest;
        }
        else if (imageSourceType == ImageSourceType.USER_UPLOADED) {
            key = "image/gzhCompany/" + gzhCompany.getId() + "/" + digest;
            thumbNailkey = "image/gzhCompany/" + gzhCompany.getId() + "/thumbNail/" + digest;
        }
        else {
            key = "image/anonymous/" + digest;
            thumbNailkey = "image/anonymous/thumbNail/" + digest;
        }
        byte[] thumbNailData = getThumbNailImage(inImageData);
        s3Service.putFile(s3BucketName, thumbNailkey, thumbNailData, fileName, size);
        s3Service.putFile(s3BucketName, key, inImageData, fileName, size);
        
        Image returnedImg = imageRepository.save(img);
        return returnedImg;
    }
    
    public byte[] getImage(Image img) {
        String digest = img.getDigest();
        ImageSourceType imageSourceType = img.getImageSourceType();
        
        String key = "image/unknown/" + digest;
        if (imageSourceType == ImageSourceType.SERVER_PROVIDED) {
            key = "image/public/" + digest;
        }
        else if (imageSourceType == ImageSourceType.USER_UPLOADED) {
            GzhCompany gzhCompany = img.getGzhCompany();
            key = "image/gzhCompany/" + gzhCompany.getId() + "/" + digest;
        }
        else {
            key = "image/anonymous/" + digest;
        }
        
        byte[] imageData = s3Service.getFile(s3BucketName, key);
        return imageData;
    }
    
    public byte[] getImageThumbNail(Image img) {
        String digest = img.getDigest();
        ImageSourceType imageSourceType = img.getImageSourceType();
        
        String thumbNailkey = "image/thumbNail/unknown/" + digest;
        if (imageSourceType == ImageSourceType.SERVER_PROVIDED) {
            thumbNailkey = "image/public/thumbNail/" + digest;
        }
        else if (imageSourceType == ImageSourceType.USER_UPLOADED) {
            GzhCompany gzhCompany = img.getGzhCompany();
            thumbNailkey = "image/gzhCompany/" + gzhCompany.getId() + "/thumbNail/" + digest;
        }
        else {
            thumbNailkey = "image/anonymous/thumbNail/" + digest;
        }
        
        byte[] imageData = s3Service.getFile(s3BucketName, thumbNailkey);
        return imageData;
    }
    
    public void removeServerProvidedImageEntries() {
        Predicate predicate = repoQueryFilter.filterImageBySourceType(ImageSourceType.SERVER_PROVIDED);
        Iterable<Image> imageList = imageRepository.findAll(predicate);
        for (Image image: imageList) {
            log.info("clearGzhImages deleting image=" + image);
            //Only remove DB entry for server provided images. Does not remove the image files in /static/pub/images folder.
            imageRepository.delete(image);
        }
    }
}
