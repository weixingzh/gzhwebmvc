package com.cju.user;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.ProviderManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import com.cju.gzhOps.GzhCompany;
import com.cju.util.ResultCode;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Controller
public class UserController {
    private Logger log = LoggerFactory.getLogger(UserController.class);
    
    @Autowired
    private UserRepository userRepository;
    
    @Autowired
    private UserService userService;
    
    @Autowired
    UserDetailsServiceImpl userDetailsServiceImpl;
    
    @Autowired
    private MessageSource messageSource;
    
    @Autowired
    private ObjectMapper objectMapper;

    @GetMapping(value={"/user","/user/"})
    public String user(Model model) {
         return "home/index";
    }
    
    //Since we configure all the urls /user/** to be protected by authentication in WebMvcSecurityConfig, 
    //and /register, /login, /logout, /logoutSuccess need to open to everyone, these URLs can not be put under /user/.
    
    //POST for login is configured in WebMvcSecurityConfig.java and handled by Spring Security
    //Get /login is sent by WebMvcSecurityAuthEntryPoint invoked by Spring Security exceptionHandling when 
    //a user tries to access a URL requires authentication. The redirectUrl parameter contains the original URL
    //the user tried to access.
    @GetMapping(value = "/pub/user/login")
    public String login(@RequestParam Map<String, String> params, Model model) {
        
        String loginError = params.get("error");
        String errMsg = "";
        if (loginError != null) {
            //This is request forwarded by AuthenticationFailureHandler in WebMvcSecurityConfig.java when login fails
            switch(loginError) {
            case "usernameNotFound": 
                errMsg = messageSource.getMessage("gzh.user.login.usernameNotFound", null, Locale.SIMPLIFIED_CHINESE);
                break;
            case "badCredentials": 
                errMsg = messageSource.getMessage("gzh.user.login.badCredentials", null, Locale.SIMPLIFIED_CHINESE);
                break;
            case "disabled": 
                errMsg = messageSource.getMessage("gzh.user.login.disabled", null, Locale.SIMPLIFIED_CHINESE);
                break;
            }
            model.addAttribute("alertLoginFailure", true);
            model.addAttribute("errMsg", errMsg);
        }
        
    	String redirectUrl = params.get("redirectUrl");
    	if (redirectUrl == null)
    		redirectUrl = "/gzhOps";
  	    model.addAttribute("redirectUrl", redirectUrl);
        return "user/userLogin";
    }
    
    //For an Ajax request to a URL that requires authentication, determineUrlToUseForThisRequest in WebMvcSecurityAuthEntryPoint
    //will direct the request to this URL. Since the user initiates an Ajax request, we will send back a response without a view.
    @GetMapping(value = "/pub/user/ajaxLogin")
    public @ResponseBody String ajaxLogin(HttpServletResponse response, Model model) {
        response.setStatus(HttpStatus.UNAUTHORIZED.value());
        String message = messageSource.getMessage("gzh.user.login.required", null, Locale.SIMPLIFIED_CHINESE);
        return message;
    }
    
    //request mapping for logout url is not defined in this controller. Spring Security provides a default one.
    //Here we define request mapping for logout success url.
    @GetMapping(value = "/pub/user/logoutSuccess")
    public String logoutSuccess(Model model) {
        String success = messageSource.getMessage("gzh.user.logout.success", null, Locale.SIMPLIFIED_CHINESE);
        model.addAttribute("message", success);
        return "gzhOps/gzhOpsMessage";
    }
    
    @GetMapping(value = "/pub/user/register")
    public String getUserRegisterForm() {
        return "user/userRegister";
    }
     
    // BindingResult must follow @Valid parameter
    // when you declare a controller like: 
    //    public String post(@Valid FormData dataVariable, BindingResult result, Model model){ 
    // Spring MVC automatically computes the name of the form-backing bean model variable from its class name 
    // --with the first letter in lower case--, not its argument name (method argument names are lost in runtime, in Java). 
    // This is, Spring MVC will automatically provide "formData", and not "dataVariable". 
    // Thus, in thymeleaf, th:object should be set to "${formData}", not "${dataVariable}"
    // Spring MVC will bind the BindingResult object containing the validation errors, which #fields.hasErrors(...) in Thymeleaf will query. 
    // Thymeleaf will execute a #fields.hasErrors(...) inside a <form> tag. This is the same as ${formData#fields.hasErrors(...)} if 
    // th:object is set to ${formData} 
    @PostMapping(value = "/pub/user/register")
    public String submitUserRegisterForm(@Valid User user, BindingResult result, 
            @CookieValue(value="referalNum", defaultValue="0") int referalNumCookie, Model model) throws Exception {
    	//Return the Locale associated with the current thread if any, or the system default Locale else(English)   
    	//log.info("submitUserRegisterForm Locale = " + LocaleContextHolder.getLocale().getDisplayName());
    	
        if (result.hasErrors()) {
        	Map<String, String> formErrorMap = new HashMap<String, String>();
        	result.getFieldErrors()
        	      .forEach(err -> formErrorMap.put(err.getField(), err.getDefaultMessage()));
            String json = objectMapper.writeValueAsString(formErrorMap);
            model.addAttribute("errorMessage", json);
        	log.info("submitted register form has errors: " + json);
            return "user/userRegister";
        }
        
        ResultCode regResult = userService.register(user, referalNumCookie);
        if (regResult.getCode() == ResultCode.CODE.OK) {
            model.addAttribute("message", regResult.getMessage());
            return "gzhOps/gzhOpsMessage";
        } 
        else {
            log.error("Error: " + regResult.getMessage());
            model.addAttribute("errorMessage", regResult.getMessage());
            return "user/userRegister";
        }
    } 
    
    //query string: activation=activationToken
    @GetMapping(value = "/pub/user/activate")
    public String clickUserActivationLink(@RequestParam Map<String, String> params, Model model) {
        String email=params.get("email");
        String activationToken = params.get("activation");
        ResultCode result = userService.registerActivate(email, activationToken);
        model.addAttribute("message", result.getMessage());
        return "gzhOps/gzhOpsMessage";
    }
    
    @GetMapping(value = "/pub/user/resetPasswordCollectEmail")
    public String getUserResetPasswordCollectEmailForm() {
        return "user/userResetPasswordCollectEmail";
    }
    
    //Form Data: email=email
    @PostMapping(value = "/pub/user/resetPasswordCollectEmail")
    public String submitUserResetPasswordCollectEmailForm(@RequestParam String email, Model model) throws Exception {
        ResultCode result = userService.resetPasswordSendActivationEmail(email);
        model.addAttribute("message", result.getMessage());
        return "gzhOps/gzhOpsMessage";
    }
    
    //Query String: email=email&resetToken=token
    @GetMapping(value = "/pub/user/resetPassword")
    public String getUserResetPasswordActivationForm(@RequestParam Map<String, String> params, Model model) {
        String email = params.get("email");
        model.addAttribute("email", email);
        String resetToken = params.get("resetToken");
        model.addAttribute("resetToken", resetToken);
        return "user/userResetPassword";
    }
    
    //Form Data: email=email&resetToken=token&password=password
    @PostMapping(value = "/pub/user/resetPassword")
    public String submitUserResetPasswordActivationForm(@RequestParam Map<String, String> params, Model model) throws Exception {
        String email = params.get("email");
        String resetToken = params.get("resetToken");
        String password = params.get("password");
        ResultCode result = userService.resetPasswordActivate(email, resetToken, password);
        model.addAttribute("message", result.getMessage());
        return "gzhOps/gzhOpsMessage";
    }
    
    @PreAuthorize("isAuthenticated()")
    @GetMapping("/user/myProfile")
    public String getMyProfile(@AuthenticationPrincipal User user,  Model model) {  
        model.addAttribute("user", user);
        return "user/userDetails";
    }

    @PreAuthorize("isWebAdmin()")
    @GetMapping("/user/list")
    public String getUserList(Pageable pageable, Model model) {	
		Page<User> users = userRepository.findAll(pageable);
		model.addAttribute("users", users);
        //float nrOfPages = users.getTotalPages();
        //model.addAttribute("maxPages", nrOfPages);
        log.info("getGzhUserList users.size=" + users.getSize() + ", users.Number " + users.getNumber());
        return "user/userList";
    }
    
    //A normal user can only view his own user details
    @PreAuthorize("isSelf(#id) || isWebAdmin()")
    @GetMapping("/user/details/{id}")
    public String getUserDetails(@PathVariable("id") long id, Model model) {	
		User user = userRepository.findOne(id);
		model.addAttribute("user", user);
        log.info("getUserDetails" + user.toString());
        return "user/userDetails";
    }
	
	@PreAuthorize("isSelf(#id) || isWebAdmin()")
    @GetMapping("/user/edit/{id}")
    public String getUserEditForm(@PathVariable("id") long id, Model model) {	
		User user = userRepository.findOne(id);
		model.addAttribute("user", user);
        log.info("getUserEditForm" + user.toString());
        return "user/userEdit";
	}
    
	@PreAuthorize("isSelf(#id) || isWebAdmin()")
    @PostMapping("/user/edit/{id}")
    public String submitUserEditForm(@ModelAttribute User user, @PathVariable("id") long id, Model model) {
    	log.info("submitUserEditForm user=" + user.toString());
        
    	//WebAdmin can be added as temporary admin for a GzhCompany in memory. 
    	//When WebAdmin is saved to DB, make sure the temporary adminedGzhCompany is not saved to DB.
    	GzhCompany tempAdminedGzhCompany = null;
    	if (user.isWebAdmin()) {
    	    tempAdminedGzhCompany = user.getAdminedGzhCompany();
    	    //clear the temporary adminedGzhComany
    	    user.setAdminedGzhCompany(null);
    	}
    	
    	User returnedUser = userService.updateUser(user);
    	
    	//Restore the temporary adminedGzhCompany
    	if (user.isWebAdmin()) {
    	    user.setAdminedGzhCompany(tempAdminedGzhCompany);
    	}
    	
    	//If there is an exception, the code will not reach here.
		model.addAttribute("user", returnedUser);
		model.addAttribute("alertSaveSuccess", true);
        return "user/userDetails";
    }
	

	@PreAuthorize("isWebAdmin()")
	@GetMapping("/user/edit/role/{id}")
	public String getUserEditRoleForm(@PathVariable("id") long id, Model model) {	
	    User user = userRepository.findOne(id);
	    model.addAttribute("user", user);
	    return "user/userEditRole";
	}
	
    @PreAuthorize("isAuthenticated()")
    @GetMapping("/user/bindChanjet")
    public String getUserBindChanjetForm(@AuthenticationPrincipal User user, Model model) {   
        model.addAttribute("user", user);
        log.info("getUserBindChanjetForm" + user.toString());
        return "user/userBindChanjet";
    }
    
    //REST APIs
    @PreAuthorize("isAuthenticated()")
    @GetMapping("/gzhOps/api/v1/user/validate")
    public @ResponseBody String postValidateUserApi(@RequestParam Map<String, String> params) throws JsonProcessingException {
        String userName = params.get("userName");
        String email = params.get("email");
        User user = userRepository.findOneByUserNameAndEmail(userName, email);
        String userJson = "{}";
        if (user != null) {
            userJson = objectMapper.writeValueAsString(user);
        }

        log.info("postValidateUserApi userJson=" + userJson);
        return userJson;
    }
    
    @PreAuthorize("isWebAdmin()")
    @PostMapping("/gzhOps/api/v1/user/{id}/loginAs")
    public @ResponseBody  String postUserLoginAsApi(@AuthenticationPrincipal User user, @PathVariable("id") long id, Model model) {
        
        User loginAsUser = userRepository.findOne(id);
        String userName = loginAsUser.getUserName();
        String password = loginAsUser.getPassword();
        UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(userName, password);
        
        //Spring Security’s web infrastructure is based entirely on standard servlet filters. 
        //Filter Ordering:
        //ChannelProcessingFilter
        //SecurityContextPersistenceFilter
        //ConcurrentSessionFilter
        //UsernamePasswordAuthenticationFilter
        //SecurityContextHolderAwareRequestFilter
        //JaasApiIntegrationFilter
        //RememberMeAuthenticationFilter
        //AnonymousAuthenticationFilter
        //ExceptionTranslationFilter,  to catch any Spring Security exceptions so that 
        //    either an HTTP error response can be returned or an appropriate AuthenticationEntryPoint can be launched
        //FilterSecurityInterceptor
        
        //The AuthenticationManager is just an interface, The default implementation in Spring Security is called ProviderManager.
        //ProviderManager delegates to a list of configured AuthenticationProvider.
        //The most common approach to verifying an authentication request is to load the corresponding UserDetails and check the loaded password 
        //against the one that has been entered by the user. This is the approach used by the DaoAuthenticationProvider. 
        //The loaded UserDetails object - and particularly the GrantedAuthority it contains - will be used when building 
        //the fully populated Authentication object which is returned from a successful authentication and stored in the SecurityContext.
        
        //We go through the authentication process to get the authentication obj and put it in SecurityContextHolder
        DaoAuthenticationProvider daoAuthenticationProvider = new DaoAuthenticationProvider();
        daoAuthenticationProvider.setUserDetailsService(userDetailsServiceImpl);
        List<AuthenticationProvider> authProviderList = new ArrayList<AuthenticationProvider>();
        authProviderList.add(daoAuthenticationProvider);
        ProviderManager providerManager = new ProviderManager(authProviderList);
        Authentication authentication = providerManager.authenticate(token);
        
        //SecurityContextHolder is ThreadLocal object that hold SecurityContext.
        SecurityContextHolder.getContext().setAuthentication(authentication);
        
        user = loginAsUser;
        
        String message = "Login as " + userName + " success.";
        return message;
    }
    
    @PreAuthorize("isWebAdmin()")
    @PostMapping("/gzhOps/api/v1/user/{id}/editRole")
    public @ResponseBody  String postUserRolesEditApi(@AuthenticationPrincipal User user, @PathVariable("id") long id, @RequestBody JsonNode json) throws JsonProcessingException {
        
        User editUser = userRepository.findOne(id);
        
        String rolesAdd = json.path("rolesAdd").asText();
        Set<String> rolesAddSet = new HashSet<String>();
        if (rolesAdd != null && !rolesAdd.isEmpty()) {
              rolesAdd = rolesAdd.replaceAll("\\s", ",");
              rolesAddSet = new HashSet<String>(Arrays.asList(rolesAdd.split(",")));
        }
        
        String rolesRemove = json.path("rolesRemove").asText();
        Set<String> rolesRemoveSet = new HashSet<String>();
        if (rolesRemove != null && !rolesRemove.isEmpty()) {
            rolesRemove = rolesRemove.replaceAll("\\s", ",");
            rolesRemoveSet = new HashSet<String>(Arrays.asList(rolesRemove.split(",")));
        }
        
        if (!user.hasRole("ROLE_SUPERUSER")) {
            if (rolesAddSet.contains("ROLE_SUPERUSER") || rolesRemoveSet.contains("ROLE_SUPERUSER")) {
                String message = messageSource.getMessage("gzh.user.no.permission.to.edit.superuser.role", null, Locale.SIMPLIFIED_CHINESE);
                return message;
            }
            if (rolesAddSet.contains("ROLE_WEBADMIN") || rolesRemoveSet.contains("ROLE_WEBADMIN")) {
                String message = messageSource.getMessage("gzh.user.no.permission.to.edit.webadmin.role", null, Locale.SIMPLIFIED_CHINESE);
                return message;
            }
        }
        
        String roles = editUser.getRoles();
        roles = roles.replaceAll("\\s", ",");
        Set<String> rolesSet = new HashSet<String>(Arrays.asList(roles.split(",")));
        rolesSet.addAll(rolesAddSet);
        rolesSet.removeAll(rolesRemoveSet);
        
        roles = String.join(",", rolesSet);
        editUser.setRoles(roles);
        userRepository.save(editUser);
        
        String editUserJson = objectMapper.writeValueAsString(editUser);
        return editUserJson;
    }
}