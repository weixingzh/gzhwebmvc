package com.cju.user;

import java.util.List;
import javax.servlet.http.HttpSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

//This UserService is used by DaoAuthenticationProvider during authentication process to retrieve password, role for a user name.
//We should not use this class directly.

//During authentication process, UserRepository is accessed to retrieve user from database.
//For controller methods that need PreAuthorization, WebMvcSecurityMethodSecurityExpressionRoot needs to access the User again.
//For controller methods that need filtering based on User, RepositoryQueryFilter needs to access the User again.
//By implementing the UserDetails interface in User and returns the User in loadUserByUsername, we save two database access in one user operation.

@Component
public class UserDetailsServiceImpl implements UserDetailsService {
	private Logger log = LoggerFactory.getLogger(UserDetailsServiceImpl.class);
    
    @Autowired
    private UserRepository repo;
    
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        log.info("loadUserByUsername() username=" + username);
        User user = repo.findOneByUserName(username);
        
        if(user == null) {
            throw new UsernameNotFoundException(username);
        }

        return user;
    }
}