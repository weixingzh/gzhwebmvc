package com.cju.user;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.security.authentication.encoding.Md5PasswordEncoder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.time.OffsetDateTime;
import java.util.Locale;
import com.cju.mail.MailService;
import com.cju.marketing.Referal;
import com.cju.marketing.ReferalRepository;
import com.cju.marketing.ReferalService;
import com.cju.util.ResultCode;

@Service
public class UserService {
	private Logger log = LoggerFactory.getLogger(UserService.class);
	
    @Value("${app.user.activation}")
    private Boolean requireActivation;
    
    @Autowired
    private UserRepository userRepository;
    
    @Autowired
    private ReferalService referalService;
   
    @Autowired
    private MailService mailService;
    
    @Autowired
    private MessageSource messageSource;
    
    private String bCryptEncode(String password) {
        BCryptPasswordEncoder bCryptEncoder = new BCryptPasswordEncoder();
        return bCryptEncoder.encode(password);
    }
    
    private String md5Encode(String inputStr, String tokenSecret) {
        Md5PasswordEncoder md5Encoder = new Md5PasswordEncoder();
        String activationToken = md5Encoder.encodePassword(inputStr, tokenSecret);
        return activationToken;
    }
    
    public ResultCode register(User user, int parentReferalNum) throws Exception {
        //check to see if username or email exist
        if (user.getUserName().toLowerCase().equals("superuser") || 
            user.getUserName().toLowerCase().equals("super") ||
            user.getUserName().toLowerCase().equals("webadmin") ||
            user.getUserName().toLowerCase().equals("admin") ||
            user.getUserName().toLowerCase().equals("support") ||
            user.getUserName().toLowerCase().equals("test")) {
            String failure = messageSource.getMessage("gzh.user.register.username.reserved", 
                new Object[] {user.getUserName()}, Locale.SIMPLIFIED_CHINESE);
            return new ResultCode(ResultCode.CODE.ERR_DB_ENTRY_EXISTS, failure);
        }
        else if (userRepository.findOneByUserName(user.getUserName()) != null) {
            String failure = messageSource.getMessage("gzh.user.register.username.exist", 
                new Object[] {user.getUserName()}, Locale.SIMPLIFIED_CHINESE);
            return new ResultCode(ResultCode.CODE.ERR_DB_ENTRY_EXISTS, failure);
        }
        else if (userRepository.findOneByEmail(user.getEmail()) != null) {
            String failure = messageSource.getMessage("gzh.user.register.email.exist", 
                new Object[] {user.getEmail()}, Locale.SIMPLIFIED_CHINESE);
            return new ResultCode(ResultCode.CODE.ERR_DB_ENTRY_EXISTS, failure);
        }
        else {
            user.setPassword(bCryptEncode(user.getPassword()));
            if (requireActivation) {
                String tokenSecret = OffsetDateTime.now().toString();
                String activationToken = md5Encode(user.getUsername(), tokenSecret);
                user.setActivationToken(activationToken);
                //set enabled when user activates account
                mailService.sendNewRegistration(user, activationToken);
            }
            else {
                //Does not require email activation. Set account enabled now.
                user.setEnabled(true);
            }
            
            userRepository.save(user);
            
            referalService.createReferalForUser(user, parentReferalNum);
            
            String success = messageSource.getMessage("gzh.user.register.success", null, Locale.SIMPLIFIED_CHINESE);
            return new ResultCode(ResultCode.CODE.OK, success);
        }
    }
    
    public ResultCode registerActivate(String email, String token) {
        User u = userRepository.findOneByEmail(email);
        if (u != null && u.getEnabled() == true) {
            String alreadyActivated = messageSource.getMessage("gzh.user.register.activation.alreadyActivated", null, Locale.SIMPLIFIED_CHINESE);
            return new ResultCode(ResultCode.CODE.OK, alreadyActivated);
        }
        if(u == null || token.equals("") || !u.getActivationToken().equals(token)) {
            log.info("registerActivate, u= " + u + ", email= " + email + ", token=" + token);
            String failure = messageSource.getMessage("gzh.user.register.activation.failure", null, Locale.SIMPLIFIED_CHINESE);
            return new ResultCode(ResultCode.CODE.ERR, failure);
        }
        
        u.setActivationToken("");
        u.setEnabled(true);
        userRepository.save(u);
        String success = messageSource.getMessage("gzh.user.register.activation.success", null, Locale.SIMPLIFIED_CHINESE);
        return new ResultCode(ResultCode.CODE.OK, success);
    }
    
    public ResultCode resetPasswordSendActivationEmail(String email) throws Exception {
        User u = userRepository.findOneByEmail(email);
        if(u == null) {
            log.info("resetPasswordSendEmail, u= " + u + ", email= " + email);
            String failure = messageSource.getMessage("gzh.user.resetpassword.email.notfound", 
                    new Object[] {email}, Locale.SIMPLIFIED_CHINESE);
            return new ResultCode(ResultCode.CODE.ERR, failure);
        }
        
        String tokenSecret = OffsetDateTime.now().toString();
        String resetToken = md5Encode(email, tokenSecret);
        //send out resetpassword email before save to database. 
        //If there is an exception in sending out email, the resetToken will not be saved to database.
        mailService.sendResetPassword(u, resetToken);
        u.setActivationToken(resetToken);
        userRepository.save(u);
        String success = messageSource.getMessage("gzh.user.resetpassword.email.sent",
            new Object[] {email}, Locale.SIMPLIFIED_CHINESE);
        return new ResultCode(ResultCode.CODE.OK, success);
    }
    
    public ResultCode resetPasswordActivate(String email, String token, String password) throws Exception {
        User u = userRepository.findOneByEmail(email);
        if(u == null || token.equals("") || !u.getActivationToken().equals(token)) {
            log.info("resetPasswordActivate, u= " + u + ", email= " + email + ", token=" + token);
            String failure = messageSource.getMessage("gzh.user.resetpassword.failure",
                null, Locale.SIMPLIFIED_CHINESE);
            return new ResultCode(ResultCode.CODE.ERR, failure);
        }
        
        //clear activationToken
        u.setActivationToken("");
        //setEnabled to true.
        //resetPassword can be used to activate account in case the registration activation email is lost.
        u.setEnabled(true);
        u.setPassword(bCryptEncode(password));
        userRepository.save(u);
        String success = messageSource.getMessage("gzh.user.resetpassword.success",
                null, Locale.SIMPLIFIED_CHINESE);
        return new ResultCode(ResultCode.CODE.OK, success);
    }

    public User updateUser(User user) {
        User currUser = userRepository.findOne(user.getId());
        if (user.getPassword() != null) {
            currUser.setPassword(bCryptEncode(user.getPassword()));
        }
        currUser.setEmail(user.getEmail());
        User returnUser = this.userRepository.save(currUser);
        return returnUser;
    }
}