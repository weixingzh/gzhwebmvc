package com.cju.user;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.UniqueConstraint;
import javax.persistence.ForeignKey;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import org.hibernate.validator.constraints.Email;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.UserDetails;
import com.cju.chanjet.ChanjetUser;
import com.cju.gzhOps.GzhCompany;
import com.cju.marketing.Referal;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

@Entity
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id", scope = User.class)
@Table(name = "users", 
       uniqueConstraints = {@UniqueConstraint(columnNames = "userName", name = "User_UNIQUE_UserName"),
                            @UniqueConstraint(columnNames = "email", name = "User_UNIQUE_Email")}
      )
// PostgreSQL does not allow to create a table name user. user is a keyword in postgresql.
public class User implements UserDetails{
    //UserDetails extends Serializable
    private static final long serialVersionUID = 2943215978650834257L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    
    @NotNull
    @Size(min = 3, max = 100, message = "Username must at least 3 characters.")
    private String userName;
    
    @NotNull
    @Size(min = 3, max = 100, message = "Password must at least 3 characters.")
    private String password;
    
    //@Transient tells JPA provider (Hibernate) not to persist the field
    //    @Transient
    //    private String confirmPassword;
    
    @Email(message = "Email address is not valid.")
    @NotNull
    private String email;
    private String activationToken;
    //TBD. We may need to add anonymous role later.
    //Comma separated roles.
    private String roles = "ROLE_USER";
    private String name;
    private String address;
    private String lastLogin;
    private Boolean enabled = false;
    
    //The GzhCompany this user is an admin.
    //adminedGzhCompanyId is the foreign key in the user table in database
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name="adminedGzhCompanyId", foreignKey = @ForeignKey(name = "User_FK_AdminedGzhCompanyId"))
    private GzhCompany adminedGzhCompany;
    
    //The gzhcompany created by this user
    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name="ownedGzhCompanyId", foreignKey = @ForeignKey(name = "User_FK_OwnedGzhCompanyId"))
    private GzhCompany ownedGzhCompany;
    
    //The chanjetUser for this user
    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name="chanjetUserId", foreignKey = @ForeignKey(name = "User_FK_ChanjetUserId"))
    private ChanjetUser chanjetUser;
    
    //The Referal related marketing data for this User
    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name="referalId", foreignKey = @ForeignKey(name = "User_FK_ReferalId"))
    private Referal referal;
    
    public User() {
    }
    
    public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }

    public String getUserName() {
        return userName;
    }
    public void setUserName(String name) {
        this.userName = name;
    }

    public String getPassword() {
        return password;
    }
    public void setPassword(String password) {
        this.password = password;
    }

    //    public String getConfirmPassword() {
    //        return confirmPassword;
    //    }
    //
    //    public void setConfirmPassword(String confirmPassword) {
    //        this.confirmPassword = confirmPassword;
    //    }
    //    public Boolean isMatchingPasswords() {
    //        return this.password.equals(this.confirmPassword);
    //    }
    
    public String getActivationToken() {
        return activationToken;
    }
    public void setActivationToken(String activationToken) {
        this.activationToken = activationToken;
    }

    public String getEmail() {
        return email;
    }
    public void setEmail(String email) {
        this.email = email;
    }

    public boolean hasRole(String role) {
        Set<String> roleSet = new HashSet<String>(Arrays.asList(roles.split(",")));
        return roleSet.contains(role);
    }
    
    public String getRoles() {
        return roles;
    }
    public void setRoles(String roles) {
        String rolesWithoutWs = roles.replaceAll("\\s", ",");
        //Use Set rather than List to remove any duplicates
        Set<String> cleanRoleSet = new HashSet<String>(Arrays.asList(rolesWithoutWs.split(",")));
        this.roles = String.join(",", cleanRoleSet);
    }
    
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }
    public void setAddress(String address) {
        this.address = address;
    }

    public String getLastLogin() {
        return lastLogin;
    }
    public void setLastLogin(String lastLogin) {
        this.lastLogin = lastLogin;
    }
    
    public GzhCompany getAdminedGzhCompany() {
        return adminedGzhCompany;
    }
    public void setAdminedGzhCompany(GzhCompany gzhCompany) {
        this.adminedGzhCompany = gzhCompany;
    }
    
    public GzhCompany getOwnedGzhCompany() {
        return ownedGzhCompany;
    }
    public void setOwnedGzhCompany(GzhCompany ownedGzhCompany) {
        this.ownedGzhCompany = ownedGzhCompany;
    }
    
    //Get either adminedGzhCompany or ownedGzhCompany
    //This is only used in masterSidemenuLayout.html.
    public GzhCompany getGzhCompany() {
        if (adminedGzhCompany != null)
            return adminedGzhCompany;
        else
            return ownedGzhCompany;
    }
    
    //WEBADMIN is GanDong admin who manage the whole web.
    public Boolean isWebAdmin() {
        return roles.contains("ROLE_WEBADMIN");
    }
    
    //chanjet related fields
    public ChanjetUser getChanjetUser() {
        return chanjetUser;
    }
    public void setChanjetUser(ChanjetUser chanjetUser) {
        this.chanjetUser = chanjetUser;
    }
    
    public Referal getReferal() {
        return referal;
    }
    public void setReferal(Referal referal) {
        this.referal = referal;
    }
    
    public Boolean getEnabled() {
        return enabled;
    }
    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }
    
    @Override
    public String toString() {
        return String.format(
                "User[id=%s, userName='%s', roles='%s', adminedGzhCompany='%s', chanjetUser='%s']",
                id, userName, roles, 
                getAdminedGzhCompany() != null ? getAdminedGzhCompany().toString() : "No AdminedGzhCompany",
                getChanjetUser() != null ? getChanjetUser().toString() : "No ChanjetUser");
    }

    //Implements UserDetails interface
    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        String roles = String.join(",", this.roles);
        List<GrantedAuthority> auth = AuthorityUtils.commaSeparatedStringToAuthorityList(roles);
        return auth;
    }
    @Override
    public String getUsername() {
        return userName;
    }
    
    //String getPassword() { ... } is defined above
    
    @Override
    public boolean isAccountNonExpired() {
        return true;
    }
    @Override
    public boolean isAccountNonLocked() {
        return true;
    }
    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }
    @Override
    public boolean isEnabled() {
        return enabled;
    }
}