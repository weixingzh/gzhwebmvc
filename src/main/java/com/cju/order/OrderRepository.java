package com.cju.order;

import org.springframework.data.querydsl.QueryDslPredicateExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface OrderRepository extends PagingAndSortingRepository<Order, Long>, QueryDslPredicateExecutor<Order> {

    Order findOneByOrderNum(int orderNum);

}