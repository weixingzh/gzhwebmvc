package com.cju.order;

import java.io.IOException;
import java.time.OffsetDateTime;
import javax.servlet.http.HttpServletResponse;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactoryConfigurationError;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import com.cju.gzh.WeiXinService;
import com.cju.gzhOps.GzhCompany;
import com.cju.gzhOps.GzhCompanyRepository;
import com.cju.security.RepositoryQueryFilter;
import com.cju.user.User;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.google.zxing.WriterException;
import com.querydsl.core.types.Predicate;

@Controller
public class OrderController {
    private Logger log = LoggerFactory.getLogger(OrderController.class);
    
    final private double MIN_PAY_ORDER_AMOUNT = 0.01;
    final private int ACCOUNT_RECHARGE_PRODUCT_ID = 888888;
    
    @Autowired
    private OrderService orderService;
    
    @Autowired 
    private GzhCompanyRepository gzhCompanyRepository; 
    
    @Autowired 
    private OrderRepository orderRepository; 
    
    @Autowired 
    RepositoryQueryFilter repoQueryFilter;
    
    @Autowired 
    WeiXinService weiXinService;
    
    @Autowired
    private ObjectMapper objectMapper;
    
    @PreAuthorize("isAuthenticated()")
    @GetMapping("/gzhOps/rechargeOrder/list")
    public String getGzhCompanyRechargeOrderList(@AuthenticationPrincipal User user, Pageable pageable, Model model) { 
        Predicate predicate = repoQueryFilter.filterRechargeOrderByUser(user);
        Page<Order> orderPage = orderRepository.findAll(predicate, pageable);
        
        OffsetDateTime now = OffsetDateTime.now();
        //Only allow the status query if the order is in PENDING state and qrCode expired.
        orderPage.forEach(order -> {
            if (order.getPayStatus() == OrderPayStatus.PENDING && order.getQrCodeExpireTime().isBefore(now))
                order.setShowPayStatusQuery(true);
            else
                order.setShowPayStatusQuery(false);
        });
        
        model.addAttribute("orderPage", orderPage);
        model.addAttribute("showRechargeOrder", true);
        log.info("getGzhCompanyOrderList orderPage.size=" + orderPage.getSize() + ", orderPage.Number " + orderPage.getNumber());
        
        return "order/orderList";
    }
    
    //An order can be a GzhCompany Recharge Order or a Referal Verify Order
    @PreAuthorize("isWebAdmin() || isGzhAdminForGzhCompanyRechargeOrder(#id) || isReferalOwnerForReferalVerifyOrder(#id)")
    @GetMapping("/gzhOps/order/{id}/details")
    public String getOrderDetails(@PathVariable("id") long id, Model model) { 
        Order order = orderRepository.findOne(id);
        model.addAttribute("order", order);
        
        if (order.getReferal() != null) {
            model.addAttribute("showReferalOrder", true);
        }
        if (order.getGzhCompany() != null) {
            model.addAttribute("showRechargeOrder", true);
        }
        
        if (order.getQrCodeExpireTime() != null) {
            long epochSeconds = order.getQrCodeExpireTime().toEpochSecond();
            model.addAttribute("epochSeconds", epochSeconds);
        }
        else {
            model.addAttribute("epochSeconds", 0);
        }
        return "order/orderDetails";
    }
    
    @PreAuthorize("isAuthenticated()")
    @GetMapping("/gzhOps/gzhCompany/{id}/rechargeOrder/generate")
    public String getGzhCompanyOrderGenerateForm(@PathVariable("id") long id, Model model) {  
        GzhCompany gzhCompany = gzhCompanyRepository.findOne(id);
        model.addAttribute("gzhCompany", gzhCompany);
        model.addAttribute("minPayOrderAmount", MIN_PAY_ORDER_AMOUNT);
        
        return "order/orderGenerate";
    }
    
    //REST API
    //When a web form is submitted to a server through an HTTP POST request, 
    //a web user that attempts to refresh the server response in certain user agents can cause 
    //the contents of the original HTTP POST request to be resubmitted, 
    //possibly causing undesired results, such as a duplicate web purchase.
    //So we use Ajax to post to web server
    @PreAuthorize("isGzhAdmin(#id)")
    @PostMapping("/gzhOps/api/v1/gzhCompany/{id}/rechargeOrder/generate")
    public @ResponseBody String postGzhCompanyRechargeOrderGenerateApi(@AuthenticationPrincipal User user, @PathVariable("id") long id, 
        @RequestBody JsonNode json, HttpServletResponse response) throws IOException, TransformerFactoryConfigurationError, TransformerException, WriterException  {
        
        GzhCompany gzhCompany = gzhCompanyRepository.findOne(id);
        
        String productBrief = json.path("productBrief").asText();
        int productId = ACCOUNT_RECHARGE_PRODUCT_ID;
        double amount = Double.parseDouble(json.path("amount").asText());
        String note = json.path("note").asText();
        
        ObjectNode jNode = orderService.generateWeiXinPayOrderForGzhCompanyRecharge(gzhCompany, productBrief, productId, amount, user.getUserName(), note);
        
        //return_code from weixin server can be either SUCCESS or FAIL
        String retCode = jNode.get("return_code").asText();
        if (!retCode.equals("SUCCESS")) {
            //need to set the http status code to failure
            response.setStatus(HttpStatus.BAD_REQUEST.value());
        }
        
        String jNodeJson = objectMapper.writeValueAsString(jNode);
        return jNodeJson;
    }
    
    @PreAuthorize("isWebAdmin()")
    @PostMapping("/gzhOps/api/v1/order/{id}/payStatusConfirm")
    public @ResponseBody String postOrderPayStatusConfirmApi(@AuthenticationPrincipal User user, @PathVariable("id") long id) throws JsonProcessingException {
        
        Order order = orderRepository.findOne(id);
        
        //This is a manual confirm process which requires the web admin to manually login to weixin pay platform to check and confirm the transaction.
        //There is not weixin API called.
        order.setPayStatusConfirmTime(OffsetDateTime.now());
        order.setPayStatusConfirmer(user.getUserName());
        orderRepository.save(order);
        
        String orderJson = objectMapper.writeValueAsString(order);
        return orderJson;
    }
    
    @PreAuthorize("isWebAdmin()")
    @PostMapping("/gzhOps/api/v1/order/{id}/payStatusQuery")
    public @ResponseBody String postOrderPayStatusQueryApi(@AuthenticationPrincipal User user, @PathVariable("id") long id) throws JsonProcessingException {
        Order order = orderRepository.findOne(id);
        ObjectNode jNode;
        jNode = orderService.queryOrderPayStatus(order);
        
        String jNodeJson = objectMapper.writeValueAsString(jNode);
        return jNodeJson;
    }
}