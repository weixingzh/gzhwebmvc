package com.cju.order;

import com.fasterxml.jackson.annotation.JsonProperty;

public enum OrderPayStatus {
    @JsonProperty("等待支付") PENDING("Pending"),
    @JsonProperty("支付成功") SUCCESS("Success"),
    @JsonProperty("微信－未支付") WEIXIN_FAIL_NOTPAY("WeiXinFailNotPay"),
    @JsonProperty("微信－已关闭") WEIXIN_FAIL_CLOSED("WeiXinFailClosed"),
    @JsonProperty("微信－支付失败") WEIXIN_FAIL_PAYERROR("WeiXinFailPayError"),
    @JsonProperty("支付状态未知") UNKNOWN_PAY_STATUS("UnknowPayStatus");
    private String payStatus;
    
    private OrderPayStatus(String payStatus) {
        this.payStatus = payStatus;
    }
    
    public static OrderPayStatus fromString(String statusStr) {
        switch(statusStr) {
            case "Pending":
                return OrderPayStatus.PENDING;
            case "Success":
                return OrderPayStatus.SUCCESS;
            case "WeiXinFailNotPay":
                return OrderPayStatus.WEIXIN_FAIL_NOTPAY;
            case "NOTPAY":
                return OrderPayStatus.WEIXIN_FAIL_NOTPAY;
            case "WeiXinFailClosed":
                return OrderPayStatus.WEIXIN_FAIL_CLOSED;
            case "CLOSED":
                return OrderPayStatus.WEIXIN_FAIL_CLOSED;
            case "WeiXinFailPayError":
                return OrderPayStatus.WEIXIN_FAIL_PAYERROR;
            case "PAYERROR":
                return OrderPayStatus.WEIXIN_FAIL_PAYERROR;
        }

        //Default return
        return UNKNOWN_PAY_STATUS;
    }
    
    @Override
    public String toString() {
        switch (payStatus) {
            case "Pending": 
                return "等待支付";
            case "Success": 
                return "支付成功";
            case "WeiXinFailNotPay": 
                return "微信－未支付";
            case "WeiXinFailClosed": 
                return "微信－已关闭";
            case "WeiXinFailPayError": 
                return "微信－支付失败";
            default: 
                return "支付状态未知";
        }
    }
}
