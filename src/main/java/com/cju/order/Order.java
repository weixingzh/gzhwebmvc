package com.cju.order;

import java.time.OffsetDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.persistence.UniqueConstraint;
import com.cju.file.QrImage;
import com.cju.gzhOps.GzhCompany;
import com.cju.marketing.Referal;
import com.cju.user.User;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

//Order is reserved keyword in postgresql and can not be used as table name
@Entity
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id", scope = Order.class)
@Table(name = "Orders",
       uniqueConstraints = {
                               @UniqueConstraint(columnNames={"orderNum"}, name="Order_UNIQUE_OrderNum"),
                               @UniqueConstraint(columnNames = "payRefNum", name = "Order_UNIQUE_PayRefNum")
                           }
       )
public class Order {
    @Id    
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    //UUID is too long for orderNum. 
    private int orderNum;
    private String productBrief;
    private int productId;
    @Column(columnDefinition= "TIMESTAMP WITH TIME ZONE")
    private OffsetDateTime submitTime;
    private double amount;
    private String note;
    private String submitter;
    @Enumerated(EnumType.STRING) 
    private OrderPayStatus payStatus;
    @Column(columnDefinition= "TIMESTAMP WITH TIME ZONE")
    private OffsetDateTime payTime;
    @Enumerated(EnumType.STRING) 
    private OrderPayType payType;
    private String payRefNum;
    private String payerOpenId;
    //code_url used to generate the QR Code img
    private String qrCodeUrl;
    @Column(columnDefinition= "TIMESTAMP WITH TIME ZONE")
    private OffsetDateTime qrCodeExpireTime;
    @Column(columnDefinition= "TIMESTAMP WITH TIME ZONE")
    private OffsetDateTime payStatusConfirmTime;
    private String payStatusConfirmer;
    @Transient
    private boolean showPayStatusQuery = false;
    
    @JsonIgnore
    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name="qrImageId", foreignKey = @ForeignKey(name = "Order_FK_QrImageId"))
    private QrImage qrImage;

    //A order is generated either for a referal verification or for recharging gzhCompany, not both.
    @JsonIgnore
    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name="referalId", foreignKey = @ForeignKey(name = "Order_FK_ReferalId"))
    private Referal referal;
    
    @JsonIgnore
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="gzhCompanyId", foreignKey = @ForeignKey(name = "Order_FK_GzhCompanyId"))
    private GzhCompany gzhCompany;
    
    public void setId(long id) {
        this.id = id;
    }
    public long getId() {
        return id;
    }
    
    public void setOrderNum(int orderNum) {
        this.orderNum = orderNum;
    }
    public int getOrderNum() {
        return orderNum;
    }
    
    public void setProductBrief(String productBrief) {
        this.productBrief = productBrief;
    }
    public String getProductBrief() {
        return productBrief;
    }
    
    public void setProductId(int productId) {
        this.productId = productId;
    }
    public int getProductId() {
        return productId;
    }
    
    public void setSubmitTime(OffsetDateTime submitTime) {
        this.submitTime = submitTime;
    }
    public OffsetDateTime getSubmitTime() {
        return submitTime;
    }
    
    public void setAmount(double amount) {
        this.amount = amount;
    }
    public double getAmount() {
        return amount;
    }
    
    public void setNote(String note) {
        this.note = note;
    }
    public String getNote() {
        return note;
    }
    
    public void setSubmitter(String submitter) {
        this.submitter = submitter;
    }
    public String getSubmitter() {
        return submitter;
    }
    
    public void setPayStatus(OrderPayStatus payStatus) {
        this.payStatus = payStatus;
    }
    public OrderPayStatus getPayStatus() {
        return payStatus;
    }
    
    public void setPayTime(OffsetDateTime payTime) {
        this.payTime = payTime;
    }
    public OffsetDateTime getPayTime() {
        return payTime;
    }
    
    public void setPayType(OrderPayType payType) {
        this.payType = payType;
    }
    public OrderPayType getPayType() {
        return payType;
    }
    
    public void setPayRefNum(String payRefNum) {
        this.payRefNum = payRefNum;
    }
    public String getPayRefNum() {
        return payRefNum;
    }
    
    public void setPayerOpenId(String payerOpenId) {
        this.payerOpenId = payerOpenId;
    }
    public String getPayerOpenId() {
        return payerOpenId;
    }
    
    public void setQrCodeUrl(String qrCodeUrl) {
        this.qrCodeUrl = qrCodeUrl;
    }
    public String getQrCodeUrl() {
        return qrCodeUrl;
    }
    
    public void setQrCodeExpireTime(OffsetDateTime qrCodeExpireTime) {
        this.qrCodeExpireTime = qrCodeExpireTime;
    }
    public OffsetDateTime getQrCodeExpireTime() {
        return qrCodeExpireTime;
    }
    
    public void setPayStatusConfirmTime(OffsetDateTime payStatusConfirmTime) {
        this.payStatusConfirmTime = payStatusConfirmTime;
    }
    public OffsetDateTime getPayStatusConfirmTime() {
        return payStatusConfirmTime;
    }
    
    public void setPayStatusConfirmer(String payStatusConfirmer) {
        this.payStatusConfirmer = payStatusConfirmer;
    }
    public String getPayStatusConfirmer() {
        return payStatusConfirmer;
    }
    
    public void setShowPayStatusQuery(boolean showPayStatusQuery) {
        this.showPayStatusQuery = showPayStatusQuery;
    }
    public boolean getShowPayStatusQuery() {
        return showPayStatusQuery;
    }
    
    public void setQrImage(QrImage qrImage) {
        this.qrImage = qrImage;
    }
    public QrImage getQrImage() {
        return qrImage;
    }
    
    public void setReferal(Referal referal) {
        this.referal = referal;
    }
    public Referal getReferal() {
        return referal;
    }
    
    public void setGzhCompany(GzhCompany gzhCompany) {
        this.gzhCompany = gzhCompany;
    }
    public GzhCompany getGzhCompany() {
        return gzhCompany;
    }
    
    @Override
    public String toString() {
        return String.format(
                "Order[id=%s, orderNum='%d', amount='%f', payStatys='%s']",
                id, orderNum, amount, payStatus);
    }
}           
