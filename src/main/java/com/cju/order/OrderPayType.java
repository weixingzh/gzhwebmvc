package com.cju.order;

import com.fasterxml.jackson.annotation.JsonProperty;

public enum OrderPayType {
    @JsonProperty("微信支付") WEIXIN("Weixin"),
    @JsonProperty("未知支付方式") UNKNOWN_PAY_TYPE("UnknowPayType");
    private String payType;
    
    private OrderPayType(String payType) {
        this.payType = payType;
    }
    
    public static OrderPayType fromString(String typeStr) {
        for (OrderPayType orderPayType: OrderPayType.values()) {
            if (orderPayType.payType.equals(typeStr)) {
                return orderPayType;
            }
        }
        //Default return
        return UNKNOWN_PAY_TYPE;
    }
    
    @Override
    public String toString() {
        switch (payType) {
            case "Weixin": 
                return "微信支付";
            default: 
                return "未知支付方式";
        }
    }
}
