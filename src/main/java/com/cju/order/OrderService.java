package com.cju.order;

import java.io.IOException;
import java.io.StringReader;
import java.time.LocalDateTime;
import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.Locale;
import java.util.UUID;

import javax.annotation.PostConstruct;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactoryConfigurationError;

import org.apache.commons.math3.util.Precision;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import com.cju.billing.Account;
import com.cju.billing.BillingService;
import com.cju.billing.TransactionType;
import com.cju.file.QrImage;
import com.cju.file.QrImageRepository;
import com.cju.gzh.WeiXinService;
import com.cju.gzhOps.GzhCompany;
import com.cju.marketing.Referal;
import com.cju.marketing.ReferalStatus;
import com.cju.marketing.ReferalRepository;
import com.cju.util.QRCode;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.google.zxing.WriterException;

@Service
public class OrderService {
    private Logger log = LoggerFactory.getLogger(OrderService.class);
    final private int QR_CODE_EXPIRE_MINUTES = 60;

    @Autowired 
    private OrderRepository orderRepository;  
    
    @Autowired 
    private ReferalRepository referalRepository;  
    
    @Autowired 
    private QrImageRepository qrImageRepository;  
    
    @Autowired
    private WeiXinService weiXinService;
    
    @Autowired
    private BillingService billingService;
    
    @Autowired
    private MessageSource messageSource;
    
    @Autowired 
    private ObjectMapper objectMapper;
    
    private DocumentBuilder w3cDomBuilder;
    
    @PostConstruct
    public void init() throws ParserConfigurationException, TransformerConfigurationException, TransformerFactoryConfigurationError {
        w3cDomBuilder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
    }
    
    private int generateOrderNum() {
        UUID uuid = UUID.randomUUID();
        //In Java, the integer is also 32 bits, but ranges from -2,147,483,648 to +2,147,483,647
        int orderNum = Math.abs(uuid.toString().hashCode());
        return orderNum;
    }
    
    public ObjectNode generateWeiXinPayOrderForGzhCompanyRecharge(GzhCompany gzhCompany, String productBrief, int productId, double amount, String submitter, String note) 
        throws TransformerFactoryConfigurationError, TransformerException, WriterException, IOException {
       
        Order order = new Order();
        int orderNum = generateOrderNum();
        order.setOrderNum(orderNum);
        order.setProductBrief(productBrief);
        order.setProductId(productId);
        order.setAmount(Precision.round(amount, 2));
        order.setPayType(OrderPayType.WEIXIN);
        order.setPayStatus(OrderPayStatus.PENDING);
        OffsetDateTime submitTime = OffsetDateTime.now();
        order.setSubmitTime(submitTime);
        OffsetDateTime qrCodeExpireTime = submitTime.plusMinutes(QR_CODE_EXPIRE_MINUTES);
        order.setQrCodeExpireTime(qrCodeExpireTime);
        order.setSubmitter(submitter);
        order.setNote(note);
        order.setGzhCompany(gzhCompany);
        //Only save the order to DB when weixin server created the pay order successfully.
        
        //Call weiXinService to send the post request to weixin to generate pay order.
        String xmlMsg = weiXinService.generatePayOrder(order);
        
        //Now parse the response xmlMsg from weixin 
        ObjectNode jNode = objectMapper.createObjectNode();
        
        //Need to parse the returned XML from weixin and update the order if necessary here rather than in OrderController
        Document document;
        try {
            document = w3cDomBuilder.parse(new InputSource(new StringReader(xmlMsg)));
        } 
        catch (SAXException e) {
            e.printStackTrace();
            jNode.put("w3c_parse_sax_exception", "Error in parsing xml message from querying weixin order pay status");
            jNode.put("weixin_query_response", xmlMsg);
            return jNode;
        }
        catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            jNode.put("w3c_parse_io_exception", "Error in parsing xml message from querying weixin order pay status");
            jNode.put("weixin_query_response", xmlMsg);
            return jNode;
        }
        
        Element root = document.getDocumentElement();
        String returnCode = root.getElementsByTagName("return_code").item(0).getTextContent();
        jNode.put("return_code", returnCode);
        
        Node returnMsgNode = root.getElementsByTagName("return_msg").item(0);
        if (returnMsgNode != null) {
            String returnMsg = returnMsgNode.getTextContent();
            jNode.put("return_msg", returnMsg);
        }
        
        if (!returnCode.equals("SUCCESS")) {
            return jNode;
        }
        
        //Now check MD5 signature
        String sign = root.getElementsByTagName("sign").item(0).getTextContent();
        String calculatedSign = weiXinService.calculateWeiXinPayOrderSign(document);
        log.info("generateWeiXinPayOrder calculatedSign=" + calculatedSign);
        if (!calculatedSign.equals(sign)) {
            String retMsg = messageSource.getMessage("gzh.weixin.payorder.sign.error", null, Locale.SIMPLIFIED_CHINESE);
            jNode.put("return_msg", retMsg);
            return jNode;
        }
        
        //resultCode SUCCESS means the pay order is created in weiXin server
        String resultCode = root.getElementsByTagName("result_code").item(0).getTextContent();
        jNode.put("result_code", resultCode);
        if (!resultCode.equals("SUCCESS")) {
            Node errCodeNode = root.getElementsByTagName("err_code").item(0);
            if (errCodeNode != null) {
                String errCode = errCodeNode.getTextContent();
                jNode.put("err_code", errCode);
            }
            Node errCodeDesNode = root.getElementsByTagName("err_code_des").item(0);
            if (errCodeDesNode != null) {
                String errCodeDes = errCodeDesNode.getTextContent();
                jNode.put("err_code_des", errCodeDes);
            }
            return jNode;
        }
        
        //weiXin resultCode is SUCCESS. The pay order is created successfully in weixin server.
        //Now we can save order to DB
        order = orderRepository.save(order);
        
        String qrCodeUrl = root.getElementsByTagName("code_url").item(0).getTextContent();
        jNode.put("code_url", qrCodeUrl);
        byte[] qrCodeImg = QRCode.generateImg(qrCodeUrl, 128, 128, "jpg"); 
        QrImage qrImage = new QrImage();
        qrImage.setData(qrCodeImg);
        qrImage.setOrder(order);
        qrImage = qrImageRepository.save(qrImage);
        
        order.setQrCodeUrl(qrCodeUrl);
        order.setQrImage(qrImage);
        order = orderRepository.save(order);
        
        jNode.putPOJO("order", order);
        
        return jNode;
    }
    
    public ObjectNode generateWeiXinPayOrderForReferalVerification(Referal referal, String productBrief, int productId, double amount) {
            
        Order order = new Order();
        int orderNum = generateOrderNum();
        order.setOrderNum(orderNum);
        order.setAmount(Precision.round(amount, 2));
        order.setPayType(OrderPayType.WEIXIN);
        order.setPayStatus(OrderPayStatus.PENDING);
        OffsetDateTime submitTime = OffsetDateTime.now();
        order.setSubmitTime(submitTime);
        order.setSubmitter(referal.getUser().getUserName());
        order.setProductBrief(productBrief);
        order.setProductId(productId);
        order.setReferal(referal);
        //Only save the order to DB when weixin server created the pay order successfully.
        
        //Call weiXinService to send the post request to weixin to generate pay order.
        String xmlMsg = weiXinService.generateReferalVerifyOrder(order);
        
        //Now parse the response xmlMsg from weixin 
        ObjectNode jNode = objectMapper.createObjectNode();
        
        //Need to parse the returned XML from weixin and update the order if necessary here rather than in OrderController
        Document document;
        try {
            document = w3cDomBuilder.parse(new InputSource(new StringReader(xmlMsg)));
        } 
        catch (SAXException e) {
            e.printStackTrace();
            jNode.put("w3c_parse_sax_exception", "Error in parsing xml message from generating referal verify order");
            jNode.put("weixin_query_response", xmlMsg);
            return jNode;
        }
        catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            jNode.put("w3c_parse_io_exception", "Error in parsing xml message from generating referal verify order");
            jNode.put("weixin_query_response", xmlMsg);
            return jNode;
        }
        
        Element root = document.getDocumentElement();
        String returnCode = root.getElementsByTagName("return_code").item(0).getTextContent();
        jNode.put("return_code", returnCode);
        
        Node returnMsgNode = root.getElementsByTagName("return_msg").item(0);
        if (returnMsgNode != null) {
            String returnMsg = returnMsgNode.getTextContent();
            jNode.put("return_msg", returnMsg);
        }
        
        if (!returnCode.equals("SUCCESS")) {
            return jNode;
        }
        
        //Now check MD5 signature
        String sign = root.getElementsByTagName("sign").item(0).getTextContent();
        String calculatedSign = weiXinService.calculateWeiXinPayOrderSign(document);
        log.info("generateWeiXinReferalVerifyOrder calculatedSign=" + calculatedSign);
        if (!calculatedSign.equals(sign)) {
            String retMsg = messageSource.getMessage("gzh.weixin.sign.error", null, Locale.SIMPLIFIED_CHINESE);
            jNode.put("return_msg", retMsg);
            return jNode;
        }
        
        //resultCode SUCCESS means the pay order is created in weiXin server
        String resultCode = root.getElementsByTagName("result_code").item(0).getTextContent();
        jNode.put("result_code", resultCode);
        if (!resultCode.equals("SUCCESS")) {
            Node errCodeNode = root.getElementsByTagName("err_code").item(0);
            if (errCodeNode != null) {
                String errCode = errCodeNode.getTextContent();
                jNode.put("err_code", errCode);
            }
            Node errCodeDesNode = root.getElementsByTagName("err_code_des").item(0);
            if (errCodeDesNode != null) {
                String errCodeDes = errCodeDesNode.getTextContent();
                jNode.put("err_code_des", errCodeDes);
            }
            return jNode;
        }
        
        //weiXin resultCode is SUCCESS. The pay order is created successfully in weixin server.
        //Now we can save the info in Referal
        order = orderRepository.save(order);

        String qrCodeUrl = root.getElementsByTagName("code_url").item(0).getTextContent();
        jNode.put("code_url", qrCodeUrl);
        byte[] qrCodeImg = QRCode.generateImg(qrCodeUrl, 128, 128, "jpg"); 
        QrImage qrImage = new QrImage();
        qrImage.setData(qrCodeImg);
        qrImage.setOrder(order);
        qrImage = qrImageRepository.save(qrImage);
        
        order.setQrCodeUrl(qrCodeUrl);
        order.setQrImage(qrImage);
        order = orderRepository.save(order);
        
        jNode.putPOJO("order", order);
        
        return jNode;
    }
    
    public ObjectNode reGenerateWeiXinPayOrderForReferalVerification(Referal referal) {
        
        Order order = referal.getVerifyOrder();
        String productBrief = order.getProductBrief();
        int productId = order.getProductId();
        double amount = order.getAmount();
        
        ObjectNode jNode = generateWeiXinPayOrderForReferalVerification(referal, productBrief, productId, amount);
        
        return jNode;
    }
        
    public Order updateOrderWithSuccessWeiXinPayInfo (Order order, OffsetDateTime payTime, String payRefNum, String openId ) {
        if (order.getPayStatus() == OrderPayStatus.PENDING) {
            //Only do the update if order is currently in PENDING state
            
            //We simplify all the cases into two cases:
            //    1. The payStatus can only be updated once from PENDING to other status.
            //    2. All non PENDING status are final and can not be modified.
            //    2. Pay Status Query button is shown to admin only if the pay order is in PENDING status and qrCode expires.
            //    
            order.setPayTime(payTime);
            order.setPayRefNum(payRefNum);
            order.setPayerOpenId(openId);
            order.setPayStatus(OrderPayStatus.SUCCESS);
            order = orderRepository.save(order);
            log.info("updateOrderWithSuccessWeiXinPayInfo updated order= " + order);
            
            //The order may be a referal apply verification order or a gzhCompany recharge order
            if (order.getReferal() != null) {
                Referal referal = order.getReferal();
                
                //First check whethere there exists a referal with openId
                Referal referalExisting = referalRepository.findOneByOpenId(openId);
                if (referalExisting != null) {
                    //The user has used the same openId to verify a referal in the past.
                    String message = messageSource.getMessage("gzh.referal.verify.weixin.alreay.used", null, Locale.SIMPLIFIED_CHINESE);
                    referal.setReferalStatusNote(message);
                    ObjectNode jNode = reGenerateWeiXinPayOrderForReferalVerification(referal);
                    Order newOrder = null;
                    try {
                        newOrder = objectMapper.treeToValue(jNode.get("order"), Order.class);
                    } catch (JsonProcessingException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                    referal.setReferalStatus(ReferalStatus.PENDING_WEIXIN_PAY_VERIFY);
                    referal.setVerifyOrder(newOrder);
                    
                    referalRepository.save(referal);
                }
                else {
                    referal.setOpenId(openId);
                    referal.setReferalStatus(ReferalStatus.UNDER_REVIEW);
                    referalRepository.save(referal);
                }
            }
            
            if (order.getGzhCompany() != null) {
                //After update the order, update the account balance
                Account account = order.getGzhCompany().getAccount();
                double adjustAmount = order.getAmount();
                String adjustNote = order.getNote();
                TransactionType txType = TransactionType.WEIXIN_RECHARGE;
                String txRefNum = payRefNum;
                String operator = messageSource.getMessage("gzh.gzhcompany.operator.system.auto", null, Locale.SIMPLIFIED_CHINESE);
                
                billingService.adjustAccountBalance(account, adjustAmount, adjustNote, txType, txRefNum, operator);
            }
        }
        else {
            log.info("updateOrderWithSuccessWeiXinPayInfo order not in PENDING state, do nothing. order= " + order);
        }
        return order;
    }
    
    public Order updateOrderWithNonSuccessWeiXinPayInfo(Order order, OrderPayStatus payStatus) {
        
        if (order.getReferal() != null) {
            //This is an referal verification order.
            //There is no expire time for referal verification order. 
            //Even though the query with weixin does not return SUCCESS this time, the user may pay successfully later.
            //Since we update order status from PENDING to a final state only once, we should not update for referal order here.
            return order;
        }
        
        //Now this is an order for GzhCompany recharge. This order has an expiration time.
        //Admin can only do the query after the qrCode expiration time. So the query result is final. 
        if (order.getPayStatus() == OrderPayStatus.PENDING && payStatus != OrderPayStatus.SUCCESS) {
            //Only do the update if order is currently in PENDING state
            //If the passed in payStatus is SUCCESS, then updateOrderWithSuccessWeiXinPayInfo should be called with payTime and payRefNum
            
            //We simplify all the cases into two cases:
            //    1. The payStatus can only be updated once from PENDING to other status.
            //    2. All non PENDING status are final and can not be modified.
            //    2. Pay Status Query button is shown to admin only if the pay order is in PENDING status and qrCode expires.
            //       
            order.setPayStatus(payStatus);
            order = orderRepository.save(order);
            log.info("updateOrderWithNonSuccessWeiXinPayInfo updated order= " + order);
            
            //Do not update the account balance since the order is not paid success.
        }
        else {
            log.info("updateOrderWithNonSuccessWeiXinPayInfo order not in PENDING state, do nothing. order= " + order);
        }
        return order;
    }
    
    public ObjectNode queryOrderPayStatus (Order order) {
        
        ObjectNode jNode = objectMapper.createObjectNode();
        
        String xmlMsg;
        try {
            xmlMsg = weiXinService.queryPayOrderStatus(order);
        } catch (TransformerException e1) {
            // queryPayOrderStatus may throw a TransformerException when preparing the xml message to send to weixin.
            // This exception usually should not happen.
            e1.printStackTrace();
            jNode.put("w3c_transformer_exception", "Error in preparing xml message for querying weixin order pay status");
            return jNode;
        }
        
        //Here we received reply from weixin on the pay order status query
        //Need to parse the returned XML from weixin and update the order here rather than in OrderController
        Document document;
        try {
            document = w3cDomBuilder.parse(new InputSource(new StringReader(xmlMsg)));
        } 
        catch (SAXException e) {
            e.printStackTrace();
            jNode.put("w3c_parse_sax_exception", "Error in parsing xml message from querying weixin order pay status");
            jNode.put("weixin_query_response", xmlMsg);
            return jNode;
        }
        catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            jNode.put("w3c_parse_io_exception", "Error in parsing xml message from querying weixin order pay status");
            jNode.put("weixin_query_response", xmlMsg);
            return jNode;
        }
        
        Element root = document.getDocumentElement();
        
        String returnCode = root.getElementsByTagName("return_code").item(0).getTextContent();
        jNode.put("return_code", returnCode);
        
        Node returnMsgNode = root.getElementsByTagName("return_msg").item(0);
        if (returnMsgNode != null) {
            String returnMsg = returnMsgNode.getTextContent();
            jNode.put("return_msg", returnMsg);
        }
        if (!returnCode.equals("SUCCESS")) {
            return jNode;
        }
        
        //Now check MD5 signature
        String sign = root.getElementsByTagName("sign").item(0).getTextContent();
        String calculatedSign = weiXinService.calculateWeiXinPayOrderSign(document);
        log.info("queryOrderPayStatus calculatedSign=" + calculatedSign);
        if (!calculatedSign.equals(sign)) {
            String retMsg = messageSource.getMessage("gzh.weixin.payorder.sign.error", null, Locale.SIMPLIFIED_CHINESE);
            jNode.put("return_msg", retMsg);
            return jNode;
        }
        
        //resultCode SUCCESS means the pay order status result is returned 
        String resultCode = root.getElementsByTagName("result_code").item(0).getTextContent();
        jNode.put("result_code", resultCode);
        if (!resultCode.equals("SUCCESS")) {
            Node errCodeNode = root.getElementsByTagName("err_code").item(0);
            if (errCodeNode != null) {
                String errCode = errCodeNode.getTextContent();
                jNode.put("err_code", errCode);
            }
            Node errCodeDesNode = root.getElementsByTagName("err_code_des").item(0);
            if (errCodeDesNode != null) {
                String errCodeDes = errCodeDesNode.getTextContent();
                jNode.put("err_code_des", errCodeDes);
            }
            return jNode;
        }
        
        String outTradeNo = root.getElementsByTagName("out_trade_no").item(0).getTextContent();
        jNode.put("out_trade_no", outTradeNo);
        int orderNum = Integer.valueOf(outTradeNo);
        if (order.getOrderNum() != orderNum) {
            //Something wrong with weixin server. This should not happen.
            log.info("queryOrderPayStatus The returned orderNum " + orderNum + " is different from the requested orderNum " + order.getOrderNum());
            String retMsg = messageSource.getMessage("gzh.weixin.payorder.order.num.not.same", null, Locale.SIMPLIFIED_CHINESE);
            jNode.put("return_msg", retMsg);
            return jNode;
        }
        
        String tradeState = root.getElementsByTagName("trade_state").item(0).getTextContent();
        jNode.put("trade_state", tradeState);
        if (!tradeState.equals("SUCCESS")) {
            //The pay order is not paid SUCCESS. 
            //Since we only do the query after qrCode expires, the pay order will never be paid SUCCESS in the future.
            //We need to update the half open pay order in our DB.
            OrderPayStatus payStatus = OrderPayStatus.fromString(tradeState);
            updateOrderWithNonSuccessWeiXinPayInfo(order, payStatus);
            return jNode;
        }
        
        //Now we got a pay order is paid SUCCESS
        String timeEnd = root.getElementsByTagName("time_end").item(0).getTextContent();
        jNode.put("time_end", timeEnd);
        //timeEnd has the format of "20140903131540". It does not have zone info and it is assumed China time.
        LocalDateTime localPayTime = LocalDateTime.parse(timeEnd, DateTimeFormatter.ofPattern("yyyyMMddHHmmss"));
        //China ZoneOffset is UTC+08:00
        OffsetDateTime payTime = localPayTime.atOffset(ZoneOffset.ofHours(8));
        String txId = root.getElementsByTagName("transaction_id").item(0).getTextContent();
        jNode.put("transaction_id", txId);
        String openId = root.getElementsByTagName("openid").item(0).getTextContent();
        
        Order updatedOrder = updateOrderWithSuccessWeiXinPayInfo (order, payTime, txId, openId);
        jNode.putPOJO("updatedOrder", updatedOrder);
        
        return jNode;
    }
    

}