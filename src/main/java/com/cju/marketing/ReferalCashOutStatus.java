package com.cju.marketing;

import com.fasterxml.jackson.annotation.JsonProperty;

public enum ReferalCashOutStatus {
    @JsonProperty("申请受理") PROCESSING ("Processing"),
    @JsonProperty("申请驳回") REJECTED_FINAL ("RejectedFinal"),
    @JsonProperty("银行错误") BANK_FAILURE ("BankFailer"),
    @JsonProperty("提现完成") COMPLETED ("Completed"),
    @JsonProperty("未知提现状态") UNKNOWN ("Unknown");

    private String status;
    
    private ReferalCashOutStatus(String status) {
        this.status = status;
    }
    
    public static ReferalCashOutStatus fromString(String statusStr) {
        switch(statusStr) {
            case "Processing":
                return ReferalCashOutStatus.PROCESSING;
            case "RejectedFinal":
                return ReferalCashOutStatus.REJECTED_FINAL;
            case "BankFailer":
                return ReferalCashOutStatus.BANK_FAILURE;
            case "Completed":
                return ReferalCashOutStatus.COMPLETED;
        }

        //Default return
        return UNKNOWN;
    }
    
    @Override
    public String toString() {
        switch (status) {
            case "Processing": 
                return "申请受理";
            case "RejectedFinal": 
                return "申请驳回";
            case "BankFailer": 
                return "银行错误";
            case "Completed": 
                return "提现完成";
            default: 
                return "未知";
        }
    }
}