package com.cju.marketing;

import org.springframework.data.querydsl.QueryDslPredicateExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ReferalAccountRepository extends PagingAndSortingRepository<ReferalAccount, Long>, QueryDslPredicateExecutor<ReferalAccount> {

}