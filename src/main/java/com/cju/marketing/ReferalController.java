package com.cju.marketing;

import java.io.IOException;
import java.time.LocalDate;
import java.time.OffsetDateTime;
import java.time.YearMonth;
import java.util.Locale;
import java.util.Map;
import java.util.UUID;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactoryConfigurationError;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import com.cju.gzhOps.GzhCompany;
import com.cju.order.Order;
import com.cju.order.OrderPayStatus;
import com.cju.order.OrderRepository;
import com.cju.order.OrderService;
import com.cju.security.RepositoryQueryFilter;
import com.cju.user.User;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.google.zxing.WriterException;
import com.querydsl.core.types.Predicate;

@Controller
public class ReferalController {
    private Logger log = LoggerFactory.getLogger(ReferalController.class);
    
    final private double REFERAL_VERIFY_AMOUNT = 0.01;
    final private int REFERAL_VERIFY_PRODUCT_ID = 666666;
    //final private int REFERER_COOKIE_EXPIRE_SECONDS = 30 * 24 * 3600;
    final private int REFERER_COOKIE_EXPIRE_SECONDS = 300;
    final private double REFERAL_CASH_OUT_MIN = 500.0;
    
    @Autowired 
    private ReferalRepository referalRepository; 
    
    @Autowired 
    private ReferalAccountRepository referalAccountRepository; 
    
    @Autowired 
    private ReferalCashOutRepository referalCashOutRepository; 
    
    @Autowired 
    private ReferalTransactionRepository referalTransactionRepository; 
    
    @Autowired 
    private ReferalService referalService; 
    
    @Autowired 
    private OrderRepository orderRepository; 
    
    @Autowired 
    private OrderService orderService; 
    
    @Autowired 
    RepositoryQueryFilter repoQueryFilter;
    
    @Autowired
    private MessageSource messageSource;
    
    @Autowired
    private ObjectMapper objectMapper;
    
    //This is the only entry page to set referal cookie
    //This is used both for PC browser and for the gzh menu on mobile
    @GetMapping(value={"/pub/referal/intro"})
    public String getReferalIntro(@RequestParam Map<String, String> params, HttpServletResponse response, @CookieValue(value="referalNum", defaultValue="0") int referalNumCookie) {
         if (referalNumCookie == 0) {
             //Set the cookie
             String referalNumParam = params.get("referalNum");
             if (referalNumParam != null) {
                 Cookie referalCookie = new Cookie("referalNum", referalNumParam);
                 referalCookie.setMaxAge(REFERER_COOKIE_EXPIRE_SECONDS);
                 response.addCookie(referalCookie);
             }
         }
         return "referal/referalIntro";
    }
    
    @PreAuthorize("isAuthenticated()")
    @GetMapping("/gzhOps/referal/apply")
    public String getReferalApplyFrom(@AuthenticationPrincipal User user, Pageable pageable, Model model) { 
        Referal selfReferal = user.getReferal();
        if (selfReferal.getReferalStatus() == ReferalStatus.NOT_APPLIED) {
            return "referal/referalApply";
        }
        else {
            model.addAttribute("referal", selfReferal);
            return "referal/referalStatusDetails";
        }
    }
    
    //When using application/x-www-form-urlencoded, Spring doesn't understand it as a RequestBody. 
    //So, if we want to use this we must remove the @RequestBody annotation.
    @PreAuthorize("isAuthenticated()")
    @PostMapping(value="/gzhOps/referal/apply")
    public String postReferalApplyForm(@AuthenticationPrincipal User user, @RequestParam Map<String, String> params, HttpServletResponse response, Model model) 
        throws TransformerFactoryConfigurationError, TransformerException, WriterException, IOException { 
        Referal selfReferal = user.getReferal();
        String name = params.get("name");
        String address = params.get("address");
        int birthYear = Integer.valueOf(params.get("birthYear"));
        String mobile = params.get("mobile");
        String work = params.get("work");
        String selfIntro = params.get("selfIntro");
        String referalPlan = params.get("referalPlan");
        selfReferal.setName(name);
        selfReferal.setAddress(address);
        selfReferal.setBirthYear(birthYear);
        selfReferal.setMobile(mobile);
        selfReferal.setWork(work);
        selfReferal.setSelfIntro(selfIntro);
        selfReferal.setReferalPlan(referalPlan);
        
        String productBrief = messageSource.getMessage("gzh.weixin.referal.verify.product", new Object[] {user.getUserName()}, Locale.SIMPLIFIED_CHINESE); 
        int productId = REFERAL_VERIFY_PRODUCT_ID;
        double amount = REFERAL_VERIFY_AMOUNT; 
        
        ObjectNode jNode = orderService.generateWeiXinPayOrderForReferalVerification(selfReferal, productBrief, productId, amount);
        
        //return_code from weixin server can be either SUCCESS or FAIL
        String retCode = jNode.get("return_code").asText();
        if (!retCode.equals("SUCCESS")) {
            //need to set the http status code to failure
            response.setStatus(HttpStatus.BAD_REQUEST.value());
        }
        else {
            Order order = objectMapper.treeToValue(jNode.get("order"), Order.class);
            selfReferal.setReferalStatus(ReferalStatus.PENDING_WEIXIN_PAY_VERIFY);
            String message = messageSource.getMessage("gzh.referal.pending.weixin.pay.verification", null, Locale.SIMPLIFIED_CHINESE);
            selfReferal.setReferalStatusNote(message);
            selfReferal.setVerifyOrder(order);
            
            referalRepository.save(selfReferal);
        }
        
        model.addAttribute("referal", selfReferal);
        return "referal/referalStatusDetails";
    }
    
    @PreAuthorize("isAuthenticated()")
    @GetMapping("/gzhOps/referal/cashOut/apply")
    public String getReferalCashOutApplyFrom(@AuthenticationPrincipal User user, Pageable pageable, Model model) { 
        Referal selfReferal = user.getReferal();
        if (selfReferal.getReferalStatus() == ReferalStatus.NOT_APPLIED) {
            String message = messageSource.getMessage("gzh.referal.not.applied.can.not.cash.out", null, Locale.SIMPLIFIED_CHINESE);
            model.addAttribute("message", message);
            return "gzhOps/gzhOpsMessage";
        }
        
        if (selfReferal.getReferalAccount().getBalance() < REFERAL_CASH_OUT_MIN) {
            String message = messageSource.getMessage("gzh.referal.balance.low.can.not.cash.out", new Object[] {REFERAL_CASH_OUT_MIN}, Locale.SIMPLIFIED_CHINESE);
            model.addAttribute("message", message);
            return "gzhOps/gzhOpsMessage";
        }
       
        Predicate predicate = repoQueryFilter.filterReferalCashOutInProcessingByUser(user);
        Iterable<ReferalCashOut> referalCashOutItr = referalCashOutRepository.findAll(predicate);
        if (referalCashOutItr.iterator().hasNext()) {
            String message = messageSource.getMessage("gzh.referal.processing.can.not.cash.out", new Object[] {REFERAL_CASH_OUT_MIN}, Locale.SIMPLIFIED_CHINESE);
            model.addAttribute("message", message);
            return "gzhOps/gzhOpsMessage";
        }
        
        model.addAttribute("referal", selfReferal);
        return "referal/referalCashOutApply";
    }
    
    @PreAuthorize("isAuthenticated()")
    @GetMapping("/gzhOps/referal/myStatus")
    public String getMyReferalProfile(@AuthenticationPrincipal User user, Pageable pageable, Model model) { 
        Referal selfReferal = user.getReferal();
       
        if (selfReferal.getReferalStatus() == ReferalStatus.NOT_APPLIED) {
            String message =  messageSource.getMessage("gzh.referal.apply.status.not.active", null, Locale.SIMPLIFIED_CHINESE);
            model.addAttribute("message", message);
            return "gzhOps/gzhOpsMessage";
        }
        else {
            model.addAttribute("referal", selfReferal);
            model.addAttribute("userEditable", true);
            return "referal/referalStatusDetails";
        }
    }
    
    @PreAuthorize("isAuthenticated()")
    @GetMapping("/gzhOps/referal/myBalance")
    public String getMyReferalBalance(@AuthenticationPrincipal User user, Pageable pageable, Model model) { 
        ReferalAccount referalAccount = user.getReferal().getReferalAccount();
        long referalAccountId = referalAccount.getId();
        Predicate predicate = repoQueryFilter.filterReferalTransactionByReferalAccountId(referalAccountId);
        Page<ReferalTransaction> transactionPage = referalTransactionRepository.findAll(predicate, pageable);
        
        model.addAttribute("referalAccount", referalAccount);
        model.addAttribute("referalTransactionPage", transactionPage);
        
        return "referal/referalBalanceDetails";
    }
    
    @PreAuthorize("isAuthenticated()")
    @GetMapping("/gzhOps/referal/myIncome")
    public String getMyReferalIncome(@AuthenticationPrincipal User user, Pageable pageable, Model model) { 
        Referal selfReferal = user.getReferal();
        String message = "";
        
        if (selfReferal.getReferalStatus() == ReferalStatus.ACTIVE) {
            return "redirect:/gzhOps/referal/income/" + user.getReferal().getId() + "/details";
        }
        else if (selfReferal.getReferalStatus() == ReferalStatus.FREEZE){
            message =  messageSource.getMessage("gzh.referal.status.freeze.income.not.show", null, Locale.SIMPLIFIED_CHINESE);
        }
        else {
            message =  messageSource.getMessage("gzh.referal.not.active.no.income", null, Locale.SIMPLIFIED_CHINESE);
        }
        
        model.addAttribute("message", message);
        return "gzhOps/gzhOpsMessage";
    }
    
    //Only admin see all the referal status list
    @PreAuthorize("isWebAdmin()")
    @GetMapping("/gzhOps/referal/status/list")
    public String getReferalStatusList(@AuthenticationPrincipal User user, Pageable pageable, Model model) { 
        Page<Referal> referalPage = referalRepository.findAll(pageable);
        model.addAttribute("referalPage", referalPage);
        log.info("getReferalStatusList referalPage.size=" + referalPage.getSize() + ", referalPage.Number " + referalPage.getNumber());
        
        return "referal/referalStatusList";
    }
    
    //Only admin needs to access referal status details via this link. 
    //A user accesses his referal status details via his referal.
    @PreAuthorize("isWebAdmin()")
    @GetMapping("/gzhOps/referal/status/{id}/details")
    public String getReferalStatusDetails(@PathVariable("id") long id, Model model) { 
        Referal referal = referalRepository.findOne(id);
        model.addAttribute("referal", referal);
        model.addAttribute("adminEditable", true);
        return "referal/referalStatusDetails";
    }
    
    //Only admin see all the referal fee status list
    @PreAuthorize("isWebAdmin()")
    @GetMapping("/gzhOps/referal/referalFeeStatus/list")
    public String getReferalFeeStatusList(@AuthenticationPrincipal User user, Pageable pageable, Model model) { 
        Predicate predicate = repoQueryFilter.filterReferalWithGzhCompany();
        Page<Referal> referalPage = referalRepository.findAll(predicate, pageable);
        model.addAttribute("referalPage", referalPage);
        log.info("getReferalFeeStatusList referalPage.size=" + referalPage.getSize() + ", referalPage.Number " + referalPage.getNumber());
        
        return "referal/referalFeeStatusList";
    }
    
    @PreAuthorize("isWebAdmin()")
    @GetMapping("/gzhOps/referal/referalFeeStatus/{id}/details")
    public String getReferalFeeStatusDetails(@AuthenticationPrincipal User user, @PathVariable("id") long id, Model model) { 
        Referal referal = referalRepository.findOne(id);
        model.addAttribute("referal", referal);
        
        Referal parentReferal = referal.getParentReferal();
        Referal grandParentReferal = null;
        if (parentReferal != null) {
            grandParentReferal = parentReferal.getParentReferal();
        }
        
        if (parentReferal != null) {
            model.addAttribute("parentReferalUserName", parentReferal.getUser().getUserName());
            model.addAttribute("parentReferalStatus", parentReferal.getReferalStatus());
            String parentReferalBalanceLink = "/gzhOps/referal/balance/" + parentReferal.getId() +"/details";
            model.addAttribute("parentReferalBalanceLink", parentReferalBalanceLink);
        }
        else {
            model.addAttribute("parentReferalUserName", null);
            model.addAttribute("parentReferalStatus", "");
            model.addAttribute("parentReferalBalanceLink", null);
        }
        
        if (grandParentReferal != null) {
            model.addAttribute("grandParentReferalUserName", grandParentReferal.getUser().getUserName());
            model.addAttribute("grandParentReferalStatus", grandParentReferal.getReferalStatus());
            String grandParentReferalBalanceLink = "/gzhOps/referal/balance/" + grandParentReferal.getId() +"/details";
            model.addAttribute("grandParentReferalBalanceLink", grandParentReferalBalanceLink);
        }
        else {
            model.addAttribute("grandParentReferalUserName", null);
            model.addAttribute("grandParentReferalStatus", "");
            model.addAttribute("grandParentReferalBalanceLink", null);
        }
        
        return "referal/referalFeeStatusDetails";
    }
    
    //Only admin see all the referal income list
    @PreAuthorize("isWebAdmin()")
    @GetMapping("/gzhOps/referal/income/list")
    public String getReferalIncomeList(@AuthenticationPrincipal User user, Pageable pageable, Model model) { 
        Page<Referal> referalPage = referalRepository.findAll(pageable);
        model.addAttribute("referalPage", referalPage);
        log.info("getReferalList referalPage.size=" + referalPage.getSize() + ", referalPage.Number " + referalPage.getNumber());
        
        return "referal/referalIncomeList";
    }
    
    @PreAuthorize("isWebAdmin() || isSelfReferal(#id) || isLevel1Referal(#id) || isLevel2Referal(#id)")
    @GetMapping("/gzhOps/referal/income/{id}/details")
    public String getReferalIncomeDetails(@AuthenticationPrincipal User user, @PathVariable("id") long id, Pageable pageable, Model model) { 
        Referal selfReferal = user.getReferal();
        
        String referalDetailsTitle = null;
        String childReferalPageTitle = null;
        Boolean showChildReferalPage = true;
        Predicate predicate = null;
        
        Referal referal = referalRepository.findOne(id);
        String referalUserName = referal.getUser().getUserName();
        
        if (user.isWebAdmin()) {
            
            referal.setChildLevel(ReferalLevel.UNDER_ADMIN);
            predicate = repoQueryFilter.filterTwoLevelReferalByReferalId(id);
            referalDetailsTitle = messageSource.getMessage("gzh.referal.details", new Object[] {referalUserName}, Locale.SIMPLIFIED_CHINESE);
            childReferalPageTitle = messageSource.getMessage("gzh.referal.two.level.list", new Object[] {referalUserName}, Locale.SIMPLIFIED_CHINESE);
            Page<Referal> childReferalPage = referalRepository.findAll(predicate, pageable);
            childReferalPage.forEach(childReferal -> {
                if (childReferal.getParentReferal().getId() == id)
                    childReferal.setChildLevel(ReferalLevel.LEVEL1);
                else
                    childReferal.setChildLevel(ReferalLevel.LEVEL2);
            });
            
            model.addAttribute("referalDetailsTitle", referalDetailsTitle);
            model.addAttribute("referal", referal);
            model.addAttribute("childReferalPageTitle", childReferalPageTitle);
            model.addAttribute("childReferalPage", childReferalPage);
            model.addAttribute("showChildReferalPage", showChildReferalPage);
            return "referal/referalIncomeDetails";
        }
        
        //The user is not webadmin
        long selfReferalId = selfReferal.getId();
        int childReferalPageLevel = 0;
        
        if (selfReferalId == id) {
            childReferalPageLevel = 2;
            referal.setChildLevel(ReferalLevel.SELF);
        }
        else {
            Referal parentReferal = referal.getParentReferal();
            
            if (parentReferal != null) {
                if (parentReferal.getId() == selfReferalId) {
                    childReferalPageLevel = 1;
                    referal.setChildLevel(ReferalLevel.LEVEL1);
                }
                else {
                    //id is a level2 child because of the security check 
                    referal.setChildLevel(ReferalLevel.LEVEL2);
                    childReferalPageLevel = 0;
                }
            }
        }
        
        if (childReferalPageLevel == 2) {
            predicate = repoQueryFilter.filterTwoLevelReferalByUser(user);
            referalDetailsTitle = messageSource.getMessage("gzh.referal.my.details", null, Locale.SIMPLIFIED_CHINESE);
            childReferalPageTitle = messageSource.getMessage("gzh.referal.my.child.list", null, Locale.SIMPLIFIED_CHINESE);
        }
        else if (childReferalPageLevel == 1) {
            predicate = repoQueryFilter.filterOneLevelReferalByReferalId(id);
            referalDetailsTitle = messageSource.getMessage("gzh.referal.details", new Object[] {referalUserName}, Locale.SIMPLIFIED_CHINESE);
            childReferalPageTitle = messageSource.getMessage("gzh.referal.level1.list", new Object[] {referalUserName}, Locale.SIMPLIFIED_CHINESE);
        }
        else {
            //predicate = repoQueryFilter.filterMatchNoneReferal();
            referalDetailsTitle = messageSource.getMessage("gzh.referal.details", new Object[] {referal.getUser().getUserName()}, Locale.SIMPLIFIED_CHINESE);
            childReferalPageTitle = messageSource.getMessage("gzh.referal.child.list.not.visable", new Object[] {referalUserName}, Locale.SIMPLIFIED_CHINESE);
            showChildReferalPage = false;
        }
        
        Page<Referal> childReferalPage = null;
        if (showChildReferalPage == true) {
            childReferalPage = referalRepository.findAll(predicate, pageable);
            childReferalPage.forEach(childReferal -> {
                if (childReferal.getParentReferal().getId() == selfReferalId)
                    childReferal.setChildLevel(ReferalLevel.LEVEL1);
                else
                    childReferal.setChildLevel(ReferalLevel.LEVEL2);
            });
        }
        
        model.addAttribute("referal", referal);
        model.addAttribute("referalDetailsTitle", referalDetailsTitle);
        model.addAttribute("childReferalPageTitle", childReferalPageTitle);
        model.addAttribute("childReferalPage", childReferalPage);
        model.addAttribute("showChildReferalPage", showChildReferalPage);
        
        return "referal/referalIncomeDetails";
    }
    
    //Only admin see all the referal account balance list
    @PreAuthorize("isWebAdmin()")
    @GetMapping("/gzhOps/referal/balance/list")
    public String getReferalBalanceList(@AuthenticationPrincipal User user,  Pageable pageable, Model model) { 
        Page<ReferalAccount> referalAccountPage = referalAccountRepository.findAll(pageable);
        model.addAttribute("referalAccountPage", referalAccountPage);
        
        return "referal/referalBalanceList";
    }
    
    //Only admin needs to access referal account details via this link. 
    //A user accesses his referal account details via his referal.
    @PreAuthorize("isWebAdmin()") 
    @GetMapping("/gzhOps/referal/balance/{id}/details")
    public String getReferalBalanceDetails(@PathVariable("id") long id, Pageable pageable, Model model) { 
        ReferalAccount referalAccount = referalAccountRepository.findOne(id);
        model.addAttribute("referalAccount", referalAccount);
        
        Predicate predicate = repoQueryFilter.filterReferalTransactionByReferalAccountId(id);
        Page<ReferalTransaction> transactionPage = referalTransactionRepository.findAll(predicate, pageable);
        model.addAttribute("referalTransactionPage", transactionPage);
        
        return "referal/referalBalanceDetails";
    }
    
    @PreAuthorize("isWebAdmin()")
    @GetMapping("/gzhOps/referal/balance/{id}/adjust")
    public String getReferalBalanceAdjustForm(@PathVariable("id") long id, Model model) {  
        ReferalAccount referalAccount = referalAccountRepository.findOne(id);
        model.addAttribute("referalAccount", referalAccount);
        
        return "referal/referalBalanceAdjust";
    }
    
    //Only admin see all the referal transaction list
    //User sees his referal transaction list via his referal account details
    @PreAuthorize("isWebAdmin()")
    @GetMapping("/gzhOps/referal/transaction/list")
    public String getReferalTransactionList(@AuthenticationPrincipal User user,  Pageable pageable, Model model) { 
        Page<ReferalTransaction> transactionPage = referalTransactionRepository.findAll(pageable);
        model.addAttribute("referalTransactionPage", transactionPage);
        log.info("getReferalTransactionList referalTransactionPage.size=" + transactionPage.getSize() + ", referalTransactionPage.Number " + transactionPage.getNumber());

        return "referal/referalTransactionList";
    }
    
    @PreAuthorize("isWebAdmin() || isReferalOwnerForTransaction(#id)")
    @GetMapping("/gzhOps/referal/transaction/{id}/details")
    public String getReferalTransactionDetails(@PathVariable("id") long id, Model model) { 
        ReferalTransaction transaction = referalTransactionRepository.findOne(id);
        model.addAttribute("transaction", transaction);
        
        String href = null;
        ReferalTransactionType referalTransactionType = transaction.getTransactionType();
        if (referalTransactionType == ReferalTransactionType.REFERAL_INCOME_ACCOUNTING || 
            referalTransactionType == ReferalTransactionType.REFERAL_INCOME_ACCOUNTING_REVERT) 
        {
            //txRefNum is the referalNum of the referal accumulating the referal fee.
            String txRefNum = transaction.getTransactionRefNum();
            Referal referal = referalRepository.findOneByReferalNum(Integer.valueOf(txRefNum));
            href ="/gzhOps/referal/income/" + referal.getId() + "/details";
        }
        if (referalTransactionType == ReferalTransactionType.BANK_CASH_OUT) {
            //txRefNum in this case is the referalCashOut requestNum
            String txRefNum = transaction.getTransactionRefNum();
            ReferalCashOut referalCashOut = referalCashOutRepository.findOneByRequestNum(Integer.valueOf(txRefNum));
            href ="/gzhOps/referal/cashOut/" + referalCashOut.getId() + "/details";
        }
        
        //empty href in browser will reload the current page 
        model.addAttribute("link", href);
        return "referal/referalTransactionDetails";
    }
    
    //User sees his cashOut list
    //Admin sees all cashOut list
    @PreAuthorize("isAuthenticated()")
    @GetMapping("/gzhOps/referal/cashOut/list")
    public String getReferalCashOutList(@AuthenticationPrincipal User user,  Pageable pageable, Model model) { 
        Predicate predicate = repoQueryFilter.filterReferalCashOutByUser(user);
        Page<ReferalCashOut> referalCashOutPage = referalCashOutRepository.findAll(predicate, pageable);
        model.addAttribute("referalCashOutPage", referalCashOutPage);
        log.info("getReferalCashOutList referalCashOutPage.size=" + referalCashOutPage.getSize() + ", referalCashOutPage.Number " + referalCashOutPage.getNumber());

        return "referal/referalCashOutList";
    }
    
    @PreAuthorize("isWebAdmin() || isReferalOwnerForCashOut(#id)")
    @GetMapping("/gzhOps/referal/cashOut/{id}/details")
    public String getReferalCashOutDetails(@AuthenticationPrincipal User user, @PathVariable("id") long id, Pageable pageable, Model model) { 
        ReferalCashOut referalCashOut = referalCashOutRepository.findOne(id);
        model.addAttribute("referalCashOut", referalCashOut);
        if (user.isWebAdmin()) {
            model.addAttribute("adminEditable", true);
        }
        return "referal/referalCashOutDetails";
    }

    @PreAuthorize("isWebAdmin()")
    @GetMapping("/gzhOps/referal/verifyOrder/list")
    public String getReferalVerifyOrderList(@AuthenticationPrincipal User user, Pageable pageable, Model model) { 
        Predicate predicate = repoQueryFilter.filterReferalOrderAll();
        Page<Order> orderPage = orderRepository.findAll(predicate, pageable);
        
        //Only allow the status query if the order is in PENDING state.
        //referal verify order does not have exipration time.
        orderPage.forEach(order -> {
            if (order.getPayStatus() == OrderPayStatus.PENDING)
                order.setShowPayStatusQuery(true);
            else
                order.setShowPayStatusQuery(false);
        });
        
        model.addAttribute("orderPage", orderPage);
        model.addAttribute("showReferalOrder", true);
        log.info("getReferalVerifyOrderList orderPage.size=" + orderPage.getSize() + ", orderPage.Number " + orderPage.getNumber());
        
        return "order/orderList";
    }
    
    //verifyOrder details is reusing the same mapping in OrderController.java
    //An order can be a GzhCompany Recharge Order or a Referal Verify Order
    //@PreAuthorize("isWebAdmin() || isGzhAdminForGzhCompanyRechargeOrder(#id) || isReferalOwnerForReferalVerifyOrder(#id)")
    //GetMapping("/gzhOps/order/{id}/details")
    //public String getOrderDetails(@PathVariable("id") long id, Model model) { ... }
    
    //API to edit referal application information for both webAdmin and user applicant
    @PreAuthorize("isWebAdmin() || isSelfReferal(#id)")
    @PostMapping("/gzhOps/api/v1/referal/{id}/status")
    public @ResponseBody String postReferalApplyEditApi(@AuthenticationPrincipal User user, @PathVariable("id") long id, @RequestBody Referal jsonReferal) throws JsonProcessingException {
        Referal referal = referalRepository.findOne(id);
        
        String name = jsonReferal.getName();
        String address = jsonReferal.getAddress();
        int birthYear = jsonReferal.getBirthYear();
        String mobile = jsonReferal.getMobile();
        String work = jsonReferal.getWork();
        String selfIntro = jsonReferal.getSelfIntro();
        String referalPlan = jsonReferal.getReferalPlan();
        String referalStatusNote = jsonReferal.getReferalStatusNote();
        ReferalStatus referalStatus = jsonReferal.getReferalStatus();
        
        if (user.isWebAdmin()) {
            //web admin can change the referalStatus and referalStatusNote
            referal = referalService.updateReferalStatus(referal, referalStatus, referalStatusNote);
            String jNodeJson = objectMapper.writeValueAsString(referal);
            return jNodeJson;
        }
        else {
            //the applicant can update other fields.
            if (referal.getReferalStatus() != ReferalStatus.FREEZE && referal.getReferalStatus() != ReferalStatus.REJECTED_FINAL) {
                referal.setName(name);
                referal.setAddress(address);
                referal.setBirthYear(birthYear);
                referal.setMobile(mobile);
                referal.setWork(work);
                referal.setSelfIntro(selfIntro);
                referal.setReferalPlan(referalPlan);
                referalRepository.save(referal);
                
                //update the referal cached in the user object
                user.setReferal(referal);
                
                String message = messageSource.getMessage("gzh.referal.apply.status.edit.success", null, Locale.SIMPLIFIED_CHINESE);
                return message;
            }
            else {
                String message = messageSource.getMessage("gzh.referal.apply.status.can.not.edit", new Object[] {referal.getReferalStatus()}, Locale.SIMPLIFIED_CHINESE);
                return message;
            }
        }
    }
    
    @PreAuthorize("isSelfReferal(#id)")
    @PostMapping("/gzhOps/api/v1/referal/{id}/cashOut/apply")
    public @ResponseBody String postReferalCashOutApplyApi(@AuthenticationPrincipal User user, @PathVariable("id") long id, @RequestBody JsonNode json, 
            HttpServletResponse response) throws JsonProcessingException {
        Referal referal = referalRepository.findOne(id);
        
        if (referal.getReferalAccount().getBalance() < REFERAL_CASH_OUT_MIN) {
            String message = messageSource.getMessage("gzh.referal.balance.low.can.not.cash.out", new Object[] {REFERAL_CASH_OUT_MIN}, Locale.SIMPLIFIED_CHINESE);
            response.setStatus(HttpStatus.BAD_REQUEST.value());
            return message;
        }
       
        Predicate predicate = repoQueryFilter.filterReferalCashOutInProcessingByUser(user);
        Iterable<ReferalCashOut> referalCashOutItr = referalCashOutRepository.findAll(predicate);
        if (referalCashOutItr.iterator().hasNext()) {
            String message = messageSource.getMessage("gzh.referal.processing.can.not.cash.out", new Object[] {REFERAL_CASH_OUT_MIN}, Locale.SIMPLIFIED_CHINESE);
            response.setStatus(HttpStatus.BAD_REQUEST.value());
            return message;
        }
        
        String receipientName = json.get("receipientName").asText();
        double amount = json.get("amount").asDouble();
        String bankName = json.get("bankName").asText();
        String bankBranchName = json.get("bankBranchName").asText();
        String bankProvince = json.get("bankProvince").asText();
        String bankCity = json.get("bankCity").asText();
        String bankAccountNum = json.get("bankAccountNum").asText();

        ReferalCashOut referalCashOut = new ReferalCashOut();
        
        referalCashOut.setReceipientName(receipientName);
        referalCashOut.setAmount(amount);
        referalCashOut.setBankName(bankName);
        referalCashOut.setBankBranchName(bankBranchName);
        referalCashOut.setBankProvince(bankProvince);
        referalCashOut.setBankCity(bankCity);
        referalCashOut.setBankAccountNum(bankAccountNum);
        referalCashOut.setReferal(referal);
        referalCashOut.setSubmitTime(OffsetDateTime.now());
        referalCashOut.setReferalCashOutStatus(ReferalCashOutStatus.PROCESSING);
        
        UUID uuid = UUID.randomUUID();
        int requestNum = Math.abs(uuid.toString().hashCode());
        referalCashOut.setRequestNum(requestNum);
        
        referalCashOutRepository.save(referalCashOut);
        
        String message = messageSource.getMessage("gzh.referal.cash.out.apply.submit.success", null, Locale.SIMPLIFIED_CHINESE);
        return message;
    }
    
    @PreAuthorize("isWebAdmin()")
    @PostMapping("/gzhOps/api/v1/referal/cashOut/{id}/statusChange")
    public @ResponseBody String postReferalCashOutStatusChangeApi(@AuthenticationPrincipal User user, @PathVariable("id") long id, 
            @RequestBody ReferalCashOut jsonReferalCashOut, HttpServletResponse response) throws JsonProcessingException {
        ReferalCashOut referalCashOut = referalCashOutRepository.findOne(id);
        ReferalCashOutStatus oldReferalCashOutStatus = referalCashOut.getReferalCashOutStatus();
        
        String bankTxRefNum = jsonReferalCashOut.getBankTxRefNum();
        String note = jsonReferalCashOut.getNote();
        ReferalCashOutStatus newReferalCashOutStatus = jsonReferalCashOut.getReferalCashOutStatus();
        OffsetDateTime statusChangeTime = OffsetDateTime.now();
        String operator = user.getUserName();
        
        ReferalCashOut updatedReferalCashOut = null;
        if (oldReferalCashOutStatus == ReferalCashOutStatus.PROCESSING) {
            if (newReferalCashOutStatus == ReferalCashOutStatus.COMPLETED) {
                //This is a update for success money transfer. 
                //Referal Account balance will be adjusted.
                updatedReferalCashOut = referalService.updateReferalCashOutStatusWithSuccessMoneyTransfer(referalCashOut, 
                        newReferalCashOutStatus, bankTxRefNum, note, statusChangeTime, operator);
            }
            else {
                //Transfer from PROCESSING to other Non-COMPLETED states, such as BANK_FAILURE or REJECTED_FINAL
                //Just update the fields. Do not adjust referal account balance.
                updatedReferalCashOut = referalService.updateReferalCashOutFields(referalCashOut, 
                        newReferalCashOutStatus, bankTxRefNum, note, statusChangeTime, operator);
            }
        }
        else {
            //oldReferalCashOutStatus is not PROCESSING. All states other than PROCESSING are final and can not change again.
            if (oldReferalCashOutStatus != newReferalCashOutStatus) {
                //An error case of ReferalCashOutStatus change.
                String message = messageSource.getMessage("gzh.referal.cash.out.status.can.not.change", 
                        new Object[] {oldReferalCashOutStatus, newReferalCashOutStatus}, Locale.SIMPLIFIED_CHINESE);
                response.setStatus(HttpStatus.BAD_REQUEST.value());
                return message;
            }
            else {
                //ReferalCashOutStatus does not change. 
                //Just update the fields. Do not adjust referal account balance.
                updatedReferalCashOut = referalService.updateReferalCashOutFields(referalCashOut, 
                        newReferalCashOutStatus, bankTxRefNum, note, statusChangeTime, operator);
            }
        }
       
        String message = objectMapper.writeValueAsString(updatedReferalCashOut);
        return message;
    }
    
    @PreAuthorize("isWebAdmin()")
    @PostMapping("/gzhOps/api/v1/referal/{id}/referalFeeStatusRevert")
    public @ResponseBody String postReferalFeeStatusRevertApi(@AuthenticationPrincipal User user, @PathVariable("id") long id, HttpServletResponse response) throws JsonProcessingException {
        Referal referal = referalRepository.findOne(id);
        GzhCompany gzhCompany = referal.getUser().getOwnedGzhCompany();
        if (gzhCompany == null) {
            String message = messageSource.getMessage("gzh.referal.no.owned.gzhcompany", new Object[] {referal.getUser().getUserName()}, Locale.SIMPLIFIED_CHINESE);
            return message;
        }
        
        if (referal.getReferalFeeStatus() != ReferalFeeStatus.ACCOUNTING_COMPLETED) {
            String gzhCompanyName = gzhCompany.getName();
            ReferalFeeStatus referalFeeStatus = referal.getReferalFeeStatus();
            String message = messageSource.getMessage("gzh.referal.not.accounting.completed.can.not.revert", new Object[] {gzhCompanyName, referalFeeStatus}, Locale.SIMPLIFIED_CHINESE);
            return message;
        }
        
        
        //Revert referal fee status from ACCOUNTING_COMPLETED to ACCUMULATING will also revert the referal fee added to parent referal and grand parent referal
        Referal updatedReferal = referalService.revertReferalFeeStatusFromAccountingCompleted(referal, user.getUserName());    
        String jNodeJson = objectMapper.writeValueAsString(updatedReferal);
        return jNodeJson;
    }
    
    @PreAuthorize("isWebAdmin()")
    @PostMapping("/gzhOps/api/v1/referal/{id}/referalFeeStatusUpdate")
    public @ResponseBody String postReferalFeeStatusUpdateApi(@PathVariable("id") long id, HttpServletResponse response) throws JsonProcessingException {
        Referal referal = referalRepository.findOne(id);
        GzhCompany gzhCompany = referal.getUser().getOwnedGzhCompany();
        if (gzhCompany == null) {
            String message = messageSource.getMessage("gzh.referal.no.owned.gzhcompany", new Object[] {referal.getUser().getUserName()}, Locale.SIMPLIFIED_CHINESE);
            return message;
        }
        
        String gzhCompanyName = gzhCompany.getName();
        LocalDate gzhCompanyActiveBillingStartDate = gzhCompany.getActiveBillingStartDate();
        LocalDate today = LocalDate.now();
        
        if (gzhCompanyActiveBillingStartDate == null || gzhCompanyActiveBillingStartDate.isAfter(today)) {
            String message = messageSource.getMessage("gzh.referal.loe.active.billing.start.date.can.not.update", new Object[] {gzhCompanyName}, Locale.SIMPLIFIED_CHINESE);
            return message;
        }
        
        referal.setReferalFeeStartDate(gzhCompanyActiveBillingStartDate);
        referal.setReferalFeeEndDate(gzhCompanyActiveBillingStartDate.plusMonths(referalService.REFERAL_FEE_ACCUMULATING_MONTHS));
        referal.setReferalFeeStatus(ReferalFeeStatus.ACCUMULATING);
        
        referalRepository.save(referal);
        String jNodeJson = objectMapper.writeValueAsString(referal);
        return jNodeJson;
    }
    
    @PreAuthorize("isWebAdmin()")
    @PostMapping("/gzhOps/api/v1/referal/{id}/referalFeeCalculate")
    public @ResponseBody String postReferalFeeCalculateApi(@PathVariable("id") long id, HttpServletResponse response) throws JsonProcessingException {
        Referal referal = referalRepository.findOne(id);
        GzhCompany gzhCompany = referal.getUser().getOwnedGzhCompany();
        if (gzhCompany == null) {
            String message = messageSource.getMessage("gzh.referal.no.owned.gzhcompany", new Object[] {referal.getUser().getUserName()}, Locale.SIMPLIFIED_CHINESE);
            return message;
        }
        
        if (referal.getReferalFeeStatus() == ReferalFeeStatus.NOT_STARTED) {
            String gzhCompanyName = gzhCompany.getName();
            String message = messageSource.getMessage("gzh.referal.not.started.can.not.recalculate", new Object[] {gzhCompanyName}, Locale.SIMPLIFIED_CHINESE);
            return message;
        }
        
        if (referal.getReferalFeeStatus() == ReferalFeeStatus.ACCOUNTING_COMPLETED) {
            String gzhCompanyName = gzhCompany.getName();
            String message = messageSource.getMessage("gzh.referal.accounting.completed.can.not.recalculate", new Object[] {gzhCompanyName}, Locale.SIMPLIFIED_CHINESE);
            return message;
        }
        
        YearMonth lastMonth = YearMonth.now().minusMonths(1);
        
        //generateAccumulatedReferalFee relies on the bills generated for the gzhCompany during the referal fee accumation period.
        //If for some reason the bill for a month is not generate, the accumulated referal fee will not be correct. 
        Referal updatedReferal = referalService.generateAccumulatedReferalFeeForReferalByEndOfYearMonth(referal, lastMonth);    
        String jNodeJson = objectMapper.writeValueAsString(updatedReferal);
        return jNodeJson;
    }
    
    @PreAuthorize("isWebAdmin()")
    @PostMapping("/gzhOps/api/v1/referal/{id}/referalFeeAccounting")
    public @ResponseBody String postReferalFeeAccountingApi(@AuthenticationPrincipal User user, @PathVariable("id") long id, HttpServletResponse response) throws JsonProcessingException {
        Referal referal = referalRepository.findOne(id);
        GzhCompany gzhCompany = referal.getUser().getOwnedGzhCompany();
        if (gzhCompany == null) {
            String message = messageSource.getMessage("gzh.referal.no.owned.gzhcompany", new Object[] {referal.getUser().getUserName()}, Locale.SIMPLIFIED_CHINESE);
            return message;
        }
        
        if (referal.getReferalFeeStatus() != ReferalFeeStatus.TOTAL_CALCULATED) {
            String gzhCompanyName = gzhCompany.getName();
            String message = messageSource.getMessage("gzh.referal.not.in.total.calculated.can.not.accounting", 
                    new Object[] {gzhCompanyName, referal.getReferalFeeStatus()}, Locale.SIMPLIFIED_CHINESE);
            return message;
        }
        
        Referal updatedReferal = referalService.changeReferalFeeStatusToAccountingCompleted(referal, user.getUserName());
        String jNodeJson = objectMapper.writeValueAsString(updatedReferal);
        return jNodeJson;
    }
    
    @PreAuthorize("isWebAdmin()")
    @PostMapping("/gzhOps/api/v1/referal/{id}/balance/adjust")
    public @ResponseBody String postReferalBalanceAdjustApi(@AuthenticationPrincipal User user, @PathVariable("id") long id, @RequestBody JsonNode json) throws JsonProcessingException {
        ReferalAccount referalAccount = referalAccountRepository.findOne(id);

        double adjustAmount = json.path("adjustAmount").asDouble();
        ReferalTransactionType txType = ReferalTransactionType.MANUAL_BALANCE_ADJUST;
        //For MANUAL_BALANCE_ADJUST, adjustNote is expected to specify what triggers the adjustment, and corresponding txRefNum if there is any.
        //The txRefNum is left empty as there is no programmatic way tell what the txRefNum mean. All the related info should be in the adjustNote.
        String adjustNote = json.path("adjustNote").asText();
        String operator = user.getUserName();
        String txRefNum = "";
        
        ReferalAccount updatedReferalAccount = referalService.adjustReferalAccountBalance(referalAccount, adjustAmount, adjustNote, txType, txRefNum, operator);
        
        String jNodeJson = objectMapper.writeValueAsString(updatedReferalAccount);
        return jNodeJson;
    }
    
    
}