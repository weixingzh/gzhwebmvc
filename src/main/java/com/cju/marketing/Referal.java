package com.cju.marketing;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.persistence.UniqueConstraint;
import com.cju.file.QrImage;
import com.cju.order.Order;
import com.cju.user.User;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

@Entity
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id", scope = Referal.class)
@Table(name = "Referal",
       uniqueConstraints = {@UniqueConstraint(columnNames={"referalNum"}, name="Referal_UNIQUE_ReferalNum"),
                            @UniqueConstraint(columnNames = "openId", name = "Referal_UNIQUE_OpenId")}
      )
public class Referal {
    @Id    
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    private int referalNum;
    private String referalLink;
    //childLevel could be 1 or 2 depending who is retrieving this referal. Both parent and grandparent can see this referal.
    @Transient
    private ReferalLevel childLevel;
  
    //private double balance;
    private double totalEarning;
    
    private double referalFeeForParent;
    private double referalFeeForGrandParent;
    private LocalDate referalFeeStartDate;
    private LocalDate referalFeeEndDate;
    private LocalDate referalFeeCurrentEndDate;
    private ReferalFeeStatus referalFeeStatus = ReferalFeeStatus.NOT_STARTED;
    
    @JsonIgnore
    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name="userId", foreignKey = @ForeignKey(name = "Referal_FK_UserId"))
    private User user;
    
    @JsonIgnore
    @OneToMany(mappedBy = "parentReferal", fetch = FetchType.LAZY)
    private List<Referal> childReferalList = new ArrayList<Referal>();
    
    @JsonIgnore
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="parentReferalId", foreignKey = @ForeignKey(name = "Referal_FK_ParentReferalId"))
    private Referal parentReferal;
    
    @JsonIgnore
    @OneToMany(mappedBy = "referal", fetch = FetchType.LAZY)
    private List<ReferalCashOut> cashOutList = new ArrayList<ReferalCashOut>();
    
    @JsonIgnore
    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name="referalAccount", foreignKey = @ForeignKey(name = "Referal_FK_ReferalAccountId"))
    private ReferalAccount referalAccount;
    
    //Fields used during application
    @Enumerated(EnumType.STRING) 
    private ReferalStatus referalStatus = ReferalStatus.NOT_APPLIED;
    private String referalStatusNote;
    //openId of the referal user with respect to GanDongTech appId.
    //openId prevents a rejected user from applying for referal using a different user account. 
    //openId is retrieved from the weixin pay notification for the referal verify order
    //If the user uses the same weixin account to verify, the openId will be the same
    private String name;
    private String openId;
    private int birthYear = 1900;
    private String address;
    private String mobile;
    private String work;
    private String selfIntro;
    private String referalPlan;
    
    @JsonIgnore
    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name="verifyOrderId", foreignKey = @ForeignKey(name = "Referal_FK_VerifyOrderId"))
    private Order verifyOrder;
    
    @JsonIgnore
    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name="qrImageId", foreignKey = @ForeignKey(name = "Referal_FK_QrImageId"))
    private QrImage qrImage;
    
    public Referal() {
        UUID uuid = UUID.randomUUID();
        //In Java, the integer is also 32 bits, but ranges from -2,147,483,648 to +2,147,483,647
        int absNum = Math.abs(uuid.toString().hashCode());
        
        //Make sure referalNum is greater than 0.
        if (absNum == 0) {
            absNum = 2147483647;
        }
        setReferalNum(absNum);
    }
    
    public void setId(Long id) {
        this.id = id;
    }
    public Long getId() {
        return id;
    }
    
    public void setReferalNum(int referalNum) {
        this.referalNum = referalNum;
    }
    public int getReferalNum() {
        return referalNum;
    }
    
    public void setChildLevel(ReferalLevel childLevel) {
        this.childLevel = childLevel;
    }
    public ReferalLevel getChildLevel() {
        return childLevel;
    }
    
    public void setReferalLink(String referalLink) {
        this.referalLink = referalLink;
    }
    public String getReferalLink() {
        return referalLink;
    }
    
    public void setTotalEarning(double totalEarning) {
        this.totalEarning = totalEarning;
    }
    public double getTotalEarning() {
        return totalEarning;
    }
    
    public void setReferalFeeForParent(double referalFeeForParent) {
        this.referalFeeForParent = referalFeeForParent;
    }
    public double getReferalFeeForParent() {
        return referalFeeForParent;
    }
    
    public void setReferalFeeForGrandParent(double referalFeeForGrandParent) {
        this.referalFeeForGrandParent = referalFeeForGrandParent;
    }
    public double getReferalFeeForGrandParent() {
        return referalFeeForGrandParent;
    }
    
    public void setReferalFeeStartDate(LocalDate referalFeeStartDate) {
        this.referalFeeStartDate = referalFeeStartDate;
    }
    public LocalDate getReferalFeeStartDate() {
        return referalFeeStartDate;
    }
    
    public void setReferalFeeEndDate(LocalDate referalFeeEndDate) {
        this.referalFeeEndDate = referalFeeEndDate;
    }
    public LocalDate getReferalFeeEndDate() {
        return referalFeeEndDate;
    }
    
    public void setReferalFeeCurrentEndDate(LocalDate referalFeeCurrentEndDate) {
        this.referalFeeCurrentEndDate = referalFeeCurrentEndDate;
    }
    public LocalDate getReferalFeeCurrentEndDate() {
        return referalFeeCurrentEndDate;
    }
    
    public void setReferalFeeStatus(ReferalFeeStatus referalFeeStatus) {
        this.referalFeeStatus = referalFeeStatus;
    }
    public ReferalFeeStatus getReferalFeeStatus() {
        return referalFeeStatus;
    }
    
    public void setUser(User user) {
        this.user = user;
    }
    public User getUser() {
        return user;
    }
    
    public void setChildReferalList(List<Referal> childReferalList) {
        this.childReferalList = childReferalList;
    }
    public List<Referal> getChildReferalList() {
        return childReferalList;
    }
    
    public void setParentReferal(Referal parentReferal) {
        this.parentReferal = parentReferal;
    }
    public Referal getParentReferal() {
        return parentReferal;
    }
    
    public void setCashOutList(List<ReferalCashOut> cashOutList) {
        this.cashOutList = cashOutList;
    }
    public List<ReferalCashOut> getCashOutList() {
        return cashOutList;
    }
    
    public void setReferalAccount(ReferalAccount referalAccount) {
        this.referalAccount = referalAccount;
    }
    public ReferalAccount getReferalAccount() {
        return referalAccount;
    }
    
    //Fields used during referal application stage
    public void setReferalStatus(ReferalStatus referalStatus) {
        this.referalStatus = referalStatus;
    }
    public ReferalStatus getReferalStatus() {
        return referalStatus;
    }
    
    public void setReferalStatusNote(String referalStatusNote) {
        this.referalStatusNote = referalStatusNote;
    }
    public String getReferalStatusNote() {
        return referalStatusNote;
    }
    
    public void setName(String name) {
        this.name = name;
    }
    public String getName() {
        return name;
    }
    
    public void setOpenId(String openId) {
        this.openId = openId;
    }
    public String getOpenId() {
        return openId;
    }
    
    public void setBirthYear(int birthYear) {
        this.birthYear = birthYear;
    }
    public int getBirthYear() {
        return birthYear;
    }
    
    public void setAddress(String address) {
        this.address = address;
    }
    public String getAddress() {
        return address;
    }
    
    public void setMobile(String mobile) {
        this.mobile = mobile;
    }
    public String getMobile() {
        return mobile;
    }
    
    public void setWork(String work) {
        this.work = work;
    }
    public String getWork() {
        return work;
    }
    
    public void setSelfIntro(String selfIntro) {
        this.selfIntro = selfIntro;
    }
    public String getSelfIntro() {
        return selfIntro;
    }
    
    public void setReferalPlan(String referalPlan) {
        this.referalPlan = referalPlan;
    }
    public String getReferalPlan() {
        return referalPlan;
    }
    
    public void setVerifyOrder(Order verifyOrder) {
        this.verifyOrder = verifyOrder;
    }
    public Order getVerifyOrder() {
        return verifyOrder;
    }
    
    public void setQrImage(QrImage qrImage) {
        this.qrImage = qrImage;
    }
    public QrImage getQrImage() {
        return qrImage;
    }
    
    @Override
    public String toString() {
        return String.format(
                "Referal[id='%s', status='%s', referalNum='%d', parentReferalId='%d']",
                id, referalStatus, referalNum, (parentReferal != null) ? parentReferal.id : 0);
    }
}