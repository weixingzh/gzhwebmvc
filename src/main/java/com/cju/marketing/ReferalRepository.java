package com.cju.marketing;

import org.springframework.data.querydsl.QueryDslPredicateExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ReferalRepository extends PagingAndSortingRepository<Referal, Long>, QueryDslPredicateExecutor<Referal> {

    Referal findOneByOpenId(String openId);

    Referal findOneByReferalNum(int parentReferalNum);

}