package com.cju.marketing;

import com.fasterxml.jackson.annotation.JsonProperty;

public enum ReferalStatus {
    @JsonProperty("未申请") NOT_APPLIED("NotApplied"),  
    @JsonProperty("等待微信支付验证") PENDING_WEIXIN_PAY_VERIFY ("PendingWeiXinPayVerify"),
    @JsonProperty("审核中") UNDER_REVIEW ("UnderReview"),
    @JsonProperty("申请退回修改") REJECTED_EDIT ("RejectedEdit"),
    @JsonProperty("申请退回终止") REJECTED_FINAL ("RejectedFinal"),
    @JsonProperty("已开通") ACTIVE ("Active"),
    @JsonProperty("已冻结") FREEZE ("Freeze"),
    @JsonProperty("未知营销用户状态") UNKNOWN_REFERAL_STATUS ("UnknownReferalStatus");

    private String status;
    
    private ReferalStatus(String status) {
        this.status = status;
    }
    
    public static ReferalStatus fromString(String statusStr) {
        switch(statusStr) {
            case "NotApplied":
                return ReferalStatus.NOT_APPLIED;
            case "PendingWeiXinPayVerify":
                return ReferalStatus.PENDING_WEIXIN_PAY_VERIFY;
            case "UnderReview":
                return ReferalStatus.UNDER_REVIEW;
            case "RejectedEdit":
                return ReferalStatus.REJECTED_EDIT;
            case "RejectedFinal":
                return ReferalStatus.REJECTED_FINAL;
            case "Active":
                return ReferalStatus.ACTIVE;
            case "Freeze":
                return ReferalStatus.FREEZE;
        }

        //Default return
        return UNKNOWN_REFERAL_STATUS;
    }
    
    @Override
    public String toString() {
        switch (status) {
            case "NotApplied": 
                return "未申请";
            case "PendingWeiXinPayVerify": 
                return "等待微信支付验证";
            case "UnderReview": 
                return "审核中";
            case "Rejected": 
                return "申请退回";
            case "Active": 
                return "已开通";
            case "Freeze": 
                return "已冻结";
            default: 
                return "未知营销用户状态";
        }
    }
}