package com.cju.marketing;

import org.springframework.data.querydsl.QueryDslPredicateExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ReferalCashOutRepository extends PagingAndSortingRepository<ReferalCashOut, Long>, QueryDslPredicateExecutor<ReferalCashOut> {

    ReferalCashOut findOneByRequestNum(Integer valueOf);

}