package com.cju.marketing;

import com.fasterxml.jackson.annotation.JsonProperty;

public enum ReferalFeeStatus {
    @JsonProperty("未开始累计") NOT_STARTED("NotStarted"),  
    @JsonProperty("累计中") ACCUMULATING ("Accumulating"),
    @JsonProperty("累计结束") ACCUMULATION_ENDED ("AccumulationEnded"),
    @JsonProperty("总额计算结束") TOTAL_CALCULATED ("TotalCalculated"),
    @JsonProperty("记入上两级账户") ACCOUNTING_COMPLETED ("AccountingCompleted");
    

    private String feeStatus;
    
    private ReferalFeeStatus(String feeStatus) {
        this.feeStatus = feeStatus;
    }
    
    @Override
    public String toString() {
        switch (feeStatus) {
            case "NotStarted": 
                return "未开始累计";
            case "Accumulating": 
                return "累计中";
            case "AccumulationEnded": 
                return "累计结束";
            case "TotalCalculated": 
                return "总额计算结束";
            case "AccountingCompleted": 
                return "记入上两级账户";
            default: 
                return "未知累计状态";
        }
    }
}