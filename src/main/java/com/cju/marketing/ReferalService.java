package com.cju.marketing;

import java.time.LocalDate;
import java.time.OffsetDateTime;
import java.time.YearMonth;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.UUID;

import javax.mail.MessagingException;

import org.apache.commons.math3.util.Precision;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;

import com.cju.billing.Bill;
import com.cju.billing.BillRepository;
import com.cju.file.QrImage;
import com.cju.file.QrImageRepository;
import com.cju.gzhOps.GzhCompany;
import com.cju.gzhOps.GzhCompanyRepository;
import com.cju.mail.MailService;
import com.cju.security.RepositoryQueryFilter;
import com.cju.user.User;
import com.cju.user.UserRepository;
import com.cju.util.QRCode;
import com.querydsl.core.types.Predicate;

@Service
public class ReferalService {
    private Logger log = LoggerFactory.getLogger(ReferalService.class);
    
    final public int REFERAL_FEE_ACCUMULATING_MONTHS = 6;
    
    @Value("${app.email.verify.url.host}")
    private String appEmailVerifyUrlHost;
    
    @Value("${app.referal.active.notification}")
    private boolean appReferalActiveNotification;
    
    @Autowired 
    private GzhCompanyRepository gzhCompanyRepository;
    
    @Autowired 
    private BillRepository billRepository; 
    
    @Autowired
    private UserRepository userRepository;
    
    @Autowired 
    private ReferalRepository referalRepository;
    
    @Autowired 
    private ReferalAccountRepository referalAccountRepository;
    
    @Autowired 
    private ReferalCashOutRepository referalCashOutRepository;
    
    @Autowired 
    private ReferalTransactionRepository referalTransactionRepository;
    
    @Autowired
    RepositoryQueryFilter repoQueryFilter;
    
    @Autowired 
    private QrImageRepository qrImageRepository;  
    
    @Autowired
    private MessageSource messageSource;
    
    @Autowired
    private MailService mailService;
    
    public void createReferalForUser(User user, int parentReferalNum) {
        if (user.getReferal() != null)
            return;
        
        //create a referal if the user does not have one
        //referalNum is set in the Referal constructor
        Referal referal = new Referal();
        referalRepository.save(referal);
        
        ReferalAccount referalAccount = new ReferalAccount();
        referalAccount.setReferal(referal);
        referalAccount.setInternalAccountOwnerName(user.getUserName());
        referalAccountRepository.save(referalAccount);
        
        user.setReferal(referal);
        userRepository.save(user);
        
        referal.setUser(user);
        referal.setReferalAccount(referalAccount);
        Referal parentReferal = referalRepository.findOneByReferalNum(parentReferalNum);
        if (parentReferal != null) {
            referal.setParentReferal(parentReferal);
        }
        referalRepository.save(referal);
    }
    
    //Referal fee related scheduled tasks defined in ReferalScheduledTask.java
    //At 1:30 in the morning every day of a month, 
    //    1) call updateReferalFeeAccumulatingStatus() to check and update the referalFeeStatus from NOT_STARTED to ACCUMULATING. 
    //    2) call updateReferalFeeAccumulationEndedStatus() to check and update the referalFeeStatus from ACCUMULATING to ACCUMULATION_ENDED. 
    //At 2:00 in the morning day 2 of a month, call generateAccumulatedReferalFeeByEndOfLastMonth() to generate accumulated referal fee.
    //At 3:00 in the morning day 2 of a month, call updateReferalFeeAccountingCompletedStatusupdate() to distribute referal fee to parent referal and grand parent referal.
    //     The referal status will change to ACCOUNTING_COMPLETED.
    
    //This is called at 1:30am every day as step 1
    public List<Referal> updateReferalFeeAccumulatingStatus() {
        List<Referal> referalList = new ArrayList<Referal>();
        LocalDate today = LocalDate.now();
        
        //Filter gzhCompany with TRIAL billing status and activeBillingStartDate is loe today
        Predicate accumulatingPredicate = repoQueryFilter.filterGzhCompanyToUpdateReferalFeeStatusAccumulating(today);
        Iterable<GzhCompany> gzhCompanyItr = gzhCompanyRepository.findAll(accumulatingPredicate);
        gzhCompanyItr.forEach(gzhCompany -> {
            Referal referal = gzhCompany.getOwnerUser().getReferal();
            if (referal.getReferalFeeStatus() == ReferalFeeStatus.NOT_STARTED) {
                LocalDate referalFeeStartDate = gzhCompany.getActiveBillingStartDate();
                referal.setReferalFeeStartDate(referalFeeStartDate);
                referal.setReferalFeeEndDate(referalFeeStartDate.plusMonths(REFERAL_FEE_ACCUMULATING_MONTHS));
                referal.setReferalFeeStatus(ReferalFeeStatus.ACCUMULATING);
                referalRepository.save(referal);
                referalList.add(referal);
            }
        });
        
        return referalList;
    }
    
    //This is called at 1:30am every day as step 2
    public List<Referal> updateReferalFeeAccumulationEndedStatus() {
        List<Referal> referalList = new ArrayList<Referal>();
        LocalDate today = LocalDate.now();
        Predicate finishedPredicate = repoQueryFilter.filterReferalToUpdateReferalFeeStatusAccumulationEnded(today);
        Iterable<Referal> referalItr = referalRepository.findAll(finishedPredicate);
        referalItr.forEach(referal -> {
            if (referal.getReferalFeeStatus() == ReferalFeeStatus.ACCUMULATING) {
                referal.setReferalFeeStatus(ReferalFeeStatus.ACCUMULATION_ENDED);
                referalRepository.save(referal);
                referalList.add(referal);
            }
        });
        
        return referalList;
    }
    
    //This is called at 2:00am of day 2 of every month
    public List<Referal> generateAccumulatedReferalFeeByEndOfLastMonth() {
        List<Referal> referalList = new ArrayList<Referal>();
        
        LocalDate now = LocalDate.now();
        YearMonth lastMonth = YearMonth.of(now.getYear(), now.getMonth()).minusMonths(1);
        
        //Get all referals with referalFeeStatus of ACCUMULATING or ACCUMULATION_ENDED
        Predicate predicate = repoQueryFilter.filterReferalOfAccumulatingOrAccumulationEnded();
        Iterable<Referal> referalItr = referalRepository.findAll(predicate);
        
        referalItr.forEach(referal -> {
            Referal referalWithAccumulatedFee = generateAccumulatedReferalFeeForReferalByEndOfYearMonth(referal, lastMonth);
            if (referalWithAccumulatedFee != null) {
                referalList.add(referalWithAccumulatedFee);
            }
        });
        
        return referalList;
    }
    
    //This is called at 3:00am of day 2 of every month after generateAccumulatedReferalFeeByEndOfLastMonth()
    public List<Referal> updateReferalFeeAccountingCompletedStatus(String operator) {
        List<Referal> referalList = new ArrayList<Referal>();
        Predicate finishedPredicate = repoQueryFilter.filterReferalOfTotalCalculated();
        Iterable<Referal> referalItr = referalRepository.findAll(finishedPredicate);
        referalItr.forEach(referal -> {
            Referal updatedReferal = changeReferalFeeStatusToAccountingCompleted(referal, operator);
            if (updatedReferal != null) {
                referalList.add(referal);
            }
        });
        
        return referalList;
    }
    
    public Referal changeReferalFeeStatusToAccountingCompleted(Referal referal, String operator) {
        //For safety, check again the referalFeeStatus must be TOTAL_CALCULATED
        if (referal.getReferalFeeStatus() != ReferalFeeStatus.TOTAL_CALCULATED) {
            //Do nothing. Skip this referal.
            return null;
        }
        
        Referal parentReferal = referal.getParentReferal();
        if (parentReferal != null && parentReferal.getReferalStatus() == ReferalStatus.ACTIVE) {
            double parentTotal = parentReferal.getTotalEarning();
            parentTotal = parentTotal + referal.getReferalFeeForParent();
            parentReferal.setTotalEarning(parentTotal);
            referalRepository.save(parentReferal);
            
            double adjustAmount = referal.getReferalFeeForParent();
            String gzhCompanyName = referal.getUser().getOwnedGzhCompany().getName();
            String adjustNote = messageSource.getMessage("gzh.referal.fee.accounting.from.level1.child", 
                    new Object[] {referal.getUser().getUserName(), gzhCompanyName, adjustAmount}, Locale.SIMPLIFIED_CHINESE);
            ReferalTransactionType txType = ReferalTransactionType.REFERAL_INCOME_ACCOUNTING;
            String txRefNum = Integer.toString(referal.getReferalNum());
            
            //parentReferal will be saved to DB, including the total earning fields.
            adjustReferalAccountBalance(parentReferal.getReferalAccount(), adjustAmount, adjustNote, txType, txRefNum, operator);
        }
        else {
            //For all the other cases,  
            referal.setReferalFeeForParent(0);
        }
        
        Referal grandParentReferal = null;
        if (parentReferal != null) {
            grandParentReferal = parentReferal.getParentReferal();
        }
        
        //Do the same for grandparent referal.
        if (grandParentReferal != null && grandParentReferal.getReferalStatus() == ReferalStatus.ACTIVE) {
            double grandParentTotal = grandParentReferal.getTotalEarning();
            grandParentTotal = grandParentTotal + referal.getReferalFeeForGrandParent();
            grandParentReferal.setTotalEarning(grandParentTotal);
            referalRepository.save(grandParentReferal);
            
            double adjustAmount = referal.getReferalFeeForGrandParent();
            String gzhCompanyName = referal.getUser().getOwnedGzhCompany().getName();
            String adjustNote = messageSource.getMessage("gzh.referal.fee.accounting.from.level2.child", 
                    new Object[] {referal.getUser().getUserName(), gzhCompanyName, adjustAmount}, Locale.SIMPLIFIED_CHINESE);
            ReferalTransactionType txType = ReferalTransactionType.REFERAL_INCOME_ACCOUNTING;
            String txRefNum = Integer.toString(referal.getReferalNum());
            
            //grandParentReferal will be saved to DB, including the total earning fields.
            adjustReferalAccountBalance(grandParentReferal.getReferalAccount(), adjustAmount, adjustNote, txType, txRefNum, operator);
        }
        else {
            //All other cases
            referal.setReferalFeeForGrandParent(0);
        }
        
        //In any case, always transit the referalFeeStatus from TOTAL_CALCULATED to ACCOUNTING_COMPLETED
        //In this state, the referalFeeForParent and referalFeeForGrandParent represents the actual referal fee distributed to the parent and grandparent account.
        referal.setReferalFeeStatus(ReferalFeeStatus.ACCOUNTING_COMPLETED);
        referalRepository.save(referal);
        
        return referal;
    }
    
    public Referal revertReferalFeeStatusFromAccountingCompleted(Referal referal, String operator) {
        //For safety, check again the referalFeeStatus must be ACCOUNTING_COMPLETED
        if (referal.getReferalFeeStatus() != ReferalFeeStatus.ACCOUNTING_COMPLETED) {
            //Do nothing. Skip this referal.
            return null;
        }
        
        String referalUserName = referal.getUser().getUserName();
        String referalGzhCompanyName = referal.getUser().getOwnedGzhCompany().getName();
        String txRefNum = Integer.toString(referal.getReferalNum());
        ReferalTransactionType txType = ReferalTransactionType.REFERAL_INCOME_ACCOUNTING_REVERT;
        
        Referal parentReferal = referal.getParentReferal();
        
        double referalFeeForParent = referal.getReferalFeeForParent();
        if (referalFeeForParent > 0) {
            if (parentReferal != null) {
                //Need to deduct the money from parent referal
                double parentTotal = parentReferal.getTotalEarning();
                parentReferal.setTotalEarning(Precision.round(parentTotal - referalFeeForParent, 2));
                referalRepository.save(parentReferal);
                
                //Add a transaction record 
                double adjustAmount = -referalFeeForParent;
                String adjustNote = messageSource.getMessage("gzh.referal.fee.revert.accounting.from.level1.child", 
                        new Object[] {referalUserName, referalGzhCompanyName, referalFeeForParent}, Locale.SIMPLIFIED_CHINESE);
                
                //parent ReferalAccount will be saved to DB.
                adjustReferalAccountBalance(parentReferal.getReferalAccount(), adjustAmount, adjustNote, txType, txRefNum, operator);
            }
        }
        
        double referalFeeForGrandParent = referal.getReferalFeeForGrandParent();
        if (referalFeeForGrandParent > 0) {
            //Need to deduct the money from grandparent referal
            if (parentReferal != null) {
                Referal grandParentReferal = parentReferal.getParentReferal();
                if (grandParentReferal != null) {
                    double grandParentTotal = grandParentReferal.getTotalEarning();
                    grandParentReferal.setTotalEarning(Precision.round(grandParentTotal - referalFeeForGrandParent, 2));
                    referalRepository.save(grandParentReferal);
                    
                    //Add a transaction record 
                    double adjustAmount = -referalFeeForGrandParent;
                    String adjustNote = messageSource.getMessage("gzh.referal.fee.revert.accounting.from.level2.child", 
                            new Object[] {referalUserName, referalGzhCompanyName, referalFeeForGrandParent}, Locale.SIMPLIFIED_CHINESE);
                    
                    //parent ReferalAccount will be saved to DB.
                    adjustReferalAccountBalance(grandParentReferal.getReferalAccount(), adjustAmount, adjustNote, txType, txRefNum, operator);
                }
            }
        }

        //In any case, always transit the referalFeeStatus from ACCOUNTING_COMPLETED to TOTAL_CALCULATED
        referal.setReferalFeeStatus(ReferalFeeStatus.ACCUMULATING);
        //reset the referalFeeForParent and referalFeeForGrandParent
        referal.setReferalFeeForParent(0);
        referal.setReferalFeeForGrandParent(0);
        
        referalRepository.save(referal);
        
        return referal;
    }
    
    public Referal generateAccumulatedReferalFeeForReferalByEndOfYearMonth(Referal referal, YearMonth yearMonth) {
        //For safety, check again the referalFeeStatus to prevent adding the referal income again to parent and grandparent
        if (referal.getReferalFeeStatus() == ReferalFeeStatus.ACCOUNTING_COMPLETED) {
            //Do nothing. Skip this referal.
            return null;
        }
        if (referal.getReferalFeeStatus() == ReferalFeeStatus.NOT_STARTED) {
            //Do nothing. Skip this referal.
            return null;
        }
        
        GzhCompany gzhCompany = referal.getUser().getGzhCompany();
        //There is a chance that the gzhCompany activeBillingStartDate is day 1 of the current month
        //In that case, the referalFee is not accumulating by the end of last month
        LocalDate activeBillingStartDate = gzhCompany.getActiveBillingStartDate();
        LocalDate endOfYearMonth = yearMonth.atEndOfMonth();
        if (activeBillingStartDate.isAfter(endOfYearMonth)) {
            //Do nothing. Skip this referal.
            return null;
        }
        else {
            LocalDate referalFeeEndDate = referal.getReferalFeeEndDate();
            
            double accumulatedReferalFee = calculateAccumulatedReferalFeeByEndOfMonth(gzhCompany.getId(), yearMonth, referalFeeEndDate);
            double referalFeeForParent = Precision.round(Math.min(accumulatedReferalFee * 0.30, 1000), 2);
            double referalFeeForGrandParent = Precision.round(Math.min(accumulatedReferalFee * 0.06, 200), 2);
            referal.setReferalFeeForParent(referalFeeForParent);
            referal.setReferalFeeForGrandParent(referalFeeForGrandParent);
            
            if (referalFeeEndDate.isAfter(endOfYearMonth)) {
                referal.setReferalFeeCurrentEndDate(endOfYearMonth);
                //There is a chance that the referalFeeEndDate is day 1 of the current month
                //In that case, the referalFee is not finished by the end of last month, even though the current referalFeeStatus is set to FINISHED on day 1 of this month
                //we will calculate the final accumulated referal fee for this referal next month.
            }
            else {
                //The referal has finished referal fee accumulating by end of last month
                referal.setReferalFeeCurrentEndDate(referalFeeEndDate);
                //By setting the referalFeeStatus to TOTAL_CALCULATED, the referal fee calculation is finalized and will not be calculated.
                if (referal.getReferalFeeStatus() == ReferalFeeStatus.ACCOUNTING_COMPLETED) {
                    // Do Nothing.
                    // The referal fee has already been accounted to parent referal and grand parent referal.
                    // We are simply doing a recalculation. In this case, do not revert the referal fee status 
                    // from ACCOUNTING_COMPLETED to TOTAL_CALCULATED. Otherwise, the referal fee will be added to
                    // parent referal and grand parent referal again.
                }
                else {
                    referal.setReferalFeeStatus(ReferalFeeStatus.TOTAL_CALCULATED);
                }
            }
            
            referalRepository.save(referal);
            return referal;
        }
    }
    
    
    //Usually when this function is called, the referalFeeEndDate is in the passed yearMonth.
    //But this function even works if the above condition is not true. It handles general cases.
    public double calculateAccumulatedReferalFeeByEndOfMonth(long gzhCompanyId, YearMonth yearMonth, LocalDate referalFeeEndDate) {
        double finishedCharge = 0;
        LocalDate lastDayOfYearMonth = yearMonth.atEndOfMonth();
        Predicate predicate = repoQueryFilter.filterBillByGzhCompanyIdAndUntilDate(gzhCompanyId, lastDayOfYearMonth);
        Iterator<Bill> billItr = billRepository.findAll(predicate).iterator();
        while (billItr.hasNext()) {
            Bill bill = billItr.next();
            LocalDate billStartDate = bill.getPeriodStart();
            LocalDate billEndDate = bill.getPeriodEnd();
            
            if (referalFeeEndDate.isBefore(billStartDate)) {
                // The referal has finished accumulating during the bill period.
                // Do not add the bill amount to the total
            }
            else if (referalFeeEndDate.isAfter(billEndDate)) {
                // The referal is accumulating during whole bill period
                finishedCharge = finishedCharge + bill.getAmount();
            }
            else {
                //The referalFeeEndDate is in the middle of the bill period
                //until does not include end date
                int referalDays = billStartDate.until(referalFeeEndDate).getDays() + 1;
                double partialAmount = bill.getAmount() * referalDays / yearMonth.lengthOfMonth();
                finishedCharge = finishedCharge + partialAmount;
            }
        }
        return finishedCharge;
    }
    
    public ReferalAccount adjustReferalAccountBalance(ReferalAccount referalAccount, double adjustAmount, String adjustNote, 
        ReferalTransactionType txType, String txRefNum, String operator) {
        
        //create a transaction to record the account balance change
        double oldBalance = referalAccount.getBalance();
        //adjustAmount can be negative 
        double newBalance = Precision.round(oldBalance + adjustAmount, 2);
        ReferalTransaction transaction = new ReferalTransaction();
        transaction.setReferalAccount(referalAccount);
        transaction.setAccountBalanceBefore(oldBalance);
        transaction.setAccountBalanceAfter(newBalance);
        transaction.setTransactionAmount(adjustAmount);
        transaction.setNote(adjustNote);
        transaction.setTimeStamp(OffsetDateTime.now());
        UUID txId = UUID.randomUUID(); 
        transaction.setTxId(txId);
        transaction.setTransactionType(txType);
        transaction.setTransactionRefNum(txRefNum);
        transaction.setInternalReferalAccountOwnerName(referalAccount.getInternalAccountOwnerName());
        transaction.setOperator(operator);
        referalTransactionRepository.save(transaction);
        log.info("adjustReferalAccountBalance generated transaction=" + transaction);
        
        //If there is an exception in saving the transaction, such as duplicate txRefNum, the account is not updated.
        //This helped avoid adjusting the account more than once with the same txRefNum
        
        //Now we need to adjust the account balance
        referalAccount.setBalance(newBalance);
        //Do not need to update the Many-To-One relationship to transaction
        //Many-To-One is maintained from the Many side
        referalAccount = referalAccountRepository.save(referalAccount);
        log.info("adjustReferalAccountBalance Adjusted account balance for referalAccount=" + referalAccount);
        
        return referalAccount;
    }
    
    public Referal updateReferalStatus(Referal referal, ReferalStatus newReferalStatus, String referalStatusNote) {
        ReferalStatus oldReferalStatus = referal.getReferalStatus();
        if ( oldReferalStatus != ReferalStatus.ACTIVE && newReferalStatus == ReferalStatus.ACTIVE) {
            //referalStatus transit from Non-ACTIVE to ACTIVE
            //generate a QrImage
            String qrCodeUrl = appEmailVerifyUrlHost + "/pub/referal/intro?referalNum=" + referal.getReferalNum();
            byte[] qrCodeImg = QRCode.generateImg(qrCodeUrl, 128, 128, "jpg"); 
            QrImage qrImage = new QrImage();
            qrImage.setData(qrCodeImg);
            qrImage = qrImageRepository.save(qrImage);
            
            //For now, only the unidirectional one-to-one from referal to qrImage.
            referal.setQrImage(qrImage);
            
            //set the referalLink
            String referalLink = appEmailVerifyUrlHost + "/pub/referal/intro?referalNum=" + referal.getReferalNum();
            referal.setReferalLink(referalLink);
            
            //schedule to send an email to the user. The email is sent async by a SharedPoolTaskExecutor.
            if (appReferalActiveNotification) {
                try {
                    mailService.sendReferalStatusActiveNotification(referal);
                } catch (MessagingException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }
        }
        
        referal.setReferalStatus(newReferalStatus);
        referal.setReferalStatusNote(referalStatusNote);
        Referal retReferal = referalRepository.save(referal);
        return retReferal;
    }
    
    public ReferalCashOut updateReferalCashOutStatusWithSuccessMoneyTransfer(ReferalCashOut referalCashOut, ReferalCashOutStatus referalCashOutStatus, 
            String bankTxRefNum, String note, OffsetDateTime statusChangeTime, String operator) {
        
        ReferalCashOutStatus currReferalCashOutStatus = referalCashOut.getReferalCashOutStatus();
        if (currReferalCashOutStatus != ReferalCashOutStatus.PROCESSING ||
            referalCashOutStatus != ReferalCashOutStatus.COMPLETED) {
            //This is an error case.
            log.error("updateReferalCashOutStatusWithSuccessMoneyTransfer, referalCashOut=" + referalCashOut + ", new referalCashOutStatus=" + referalCashOutStatus);
            return null;
        }
            
        //Make a ReferalTransaction to record the referal account balance change
        ReferalTransaction referalTransaction = new ReferalTransaction();
        UUID txId = UUID.randomUUID(); 
        referalTransaction.setTxId(txId);
        double transactionAmount = referalCashOut.getAmount();
        referalTransaction.setTransactionAmount(transactionAmount);
        ReferalAccount referalAccount = referalCashOut.getReferal().getReferalAccount();
        double accountBalanceBefore = referalAccount.getBalance();
        double accountBalanceAfter = accountBalanceBefore - transactionAmount;
        referalTransaction.setAccountBalanceBefore(accountBalanceBefore);
        referalTransaction.setAccountBalanceAfter(accountBalanceAfter);
        referalTransaction.setTransactionType(ReferalTransactionType.BANK_CASH_OUT);
        int referalCashOutRequestNum = referalCashOut.getRequestNum();
        referalTransaction.setTransactionRefNum(String.valueOf(referalCashOutRequestNum));
        referalTransaction.setTimeStamp(statusChangeTime);
        referalTransaction.setNote(note);
        referalTransaction.setOperator(operator);
        referalTransaction.setReferalAccount(referalAccount);
        referalTransactionRepository.save(referalTransaction);
        
        //After transaction is persisted, adjust referal account balance
        referalAccount.setBalance(accountBalanceAfter);
        referalAccountRepository.save(referalAccount);
        
        //Finally, update the referalCashOut
        referalCashOut.setReferalCashOutStatus(referalCashOutStatus);
        referalCashOut.setBankTxRefNum(bankTxRefNum);
        referalCashOut.setNote(note);
        referalCashOut.setStatusChangeTime(statusChangeTime);
        referalCashOut.setOperator(operator);
        referalCashOutRepository.save(referalCashOut);
        
        return referalCashOut;
    }
    
    public ReferalCashOut updateReferalCashOutFields(ReferalCashOut referalCashOut, ReferalCashOutStatus referalCashOutStatus, 
            String bankTxRefNum, String note, OffsetDateTime statusChangeTime, String operator) {
        
        ReferalCashOutStatus currReferalCashOutStatus = referalCashOut.getReferalCashOutStatus();
        if (referalCashOutStatus == ReferalCashOutStatus.COMPLETED) {
            //This is an error case.
            log.error("updateReferalCashOutFields, referalCashOut=" + referalCashOut + ", new referalCashOutStatus=" + referalCashOutStatus);
            return null;
        }
            
        //Just update the referalCashOut fields
        referalCashOut.setReferalCashOutStatus(referalCashOutStatus);
        referalCashOut.setBankTxRefNum(bankTxRefNum);
        referalCashOut.setNote(note);
        if (currReferalCashOutStatus != referalCashOutStatus) {
            referalCashOut.setStatusChangeTime(statusChangeTime);
            referalCashOut.setOperator(operator);
        }
        referalCashOutRepository.save(referalCashOut);
        
        return referalCashOut;
    }
    
}