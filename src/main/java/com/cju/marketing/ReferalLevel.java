package com.cju.marketing;

import com.fasterxml.jackson.annotation.JsonProperty;

public enum ReferalLevel {
    @JsonProperty("我自己") SELF("Self"),  
    @JsonProperty("下一级") LEVEL1("Level1"),  
    @JsonProperty("下二级") LEVEL2("Level2"),
    @JsonProperty("无层级关系") UNDER_ADMIN("UnderAdmin");

    private String level;
    
    private ReferalLevel(String level) {
        this.level = level;
    }
    
    @Override
    public String toString() {
        switch (level) {
            case "Self": 
                return "我自己";
            case "Level1": 
                return "下一级";
            case "Level2": 
                return "下二级";
            case "UnderAdmin": 
                return "无层级关系";
            default: 
                return "未知级别";
        }
    }
}