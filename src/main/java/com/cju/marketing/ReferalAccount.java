package com.cju.marketing;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

@Entity
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id", scope = ReferalAccount.class)
@Table(name = "ReferalAccount")
public class ReferalAccount {
    @Id    
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    
    private double balance = 0;
    
    //internalAccountOwnerName is set to the userName of referal owner when the account is created.
    //This information looks duplicate and is kept in DB for internal use only.
    private String internalAccountOwnerName;
    
    //The gzhcompany associated with this account
    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name="referalId", foreignKey = @ForeignKey(name = "ReferalAccount_FK_ReferalId"))
    private Referal referal;
    
    @OneToMany(mappedBy = "referalAccount", fetch = FetchType.LAZY)
    private List<ReferalTransaction> referalTransactionList = new ArrayList<ReferalTransaction>();
        
    public ReferalAccount() {}
    
    public void setId(long id) {
        this.id = id;
    }
    public long getId() {
        return id;
    }
    
    public void setBalance(double balance) {
        this.balance = balance;
    }
    public double getBalance() {
        return balance;
    }
    
    public void setInternalAccountOwnerName(String internalAccountOwnerName) {
        this.internalAccountOwnerName = internalAccountOwnerName;
    }
    public String getInternalAccountOwnerName() {
        return internalAccountOwnerName;
    }
    
    public void setReferal(Referal referal) {
        this.referal = referal;
    }
    public Referal getReferal() {
        return referal;
    }

    public void setReferalTransactionList(List<ReferalTransaction> referalTransactionList) {
        this.referalTransactionList = referalTransactionList;
    }
    public List<ReferalTransaction> getReferalTransactionList() {
        return referalTransactionList;
    }
    
    @Override
    public String toString() {
        return String.format(
                "ReferalAccount[id=%s, internalAccountName='%s', accountBalance='%f']",
                id, internalAccountOwnerName, balance);
    }
}
