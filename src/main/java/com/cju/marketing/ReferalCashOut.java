package com.cju.marketing;

import java.time.OffsetDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

@Entity
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id", scope = ReferalCashOut.class)
@Table(name = "ReferalCashOut")
public class ReferalCashOut {
    @Id    
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    
    private int    requestNum;
    private double amount;
    private String receipientName;
    private String bankName;
    private String bankBranchName;
    private String bankProvince;
    private String bankCity;
    private String bankAccountNum;
    @Column(columnDefinition= "TIMESTAMP WITH TIME ZONE")
    private OffsetDateTime submitTime;
    @Enumerated(EnumType.STRING) 
    private ReferalCashOutStatus referalCashOutStatus;
    @Column(columnDefinition= "TIMESTAMP WITH TIME ZONE")
    private OffsetDateTime statusChangeTime;
    private String bankTxRefNum;
    private String note;
    private String operator;
    
    @JsonIgnore
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="referalId", foreignKey = @ForeignKey(name = "ReferalCashOut_FK_ReferalId"))
    private Referal referal;
    
    public ReferalCashOut() {
    }
    
    public void setId(Long id) {
        this.id = id;
    }
    public Long getId() {
        return id;
    }
    
    public void setRequestNum(int requestNum) {
        this.requestNum = requestNum;
    }
    public int getRequestNum() {
        return requestNum;
    }
    
    public void setAmount(double amount) {
        this.amount = amount;
    }
    public double getAmount() {
        return amount;
    }
    
    public void setReceipientName(String receipientName) {
        this.receipientName = receipientName;
    }
    public String getReceipientName() {
        return receipientName;
    }
    
    public void setBankName(String bankName) {
        this.bankName = bankName;
    }
    public String getBankName() {
        return bankName;
    }
    
    public void setBankBranchName(String bankBranchName) {
        this.bankBranchName = bankBranchName;
    }
    public String getBankBranchName() {
        return bankBranchName;
    }
    
    public void setBankProvince(String bankProvince) {
        this.bankProvince = bankProvince;
    }
    public String getBankProvince() {
        return bankProvince;
    }
    
    public void setBankCity(String bankCity) {
        this.bankCity = bankCity;
    }
    public String getBankCity() {
        return bankCity;
    }
    
    public void setBankAccountNum(String bankAccountNum) {
        this.bankAccountNum = bankAccountNum;
    }
    public String getBankAccountNum() {
        return bankAccountNum;
    }
    
    public void setSubmitTime(OffsetDateTime submitTime) {
        this.submitTime = submitTime;
    }
    public OffsetDateTime getSubmitTime() {
        return submitTime;
    }
    
    public void setReferalCashOutStatus(ReferalCashOutStatus referalCashOutStatus) {
        this.referalCashOutStatus = referalCashOutStatus;
    }
    public ReferalCashOutStatus getReferalCashOutStatus() {
        return referalCashOutStatus;
    }
    
    public void setStatusChangeTime(OffsetDateTime statusChangeTime) {
        this.statusChangeTime = statusChangeTime;
    }
    public OffsetDateTime getStatusChangeTime() {
        return statusChangeTime;
    }

    public void setBankTxRefNum(String bankTxRefNum) {
        this.bankTxRefNum = bankTxRefNum;
    }
    public String getBankTxRefNum() {
        return bankTxRefNum;
    }
    
    public void setNote(String note) {
        this.note = note;
    }
    public String getNote() {
        return note;
    }
    
    public void setOperator(String operator) {
        this.operator = operator;
    }
    public String getOperator() {
        return operator;
    }
    
    public void setReferal(Referal referal) {
        this.referal = referal;
    }
    public Referal getReferal() {
        return referal;
    }
    
    @Override
    public String toString() {
        return String.format(
                "ReferalCashOut[id='%s', receipient='%s', amount='%d', status='%s']",
                id, receipientName, amount, referalCashOutStatus);
    }
}