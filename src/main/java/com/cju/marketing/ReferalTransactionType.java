package com.cju.marketing;

import com.fasterxml.jackson.annotation.JsonProperty;

public enum ReferalTransactionType {
    @JsonProperty("营销收入记账") REFERAL_INCOME_ACCOUNTING ("ReferalIncomeAccounting"),
    @JsonProperty("营销收入记账回退") REFERAL_INCOME_ACCOUNTING_REVERT ("ReferalIncomeAccountingRevert"),
    @JsonProperty("银行提现") BANK_CASH_OUT("BankCashOut"),  
    @JsonProperty("手工余额调整") MANUAL_BALANCE_ADJUST ("ManualBalanceAdjust");
    private String type;
    
    private ReferalTransactionType(String type) {
        this.type = type;
    }
    
    public static ReferalTransactionType fromString(String statusStr) {
        switch(statusStr) {
            case "ReferalIncomeAccounting":
                return ReferalTransactionType.REFERAL_INCOME_ACCOUNTING;
            case "ReferalIncomeAccountingRevert":
                return ReferalTransactionType.REFERAL_INCOME_ACCOUNTING_REVERT;
            case "BankCashOut":
                return ReferalTransactionType.BANK_CASH_OUT;
            case "ManualBalanceAdjust":
                return ReferalTransactionType.MANUAL_BALANCE_ADJUST;
        }

        //Default return
        return null;
    }
    
    @Override
    public String toString() {
        switch (type) {
            case "ReferalIncomeAccounting": 
                return "营销收入记账";
            case "ReferalIncomeAccountingRevert": 
                return "营销收入记账回退";
            case "BankCashOut": 
                return "银行提现";
            case "ManualBalanceAdjust": 
                return "手工余额调整";
            default: 
                return "未知交易类型";
        }
    }
}