package com.cju.marketing;

import java.time.OffsetDateTime;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

@Entity
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id", scope = ReferalTransaction.class)
@Table(name = "ReferalTransaction")
public class ReferalTransaction {
    @Id    
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    
    private UUID txId;
    @Column(columnDefinition= "TIMESTAMP WITH TIME ZONE")
    private OffsetDateTime timeStamp;
    @Enumerated(EnumType.STRING) 
    private ReferalTransactionType transactionType;
    private String transactionRefNum;
    private double transactionAmount;
    private double accountBalanceBefore;
    private double accountBalanceAfter;
    private String operator;
    //This is set to the internalReferalUserName of the associated referal when the transaction record is create.
    //This information looks duplicate and is kept in DB for internal use only.
    private String internalReferalAccountOwnerName;
    
    //text has no size limit
    @Column(columnDefinition = "text")
    private String note;
        
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="referalAccountId", foreignKey = @ForeignKey(name = "ReferalTransaction_FK_ReferalAccountId"))
    private ReferalAccount referalAccount;
    
    public ReferalTransaction() {}
    
    public void setId(long id) {
        this.id = id;
    }
    public long getId() {
        return id;
    }
    
    public void setTxId(UUID txId) {
        this.txId = txId;
    }
    public UUID getTxId() {
        return txId;
    }
    
    public void setTimeStamp(OffsetDateTime timeStamp) {
        this.timeStamp = timeStamp;
    }
    public OffsetDateTime getTimeStamp() {
        return timeStamp;
    }
    
    public void setTransactionType(ReferalTransactionType transactionType) {
        this.transactionType = transactionType;
    }
    public ReferalTransactionType getTransactionType() {
        return transactionType;
    }
    
    public void setTransactionRefNum(String transactionRefNum) {
        this.transactionRefNum = transactionRefNum;
    }
    public String getTransactionRefNum() {
        return transactionRefNum;
    }
    
    public void setTransactionAmount(double transactionAmount) {
        this.transactionAmount = transactionAmount;
    }
    public double getTransactionAmount() {
        return transactionAmount;
    }
    
    public void setAccountBalanceBefore(double accountBalanceBefore) {
        this.accountBalanceBefore = accountBalanceBefore;
    }
    public double getAccountBalanceBefore() {
        return accountBalanceBefore;
    }
    
    public void setAccountBalanceAfter(double accountBalanceAfter) {
        this.accountBalanceAfter = accountBalanceAfter;
    }
    public double getAccountBalanceAfter() {
        return accountBalanceAfter;
    }
    
    public void setOperator(String operator) {
        this.operator = operator;
    }
    public String getOperator() {
        return operator;
    }
    
    public void setInternalReferalAccountOwnerName(String internalReferalAccountOwnerName) {
        this.internalReferalAccountOwnerName = internalReferalAccountOwnerName;
    }
    public String getInternalReferalAccountOwnerName() {
        return internalReferalAccountOwnerName;
    }
    
    public void setNote(String note) {
        this.note = note;
    }
    public String getNote() {
        return note;
    }
    
    public void setReferalAccount(ReferalAccount referalAccount) {
        this.referalAccount = referalAccount;
    }
    public ReferalAccount getReferalAccount() {
        return referalAccount;
    }
    
    @Override
    public String toString() {
        return String.format(
                "ReferalTransaction[id=%s, timeStamp='%s', transactionType='%s', transactionAmount='%f', accountBalanceBefore='%f', accountBalanceAfter='%f']",
                id, timeStamp, transactionType, transactionAmount, accountBalanceBefore, accountBalanceAfter);
    }

}
