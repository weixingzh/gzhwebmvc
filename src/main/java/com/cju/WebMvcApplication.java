package com.cju;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.scheduling.annotation.EnableScheduling;

//To build a jar to run stand alone with java -jar jarname.jar, 
//it is not needed to extend SpringBootServletInitializer and override the configure method.

//To build a war to run in CloudFoundry or BlueMix
//The first step in producing a deployable war file is to provide a SpringBootServletInitializer subclass
//and override its configure method. This makes use of Spring Frameworks Servlet 3.0 support and
//allows you to configure your application when its launched by the servlet container.
//Typically, you update your applications main class to extend SpringBootServletInitializer:

//@SpringBootApplication annotation provides a load of defaults (like the embedded servlet container) depending on the contents 
//of your classpath, and other things. It also turns on Spring MVC @EnableWebMvc annotation that activates web endpoints.
//@SpringBootApplication also brings in a @ComponentScan, which tells Spring to scan the package for those controllers 
//(along with any other annotated component classes).

//@SpringBootApplication is a convenience annotation that adds all of the following:
//    @Configuration tags the class as a source of bean definitions for the application context.
//    @EnableAutoConfiguration tells Spring Boot to start adding beans based on classpath settings, other beans, and various property settings.
//    @EnableWebMvc when spring-webmvc is on the classpath. This flags the application as a web application and activates key behaviors such as setting up a DispatcherServlet.
//    @ComponentScan tells Spring to look for other components, configurations, and services in the hello package, allowing it to find the controllers.

//By default, Spring Boot will enable JPA repository support and look in the package (and its subpackages) where @SpringBootApplication is located. 
//If your configuration has JPA repository interface definitions located in a package not visible, you can point out alternate packages using @EnableJpaRepositories and 
//its type-safe basePackageClasses=MyRepository.class parameter.
@SpringBootApplication
public class WebMvcApplication extends SpringBootServletInitializer {
	private static final Logger log = LoggerFactory.getLogger(WebMvcApplication.class);
	
    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(WebMvcApplication.class);
    }
    
	public static void main(String[] args) {
		log.info("Application starts running!");
		SpringApplication.run(WebMvcApplication.class, args);
	}
}


