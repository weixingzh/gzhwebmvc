package com.cju;

import java.time.OffsetDateTime;
import java.time.temporal.ChronoUnit;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.cju.stats.TaskTime;
import com.cju.stats.TaskTimeRepository;

@Aspect
@Component
public class WebMvcAspect {
    private static final Logger log = LoggerFactory.getLogger(WebMvcAspect.class);
    
    @Autowired
    TaskTimeRepository taskTimeRepository;
    
    //The value returned by the around advice will be the return value seen by the caller of the method. 
    @SuppressWarnings("unchecked")
    @Around("execution(public java.util.Map<String, String> com.cju.WebMvcTask.*(..))")
    public Object logTaskTime(final ProceedingJoinPoint proceedingJoinPoint) throws Throwable {
        log.info("logTaskTime called on task: " + proceedingJoinPoint.toString());
        OffsetDateTime startTime = OffsetDateTime.now();
        Object retVal = proceedingJoinPoint.proceed();
        OffsetDateTime endTime = OffsetDateTime.now();
        
        long totalTime = startTime.until(endTime, ChronoUnit.MILLIS);
        long minutes = TimeUnit.MILLISECONDS.toMinutes(totalTime);
        long seconds = TimeUnit.MILLISECONDS.toSeconds(totalTime) - 60 * minutes;
        long millis = totalTime - 1000 * seconds;
        String duration = String.format("%d min, %d sec, %d mills", minutes, seconds, millis);
        
        Map<String, String> taskResult = (Map<String, String>)retVal;
        String taskName = taskResult.get("taskName");
        String note = taskResult.get("note");
        int itemCount = Integer.valueOf(taskResult.get("itemCount"));
        
        TaskTime taskTime = new TaskTime();
        taskTime.setTaskName(taskName);
        taskTime.setStartTime(startTime);
        taskTime.setEndTime(endTime);
        taskTime.setDuration(duration);
        taskTime.setNote(note);
        taskTime.setItemCount(itemCount);
        taskTimeRepository.save(taskTime);
        
        return retVal;
    }
}
