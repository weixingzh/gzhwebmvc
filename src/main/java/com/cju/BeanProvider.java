package com.cju;

import javax.annotation.PostConstruct;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Component;

import com.cju.billing.BillRepository;
import com.cju.billing.BillingStatusChangeRepository;
import com.cju.billing.TransactionRepository;
import com.cju.file.QrImageRepository;
import com.cju.gzh.WeiXinService;
import com.cju.gzhOps.GzhClientCompanyReportRepository;
import com.cju.gzhOps.GzhClientCompanyRepository;
import com.cju.gzhOps.GzhHtmlPageRepository;
import com.cju.gzhOps.GzhCompanyRepository;
import com.cju.gzhOps.GzhTaskRepository;
import com.cju.gzhOps.GzhUserRepository;
import com.cju.marketing.ReferalCashOutRepository;
import com.cju.marketing.ReferalRepository;
import com.cju.marketing.ReferalTransactionRepository;
import com.cju.order.OrderRepository;
import com.cju.security.RepositoryQueryFilter;
import com.cju.user.UserRepository;

//BeanProvider provides bean instance access from POJO that not managed by Spring.
//WebMvcSecurityMethodSecurityExpressionRoot is instantiated with a different Authentication instance for a @PreAuthorize call.
//Thus WebMvcSecurityMethodSecurityExpressionRoot can not be a bean and it is not managed by Spring.
//In GzhOpsController.java, we have following use case of @PreAuthorize
//    @PreAuthorize("isMemberOf(#id) || isWebAdmin()")
//    @GetMapping("/gzhCompany/details/{id}")
//    public String getGzhCompanyDetails(@PathVariable("id") long id, Model model) { ... }

@Component
public class BeanProvider implements ApplicationContextAware{
    private Logger log = LoggerFactory.getLogger(BeanProvider.class);
    private static ApplicationContext context;
    //Can not autowire a static field. 
    private static UserRepository userRepository;
    private static GzhUserRepository gzhUserRepository;
    private static GzhTaskRepository gzhTaskRepository;
    private static GzhCompanyRepository gzhCompanyRepository;
    private static GzhHtmlPageRepository gzhHtmlPageRepository;
    private static GzhClientCompanyRepository gzhClientCompanyRepository;
    private static GzhClientCompanyReportRepository gzhClientCompanyReportRepository;
    private static BillRepository billRepository;
    private static BillingStatusChangeRepository billingStatusChangeRepository;
    private static OrderRepository orderRepository;
    private static QrImageRepository qrImageRepository;
    private static TransactionRepository transactionRepository;
    private static RepositoryQueryFilter repositoryQueryFilter;
    private static WeiXinService weiXinService;
    private static ReferalRepository referalRepository;
    private static ReferalTransactionRepository referalTransactionRepository;
    private static ReferalCashOutRepository referalCashOutRepository;
    
    @Override
    public void setApplicationContext(ApplicationContext ctx) {
        context = ctx;
    }
   
    public static ApplicationContext getApplicationContext() {
        return context;
    }
    
    @PostConstruct
    void init() {
        log.info("@PostConstruct init() called!");
        userRepository = context.getBean(UserRepository.class);
        gzhUserRepository = context.getBean(GzhUserRepository.class);
        gzhTaskRepository = context.getBean(GzhTaskRepository.class);
        gzhCompanyRepository = context.getBean(GzhCompanyRepository.class);
        gzhHtmlPageRepository = context.getBean(GzhHtmlPageRepository.class);
        gzhClientCompanyRepository = context.getBean(GzhClientCompanyRepository.class);
        gzhClientCompanyReportRepository = context.getBean(GzhClientCompanyReportRepository.class);
        billRepository = context.getBean(BillRepository.class);
        billingStatusChangeRepository = context.getBean(BillingStatusChangeRepository.class);
        orderRepository = context.getBean(OrderRepository.class);
        qrImageRepository = context.getBean(QrImageRepository.class);
        transactionRepository = context.getBean(TransactionRepository.class);
        repositoryQueryFilter = context.getBean(RepositoryQueryFilter.class);
        weiXinService = context.getBean(WeiXinService.class);
        referalRepository = context.getBean(ReferalRepository.class);
        referalTransactionRepository = context.getBean(ReferalTransactionRepository.class);
        referalCashOutRepository = context.getBean(ReferalCashOutRepository.class);
    }
    
    public static UserRepository getUserRepository() {
        return userRepository;
    }
    public static GzhUserRepository getGzhUserRepository() {
        return gzhUserRepository;
    }
    public static GzhTaskRepository getGzhTaskRepository() {
        return gzhTaskRepository;
    }
    public static GzhCompanyRepository getGzhCompanyRepository() {
        return gzhCompanyRepository;
    }
    public static GzhHtmlPageRepository getGzhHtmlPageRepository() {
        return gzhHtmlPageRepository;
    }
    public static GzhClientCompanyRepository getGzhClientCompanyRepository() {
        return gzhClientCompanyRepository;
    }
    public static GzhClientCompanyReportRepository getGzhClientCompanyReportRepository() {
        return gzhClientCompanyReportRepository;
    }    
    public static BillRepository getBillRepository() {
        return billRepository;
    }    
    public static BillingStatusChangeRepository getBillingStatusChangeRepository() {
        return billingStatusChangeRepository;
    }    
    public static OrderRepository getOrderRepository() {
        return orderRepository;
    }    
    public static QrImageRepository getQrImageRepository() {
        return qrImageRepository;
    }    
    public static TransactionRepository getTransactionRepository() {
        return transactionRepository;
    }    
    public static RepositoryQueryFilter getRepositoryQueryFilter() {
        return repositoryQueryFilter;
    } 
    public static WeiXinService getWeiXinService() {
        return weiXinService;
    } 
    public static ReferalRepository getReferalRepository() {
        return referalRepository;
    }    
    public static ReferalTransactionRepository getReferalTransactionRepository() {
        return referalTransactionRepository;
    }    
    public static ReferalCashOutRepository getReferalCashOutRepository() {
        return referalCashOutRepository;
    }    
}
