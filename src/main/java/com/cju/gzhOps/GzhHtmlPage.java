package com.cju.gzhOps;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

@Entity
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id", scope = GzhHtmlPage.class)
@Table(name = "GzhHtmlPage",
       uniqueConstraints = { @UniqueConstraint(columnNames={"gzhCompanyId", "title"}, name="GzhHtmlPage_UNIQUE_GzhCompanyAndTitle")})
public class GzhHtmlPage {
    @Transient
    final int HTML_PAGE_SIZE = 10 * 1024 * 1024;
	@Transient
	private Logger log = LoggerFactory.getLogger(GzhHtmlPage.class);

	//AUTO strategy uses the global number generator to generate a primary key for every new entity object
	//IDENTIFY strategy uses a separate identity generator per type hierarchy, so generated values are unique only per type hierarchy.
	//SEQUENCE strategy generates an automatic value as soon as a new entity object is persisted (i.e. before commit)
	//@SequenceGenerator(name="seq", initialValue=1, allocationSize=100)
	//@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="seq")
    @Id    
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    
    private String title;
    
    @NotNull
    @Enumerated(EnumType.STRING)
    private GzhHtmlPageType pageType;
    
    @NotNull
    @Enumerated(EnumType.STRING)
    private GzhHtmlPageLinkType linkType = GzhHtmlPageLinkType.EXTERNAL_LINK;
    
    private String externalLink;
    
    //Html text for LOCAL_HTML
    //database column type is "text" which does not have size limit.
    @Column(length=HTML_PAGE_SIZE)
    private String localHtml;
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="gzhCompanyId", foreignKey = @ForeignKey(name = "GzhHtmlPage_FK_GzhCompanyId"))
    private GzhCompany  gzhCompany;  

    public GzhHtmlPage() {}
    public GzhHtmlPage(long id) {
    	this.id = id;
    }

    public void setId(long id) {
    	this.id = id;
    }
    public long getId() {
    	return id;
    }

    public void setTitle(String title) {
        this.title = title;
    }
    public String getTitle() {
        return title;
    }
    
    public void setPageType(GzhHtmlPageType pageType) {
        this.pageType = pageType;
    }
    public GzhHtmlPageType getPageType() {
        return pageType;
    }
    
    public void setLinkType(GzhHtmlPageLinkType linkType) {
    	this.linkType = linkType;
    }
    public GzhHtmlPageLinkType getLinkType() {
    	return linkType;
    }
    
    public void setExternalLink(String externalLink) {
        this.externalLink = externalLink;
    }
    public String getExternalLink() {
        return externalLink;
    }
    
    public void setLocalHtml(String localHtml) {
        this.localHtml = localHtml;
    }
    public String getLocalHtml() {
        return localHtml;
    }
    
    public void setGzhCompany(GzhCompany gzhCompany) {
        this.gzhCompany = gzhCompany;
    }
    public GzhCompany getGzhCompany() {
        return gzhCompany;
    }
    
    @Override
    public String toString() {
        return String.format("GzhHtmlPage[id=%d,  pageType='%s', linkType='%s']", id,  pageType, linkType);
    }
}