package com.cju.gzhOps;

import java.util.List;

public class GzhAutoReplyHttpPostData {
    private List<GzhAutoReply> createOrEditList;
    private List<Long> removeList;
    
    public void setCreateOrEditList(List<GzhAutoReply> createOrEditList) {
        this.createOrEditList = createOrEditList;
    }
    public List<GzhAutoReply> getCreateOrEditList() {
        return createOrEditList;
    }
    
    public void setRemoveList(List<Long> removeList) {
        this.removeList = removeList;
    }
    public List<Long> getRemoveList() {
        return removeList;
    }
    
}