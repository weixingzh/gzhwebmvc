package com.cju.gzhOps;

import com.fasterxml.jackson.annotation.JsonProperty;

public enum GzhHtmlPageLinkType {
    @JsonProperty("外部链接") EXTERNAL_LINK("ExternalLink"), @JsonProperty("本地网页") LOCAL_HTML("LocalHtml");
    private String htmlPageLinkType;
    private GzhHtmlPageLinkType(String htmlPageLinkType) {
        this.htmlPageLinkType = htmlPageLinkType;
    }
    
    public static GzhHtmlPageLinkType fromString(String str) {
        for (GzhHtmlPageLinkType type: GzhHtmlPageLinkType.values()) {
            if (type.htmlPageLinkType.equals(str)) {
                return type;
            }
        }
        
        //Default return
        return EXTERNAL_LINK;
    }
    
    @Override
    public String toString() {
        switch (htmlPageLinkType) {
            case "ExternalLink": 
                return "外部链接";
            case "LocalHtml":
                return "本地网页";
            default: 
                return "外部链接";
        }
    }
}
