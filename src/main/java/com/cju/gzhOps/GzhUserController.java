package com.cju.gzhOps;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import javax.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import com.cju.security.RepositoryQueryFilter;
import com.cju.user.User;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.querydsl.core.types.Predicate;

//OpsController is for providing OPS to accounting Company.
//We need to associate a user and a client company to the user's openId for this gzh. Since the openId is only known 
//when user access the website through his weixin client, the user-creation and client-company-creation functionality
//are provided via weixin gzh UI, not here.

@Controller
public class GzhUserController {
    private Logger log = LoggerFactory.getLogger(GzhUserController.class);
    
    //gzh user who subscribes to gzh. gzh user can not login to our gzhOps website.
    @Autowired 
    private GzhUserRepository gzhUserRepository; 
    
    @Autowired 
    private GzhClientCompanyRepository gzhClientCompanyRepository; 
    
    @Autowired 
    RepositoryQueryFilter repoQueryFilter;
    
    @Autowired
    private ObjectMapper objectMapper;
        
    //When a user subscribes to gzh, we will receive a subscribe message from weixin server containing FromUserName and ToUserName.
    //These messages from weixin server are handled in GzhController. When we receive a subscribe message, we will create a user
    //by calling gzhUserRepository.save(). So we do not have GzhUserCreate in this GzhOpsController.java file.
    @PreAuthorize("isAuthenticated()")
    @GetMapping("/gzhOps/gzhUser/list")
    public String getGzhUserList(@AuthenticationPrincipal User user, Pageable pageable, Model model) {  
        Predicate predicate = repoQueryFilter.filterGzhUserByUser(user);
        Page<GzhUser> gzhUserPage = gzhUserRepository.findAll(predicate, pageable);
        model.addAttribute("gzhUserPage", gzhUserPage);
        log.info("getGzhUserList gzhUserPage.size=" + gzhUserPage.getSize() + ", gzhUserPage.Number " + gzhUserPage.getNumber());

        return "gzhOps/gzhUserList";
    }

    @PreAuthorize("isGzhAdminForGzhUser(#id) || isWebAdmin()")
    @GetMapping("/gzhOps/gzhUser/details/{id}")
    public String getGzhUserDetails(@PathVariable("id") long id, Model model) {    
        GzhUser gzhUser = gzhUserRepository.findOne(id);
        model.addAttribute("gzhUser", gzhUser);
        log.info("getGzhUserDetails" + gzhUser.toString());
        return "gzhOps/gzhUserDetails";
    }

    // REST APIs. 
    // The following REST API will return JSON
    
    @PreAuthorize("isGzhAdminForGzhUser(#id) || isWebAdmin()")
    @PostMapping("/gzhOps/api/v1/gzhUser/edit/{id}")
    public @ResponseBody String postGzhUserEditApi(@PathVariable("id") long id, @RequestBody GzhUser gzhUser) throws JsonProcessingException {
        log.info("postGzhUserEditApi GzhUser=" + gzhUser.toString());
        GzhUser currGzhUser = gzhUserRepository.findOne(id);
        currGzhUser.setName(gzhUser.getName());
        currGzhUser.setMobile(gzhUser.getMobile());
        currGzhUser.setWeixin(gzhUser.getWeixin());
        currGzhUser.setEmail(gzhUser.getEmail());
        gzhUserRepository.save(currGzhUser);
        //If there is an exception, the code will not reach here.
        String gzhUserJson = objectMapper.writeValueAsString(gzhUser);
        return gzhUserJson;
    }
    
    //Get a list of gzhUser in a gzhCompany
    @PreAuthorize("isGzhAdmin(#gzhCompanyId) || isWebAdmin()")
    @GetMapping("/gzhOps/api/v1/gzhUser/list/gzhCompany/{gzhCompanyId}")
    public @ResponseBody String getGzhUserListByGzhCompanyId(@PathVariable("gzhCompanyId") long gzhCompanyId, HttpServletResponse response) {  
        Predicate predicate = repoQueryFilter.filterGzhUserByGzhCompanyId(gzhCompanyId);
        List<GzhUser> gzhUserList = gzhUserRepository.findAll(predicate);
        List<ObjectNode> objectNodeList = gzhUserList.stream()
            .map(gzhUser -> { 
                 ObjectNode gzhUserJsonNode = objectMapper.createObjectNode();
                 gzhUserJsonNode.put("id", gzhUser.getId());
                 gzhUserJsonNode.put("nickName", gzhUser.getNickName());
                 gzhUserJsonNode.put("headImgUrl", gzhUser.getHeadImgUrl());
                 gzhUserJsonNode.put("name", gzhUser.getName());
                 gzhUserJsonNode.put("mobile", gzhUser.getMobile());
                 gzhUserJsonNode.put("weixin", gzhUser.getWeixin());
                 return gzhUserJsonNode;
             })
            .collect(Collectors.toList());

        try {
            String gzhUserListJson = objectMapper.writeValueAsString(objectNodeList);
            return  gzhUserListJson;
        } catch (JsonProcessingException e) {
            response.setStatus(HttpStatus.BAD_REQUEST.value());
            return e.getLocalizedMessage();
        }
    }
    
    //Get a list of gzhUser of a gzhClientCompany
    @PreAuthorize("isGzhAdminForGzhClientCompany(#gzhClientCompanyId) || isWebAdmin()")
    @GetMapping("/gzhOps/api/v1/gzhUser/list/gzhClientCompany/{gzhClientCompanyId}")
    public @ResponseBody String getGzhUserListByGzhClientCompanyId(@PathVariable("gzhClientCompanyId") long gzhClientCompanyId, HttpServletResponse response) {  
        Predicate predicate = repoQueryFilter.filterGzhUserByGzhClientCompanyId(gzhClientCompanyId);
        List<GzhUser> gzhUserList = gzhUserRepository.findAll(predicate);
        List<ObjectNode> objectNodeList = gzhUserList.stream()
            .map(gzhUser -> { 
                 ObjectNode gzhUserJsonNode = objectMapper.createObjectNode();
                 gzhUserJsonNode.put("id", gzhUser.getId());
                 gzhUserJsonNode.put("nickName", gzhUser.getNickName());
                 gzhUserJsonNode.put("headImgUrl", gzhUser.getHeadImgUrl());
                 gzhUserJsonNode.put("name", gzhUser.getName());
                 gzhUserJsonNode.put("mobile", gzhUser.getMobile());
                 gzhUserJsonNode.put("weixin", gzhUser.getWeixin());
                 return gzhUserJsonNode;
             })
            .collect(Collectors.toList());
        
        try {
            String gzhUserListJson = objectMapper.writeValueAsString(objectNodeList);
            return  gzhUserListJson;
        } catch (JsonProcessingException e) {
            response.setStatus(HttpStatus.BAD_REQUEST.value());
            return e.getLocalizedMessage();
        }
    }
    
    //update the gzhUser list of gzhClientCompany
    @PreAuthorize("isGzhAdminForGzhClientCompany(#id) || isWebAdmin()")
    @PostMapping("/gzhOps/api/v1/gzhUser/list/gzhClientCompany/{id}")
    public @ResponseBody String gzhClientCompanySetGzhUserList(@AuthenticationPrincipal User user, @PathVariable("id") long id, 
        @RequestBody List<JsonNode> jsonNodeList, HttpServletResponse response) {
        GzhClientCompany gzhClientCompany = gzhClientCompanyRepository.findOne(id);
        List<GzhUser> gzhUserList = new ArrayList<GzhUser>();
        jsonNodeList.forEach(jsonNode -> {
            long gzhUserId = jsonNode.path("id").asLong(); 
            GzhUser gzhUser = gzhUserRepository.findOne(gzhUserId);
            gzhUserList.add(gzhUser);
        });
        
        //Persistent action on gzhClientCompany will update the join database table gzh_client_company_gzh_user
        //as GzhClientCompany is the owning side (with @JoinTable annotation) of the ManyToMany relationship with GzhUser
        gzhClientCompany.setGzhUserList(gzhUserList);
        gzhClientCompanyRepository.save(gzhClientCompany);
        log.info("gzhClientCompanySetGzhUserList gzhUserList=" + gzhUserList);
        return "Success";
    }
}
