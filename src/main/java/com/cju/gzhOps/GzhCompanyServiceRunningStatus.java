package com.cju.gzhOps;

import com.fasterxml.jackson.annotation.JsonProperty;

public enum GzhCompanyServiceRunningStatus {
    @JsonProperty("服务未启动") NOT_STARTED("NotStarted"), 
    @JsonProperty("服务运行中") RUNNING("Running"), 
    @JsonProperty("服务已停止") STOPPED("Stopped"),
    @JsonProperty("未知运行状态") UNKNOWN("Unknown");
    private String paymentStatus;
    private GzhCompanyServiceRunningStatus(String paymentStatus) {
        this.paymentStatus = paymentStatus;
    }
    
    public static GzhCompanyServiceRunningStatus fromString(String str) {
        for (GzhCompanyServiceRunningStatus status: GzhCompanyServiceRunningStatus.values()) {
            if (status.paymentStatus.equals(str)) {
                return status;
            }
        }
        //Default return
        return UNKNOWN;
    }
    
    @Override
    public String toString() {
        switch (paymentStatus) {
            case "NotStarted":
                return "服务未启动";
            case "Running":
                return "服务运行中";
            case "Stopped":
                return "服务已停止";
            default: 
                return "未知运行状态";
        }
    }
}
