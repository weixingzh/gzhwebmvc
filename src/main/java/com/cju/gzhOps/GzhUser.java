package com.cju.gzhOps;

import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import com.cju.BeanProvider;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;


//@JsonManagedReference is the forward part of reference – the one that gets serialized normally. 
//@JsonBackReference is the back part of reference – it will be omitted from serialization.
//@JsonManagedReference and @JsonBackReference does not handle bi-directional circular reference well in deserialization.
//Use @JsonIdentityInfo for serialization/deserialization rather than @JsonManagedReference and @JsonBackReference

@Entity
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id", scope = GzhUser.class)
@Table(name = "GzhUser", 
       uniqueConstraints = {@UniqueConstraint(columnNames = "openId", name = "GzhUser_UNIQUE_OpenId")}
)
public class GzhUser {
    @Id    
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    private String name;
    private String mobile;
    private String weixin;
    private String email;
    
    //The following are from WeiXin API
    private String nickName;
    private String city;
    private String province;
    private String country;
    private String openId;
    private String headImgUrl;
    private GzhUserStatus status;
    @Column(columnDefinition= "TIMESTAMP WITH TIME ZONE")
    private OffsetDateTime lastStatusChange;
    
    //The GzhCompany which services this gzhUser. This gzhUser subscribes to the gzhId of the GzhCompany
    //gzhCompanyId is the foreign key in the gzhUser table in database
    @JsonIgnore
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="gzhCompanyId", foreignKey = @ForeignKey(name = "GzhUser_FK_GzhCompanyId"))
    private GzhCompany gzhCompany;
    
    //We model the relationship between GzhUser and GzhClientCompany to be ManyToMany. 
    //LAZY means the gzhClientCompanyList will be loaded from database only when getGzhClientCompanyList() is called
    @JsonIgnore
    @ManyToMany(mappedBy="gzhUserList", fetch = FetchType.LAZY)
    private List<GzhClientCompany> gzhClientCompanyList = new ArrayList<GzhClientCompany>();
    
    //LAZY means the gzhUserList will be loaded from database only when gzhUserList() is called
    @JsonIgnore
    @OneToMany(mappedBy = "gzhUser", fetch = FetchType.LAZY)
    private List<GzhTask> gzhTaskList = new ArrayList<GzhTask>();
   
    public GzhUser() {}
    public GzhUser(long id) {
        this.id = id;
    }
    public GzhUser(String name) {
        this.name = name;
    }
    
    public void setId(long id) {
        this.id = id;
    }
    public long getId() {
        return id;
    }

    public void setName(String name) {
        this.name = name;
    }
    public String getName() {
        return name;
    }
    
    public void setMobile(String mobile) {
        this.mobile = mobile;
    }
    public String getMobile() {
        return mobile;
    }
    
    public void setWeixin(String weixin) {
        this.weixin = weixin;
    }
    public String getWeixin() {
        return weixin;
    }
    
    public String getEmail() {
        return email;
    }
    public void setEmail(String email) {
        this.email = email;
    }
    
    public void setOpenId(String openId) {
        this.openId = openId;
    }
    public String getOpenId() {
        return openId;
    }
    
    public void setHeadImgUrl(String headImgUrl) {
        this.headImgUrl = headImgUrl;
    }
    public String getHeadImgUrl() {
        return headImgUrl;
    }
    
    public void setNickName(String nickName) {
        this.nickName = nickName;
    }
    public String getNickName() {
        return nickName;
    }
    
    public void setCity(String city) {
        this.city = city;
    }
    public String getCity() {
        return city;
    }
    
    public void setProvince(String province) {
        this.province = province;
    }
    public String getProvince() {
        return province;
    }
    
    public void setCountry(String country) {
        this.country = country;
    }
    public String getCountry() {
        return country;
    }
    
    public void setGzhCompany(GzhCompany gzhCompany) {
        this.gzhCompany = gzhCompany;
    }
    public GzhCompany getGzhCompany() {
        return gzhCompany;
    }
    public void setGzhCompanyByGzhId(String gzhId) {
        GzhCompany gzhCompany = BeanProvider.getGzhCompanyRepository().findOneByGzhId(gzhId);
        this.gzhCompany = gzhCompany;
    }
    
    public void setGzhClientCompanyList(List<GzhClientCompany> gzhClientCompanyList) {
        this.gzhClientCompanyList = gzhClientCompanyList;
    }
    public List<GzhClientCompany> getGzhClientCompanyList() {
        return gzhClientCompanyList;
    }
    public void addGzhClientCompany(GzhClientCompany gzhClientCompany) {
        //Use set to remove duplication
        Set<GzhClientCompany> set = new HashSet<GzhClientCompany>(gzhClientCompanyList);
        set.add(gzhClientCompany);
        gzhClientCompanyList = new ArrayList<GzhClientCompany>(set);
    }
    
    public void setStatus(GzhUserStatus status) {
        this.status = status;
    }
    public GzhUserStatus getStatus() {
        return status;
    }
    
    public void setLastStatusChange(OffsetDateTime lastStatusChange) {
        this.lastStatusChange = lastStatusChange;
    }
    public OffsetDateTime getLastStatusChange() {
        return lastStatusChange;
    }
    
    @Override
    public String toString() {
        return String.format(
                "GzhUser[id=%s, name='%s', openId='%s']",
                id, name, openId);
    }

}