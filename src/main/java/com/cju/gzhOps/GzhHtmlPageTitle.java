package com.cju.gzhOps;

import com.fasterxml.jackson.annotation.JsonProperty;

public enum GzhHtmlPageTitle {
    @JsonProperty("自定义标题") SELFDEFINED_TITLE("SelfDefinedTitle"), 
    @JsonProperty("公司简介") COMPANY_INTRO("IntroPage"), 
    @JsonProperty("公司业务") COMPANY_SERVICE("ServicePage"), 
    @JsonProperty("合作伙伴") COMPANY_PARTNER("PartnerPage");
    private String htmlPageTitle;
    private GzhHtmlPageTitle(String htmlPageTitle) {
        this.htmlPageTitle = htmlPageTitle;
    }
    
    public static GzhHtmlPageTitle fromString(String str) {
        for (GzhHtmlPageTitle type: GzhHtmlPageTitle.values()) {
            if (type.htmlPageTitle.equals(str)) {
                return type;
            }
        }
        //Default return
        return SELFDEFINED_TITLE;
    }
        
    @Override
    public String toString() {
        switch (htmlPageTitle) {
            case "SelfDefinedTitle": 
                return "自定义标题";
            case "IntroPage": 
                return "公司简介";
            case "ServicePage": 
                return "公司业务";
            case "PartnerPage":
                return "合作伙伴";
            default: 
                return "无标题";
        }
    }
}
