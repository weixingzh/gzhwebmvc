package com.cju.gzhOps;

import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

// Javascript Date() uses the local time zone to interpret arguments in RFC2822 Section 3.3 format that do not contain time zone information.

// ISO 8601 string format is used to pass time between browser and server.
// From browser, Javascript Date().toISOString() will have the timezone letter Z in the end. Z means GMT timezone which has datetime offset of 0. This is sent to server.
// On the server, Jackson2 will parse this ISO 8601 string format into Java8 date type. 
// If the Java Entity uses LocalDateTime, the ISO 8601 string is stored unchanged into database as database Timestamp type. When the server read 
// this date and send back to client, the Z letter is not presented at the end of the ISO 8601 string. Javascript client will treat this time as local time.
// So we need to use OffsetDateTime on server. When server save the ISO 8601 string into database, the Timestamp is adjusted with the timezone offset.
// When the server read the date and send back to client, the ISO 8601 string has the timezone offset, for example, +08:00 if the server is in China.
// Javascript Date() can interpret the offset datetime correctly.

@Entity
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id", scope = GzhTask.class)
@Table(name = "GzhTask")
public class GzhTask {
    @Id    
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    
    @Enumerated(EnumType.STRING)
    private GzhTaskType type;
    private String name;
    @Enumerated(EnumType.STRING)
    private GzhTaskStatus status = GzhTaskStatus.NOT_STARTED;
    @Column(columnDefinition = "text")
    private String note;
    
    //Without hibernate-java8.jar, JPA (via hibernate) will store LocalDataTime as BLOB database type.
    //With hibernate-java8.jar in build.gradle, LocalDataTime will be stored in database as Timestamp database date type.
    @Column(columnDefinition= "TIMESTAMP WITH TIME ZONE")
    private OffsetDateTime lastStatusChange;
    
    //The gzhCopmany of this task if it is a private task template. 
    @JsonIgnore
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="gzhCompanyId", foreignKey = @ForeignKey(name = "GzhTask_FK_GzhCompanyId"))
    private GzhCompany gzhCompany;
    
    //The gzhUser this task belongs to. 
    @JsonIgnore
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="gzhUserId", foreignKey = @ForeignKey(name = "GzhTask_FK_GzhUserId"))
    private GzhUser gzhUser;
    
    //Must use EAGER here as we may need to clearGzhTaskTemplates in GzhStartUp, which is outside of web request.
    //Please see comments in GzhStartUp for details.
    @OneToMany(mappedBy = "gzhTask", fetch = FetchType.LAZY)
    @OrderBy("order ASC")
    private List<GzhTaskStep> steps = new ArrayList<GzhTaskStep>();
    
    public GzhTask() {}
    public GzhTask(long id) {
        this.id = id;
    }
    public GzhTask(String name) {
        this.name = name;
    }
    
    public void setId(long id) {
    	this.id = id;
    }
    public long getId() {
    	return id;
    }
    
    public void setType(GzhTaskType type) {
        this.type = type;
    }
    public GzhTaskType getType() {
        return type;
    }

    public void setName(String name) {
    	this.name = name;
    }
    public String getName() {
    	return name;
    }
    
    public void setGzhCompany(GzhCompany gzhCompany) {
        this.gzhCompany = gzhCompany;
    }
    public GzhCompany getGzhCompany() {
        return gzhCompany;
    }
    
    public void setGzhUser(GzhUser gzhUser) {
        this.gzhUser = gzhUser;
    }
    public GzhUser getGzhUser() {
        return gzhUser;
    }
    
    public void setSteps(List<GzhTaskStep> steps) {
        this.steps = steps;
    }
    public List<GzhTaskStep> getSteps() {
        return steps;
    }
    
    public void setStatus(GzhTaskStatus status) {
        this.status = status;
    }
    public GzhTaskStatus getStatus() {
        return status;
    }
    
    public void setNote(String note) {
        this.note = note;
    }
    public String getNote() {
        return note;
    }
    
    public void setLastStatusChange(OffsetDateTime lastStatusChange) {
        this.lastStatusChange = lastStatusChange;
    }
    public OffsetDateTime getLastStatusChange() {
        return lastStatusChange;
    }
    
    @Override
    public String toString() {
        return String.format(
                "GzhTask[id=%s, name='%s']",
                id, name );
    }

}