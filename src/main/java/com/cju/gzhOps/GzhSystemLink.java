package com.cju.gzhOps;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import com.cju.BeanProvider;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;


//@JsonManagedReference is the forward part of reference – the one that gets serialized normally. 
//@JsonBackReference is the back part of reference – it will be omitted from serialization.
//@JsonManagedReference and @JsonBackReference does not handle bi-directional circular reference well in deserialization.
//Use @JsonIdentityInfo for serialization/deserialization rather than @JsonManagedReference and @JsonBackReference

@Entity
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id", scope = GzhSystemLink.class)
@Table(name = "GzhSystemLink")
public class GzhSystemLink {
    @Id    
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    private String name;
    private String url;
    private String description;
    
    public GzhSystemLink() {}
    public GzhSystemLink(long id) {
        this.id = id;
    }
    
    public void setId(long id) {
    	this.id = id;
    }
    public long getId() {
    	return id;
    }
    
    public void setName(String name) {
        this.name = name;
    }
    public String getName() {
        return name;
    }

    public void setUrl(String url) {
        this.url = url;
    }
    public String getUrl() {
        return url;
    }
    
    public void setDescription(String description) {
        this.description = description;
    }
    public String getDescription() {
        return description;
    }
    
    @Override
    public String toString() {
        return String.format(
                "GzhSystemLink[id=%s, name='%s', url='%s']",
                id, name, url);
    }
}