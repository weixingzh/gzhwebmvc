package com.cju.gzhOps;

import java.time.OffsetDateTime;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;


//@JsonManagedReference is the forward part of reference – the one that gets serialized normally. 
//@JsonBackReference is the back part of reference – it will be omitted from serialization.
//@JsonManagedReference and @JsonBackReference does not handle bi-directional circular reference well in deserialization.
//Use @JsonIdentityInfo for serialization/deserialization rather than @JsonManagedReference and @JsonBackReference

@Entity
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id", scope = GzhTaskStep.class)
@Table(name = "GzhTaskStep")
public class GzhTaskStep {
    @Id    
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    //PostgreSQL does not allow "order" to be column name
    @Column(name="stepOrder")
    private int order;
    private String name;
    @Column(length=1024)
    private String description;
    private GzhTaskStatus status;
    @Column(columnDefinition= "TIMESTAMP WITH TIME ZONE")
    private OffsetDateTime lastStatusChange;
    
    //The gzhUser this service item belongs to. 
    //gzhUserId is the foreign key in the gzhServiceItem table in database
    @JsonIgnore
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="gzhTaskId", foreignKey = @ForeignKey(name = "GzhTaskStep_FK_GzhTaskId"))
    private GzhTask gzhTask;
    
    public GzhTaskStep() {}
    public GzhTaskStep(long id) {
        this.id = id;
    }
    public GzhTaskStep(String name) {
        this.name = name;
    }
    
    public void setId(long id) {
    	this.id = id;
    }
    public long getId() {
    	return id;
    }
    
    public void setOrder(int order) {
        this.order = order;
    }
    public int getOrder() {
        return order;
    }
    
    public void setName(String name) {
    	this.name = name;
    }
    public String getName() {
    	return name;
    }
    
    public void setDescription(String description) {
        this.description = description;
    }
    public String getDescription() {
        return description;
    }
    
    public void setStatus(GzhTaskStatus status) {
        this.status = status;
    }
    public GzhTaskStatus getStatus() {
        return status;
    }
    
    public void setLastStatusChange(OffsetDateTime lastStatusChange) {
        this.lastStatusChange = lastStatusChange;
    }
    public OffsetDateTime getLastStatusChange() {
        return lastStatusChange;
    }
    
    public void setGzhTask(GzhTask gzhTask) {
        this.gzhTask = gzhTask;
    }
    public GzhTask getGzhTask() {
        return gzhTask;
    }
    
    
    @Override
    public String toString() {
        return String.format(
                "GzhServiceStep[id=%s, name='%s']",
                id, name );
    }

}