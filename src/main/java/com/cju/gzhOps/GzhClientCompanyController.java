package com.cju.gzhOps;

import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cju.chanjet.ChanjetClientCompany;
import com.cju.chanjet.ChanjetClientCompanyIncomeItem;
import com.cju.chanjet.ChanjetClientCompanyRepository;
import com.cju.security.RepositoryQueryFilter;
import com.cju.user.User;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.querydsl.core.types.Predicate;


//OpsController is for providing OPS to accounting Company.
//We need to associate a user and a client company to the user's openId for this gzh. Since the openId is only known 
//when user access the website through his weixin client, the user-creation and client-company-creation functionality
//are provided via weixin gzh UI, not here.


@Controller
public class GzhClientCompanyController {
	private Logger log = LoggerFactory.getLogger(GzhClientCompanyController.class);
	
    @PersistenceContext
    private EntityManager entityManager;
    @Autowired 
    private GzhCompanyRepository gzhCompanyRepository; 
    @Autowired 
    private GzhClientCompanyRepository gzhClientCompanyRepository; 
    @Autowired 
    private GzhClientCompanyService gzhClientCompanyService; 
    @Autowired 
    private GzhClientCompanyReportRepository gzhClientCompanyReportRepository;  
    @Autowired 
    private ChanjetClientCompanyRepository chanjetClientCompanyRepository; 
    @Autowired 
    private GzhClientCompanyAssetReportItemRepository gzhClientCompanyAssetReportItemRepository;  
    @Autowired 
    private GzhClientCompanyIncomeReportItemRepository gzhClientCompanyIncomeReportItemRepository;  
    @Autowired 
    private GzhClientCompanyCashReportItemRepository gzhClientCompanyCashReportItemRepository;
    @Autowired 
    RepositoryQueryFilter repoQueryFilter;
    @Autowired
    private ObjectMapper objectMapper;
    
	//Any logged in user can get the list. But we need to filter the list for the user.
    @PreAuthorize("isAuthenticated()")
    @GetMapping("/gzhOps/gzhClientCompany/list")
    public String getGzhClientCompanyList(@AuthenticationPrincipal User user, Pageable pageable, Model model) {  
        Predicate predicate = repoQueryFilter.filterGzhClientCompanyByUser(user);
        Page<GzhClientCompany> gzhClientCompanyPage = gzhClientCompanyRepository.findAll(predicate, pageable);
        model.addAttribute("gzhClientCompanyPage", gzhClientCompanyPage);
        log.info("gzhClientCompany gzhClientCompanyPage.size=" + gzhClientCompanyPage.getSize() + ", gzhClientCompanyPage.Number " + gzhClientCompanyPage.getNumber());
        return "gzhOps/gzhClientCompanyList";
    }
  
    @PreAuthorize("isGzhAdminForGzhClientCompany(#id) || isWebAdmin()")
    @GetMapping("/gzhOps/gzhClientCompany/details/{id}")
    public String getGzhClientCompanyDetails(@PathVariable("id") long id, Model model) {  
        GzhClientCompany gzhClientCompany = gzhClientCompanyRepository.findOne(id);
        model.addAttribute("gzhClientCompany", gzhClientCompany);
        return "gzhOps/gzhClientCompanyDetails";
    }
    
    @PreAuthorize("isGzhAdminForGzhClientCompany(#id)")
    @GetMapping("/gzhOps/gzhClientCompany/{id}/report/import")
    public String getGzhClientCompanyReportImportForm(@PathVariable("id") long id, Model model) {
        log.info("getGzhClientCompanyReportImportForm id=" + id);
        GzhClientCompany gzhClientCompany = gzhClientCompanyRepository.findOne(id);
        model.addAttribute("gzhClientCompany", gzhClientCompany);
        return "gzhOps/gzhClientCompanyReportImport";
    }
    

    //Any logged in user can get the list. But we need to filter the list for the user.
    @PreAuthorize("isAuthenticated()")
    @GetMapping("/gzhOps/gzhClientCompany/report/list")
    public String getGzhClientCompanyReportListByUser(@AuthenticationPrincipal User user, Pageable pageable, Model model) {  
        Predicate predicate = repoQueryFilter.filterGzhClientCompanyReportByUser(user);
        Page<GzhClientCompanyReport> gzhClientCompanyReportPage = gzhClientCompanyReportRepository.findAll(predicate, pageable);
        model.addAttribute("gzhClientCompanyReportPage", gzhClientCompanyReportPage);
        return "gzhOps/gzhClientCompanyReportList";
    }
    
    @PreAuthorize("isGzhAdminForGzhClientCompany(#id) || isWebAdmin()")
    @GetMapping("/gzhOps/gzhClientCompany/{id}/report/list")
    public String getGzhClientCompanyReportListById(@PathVariable("id") long id, Pageable pageable, Model model) {  
        Predicate predicate = repoQueryFilter.filterGzhClientCompanyReportByClientCompanyId(id);
        Page<GzhClientCompanyReport> gzhClientCompanyReportPage = gzhClientCompanyReportRepository.findAll(predicate, pageable);
        model.addAttribute("gzhClientCompanyReportPage", gzhClientCompanyReportPage);
        return "gzhOps/gzhClientCompanyReportList";
    }
    
    //@PathVariable("id") is the gzhClientCompanyId
    //query parameters are period=period&reportType=reportType
    @PreAuthorize("isGzhAdminForGzhClientCompany(#id) || isWebAdmin()")
    @GetMapping("/gzhOps/gzhClientCompany/{id}/report/details")
    public String getGzhClientCompanyReportDetailsById(@PathVariable("id") long id, @RequestParam Map<String, String> params, Model model) {  
        GzhClientCompany gzhClientCompany = gzhClientCompanyRepository.findOne(id);
        long bookId = gzhClientCompany.getChanjetClientCompany().getBookId();
        String period = params.get("period");
        GzhClientCompanyReportType reportType = GzhClientCompanyReportType.fromString(params.get("reportType"));
        log.info("getGzhClientCompanyReportDetails() bookId=" + bookId + ", peroid=" + period + ", reportType=" + reportType); 
        Predicate predicate = repoQueryFilter.filterGzhClientCompanyReportByBookIdAndPeriodAndReportType(bookId, period, reportType);
        GzhClientCompanyReport gzhClientCompanyReport = gzhClientCompanyReportRepository.findOne(predicate);
        model.addAttribute("gzhClientCompanyReport", gzhClientCompanyReport);
        String view="";
        switch (reportType) {
        case ASSET: 
            view = "gzhOps/gzhClientCompanyReportAssetDetails";
            break;
        case INCOME:
            view = "gzhOps/gzhClientCompanyReportIncomeDetails";
            break;
        case CASH:
            view = "gzhOps/gzhClientCompanyReportCashDetails";
            break;
        }
        return view;
    }
    

    //REST API
    
    @PreAuthorize("isGzhAdminForGzhClientCompany(#id)")
    @PostMapping("/gzhOps/api/v1/gzhClientCompany/{id}/report/import")
    public @ResponseBody String postGzhClientCompanyReportImportApi(@AuthenticationPrincipal User user, @PathVariable("id") long id, 
            @RequestBody JsonNode json, HttpServletResponse response) {
        
        String reportTypeValue = json.path("reportTypeValue").asText();
        String period = json.path("period").asText();
        GzhClientCompany gzhClientCompany = gzhClientCompanyRepository.findOne(id);
        GzhClientCompanyReportType reportType = GzhClientCompanyReportType.fromString(reportTypeValue);
        
        ObjectNode importedReport = objectMapper.createObjectNode();
        importedReport.put("name", gzhClientCompany.getChanjetClientCompany().getName());
        importedReport.put("reportType", reportType.toString());
        importedReport.put("period", period.toString());
        
        //User manually import report for one reportType, one period, for one GzhClientCompany. 
        //The javascript in gzhClientCompanyReportImport.html iterates through reportType and period for one GzhClientCompany.
        //For user manual import, delete existing reports if found so that new import can override the existing one.
        //For scheduled report imports (which does not go through this controller), clearIfExist is set to false.
        boolean clearIfExist = true;
        
        //gzhClientCompanyService import methods may throw a self generated exception to indicate the chanjet server failure
        try {
            switch(reportType) {
            case ASSET:
                gzhClientCompanyService.importAssetReport(gzhClientCompany, period, clearIfExist);
                break;
            case INCOME:
                gzhClientCompanyService.importIncomeReport(gzhClientCompany, period, clearIfExist);
                break;            
            case CASH:
                gzhClientCompanyService.importCashReport(gzhClientCompany, period, clearIfExist);
                break;            
            default:
                break;
            }
        } catch (Exception e) {
            response.setStatus(HttpStatus.BAD_REQUEST.value());
            importedReport.put("errMsg", e.getLocalizedMessage());
        }
        
        try {
            String importedReportJson = objectMapper.writeValueAsString(importedReport);
            return importedReportJson;
        } catch (JsonProcessingException e) {
            response.setStatus(HttpStatus.BAD_REQUEST.value());
            return e.getLocalizedMessage();
        }
    }
    
    //Only webAdmin can manually create gzhClientCompany. 
    //The manually created fake gzhClientCompany is for demo purpose.
    @PreAuthorize("isWebAdmin()")
    @PostMapping("/gzhOps/api/v1/gzhCompany/createClientCompany/{id}")
    public @ResponseBody String gzhCompanyCreateClientCompanyApi(@PathVariable("id") long id, @RequestBody JsonNode json) {
        
        String gzhClientCompanyName = json.path("gzhClientCompanyName").asText();
        ChanjetClientCompany chanjetClientCompany = new ChanjetClientCompany();
        chanjetClientCompany.setName(gzhClientCompanyName);
        chanjetClientCompany.setAccId(null);
        chanjetClientCompanyRepository.save(chanjetClientCompany);
        
        GzhCompany gzhCompany = gzhCompanyRepository.findOne(id);
        GzhClientCompany gzhClientCompany = new GzhClientCompany();
        gzhClientCompany.setChanjetClientCompany(chanjetClientCompany);
        gzhClientCompany.setImportedByUser(null);
        gzhClientCompany.setGzhCompany(gzhCompany);
        gzhClientCompanyRepository.save(gzhClientCompany);
        
        chanjetClientCompany.setGzhClientCompany(gzhClientCompany);
        chanjetClientCompanyRepository.save(chanjetClientCompany);
        
        return "Success";
    }
    
    @PreAuthorize("isWebAdmin()")
    @GetMapping("/gzhOps/api/v1/gzhClientCompany/fakeGzhClientCompany/list")
    public @ResponseBody String getFakeGzhClientCompanyListApi(HttpServletResponse response) {  
        Predicate predicate = repoQueryFilter.filterFakeGzhClientCompany();
        List<GzhClientCompany> gzhClientCompanyList = gzhClientCompanyRepository.findAll(predicate);
        
        try {
            String fakeGzhClientCompanyListJson = objectMapper.writeValueAsString(gzhClientCompanyList);
            return fakeGzhClientCompanyListJson;
        } catch (JsonProcessingException e) {
            response.setStatus(HttpStatus.BAD_REQUEST.value());
            return e.getLocalizedMessage();
        }
    }
    
    @PreAuthorize("isWebAdmin()")
    @PostMapping("/gzhOps/api/v1/gzhClientCompany/{id}/fakeReport/import")
    public @ResponseBody String postGzhClientCompanyFakeReportImportApi(@PathVariable("id") long id, @RequestBody JsonNode json) {
        
        GzhClientCompany gzhClientCompany = gzhClientCompanyRepository.findOne(id);
        
        long reportId = json.path("reportId").asLong();
        GzhClientCompanyReport realReport = gzhClientCompanyReportRepository.findOne(reportId);
        
        String period = realReport.getPeriod();
        GzhClientCompanyReportType reportType = realReport.getReportType();
        Predicate predicate = repoQueryFilter.filterFakeGzhClientCompanyReport(id, period, reportType);
        GzhClientCompanyReport fakeReport = gzhClientCompanyReportRepository.findOne(predicate);
        
        if (fakeReport == null) {
            //create a new fake report
            fakeReport = new GzhClientCompanyReport();
            fakeReport.setId(0);
            //Fake report must set bookId to 0 so that it is not returned when the real one is searched.
            fakeReport.setBookId(0L);
            fakeReport.setGzhClientCompany(gzhClientCompany);
            fakeReport.setPeriod(period);
            fakeReport.setReportType(reportType);
            gzhClientCompanyReportRepository.save(fakeReport);
        }
        else {
            //Fake report already exists. Clear the existing report items.
            switch(reportType) {
            case ASSET:
                List<GzhClientCompanyAssetReportItem> gzhClientCompanyAssetReportItemList = fakeReport.getGzhClientCompanyAssetReportItemList();
                for (GzhClientCompanyAssetReportItem gzhClientCompanyAssetReportItem: gzhClientCompanyAssetReportItemList) {
                    gzhClientCompanyAssetReportItemRepository.delete(gzhClientCompanyAssetReportItem);
                }
                break;
            case INCOME:
                List<GzhClientCompanyIncomeReportItem> gzhClientCompanyIncomeReportItemList = fakeReport.getGzhClientCompanyIncomeReportItemList();
                for (GzhClientCompanyIncomeReportItem gzhClientCompanyIncomeReportItem: gzhClientCompanyIncomeReportItemList) {
                    gzhClientCompanyIncomeReportItemRepository.delete(gzhClientCompanyIncomeReportItem);
                }
                break;            
            case CASH:
                List<GzhClientCompanyCashReportItem> cashReportItemList = fakeReport.getGzhClientCompanyCashReportItemList();
                for (GzhClientCompanyCashReportItem cashReportItem: cashReportItemList) {
                    gzhClientCompanyCashReportItemRepository.delete(cashReportItem);
                }
                break;            
            default:
                break;
            }
        }
        
        //Second clone the report item list
        switch(reportType) {
        case ASSET:
            List<GzhClientCompanyAssetReportItem> gzhClientCompanyAssetReportItemList = realReport.getGzhClientCompanyAssetReportItemList();
            for (GzhClientCompanyAssetReportItem realReportItem: gzhClientCompanyAssetReportItemList) {
                //In order to change the id, which is the primary key, we need to detach the entity first.
                entityManager.detach(realReportItem);
                realReportItem.setId(0);
                //Must set the bookId to 0
                realReportItem.getChanjetClientCompanyAssetItem().setBookId(0);
                realReportItem.setGzhClientCompanyReport(fakeReport);
                
                //save will create a new fake report database entry because the report id is set to 0 
                gzhClientCompanyAssetReportItemRepository.save(realReportItem);
                //We do not need to update the Java OneToMany relationship in gzhClientCompanyIncomeReport 
                //since we are not using gzhClientCompanyIncomeReport in this function.
            }
            break;
        case INCOME:
            List<GzhClientCompanyIncomeReportItem> gzhClientCompanyAssetIncomeItemList = realReport.getGzhClientCompanyIncomeReportItemList();
            for (GzhClientCompanyIncomeReportItem realReportItem: gzhClientCompanyAssetIncomeItemList) {
                //In order to change the id, which is the primary key, we need to detach the entity first.
                entityManager.detach(realReportItem);
                realReportItem.setId(0);
                //Must set the bookId to 0
                realReportItem.getChanjetClientCompanyIncomeItem().setBookId(0);
                realReportItem.setGzhClientCompanyReport(fakeReport);
                
                //save will create a new fake report database entry because the report id is set to 0 
                gzhClientCompanyIncomeReportItemRepository.save(realReportItem);
                //We do not need to update the Java OneToMany relationship in gzhClientCompanyIncomeReport 
                //since we are not using gzhClientCompanyIncomeReport in this function.
            }
            break;            
        case CASH:
            List<GzhClientCompanyCashReportItem> gzhClientCompanyCashIncomeItemList = realReport.getGzhClientCompanyCashReportItemList();
            for (GzhClientCompanyCashReportItem realReportItem: gzhClientCompanyCashIncomeItemList) {
                //In order to change the id, which is the primary key, we need to detach the entity first.
                entityManager.detach(realReportItem);
                realReportItem.setId(0);
                //Must set the bookId to 0
                realReportItem.getChanjetClientCompanyCashItem().setBookId(0);
                realReportItem.setGzhClientCompanyReport(fakeReport);
                
                //save will create a new fake report database entry because the report id is set to 0 
                gzhClientCompanyCashReportItemRepository.save(realReportItem);
                //We do not need to update the Java OneToMany relationship in gzhClientCompanyIncomeReport 
                //since we are not using gzhClientCompanyIncomeReport in this function.
            }
            break;            
        default:
            break;
        }
        
        return "Success";
    }
}
