package com.cju.gzhOps;

import com.fasterxml.jackson.annotation.JsonProperty;

public enum GzhUserStatus {
    @JsonProperty("已关注")SUBSCRIBE("Subscribe"), @JsonProperty("已取消关注")UNSUBSCRIBE("Unsubscribe");
    private String status;
    GzhUserStatus(String status) {
        this.status = status;
    }
    
    @Override
    public String toString() {
        switch (status) {
            case "Subscribe": 
                return "已关注";
            case "Unsubscribe": 
                return "已取消关注";
            default: 
                return "已取消关注";
        }
    }
}
