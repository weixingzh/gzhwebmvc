package com.cju.gzhOps;

import com.fasterxml.jackson.annotation.JsonProperty;

public enum GzhCompanyMenuType {
    @JsonProperty("系统缺省菜单") SYSTEM_DEFAULT("SystemDefault"), 
    @JsonProperty("自定义菜单") SELF_DEFINED("SelfDefined"), 
    @JsonProperty("未知菜单类型") UNKNOWN("Unknown");
    private String menuType;
    private GzhCompanyMenuType(String menuType) {
        this.menuType = menuType;
    }
    
    public static GzhCompanyMenuType fromString(String str) {
        for (GzhCompanyMenuType menu: GzhCompanyMenuType.values()) {
            if (menu.menuType.equals(str)) {
                return menu;
            }
        }
        //Default return
        return UNKNOWN;
    }
    
    @Override
    public String toString() {
        switch (menuType) {
            case "SystemDefault": 
                return "系统缺省菜单";
            case "SelfDefined": 
                return "自定义菜单";
            default: 
                return "未知菜单类型";
        }
    }
}
