package com.cju.gzhOps;

import java.util.List;

import org.springframework.data.querydsl.QueryDslPredicateExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.querydsl.core.types.Predicate;

//Spring Data REST takes the features of Spring HATEOAS and Spring Data JPA and combines them together automatically.
//Spring Data REST also supports Spring Data Neo4j, Spring Data Gemfire and Spring Data MongoDB as backend data stores.

//In a typical Java application, you’d expect to write a class that implements CustomerRepository. 
//But that’s what makes Spring Data JPA so powerful: You don’t have to write an implementation of the repository interface. 
//Spring Data JPA creates an implementation on the fly when you run the application.

//All of your application components (@Component, @Service, @Repository, @Controller etc.) 
//will be automatically registered as Spring Beans.

@Repository
public interface GzhSystemLinkRepository 
    extends PagingAndSortingRepository<GzhSystemLink, Long>, QueryDslPredicateExecutor<GzhSystemLink> {
    List<GzhSystemLink> findAll();

    GzhSystemLink findOneByName(String name);
}