package com.cju.gzhOps;

import com.fasterxml.jackson.annotation.JsonProperty;

public enum GzhCompanyBillingStatus {
    @JsonProperty("免费试用未开始") TRIAL_NOT_STARTED("TrialNotStarted"), 
    @JsonProperty("免费试用") TRIAL("Trial"), 
    @JsonProperty("正常计费") ACTIVE("Active"), 
    @JsonProperty("停止计费") STOPPED("Stopped"),
    @JsonProperty("未知记费状态") UNKNOWN("Unknown");
    private String billingStatus;
    private GzhCompanyBillingStatus(String billingStatus) {
        this.billingStatus = billingStatus;
    }
    
    public static GzhCompanyBillingStatus fromString(String str) {
        for (GzhCompanyBillingStatus status: GzhCompanyBillingStatus.values()) {
            if (status.billingStatus.equals(str)) {
                return status;
            }
        }
        //Default return
        return UNKNOWN;
    }
    
    @Override
    public String toString() {
        switch (billingStatus) {
            case "TrialNotStarted": 
                return "免费试用未开始";
            case "Trial": 
                return "免费试用";
            case "Active":
                return "正常计费";
            case "Stopped":
                return "停止计费";
            default: 
                return "未知记费状态";
        }
    }
}
