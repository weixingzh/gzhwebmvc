package com.cju.gzhOps;

import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.stream.Collectors;
import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletResponse;
import org.springframework.http.HttpStatus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import com.cju.security.RepositoryQueryFilter;
import com.cju.user.User;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.querydsl.core.types.Predicate;


//OpsController is for providing OPS to accounting Company.
//We need to associate a user and a client company to the user's openId for this gzh. Since the openId is only known 
//when user access the website through his weixin client, the user-creation and client-company-creation functionality
//are provided via weixin gzh UI, not here.

@Controller
public class GzhTaskController {
    private Logger log = LoggerFactory.getLogger(GzhTaskController.class);
    
    @Autowired 
    private GzhTaskRepository gzhTaskRepository; 
    
    @Autowired 
    private GzhTaskStepRepository gzhTaskStepRepository; 
    
    @Autowired 
    private GzhUserRepository gzhUserRepository; 
    
    @Autowired
    private MessageSource messageSource;

    @Autowired 
    RepositoryQueryFilter repoQueryFilter;
    
    @Autowired
    private ObjectMapper objectMapper;
    
    @PreAuthorize("isAuthenticated()")
    @GetMapping("/gzhOps/task/template/private/list")
    public String getGzhTaskTemplatePrivateList(@AuthenticationPrincipal User user, Pageable pageable, Model model) {
        Predicate predicate = repoQueryFilter.filterGzhTaskPrivateTemplateByUser(user);
        Page<GzhTask> gzhTaskTemplatePage = gzhTaskRepository.findAll(predicate, pageable);
        model.addAttribute("gzhTaskTemplatePage", gzhTaskTemplatePage);
        log.info("getGzhTaskTemplateList gzhTaskTemplatePage.size=" + gzhTaskTemplatePage.getSize() + ", gzhTaskTemplatePage.Number " + gzhTaskTemplatePage.getNumber());
        
        return "gzhOps/gzhTaskTemplateList";
    }
    
    @PreAuthorize("isAuthenticated()")
    @GetMapping("/gzhOps/task/template/public/list")
    public String getGzhTaskTemplatePublicList(@AuthenticationPrincipal User user, Pageable pageable, Model model) {
        Predicate predicate = repoQueryFilter.filterGzhTaskPublicTemplate();
        Page<GzhTask> gzhTaskTemplatePage = gzhTaskRepository.findAll(predicate, pageable);
        model.addAttribute("gzhTaskTemplatePage", gzhTaskTemplatePage);
        log.info("getGzhTaskTemplateList gzhTaskTemplatePage.size=" + gzhTaskTemplatePage.getSize() + ", gzhTaskTemplatePage.Number " + gzhTaskTemplatePage.getNumber());

        return "gzhOps/gzhTaskTemplateList";
    }

    
    //Only task list, excluding task templates
    @PreAuthorize("isAuthenticated()")
    @GetMapping("/gzhOps/task/list")
    public String getGzhTaskListByUser(@AuthenticationPrincipal User user, Pageable pageable, Model model) {
        Predicate predicate = repoQueryFilter.filterGzhTaskByUser(user);
        Page<GzhTask> gzhTaskPage = gzhTaskRepository.findAll(predicate, pageable);
        model.addAttribute("gzhTaskPage", gzhTaskPage);
        log.info("getGzhCompanyTaskListByUser gzhTaskPage.size=" + gzhTaskPage.getSize() + ", gzhTaskPage.Number " + gzhTaskPage.getNumber());
        
        return "gzhOps/gzhTaskList";
    }
    
    //gzhUserId is a path variable so that we can do security check on it.
    @PreAuthorize("isGzhAdminForGzhUser(#id) || isWebAdmin()")
    @GetMapping("/gzhOps/task/list/gzhUser/{id}")
    public String getGzhTaskListByGzhUser(@PathVariable("id") long id, Pageable pageable, Model model) {
        GzhUser gzhUser = gzhUserRepository.findOne(id);
        model.addAttribute("gzhUser", gzhUser);
        Predicate predicate = repoQueryFilter.filterGzhTaskByGzhUserId(id);
        Page<GzhTask> gzhTaskPage = gzhTaskRepository.findAll(predicate, pageable);
        model.addAttribute("gzhTaskPage", gzhTaskPage);
        log.info("getGzhUserTaskListByGzhUser gzhTaskPage.size=" + gzhTaskPage.getSize() + ", gzhTaskPage.Number " + gzhTaskPage.getNumber());

        String taskListScope = gzhUser.getName();
        model.addAttribute("taskListScope", taskListScope);
        
        return "gzhOps/gzhTaskList";
    }
    
    @PreAuthorize("isGzhAdminForGzhTask(#id) || isGzhTaskPublicTemplate(#id) || isWebAdmin()")
    @GetMapping("/gzhOps/task/{id}/details")
    public String getGzhTaskDetails(@AuthenticationPrincipal User user, @PathVariable("id") long id, Model model) { 
        GzhTask gzhTask = gzhTaskRepository.findOne(id);
        model.addAttribute("gzhTask", gzhTask);
        if(user.isWebAdmin()) {
            model.addAttribute("enableEdit", true);
        }
        else if (gzhTask.getType() == GzhTaskType.CUSTOMER_TASK || gzhTask.getType() == GzhTaskType.PRIVATE_TEMPLATE){
            //User is gzhAdmin, otherwise security check will fail
            model.addAttribute("enableEdit", true);
        }
        else {
            //user is not WebAdmin and task is a public template
            model.addAttribute("enableEdit", false);
        }
    
        if (gzhTask.getType() == GzhTaskType.CUSTOMER_TASK) {
            GzhUser gzhUser = gzhTask.getGzhUser();
            model.addAttribute("gzhUser", gzhUser);
        }
        
        return "gzhOps/gzhTaskDetails";
    }
    
    
    @PreAuthorize("isAuthenticated()")
    @GetMapping("/gzhOps/task/template/private/create")
    public String getGzhTaskPrivateTemplateCreatePage(@AuthenticationPrincipal User user,  @RequestParam Map<String, String> params, Model model) {  
        GzhCompany gzhCompany = user.getAdminedGzhCompany();
        
        if (gzhCompany == null) {
            String message = messageSource.getMessage("gzh.gzhcompany.admined.not.bound", null, Locale.SIMPLIFIED_CHINESE);
            model.addAttribute("message", message);
            return "gzhOps/gzhOpsMessage";
        }
        model.addAttribute("gzhCompany", gzhCompany);
        
        return "gzhOps/gzhTaskCreatePrivateTemplate";
    }
    
    //QueryString gzhUserId is present when calling from gzhUser details page.
    //QueryString gzhUserId is not present when calling from the sidemenu.
    @PreAuthorize("isAuthenticated()")
    @GetMapping("/gzhOps/task/create")
    public String getGzhTaskCreatePage(@AuthenticationPrincipal User user,  @RequestParam Map<String, String> params, Model model) {  
        GzhCompany gzhCompany = user.getAdminedGzhCompany();
        
        if (gzhCompany == null) {
            String message = messageSource.getMessage("gzh.gzhcompany.admined.not.bound", null, Locale.SIMPLIFIED_CHINESE);
            model.addAttribute("message", message);
            return "gzhOps/gzhOpsMessage";
        }
        model.addAttribute("gzhCompany", gzhCompany);
        
        String gzhUserId = params.get("gzhUserId");
        if (gzhUserId != null) {
            GzhUser gzhUser = gzhUserRepository.findOne(Long.valueOf(gzhUserId));
            model.addAttribute("gzhUser", gzhUser);
        }

        return "gzhOps/gzhTaskCreate";
    }
    
    //REST API
    
    //create a private task template
    @PreAuthorize("isAuthenticated()")
    @PostMapping("/gzhOps/api/v1/task/template/create")
    public @ResponseBody String createGzhTaskPrivateTemplateApi(@AuthenticationPrincipal User user, @RequestBody GzhTask gzhTask, 
        HttpServletResponse response, Model model) {
        
        GzhCompany gzhCompany = user.getAdminedGzhCompany();
        if (gzhCompany == null) {
            String message = messageSource.getMessage("gzh.gzhcompany.admined.not.bound", null, Locale.SIMPLIFIED_CHINESE);
            model.addAttribute("message", message);
            return "gzhOps/gzhOpsMessage";
        }
        
        gzhTask.setType(GzhTaskType.PRIVATE_TEMPLATE);
        gzhTask.setGzhCompany(gzhCompany);
        gzhTaskRepository.save(gzhTask);
        
        for (GzhTaskStep step: gzhTask.getSteps()) {
            step.setGzhTask(gzhTask);
            gzhTaskStepRepository.save(step);
        }
        
        try {
            String gzhTaskJson = objectMapper.writeValueAsString(gzhTask);
            return  gzhTaskJson;
        } catch (JsonProcessingException e) {
            response.setStatus(HttpStatus.BAD_REQUEST.value());
            return e.getLocalizedMessage();
        }
    }
    
    //create a new task for gzhUser
    @PreAuthorize("isGzhAdminForGzhUser(#id) || isWebAdmin()")
    @PostMapping("/gzhOps/api/v1/task/create/gzhUser/{id}")
    public @ResponseBody String createGzhTaskForGzhUserApi(@AuthenticationPrincipal User user, @PathVariable("id") long id, @RequestBody GzhTask gzhTask, HttpServletResponse response) {  
        GzhUser gzhUser = gzhUserRepository.findOne(id);
        OffsetDateTime lastStatusChange = OffsetDateTime.now();
        log.info("createGzhTaskForGzhUserApi lastStatusChange=" + lastStatusChange);
        
        gzhTask.setType(GzhTaskType.CUSTOMER_TASK);
        gzhTask.setLastStatusChange(lastStatusChange);
        gzhTask.setGzhUser(gzhUser);
        gzhTaskRepository.save(gzhTask);
        
        for (GzhTaskStep step: gzhTask.getSteps()) {
            step.setGzhTask(gzhTask);
            step.setLastStatusChange(lastStatusChange);
            gzhTaskStepRepository.save(step);
        }
        
        try {
            String gzhTaskJson = objectMapper.writeValueAsString(gzhTask);
            return  gzhTaskJson;
        } catch (JsonProcessingException e) {
            response.setStatus(HttpStatus.BAD_REQUEST.value());
            return e.getLocalizedMessage();
        }
    }
    
    //save a task or template 
    @PreAuthorize("isGzhAdminForGzhTask(#id) || isWebAdmin()")
    @PostMapping("/gzhOps/api/v1/task/{id}/save")
    public @ResponseBody String saveGzhTaskApi(@AuthenticationPrincipal User user, @PathVariable("id") long id, @RequestBody GzhTask gzhTask, HttpServletResponse response) {  
        GzhTask currGzhTask = gzhTaskRepository.findOne(id);
        
        List<Long> newTaskStepIdList = gzhTask.getSteps()
            .stream()
            .map(step -> step.getId())
            .collect(Collectors.toList());
        log.info("updateGzhUserTaskOrTemplateDetails newTaskStepIdList=" + newTaskStepIdList);
        
        List<Long> oldTaskStepIdList = currGzhTask.getSteps()
            .stream()
            .map(step -> step.getId())
            .collect(Collectors.toList());
        log.info("updateGzhUserTaskOrTemplateDetails oldTaskStepIdList=" + oldTaskStepIdList);
        
        currGzhTask.setName(gzhTask.getName());
        currGzhTask.setNote(gzhTask.getNote());
        currGzhTask.setStatus(gzhTask.getStatus());
        currGzhTask.setLastStatusChange(gzhTask.getLastStatusChange());     
        gzhTaskRepository.save(currGzhTask);
        
        //save the steps in gzhTask. 
        //If the step.id is 0, a new step is created in db.
        //If the step.id is not 0, the step in db is updated.
        for (GzhTaskStep step: gzhTask.getSteps()) {
            step.setGzhTask(currGzhTask);
            gzhTaskStepRepository.save(step);
        }
        
        oldTaskStepIdList.removeAll(newTaskStepIdList);
        for (Long stepId: oldTaskStepIdList) {
            gzhTaskStepRepository.delete(stepId);
        }
        
        return "Success";
    }
    
    //Get Task template list including both public and private
    @PreAuthorize("isAuthenticated()")
    @GetMapping("/gzhOps/api/v1/task/template/list")
    public @ResponseBody String getGzhTaskTemplateListApi(@AuthenticationPrincipal User user, @RequestParam Map<String, String> params) throws JsonProcessingException {
        //QueryDSL has a bug to generate correct SQL for publicPredicate.or(privatePredicate)
        //It generate something like the following with cross join 
        //select ... from GzhTask cross join GzhUser 
        //    where gzhTask.gzhUser.id == gzhUser.id AND (gzhTaskTemplateType == PUBLIC OR gzhTask.gzhUser.gzhCompanyId == gzhUser.gzhCompany.id)
        //Since all the public templates does not have gzhUser, they will not be in the result set.
        Predicate publicPredicate = repoQueryFilter.filterGzhTaskPublicTemplate();
        List<GzhTask> gzhTaskpublicTemplateList = gzhTaskRepository.findAll(publicPredicate);
        log.info("getGzhTaskTemplateListApi gzhTaskPublicTemplateList=" + gzhTaskpublicTemplateList);
        
        Predicate privatePredicate = repoQueryFilter.filterGzhTaskPrivateTemplateByUser(user);
        List<GzhTask> gzhTaskPrivateTemplateList = gzhTaskRepository.findAll(privatePredicate);
        log.info("getGzhTaskTemplateListApi gzhTaskPrivateTemplateList=" + gzhTaskPrivateTemplateList);
        
        List<GzhTask> gzhTaskTemplateList = new ArrayList<GzhTask>();
        gzhTaskTemplateList.addAll(gzhTaskpublicTemplateList);
        gzhTaskTemplateList.addAll(gzhTaskPrivateTemplateList);
        String gzhTaskTemplateListJson = objectMapper.writeValueAsString(gzhTaskTemplateList);
        return  gzhTaskTemplateListJson;
    }
    
    //Get public task template
    @PreAuthorize("isAuthenticated()")
    @GetMapping("/gzhOps/api/v1/task/template/public/list")
    public @ResponseBody String getGzhTaskTemplatePublicListApi(@AuthenticationPrincipal User user) throws JsonProcessingException {

        Predicate publicPredicate = repoQueryFilter.filterGzhTaskPublicTemplate();
        List<GzhTask> gzhTaskpublicTemplateList = gzhTaskRepository.findAll(publicPredicate);
        log.info("getGzhTaskTemplateListApi gzhTaskPublicTemplateList=" + gzhTaskpublicTemplateList);

        String gzhTaskTemplateListJson = objectMapper.writeValueAsString(gzhTaskpublicTemplateList);
        return  gzhTaskTemplateListJson;
    }
}
