package com.cju.gzhOps;

import java.util.ArrayList;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

@Entity
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id", scope = GzhClientCompanyReport.class)
@Table(name="GzhClientCompanyReport")
public class GzhClientCompanyReport {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    
    //chanjet uses either book_id or tax_no to retrieve the report
    //chanjet should use acc_id consistently to retrieve all the information
    @Column(name = "book_id")
    private long bookId;
    //Year and Month for the report
    private String period;
    @Enumerated(EnumType.STRING)
    private GzhClientCompanyReportType reportType;
    
    //gzhClientCompanyId is the foreign key in the gzhClientCompany table in database
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name="gzhClientCompanyId", foreignKey=@ForeignKey(name = "GzhClientCompanyReport_FK_GzhClientCompanyId"))
    private GzhClientCompany gzhClientCompany;
    
    @OneToMany(mappedBy = "gzhClientCompanyReport", fetch = FetchType.LAZY)
    @OrderBy("chanjetClientCompanyAssetItem.assetId ASC")
    private List<GzhClientCompanyAssetReportItem> gzhClientCompanyAssetReportItemList = new ArrayList<GzhClientCompanyAssetReportItem>();

    @OneToMany(mappedBy = "gzhClientCompanyReport", fetch = FetchType.LAZY)
    @OrderBy("chanjetClientCompanyIncomeItem.itemId ASC")
    private List<GzhClientCompanyIncomeReportItem> gzhClientCompanyIncomeReportItemList = new ArrayList<GzhClientCompanyIncomeReportItem>();
    
    @OneToMany(mappedBy = "gzhClientCompanyReport", fetch = FetchType.LAZY)
    @OrderBy("chanjetClientCompanyCashItem.code ASC")
    private List<GzhClientCompanyCashReportItem> gzhClientCompanyCashReportItemList = new ArrayList<GzhClientCompanyCashReportItem>();
    
    protected GzhClientCompanyReport() {}
    public GzhClientCompanyReport(long id) {
        this.id = id;
    }
    
    public void setId(long id) {
        this.id = id;
    }
    public long getId() {
        return id;
    }

    public void setBookId(long bookId) {
        this.bookId = bookId;
    }
    public long getBookId() {
        return bookId;
    }
    
    public void setPeriod(String period) {
        this.period = period;
    }
    public String getPeriod() {
        return period;
    }
    
    public void setReportType(GzhClientCompanyReportType reportType) {
        this.reportType = reportType;
    }
    public GzhClientCompanyReportType getReportType() {
        return reportType;
    }
    
//    public void setErrMsg(String errMsg) {
//        this.errMsg = errMsg;
//    }
//    public String getErrMsg() {
//        return errMsg;
//    }
    
    public void setGzhClientCompany(GzhClientCompany gzhClientCompany) {
        this.gzhClientCompany = gzhClientCompany;
    }
    public GzhClientCompany getGzhClientCompany() {
        return gzhClientCompany;
    }
    
    public void setGzhClientCompanyAssetReportItemList(List<GzhClientCompanyAssetReportItem> gzhClientCompanyAssetReportItemList) {
        this.gzhClientCompanyAssetReportItemList = gzhClientCompanyAssetReportItemList;
    }
    public List<GzhClientCompanyAssetReportItem> getGzhClientCompanyAssetReportItemList() {
        return gzhClientCompanyAssetReportItemList;
    }
    
    public void setGzhClientCompanyIncomeReportItemList(List<GzhClientCompanyIncomeReportItem> gzhClientCompanyIncomeReportItemList) {
        this.gzhClientCompanyIncomeReportItemList = gzhClientCompanyIncomeReportItemList;
    }
    public List<GzhClientCompanyIncomeReportItem> getGzhClientCompanyIncomeReportItemList() {
        return gzhClientCompanyIncomeReportItemList;
    }
    
    public void setGzhClientCompanyCashReportItemList(List<GzhClientCompanyCashReportItem> gzhClientCompanyCashReportItemList) {
        this.gzhClientCompanyCashReportItemList = gzhClientCompanyCashReportItemList;
    }
    public List<GzhClientCompanyCashReportItem> getGzhClientCompanyCashReportItemList() {
        return gzhClientCompanyCashReportItemList;
    }
    
    @Override
    public String toString() {
        return String.format(
                "GzhClientCompanyReport[id=%d, bookId='%s', period='%s', reportType='%s']",
                id, bookId, period, reportType);
    }
}