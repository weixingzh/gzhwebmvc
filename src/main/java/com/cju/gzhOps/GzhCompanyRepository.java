package com.cju.gzhOps;

import java.util.List;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

//Spring Data REST takes the features of Spring HATEOAS and Spring Data JPA and combines them together automatically.
//Spring Data REST also supports Spring Data Neo4j, Spring Data Gemfire and Spring Data MongoDB as backend data stores.

//In a typical Java application, you’d expect to write a class that implements CustomerRepository. 
//But that’s what makes Spring Data JPA so powerful: You don’t have to write an implementation of the repository interface. 
//Spring Data JPA creates an implementation on the fly when you run the application.

//@RepositoryRestResource will create RESTful endpoints automatically
@Repository
public interface GzhCompanyRepository extends PagingAndSortingRepository<GzhCompany, Long>, QueryDslPredicateExecutor<GzhCompany> {

    List<GzhCompany> findByName(String name);
    List<GzhCompany> findById(Long id);
    GzhCompany findOneByGzhId(String gzhId);
}