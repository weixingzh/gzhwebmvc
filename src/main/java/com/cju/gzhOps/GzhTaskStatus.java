package com.cju.gzhOps;

import com.fasterxml.jackson.annotation.JsonProperty;

public enum GzhTaskStatus {
    @JsonProperty("未开始") NOT_STARTED("NotStarted"), @JsonProperty("办理中") IN_PROCESS("InProcess"),  @JsonProperty("已完成") COMPLETED("Completed");
    
    private String serviceItemStatus;
    GzhTaskStatus(String serviceItemStatus) {
        this.serviceItemStatus = serviceItemStatus;
    }
    
    @Override
    public String toString() {
        switch (serviceItemStatus) {
            case "NotStarted": 
                return "未开始";
            case "InProcess":
                return "办理中";
            case "Completed":
                return "已完成";
            default: 
                return "未开始";
        }
    }
}
