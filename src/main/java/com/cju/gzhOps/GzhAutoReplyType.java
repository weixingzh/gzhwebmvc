package com.cju.gzhOps;

import com.fasterxml.jackson.annotation.JsonProperty;

public enum GzhAutoReplyType {
    @JsonProperty("公共") PUBLIC("Public"),  @JsonProperty("私有") PRIVATE("Private");
    private String type;
    private 
    GzhAutoReplyType(String type) {
        this.type = type;
    }
    
    @Override
    public String toString() {
        switch (type) {
            case "Public": 
                return "公共";
            case "Private": 
                return "私有";
            default: 
                return "公共";
        }
    }
}
