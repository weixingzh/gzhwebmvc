package com.cju.gzhOps;

import java.io.IOException;
import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.annotation.PostConstruct;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;
import org.springframework.util.StreamUtils;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;

@Service
public class GzhCompanyService {
    private Logger log = LoggerFactory.getLogger(GzhCompanyService.class);
    
    @Value(value = "classpath:/gzh/gsswdl_A_menu.json")
    private Resource gsswdl_A_menuJson;

    @Value(value = "classpath:/gzh/gdtech_menu.json")
    private Resource gdTech_menuJson;
    
    @Value("${gzh.weixin.menu.host}")
    private String weixinMenuHost;
    
    @Autowired 
    private ObjectMapper objectMapper;
    
    private Map<GzhCompanyServiceType, Resource> gzhCompanyToMenuMap = new HashMap<GzhCompanyServiceType, Resource>();
    
    @PostConstruct
    public void init() {
        gzhCompanyToMenuMap.put(GzhCompanyServiceType.GDTECH, gdTech_menuJson);
        gzhCompanyToMenuMap.put(GzhCompanyServiceType.GSSWDL_A, gsswdl_A_menuJson);
    }
    
    public String getSystemDefaultGzhMenuJson(GzhCompany gzhCompany) {
        long id = gzhCompany.getId();
        long referalNum = gzhCompany.getOwnerUser().getReferal().getReferalNum();
        Resource menuJson = gzhCompanyToMenuMap.get(gzhCompany.getServiceType());
        
        String menuJsonStr;
        
        //getInputStream may throw an IOException
        try {
            menuJsonStr = StreamUtils
                    .copyToString(menuJson.getInputStream(), Charset.forName("UTF-8"))
                    .replaceAll("@GZHWEBHOST", weixinMenuHost)
                    .replaceAll("@ID", Long.toString(id))
                    .replaceAll("@REFERAL_NUM", Long.toString(referalNum));
        } catch (JsonProcessingException e) {
            e.printStackTrace();
            return "{}";
        }
        catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return "{}";
        }

        //If a user creates a html page with predefined title as external link, we still use the url with the following format
        //rather than replace the url with the external link.
        //    "url":  "http://@GZHWEBHOST/pub/gzh/company/@ID/localHtml?pageTitle=IntroPage"
        //When a gzh subscriber access this url, a redirect to the external link is returned in gzhController.java.
        log.info("getSystemDefaultGzhMenuJson menuJsonStr=" + menuJsonStr);
        return menuJsonStr;
    }
    
    public String getGzhMenuJson(GzhCompany gzhCompany) {
        if (gzhCompany.getGzhMenuType() == GzhCompanyMenuType.SELF_DEFINED) {
            String selfDefinedGzhMenuJson = gzhCompany.getSelfDefinedGzhMenu();
            //selfDefinedGzhMenu does not contain any placeholders
            if (selfDefinedGzhMenuJson == null)
                return "{}";
            else
                return selfDefinedGzhMenuJson;
        }
        else if (gzhCompany.getGzhMenuType() == GzhCompanyMenuType.SYSTEM_DEFAULT) {
            String defaultGzhMenuJson = getSystemDefaultGzhMenuJson(gzhCompany);
            return defaultGzhMenuJson;
        }
        else
            return "{}";
    }
}
