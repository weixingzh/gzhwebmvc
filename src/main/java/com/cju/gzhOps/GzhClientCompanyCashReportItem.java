package com.cju.gzhOps;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import com.cju.chanjet.ChanjetClientCompanyCashItem;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

@Entity
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id", scope = GzhClientCompanyCashReportItem.class)
public class GzhClientCompanyCashReportItem {
    @Id    
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    
    @Embedded
    private ChanjetClientCompanyCashItem chanjetClientCompanyCashItem;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name="gzhClientCompanyReportId", foreignKey=@ForeignKey(name = "GzhClientCompanyCashReportItem_FK_GzhClientCompanyReportId"))
    private GzhClientCompanyReport gzhClientCompanyReport;
    
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name="parentItemId", foreignKey=@ForeignKey(name = "GzhClientCompanyCashReportItem_FK_ParentItemId"))
    private GzhClientCompanyCashReportItem parentItem;
    
    @OneToMany(mappedBy = "parentItem", fetch = FetchType.LAZY)
    @OrderBy("chanjetClientCompanyCashItem.code ASC")
    private List<GzhClientCompanyCashReportItem> childItemList = new ArrayList<GzhClientCompanyCashReportItem>();
    
    protected GzhClientCompanyCashReportItem() {}
    public GzhClientCompanyCashReportItem(long id) {
        this.id = id;
    }
    
    public void setId(long id) {
        this.id = id;
    }
    public long getId() {
        return id;
    }

    public void setChanjetClientCompanyCashItem(ChanjetClientCompanyCashItem chanjetClientCompanyCashItem) {
        this.chanjetClientCompanyCashItem = chanjetClientCompanyCashItem;
    }
    public ChanjetClientCompanyCashItem getChanjetClientCompanyCashItem() {
        return chanjetClientCompanyCashItem;
    }
    
    public void setGzhClientCompanyReport(GzhClientCompanyReport gzhClientCompanyReport) {
        this.gzhClientCompanyReport = gzhClientCompanyReport;
    }
    public GzhClientCompanyReport getGzhClientCompanyReport() {
        return gzhClientCompanyReport;
    }
    
    public void setParentItem(GzhClientCompanyCashReportItem parentItem) {
        this.parentItem = parentItem;
    }
    public GzhClientCompanyCashReportItem getParentItem() {
        return parentItem;
    }

    public void setChildItemList(List<GzhClientCompanyCashReportItem> childItemList) {
        this.childItemList = childItemList;
    }
    public List<GzhClientCompanyCashReportItem> getChildItemList() {
        return childItemList;
    }
    
    @Override
    public String toString() {
        return String.format(
                "GzhClientCompanyAsset[id=%d, chanjetClientCompanyCashItem='%s']",
                id, chanjetClientCompanyCashItem.toString());
    }
}