package com.cju.gzhOps;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.PostLoad;
import javax.persistence.Table;
import javax.persistence.Transient;
import com.cju.BeanProvider;
import com.cju.chanjet.ChanjetClientCompany;
import com.cju.user.User;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

// Company refers to accounting company who provides accounting service to its customers, who are small businesses. 
// Company will be created from Admin website.

@Entity
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id", scope = GzhClientCompany.class)
@Table(name = "GzhClientCompany")
public class GzhClientCompany {
    @Id    
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    
    //The chanjetClientCompany for this GzhClientCompany
    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name="chanjetClientCompanyId", foreignKey = @ForeignKey(name = "GzhClientCompany_FK_ChanjetClientCompanyId"))
    private ChanjetClientCompany chanjetClientCompany;
    
    private LocalDate firstImportDate;
    
    //This is the managing side of the ManyToMany relationship. 
    //Only persistent action on the managing side will update the database table.
    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name="GzhClientCompany_GzhUser",
        joinColumns=@JoinColumn(name="gzhClientCompanyId", referencedColumnName="id", foreignKey = @ForeignKey(name = "GzhClientCompanyGzhUser_FK_GzhClientCompanyId")),
        inverseJoinColumns=@JoinColumn(name="gzhUserId", referencedColumnName="id", foreignKey = @ForeignKey(name = "GzhClientCompanyGzhUser_FK_GzhUserId")))
    private List<GzhUser> gzhUserList = new ArrayList<GzhUser>();
    
    //All the relationships in JPA are unidirectional. 
    //It is not needed for User to keep the reverse unidirectional OneToMany relationship.
    //importedByUserId is the foreign key in the gzhClientCompany table in database
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name="importedByUserId", foreignKey = @ForeignKey(name = "GzhClientCompany_FK_ImportedByUserId"))
    private User importedByUser;

    //gzhCompanyId is the foreign key in the gzhClientCompany table in database
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name="gzhCompanyId", foreignKey = @ForeignKey(name = "GzhClientCompany_FK_GzhCompanyId"))
    private GzhCompany gzhCompany;
    
    //LAZY means the gzhClientCompanyReportList will be loaded from database only when getGzhClientCompanyReportList() is called
    @OneToMany(mappedBy = "gzhClientCompany", fetch = FetchType.LAZY)
    private List<GzhClientCompanyReport> gzhClientCompanyReportList = new ArrayList<GzhClientCompanyReport>();
    
    public GzhClientCompany() {}
    public GzhClientCompany(long id) {
    	this.id = id;
    }

    @PostLoad
    public void postLoad() {
        //System.out.println("GzhClientCompany postLoad() called: ");
        //GzhClientCompanyReportRepository repo = BeanProvider.getGzhClientCompanyReportRepository();
        //long bookId = chanjetClientCompany.getBookId(); 
        //Predicate predicate = BeanProvider.getRepositoryQueryFilter().filterGzhClientCompanyReportBaseByBookIdAndReportType(bookId, "Asset");
        //OrderSpecifier<String> sortSpec = QGzhClientCompanyReportBase.gzhClientCompanyReportBase.period.desc();
        //GzhClientCompanyReportBase report = repo.findFirstByBookIdAndReportTypeOrderByPeriodDesc(bookId, "Asset");
        //setLastReportAsset(report);
    }
    
    public void setId(long id) {
    	this.id = id;
    }
    public long getId() {
    	return id;
    }
    
    public void setFirstImportDate(LocalDate firstImportDate) {
        this.firstImportDate = firstImportDate;
    }
    public LocalDate getFirstImportDate() {
        return firstImportDate;
    }
    
    public void setImportedByUser(User importedByUser) {
    	this.importedByUser = importedByUser;
    }
    public User getImportedByUser() {
    	return importedByUser;
    }
    
    public void addGzhUser(GzhUser gzhUser) {
        this.gzhUserList.add(gzhUser);
    }
    public void setGzhUserList(List<GzhUser> gzhUserList) {
        this.gzhUserList = gzhUserList;
    }
    public List<GzhUser> getGzhUserList() {
        return gzhUserList;
    }
    
    public void setChanjetClientCompany(ChanjetClientCompany chanjetClientCompany) {
    	this.chanjetClientCompany = chanjetClientCompany;
    }
    public ChanjetClientCompany getChanjetClientCompany() {
    	return chanjetClientCompany;
    }
    
    public void setGzhCompany(GzhCompany gzhCompany) {
        this.gzhCompany = gzhCompany;
    }
    public GzhCompany getGzhCompany() {
        return gzhCompany;
    }
    
    //    public void setLastReportAsset(GzhClientCompanyReport lastReport) {
    //        this.lastReportAsset = lastReport;
    //    }
    //    public GzhClientCompanyReport getLastReportAsset() {
    //        return lastReportAsset;
    //    }
    
    public void setGzhClientCompanyReportList(List<GzhClientCompanyReport> gzhClientCompanyReportList) {
        this.gzhClientCompanyReportList = gzhClientCompanyReportList;
    }
    public List<GzhClientCompanyReport> getGzhClientCompanyReportList() {
        return gzhClientCompanyReportList;
    }
        
    @Override
    public String toString() {
        return String.format(
                "Company[id=%d]",
                id);
    }
}