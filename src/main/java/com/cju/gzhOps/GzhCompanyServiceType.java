package com.cju.gzhOps;

import com.fasterxml.jackson.annotation.JsonProperty;

public enum GzhCompanyServiceType {
    @JsonProperty("感动科技") GDTECH("GDTech"), 
    @JsonProperty("工商税务代理_A") GSSWDL_A("GSSWDL_A"), 
    @JsonProperty("未知服务类型") UNKNOWN("Unknown");
    private String serviceType;
    private GzhCompanyServiceType(String serviceType) {
        this.serviceType = serviceType;
    }
    
    public static GzhCompanyServiceType fromString(String str) {
        for (GzhCompanyServiceType service: GzhCompanyServiceType.values()) {
            if (service.serviceType.equals(str)) {
                return service;
            }
        }
        //Default return
        return UNKNOWN;
    }
    
    @Override
    public String toString() {
        switch (serviceType) {
            case "GDTech": 
                return "感动科技";
            case "GSSWDL_A": 
                return "工商税务代理_A";
            default: 
                return "未知服务类型";
        }
    }
}
