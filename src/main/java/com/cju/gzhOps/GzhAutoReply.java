package com.cju.gzhOps;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

@Entity
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id", scope = GzhAutoReply.class)
@Table(name = "GzhAutoReply",
       uniqueConstraints = { @UniqueConstraint(columnNames={"gzhCompanyId", "key"}, name="GzhAutoReply_UNIQUE_GzhCompanyAndKey")})
public class GzhAutoReply {
    @Id    
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    
    private String key;
    private String message;
    @Enumerated(EnumType.STRING)
    private GzhAutoReplyType type;
    
    //For private GzhAutoReply, gzhCompany is not null.
    //For public GzhAutoReply, gzhCompany is null.
    //@JsonIgnore means do not serialize the field into Json.
    @JsonIgnore
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="gzhCompanyId", foreignKey = @ForeignKey(name = "GzhAutoReply_FK_GzhCompanyId"))
    private GzhCompany gzhCompany;
    
    public GzhAutoReply() {}
    public GzhAutoReply(long id) {
        this.id = id;
    }
    
    public void setId(long id) {
    	this.id = id;
    }
    public long getId() {
    	return id;
    }
    
    public void setType(GzhAutoReplyType type) {
        this.type = type;
    }
    public GzhAutoReplyType getType() {
        return type;
    }
    
    public void setKey(String key) {
    	this.key = key;
    }
    public String getKey() {
    	return key;
    }
    
    public void setMessage(String message) {
        this.message = message;
    }
    public String getMessage() {
        return message;
    }
    
    public void setGzhCompany(GzhCompany gzhCompany) {
        this.gzhCompany = gzhCompany;
    }
    public GzhCompany getGzhCompany() {
        return gzhCompany;
    }
    
    @Override
    public String toString() {
        return String.format(
                "GzhAutoReply[id=%s, key='%s', message='%s']",
                id, key, message);
    }

}