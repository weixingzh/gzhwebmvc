package com.cju.gzhOps;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletResponse;
import org.apache.http.HttpStatus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.cju.security.RepositoryQueryFilter;
import com.cju.user.User;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.querydsl.core.types.Predicate;

@Controller
public class GzhContactController {
    private Logger log = LoggerFactory.getLogger(GzhContactController.class);

    @Autowired 
    private GzhContactRepository gzhContactRepository; 
    
    @Autowired
    private MessageSource messageSource;
    
    @Autowired 
    RepositoryQueryFilter repoQueryFilter;
    
    @Autowired
    private ObjectMapper objectMapper;
    
    @PreAuthorize("isAuthenticated()")
    @GetMapping("/gzhOps/contact/list")
    public String getGzhContactMyList(@AuthenticationPrincipal User user, Pageable pageable, Model model) {
        Predicate predicate = repoQueryFilter.filterGzhContactByUser(user);
        Page<GzhContact> gzhContactPage = gzhContactRepository.findAll(predicate, pageable);
        float nrOfPages = gzhContactPage.getTotalPages();
        model.addAttribute("maxPages", nrOfPages);
        model.addAttribute("gzhContactPage", gzhContactPage);
        log.info("getGzhContactMyList gzhContactPage.size=" + gzhContactPage.getSize() + ", gzhContactPage.Number " + gzhContactPage.getNumber());

        return "gzhOps/gzhContactList";
    }
    
    //Rest API

    //postGzhContactkApi() handle both create and update cases.
    //For create, gzhContact.id is 0. For update, gzhContact.id is not 0.
    //Data is posted in Json
    @PreAuthorize("isAuthenticated()")
    @PostMapping("/gzhOps/api/v1/contact/myList")
    public @ResponseBody String postGzhContactApi(HttpServletResponse response, @AuthenticationPrincipal User user, 
        @RequestBody GzhContactHttpPostData gzhContactHttpPostData) throws IOException { 
        GzhCompany gzhCompany = user.getAdminedGzhCompany();
        if (gzhCompany == null && !user.isWebAdmin()) {
            String message = messageSource.getMessage("gzh.gzhcompany.admined.not.bound", null, Locale.SIMPLIFIED_CHINESE);
            log.error("postGzhContactApi, user's adminedGzhCompany is null. " + user);
            response.sendError(HttpStatus.SC_UNAUTHORIZED, message);
            //Need to return to stop further processing.
            return "Fail";
        }
        
        List<Long> removeList = gzhContactHttpPostData.getRemoveList();
        for(Long removeId: removeList) {
            log.info("removeId=" + removeId);
            gzhContactRepository.delete(removeId);
        }
        List<GzhContact> createOrEditList = gzhContactHttpPostData.getCreateOrEditList();
        
        for(GzhContact contact: createOrEditList) {
            log.info("contact:" + contact);
            //gzhCompany is not set in contact from request
            contact.setGzhCompany(gzhCompany);
            gzhContactRepository.save(contact);
        }
        
        //gzhCompany is not serialized due to @JsonIgnore in GzhContact.java
        //gzhCompany is from user, which is retrieved from UserDetailsServiceImpl in Spring Security context outside of controller. 
        //That hibernate session is closed after user is retrieved. Spring MVC create a new hibernate session in controller.
        //Thus it fails to get the LAZY load values from gzhCompany in controller and thymeleaf because the session used to retrieve user is closed.
        //So gzhCompany is annotated with @JsonIgnore in GzhContact.
        
        return  "Success";
    }
}
