package com.cju.gzhOps;

import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import com.cju.chanjet.ChanjetClientCompanyAssetItem;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

@Entity
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id", scope = GzhClientCompanyAssetReportItem.class)
public class GzhClientCompanyAssetReportItem {
    @Id    
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    
    @Embedded
    private ChanjetClientCompanyAssetItem chanjetClientCompanyAssetItem;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name="gzhClientCompanyReportId", foreignKey=@ForeignKey(name = "GzhClientCompanyAssetReportItem_FK_GzhClientCompanyReportId"))
    private GzhClientCompanyReport gzhClientCompanyReport;

    protected GzhClientCompanyAssetReportItem() {}
    public GzhClientCompanyAssetReportItem(long id) {
        this.id = id;
    }
    
    public void setId(long id) {
        this.id = id;
    }
    public long getId() {
        return id;
    }
    
    public void setChanjetClientCompanyAssetItem(ChanjetClientCompanyAssetItem chanjetClientCompanyAssetItem) {
        this.chanjetClientCompanyAssetItem = chanjetClientCompanyAssetItem;
    }
    public ChanjetClientCompanyAssetItem getChanjetClientCompanyAssetItem() {
        return chanjetClientCompanyAssetItem;
    }

    public void setGzhClientCompanyReport(GzhClientCompanyReport gzhClientCompanyReport) {
        this.gzhClientCompanyReport = gzhClientCompanyReport;
    }
    public GzhClientCompanyReport getGzhClientCompanyReport() {
        return gzhClientCompanyReport;
    }
    
    @Override
    public String toString() {
        return String.format("GzhClientCompanyAssetItem[id=%d, chanjetClientCompanyAssetItem='%s']",
                id, chanjetClientCompanyAssetItem.toString());
    }
}