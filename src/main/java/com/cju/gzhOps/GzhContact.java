package com.cju.gzhOps;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import com.cju.BeanProvider;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;


//@JsonManagedReference is the forward part of reference – the one that gets serialized normally. 
//@JsonBackReference is the back part of reference – it will be omitted from serialization.
//@JsonManagedReference and @JsonBackReference does not handle bi-directional circular reference well in deserialization.
//Use @JsonIdentityInfo for serialization/deserialization rather than @JsonManagedReference and @JsonBackReference

@Entity
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id", scope = GzhContact.class)
@Table(name = "GzhContact",
       uniqueConstraints = { @UniqueConstraint(columnNames={"gzhCompanyId", "name"}, name="GzhContact_UNIQUE_GzhCompanyAndName")}
)
public class GzhContact {
    @Id    
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    private String name;
    private String jobTitle;
    private String mobile;
    private String weixin;
    private String email;
    
    //gzhCompanyId is the foreign key in the GzhContact table in database
    //@JsonIgnore means do not serialize the field into Json.
    @JsonIgnore
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="gzhCompanyId", foreignKey = @ForeignKey(name = "GzhContact_FK_GzhCompanyId"))
    private GzhCompany gzhCompany;
     
    public GzhContact() {}
    public GzhContact(long id) {
        this.id = id;
    }
    public GzhContact(String name) {
        this.name = name;
    }
    
    public void setId(long id) {
    	this.id = id;
    }
    public long getId() {
    	return id;
    }
    
    public void setName(String name) {
        this.name = name;
    }
    public String getName() {
        return name;
    }

    public void setJobTitle(String jobTitle) {
        this.jobTitle = jobTitle;
    }
    public String getJobTitle() {
        return jobTitle;
    }
    
    public void setMobile(String mobile) {
        this.mobile = mobile;
    }
    public String getMobile() {
        return mobile;
    }
    
    public void setWeixin(String weixin) {
        this.weixin = weixin;
    }
    public String getWeixin() {
        return weixin;
    }
    
    public String getEmail() {
        return email;
    }
    public void setEmail(String email) {
        this.email = email;
    }
    
    public void setGzhCompany(GzhCompany gzhCompany) {
        this.gzhCompany = gzhCompany;
    }
    public GzhCompany getGzhCompany() {
        return gzhCompany;
    }
    public void setGzhCompanyByGzhId(String gzhId) {
        GzhCompany gzhCompany = BeanProvider.getGzhCompanyRepository().findOneByGzhId(gzhId);
        this.gzhCompany = gzhCompany;
    }
    
    @Override
    public String toString() {
        return String.format(
                "GzhUser[id=%s, name='%s', mobile='%s']",
                id, name, mobile);
    }

}