package com.cju.gzhOps;

import java.util.Locale;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import com.cju.user.User;

@Controller
public class GzhSystemLinkController {
    private Logger log = LoggerFactory.getLogger(GzhSystemLinkController.class);

    @Autowired 
    private GzhSystemLinkRepository gzhSystemLinkRepository; 
    
    @Autowired
    private MessageSource messageSource;
    
    @Value("${gzh.weixin.menu.host}")
    private String weixinMenuHost;
    
    @PreAuthorize("isAuthenticated()")
    @GetMapping("/gzhOps/systemLink/list")
    public String getGzhSystemLinkList(@AuthenticationPrincipal User user, Pageable pageable, Model model) {
        GzhCompany gzhCompany = user.getAdminedGzhCompany();
        if (gzhCompany == null) {
            String message = messageSource.getMessage("gzh.gzhcompany.admined.not.bound", null, Locale.SIMPLIFIED_CHINESE);
            model.addAttribute("message", message);
            return "gzhOps/gzhOpsMessage";
        }
        long id = gzhCompany.getId();
        long referalNum = gzhCompany.getOwnerUser().getReferal().getReferalNum();
        
        Page<GzhSystemLink> gzhSystemLinkPage = gzhSystemLinkRepository.findAll(pageable);
        //float nrOfPages = gzhSystemLinkPage.getTotalPages();
        //model.addAttribute("maxPages", nrOfPages);
        gzhSystemLinkPage.forEach(gzhSystemLink -> {
            String url = gzhSystemLink.getUrl()
               .replaceAll("@GZHWEBHOST", weixinMenuHost)
               .replaceAll("@ID", Long.toString(id))
               .replaceAll("@REFERAL_NUM", Long.toString(referalNum));
            
            gzhSystemLink.setUrl(url);
        });
        
        model.addAttribute("gzhSystemLinkPage", gzhSystemLinkPage);
        log.info("getGzhContactMyList gzhContactPage.size=" + gzhSystemLinkPage.getSize() + ", gzhSystemLinkPage.Number " + gzhSystemLinkPage.getNumber());

        return "gzhOps/gzhSystemLinkList";
    }
}
