package com.cju.gzhOps;

import com.fasterxml.jackson.annotation.JsonProperty;

public enum GzhHtmlPageType {
    @JsonProperty("网页模板") TEMPLATE("Template"), 
    @JsonProperty("自定义网页") SELF_DEFINED("SelfDefinedPage");
    private String htmlPageType;
    private GzhHtmlPageType(String htmlPageType) {
        this.htmlPageType = htmlPageType;
    }
    
    public static GzhHtmlPageType fromString(String str) {
        for (GzhHtmlPageType type: GzhHtmlPageType.values()) {
            if (type.htmlPageType.equals(str)) {
                return type;
            }
        }
        //Default return
        return TEMPLATE;
    }
    
    //used in gzhHtmlPageDetails.html
    public String getValue() {
        return htmlPageType;
    }
    
    @Override
    public String toString() {
        switch (htmlPageType) {
            case "Template": 
                return "网页模板";
            case "SelfDefinedPage": 
                return "自定义网页";
            default: 
                return "网页模板";
        }
    }
}
