package com.cju.gzhOps;


import java.time.LocalDate;
import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import com.cju.BeanProvider;
import com.cju.billing.Account;
import com.cju.billing.BillingStatusChange;
import com.cju.chanjet.YesNoType;
import com.cju.file.Image;
import com.cju.gzh.WeiXinService;
import com.cju.gzh.WeixinAccessToken;
import com.cju.order.Order;
import com.cju.user.User;

// Company refers to accounting company who provides accounting service to its customers, who are small businesses. 
// Company will be created from Admin website.

@Entity
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id", scope = GzhCompany.class)
@Table(name = "GzhCompany", 
       uniqueConstraints = {@UniqueConstraint(columnNames = "appId", name = "GzhCompany_UNIQUE_AppId"),
                            @UniqueConstraint(columnNames = "gzhId", name = "GzhCompany_UNIQUE_GzhId"),
                            @UniqueConstraint(columnNames = "name", name = "GzhCompany_UNIQUE_Name")}
      ) 
//Not needed if database table name is the same as the class name.
public class GzhCompany {
	
	@Transient
	private Logger log = LoggerFactory.getLogger(GzhCompany.class);

	//AUTO strategy uses the global number generator to generate a primary key for every new entity object
	//IDENTIFY strategy uses a separate identity generator per type hierarchy, so generated values are unique only per type hierarchy.
	//SEQUENCE strategy generates an automatic value as soon as a new entity object is persisted (i.e. before commit)
	//@SequenceGenerator(name="seq", initialValue=1, allocationSize=100)
	//@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="seq")
	
    @Id    
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    @NotNull
    private String name;
    //For each gzh, there is a corresponding weixin ID. This is the ToUserName in event message from user to gzh.
    private String gzhId;
    private String appId;  
    @Enumerated(EnumType.STRING) 
    private YesNoType appIdEditable= YesNoType.YES;
    private String appSecret;
    private String accessToken;
    //For now, assume chanjetAppKey is valid across all gzhCompany. chanjetAppKey is kept just in case when each gzhCompany needs a chanjetAppKey.
    private String chanjetAppKey;
    @Column(columnDefinition= "TIMESTAMP WITH TIME ZONE")
    private OffsetDateTime accessTokenExpires = OffsetDateTime.now();
    @Enumerated(EnumType.STRING) 
    private GzhCompanyServiceType serviceType;
    @Enumerated(EnumType.STRING)
    private GzhCompanyBillingStatus billingStatus;
    @Enumerated(EnumType.STRING)
    private GzhCompanyPaymentStatus paymentStatus;
    private LocalDate paymentInDebtDate;
    @Enumerated(EnumType.STRING)
    private GzhCompanyServiceRunningStatus serviceRunningStatus;
    private LocalDate trialStartDate;
    private LocalDate activeBillingStartDate;
    
    @Enumerated(EnumType.STRING)
    private GzhCompanyMenuType gzhMenuType;
    @Column(columnDefinition = "text")
    private String selfDefinedGzhMenu;
    
    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name="ownerUserId", foreignKey = @ForeignKey(name = "GzhCompany_FK_OwnerUserId"))
    private User ownerUser;
    
    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name="accountId", foreignKey = @ForeignKey(name = "GzhCompany_FK_AccountId"))
    private Account account;
    
    @OneToMany(mappedBy = "adminedGzhCompany", fetch = FetchType.EAGER)
    private List<User> adminUserList = new ArrayList<User>();
   
    //LAZY means the gzhUserList will be loaded from database only when gzhUserList() is called
    @OneToMany(mappedBy = "gzhCompany", fetch = FetchType.LAZY, cascade = CascadeType.REMOVE)
    private List<GzhUser> gzhUserList = new ArrayList<GzhUser>();
    
    //LAZY means the gzhClientCompanyList will be loaded from database only when getGzhClientCompanyList() is called
    @OneToMany(mappedBy = "gzhCompany", fetch = FetchType.LAZY, cascade = CascadeType.REMOVE)
    private List<GzhClientCompany> gzhClientCompanyList = new ArrayList<GzhClientCompany>();
    
    //When a gzhCompany is removed, all its GzhHtmlPage are removed by hibernate due to Cascade.REMOVE setting.
    //This relationship is not used in other places for now.
    @OneToMany(mappedBy = "gzhCompany", fetch = FetchType.LAZY, cascade = CascadeType.REMOVE)
    private List<GzhHtmlPage> gzhHtmlPageList = new ArrayList<GzhHtmlPage>();
    
    //When a gzhCompany is removed, all its GzhContact are removed by hibernate due to Cascade.REMOVE setting.
    @OneToMany(mappedBy = "gzhCompany", fetch = FetchType.LAZY, cascade = CascadeType.REMOVE)
    private List<GzhContact> gzhContactList = new ArrayList<GzhContact>();
    
    //LAZY means the gzhUserList will be loaded from database only when getImageList() is called
    @OneToMany(mappedBy = "gzhCompany", fetch = FetchType.LAZY, cascade = CascadeType.REMOVE)
    private List<Image> imageList = new ArrayList<Image>();
    
    @OneToMany(mappedBy = "gzhCompany", fetch = FetchType.LAZY, cascade = CascadeType.REMOVE)
    private List<GzhTask> taskPrivateTemplateList = new ArrayList<GzhTask>();
    
    @OneToMany(mappedBy = "gzhCompany", fetch = FetchType.LAZY, cascade = CascadeType.REMOVE)
    private List<BillingStatusChange> billingStatusChangeList = new ArrayList<BillingStatusChange>();
    
    @OneToMany(mappedBy = "gzhCompany", fetch = FetchType.LAZY, cascade = CascadeType.REMOVE)
    private List<Order> orderList = new ArrayList<Order>();
    
    public GzhCompany() {}
    public GzhCompany(long id) {
    	this.id = id;
    }
    public GzhCompany(String name, String appId, String appSecret, String gzhId) {
        this.name = name;
        this.appId = appId;
        this.appSecret = appSecret;
        this.gzhId = gzhId;
    }

    public void setId(long id) {
    	this.id = id;
    }
    public long getId() {
    	return id;
    }

    public void setName(String name) {
    	this.name = name;
    }
    public String getName() {
    	return name;
    }
    
    public void setGzhId(String gzhId) {
    	this.gzhId = gzhId;
    }
    public String getGzhId() {
    	return gzhId;
    }
    
    public void setAppId(String appId) {
    	this.appId = appId;
    }
    public String getAppId() {
    	return appId;
    }
    
    public void setAppIdEditable(YesNoType appIdEditable) {
        this.appIdEditable = appIdEditable;
    }
    public YesNoType getAppIdEditable() {
        return appIdEditable;
    }
    
    public void setAppSecret(String appSecret) {
    	this.appSecret = appSecret;
    }
    public String getAppSecret() {
    	return appSecret;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }
    public String getAccessToken() {
        return accessToken;
    }
    
    public void setChanjetAppKey(String chanjetAppKey) {
        this.chanjetAppKey = chanjetAppKey;
    }
    public String getChanjetAppKey() {
        return chanjetAppKey;
    }

    public void setAccessTokenExipres(OffsetDateTime accessTokenExpires) {
        this.accessTokenExpires = accessTokenExpires;
    }
    public OffsetDateTime getAccessTokenExipres() {
        return accessTokenExpires;
    }
    
    public String checkAndGetAccessToken() {
        OffsetDateTime currTime = OffsetDateTime.now();
        if (currTime.isAfter(accessTokenExpires)) {
            WeiXinService weiXinService = BeanProvider.getWeiXinService();
            WeixinAccessToken weixinAccessToken = weiXinService.getGzhCompanyAccessToken(appId, appSecret);
            if (weixinAccessToken.getErrCode() != null) {
                log.error("checkAndGetAccessToken() Error in getting gzhCompany accessToken for gzhCompany " + name);
                setAccessToken(null);
                setAccessTokenExipres(OffsetDateTime.now());
            }
            else {
                setAccessToken(weixinAccessToken.getAccessToken());
                setAccessTokenExipres(OffsetDateTime.now().plusSeconds(weixinAccessToken.getExpiresIn()));
                //Save to DB
                BeanProvider.getGzhCompanyRepository().save(this);
            }
        }
        return accessToken;
    }
    
    public void setServiceType(GzhCompanyServiceType serviceType) {
        this.serviceType = serviceType;
    }
    public GzhCompanyServiceType getServiceType() {
        return serviceType;
    }
    
    public void setBillingStatus(GzhCompanyBillingStatus billingStatus) {
    	this.billingStatus = billingStatus;
    }
    public GzhCompanyBillingStatus getBillingStatus() {
    	return billingStatus;
    }
    
    public void setPaymentStatus(GzhCompanyPaymentStatus paymentStatus) {
        this.paymentStatus = paymentStatus;
    }
    public GzhCompanyPaymentStatus getPaymentStatus() {
        return paymentStatus;
    }
    
    public void setPaymentInDebtDate(LocalDate paymentInDebtDate) {
        this.paymentInDebtDate = paymentInDebtDate;
    }
    public LocalDate getPaymentInDebtDate() {
        return paymentInDebtDate;
    }
    
    public void setServiceRunningStatus(GzhCompanyServiceRunningStatus serviceRunningStatus) {
        this.serviceRunningStatus = serviceRunningStatus;
    }
    public GzhCompanyServiceRunningStatus getServiceRunningStatus() {
        return serviceRunningStatus;
    }
    
    public void setTrialStartDate(LocalDate trialStartDate) {
        this.trialStartDate = trialStartDate;
    }
    public LocalDate getTrialStartDate() {
        return trialStartDate;
    }
    
    public void setActiveBillingStartDate(LocalDate activeBillingStartDate) {
        this.activeBillingStartDate = activeBillingStartDate;
    }
    public LocalDate getActiveBillingStartDate() {
        return activeBillingStartDate;
    }
    
    public void setGzhMenuType(GzhCompanyMenuType gzhMenuType) {
        this.gzhMenuType = gzhMenuType;
    }
    public GzhCompanyMenuType getGzhMenuType() {
        return gzhMenuType;
    }
    
    public void setSelfDefinedGzhMenu(String selfDefinedGzhMenu) {
        this.selfDefinedGzhMenu = selfDefinedGzhMenu;
    }
    public String getSelfDefinedGzhMenu() {
        return selfDefinedGzhMenu;
    }
    
    public void setOwnerUser(User ownerUser) {
        this.ownerUser = ownerUser;
    }
    public User getOwnerUser() {
        return ownerUser;
    }
    
    public void setAccount(Account account) {
        this.account = account;
    }
    public Account getAccount() {
        return account;
    }
    
    public void addAdminUser(User user) {
        adminUserList.add(user);
    }
    public void setAdminUserList(List<User> adminUserList) {
        this.adminUserList = adminUserList;
    }
    public List<User> getAdminUserList() {
        return adminUserList;
    }
    
    public void setGzhUserList(List<GzhUser> gzhUserList) {
        this.gzhUserList = gzhUserList;
    }
    public List<GzhUser> getGzhUserList() {
        return gzhUserList;
    }
    public void addGzhUser(GzhUser gzhUser) {
        gzhUserList.add(gzhUser);
    }
    
    public void setGzhClientCompanyList(List<GzhClientCompany> gzhClientCompanyList) {
        this.gzhClientCompanyList = gzhClientCompanyList;
    }
    public List<GzhClientCompany> getGzhClientCompanyList() {
        return gzhClientCompanyList;
    }
    public void addGzhClientCompany(GzhClientCompany gzhClientCompany) {
        gzhClientCompanyList.add(gzhClientCompany);
    }
    
    public void setGzhHtmlPageList(List<GzhHtmlPage> gzhHtmlPageList) {
        this.gzhHtmlPageList = gzhHtmlPageList;
    }
    public List<GzhHtmlPage> getGzhHtmlPageList() {
        return gzhHtmlPageList;
    }
    
    public void setGzhContactList(List<GzhContact> gzhContactList) {
        this.gzhContactList = gzhContactList;
    }
    public List<GzhContact> getGzhContactList() {
        return gzhContactList;
    }
    
    public void setImageList(List<Image> imageList) {
        this.imageList = imageList;
    }
    public List<Image> getImageList() {
        return imageList;
    }
    
    public void setTaskPrivateTemplateList(List<GzhTask> taskPrivateTemplateList) {
        this.taskPrivateTemplateList = taskPrivateTemplateList;
    }
    public List<GzhTask> getTaskPrivateTemplateList() {
        return taskPrivateTemplateList;
    }
    
    public void setBillingStatusChangeList(List<BillingStatusChange> billingStatusChangeList) {
        this.billingStatusChangeList = billingStatusChangeList;
    }
    public List<BillingStatusChange> getBillingStatusChangeList() {
        return billingStatusChangeList;
    }
    
    public void setOrderList(List<Order> orderList) {
        this.orderList = orderList;
    }
    public List<Order> getOrderList() {
        return orderList;
    }
    
    @Override
    public String toString() {
        return String.format(
                "Company[id=%d,  name='%s', gzhId='%s', appId='%s', appSecret='%s', paymentStatus='%s', serviceType='%s']",
                id,  name, gzhId, appId, appSecret, paymentStatus, serviceType);
    }
}