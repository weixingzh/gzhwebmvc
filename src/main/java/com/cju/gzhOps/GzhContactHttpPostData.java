package com.cju.gzhOps;

import java.util.List;

public class GzhContactHttpPostData {
    private List<GzhContact> createOrEditList;
    private List<Long> removeList;
    
    public void setCreateOrEditList(List<GzhContact> createOrEditList) {
        this.createOrEditList = createOrEditList;
    }
    public List<GzhContact> getCreateOrEditList() {
        return createOrEditList;
    }
    
    public void setRemoveList(List<Long> removeList) {
        this.removeList = removeList;
    }
    public List<Long> getRemoveList() {
        return removeList;
    }
    
}