package com.cju.gzhOps;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletResponse;
import org.apache.http.HttpStatus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.cju.security.RepositoryQueryFilter;
import com.cju.user.User;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.querydsl.core.types.Predicate;

@Controller
public class GzhAutoReplyController {
    private Logger log = LoggerFactory.getLogger(GzhAutoReplyController.class);

    @Autowired 
    private GzhAutoReplyRepository gzhAutoReplyRepository; 
    
    @Autowired
    private MessageSource messageSource;
    
    @Autowired 
    RepositoryQueryFilter repoQueryFilter;
    
    @PreAuthorize("isAuthenticated()")
    @GetMapping("/gzhOps/autoReply/list")
    public String getGzhAutoReplyMyList(@AuthenticationPrincipal User user, Pageable pageable, Model model) {
        Predicate predicate = repoQueryFilter.filterGzhAutoReplyByPublicOrUser(user);
        Page<GzhAutoReply> gzhAutoReplyPage = gzhAutoReplyRepository.findAll(predicate, pageable);
        model.addAttribute("gzhAutoReplyPage", gzhAutoReplyPage);
        log.info("getGzhAutoReplyList gzhAutoReplyPage.size=" + gzhAutoReplyPage.getSize() + ", gzhAutoReplyPage.Number " + gzhAutoReplyPage.getNumber());

        return "gzhOps/gzhAutoReplyList";
    }
    
    //Rest API

    //postGzhUserTaskApi() handle both create and update cases.
    //For create, gzhAutoReply.id is 0. For update, gzhAutoReply.id is not 0.
    //Data is posted in Json
    @PreAuthorize("isAuthenticated()")
    @PostMapping("/gzhOps/api/v1/autoReply")
    public @ResponseBody String postGzhAutoReplyApi(HttpServletResponse response, @AuthenticationPrincipal User user, 
        @RequestBody GzhAutoReplyHttpPostData gzhAutoReplyHttpPostData) throws IOException { 
        GzhCompany gzhCompany = user.getAdminedGzhCompany();
        if (gzhCompany == null && !user.isWebAdmin()) {
            String message = messageSource.getMessage("gzh.gzhcompany.admined.not.bound", null, Locale.SIMPLIFIED_CHINESE);
            log.error("postGzhAutoReplyApi, user's adminedGzhCompany is null. " + user);
            response.sendError(HttpStatus.SC_UNAUTHORIZED, message);
            //Need to return to stop further processing.
            return "Fail";
        }
        
        List<Long> removeList = gzhAutoReplyHttpPostData.getRemoveList();
        for(Long removeId: removeList) {
            log.info("removeId=" + removeId);
            gzhAutoReplyRepository.delete(removeId);
        }
        List<GzhAutoReply> createOrEditList = gzhAutoReplyHttpPostData.getCreateOrEditList();
        
        for(GzhAutoReply autoReply: createOrEditList) {
            log.info("autoReply:" + autoReply);
            //gzhCompany is not set in autoReply in request
            Predicate predicate = repoQueryFilter.filterGzhAutoReplyByGzhCompanyIdAndKey(gzhCompany.getId(), autoReply.getKey());
            GzhAutoReply currAutoReply = gzhAutoReplyRepository.findOne(predicate);
            if (currAutoReply == null) {
                autoReply.setGzhCompany(gzhCompany);
                gzhAutoReplyRepository.save(autoReply);
            }
            else {
                currAutoReply.setMessage(autoReply.getMessage());
                gzhAutoReplyRepository.save(currAutoReply);
            }
        }
        
        return  "Success";
    }
}
