package com.cju.gzhOps;

import com.fasterxml.jackson.annotation.JsonProperty;

public enum GzhCompanyPaymentStatus {
    @JsonProperty("未欠费") NO_DEBT("NoDebt"), 
    @JsonProperty("欠费") IN_DEBT("InDebt"),
    @JsonProperty("欠费超期") OVER_DEBT("OverDebt"),
    @JsonProperty("未知付费状态") UNKNOWN("Unknown");
    private String paymentStatus;
    private GzhCompanyPaymentStatus(String paymentStatus) {
        this.paymentStatus = paymentStatus;
    }
    
    public static GzhCompanyPaymentStatus fromString(String str) {
        for (GzhCompanyPaymentStatus status: GzhCompanyPaymentStatus.values()) {
            if (status.paymentStatus.equals(str)) {
                return status;
            }
        }
        //Default return
        return UNKNOWN;
    }
    
    @Override
    public String toString() {
        switch (paymentStatus) {
            case "NoDebt":
                return "未欠费";
            case "InDebt":
                return "欠费";
            case "OverDebt":
                return "欠费超期";
            default: 
                return "未知付费状态";
        }
    }
}
