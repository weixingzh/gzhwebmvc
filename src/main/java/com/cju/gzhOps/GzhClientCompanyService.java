package com.cju.gzhOps;

import java.util.List;
import java.util.Locale;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;

import com.cju.chanjet.ChanjetClientCompany;
import com.cju.chanjet.ChanjetClientCompanyAssetItem;
import com.cju.chanjet.ChanjetClientCompanyCashItem;
import com.cju.chanjet.ChanjetClientCompanyIncomeItem;
import com.cju.chanjet.ChanjetClientCompanyRepository;
import com.cju.chanjet.ChanjetService;
import com.cju.chanjet.ChanjetUser;
import com.cju.chanjet.ChanjetUserRepository;
import com.cju.security.RepositoryQueryFilter;
import com.fasterxml.jackson.databind.JsonNode;
import com.querydsl.core.types.Predicate;

@Service
public class GzhClientCompanyService {
    private Logger log = LoggerFactory.getLogger(GzhClientCompanyService.class);
    
    @Autowired 
    private GzhClientCompanyReportRepository gzhClientCompanyReportRepository;  
    
    @Autowired 
    private ChanjetClientCompanyRepository chanjetClientCompanyRepository;  
    
    @Autowired 
    private GzhClientCompanyAssetReportItemRepository gzhClientCompanyAssetReportItemRepository;  
    
    @Autowired 
    private GzhClientCompanyIncomeReportItemRepository gzhClientCompanyIncomeReportItemRepository;  
    
    @Autowired 
    private GzhClientCompanyCashReportItemRepository gzhClientCompanyCashReportItemRepository;  
    
    @Autowired 
    RepositoryQueryFilter repoQueryFilter;
    
    @Autowired
    private ChanjetService chanjetService;
    
    @Autowired
    private ChanjetUserRepository chanjetUserRepository;
    
    @Autowired
    private MessageSource messageSource;
    
    public void updateReportCurrentPeriod(GzhClientCompany gzhClientCompany) throws Exception {
        
        ChanjetClientCompany chanjetClientCompany = gzhClientCompany.getChanjetClientCompany();
        long accountantUserId = chanjetClientCompany.getUserId();
        ChanjetUser accountantUser = chanjetUserRepository.findOneByChanjetUserId(accountantUserId);
        if (accountantUser == null) {
            String chanjetClientCompanyName = chanjetClientCompany.getName();
            String message = messageSource.getMessage("gzh.chanjet.accoutant.not.found", 
                    new Object[] {chanjetClientCompanyName, String.valueOf(accountantUserId), chanjetClientCompany.getUserNoteName()}, Locale.SIMPLIFIED_CHINESE);
            Exception e = new Exception(message);
            throw e;
        }
        
        //Pass chanjetCompanyName for logging purpose
        String chanjetCompanyName = chanjetClientCompany.getName();
        long accId = chanjetClientCompany.getAccId();
        long bookId = chanjetClientCompany.getBookId();
        
        JsonNode rootNode = chanjetService.getChanjetCompanyReportCurrentPeriod(chanjetCompanyName, accountantUser, accId, bookId);
        JsonNode dataNode = rootNode.path("data");
        //closePeriod may be of String value "null"
        String closePeriod = dataNode.path("closePeriod").asText();
        //currentPeriod may be of String value "null"
        String currentPeriod = dataNode.path("currentPeriod").asText();
        
        String oldClosePeriod = chanjetClientCompany.getClosePeriod();
        if(oldClosePeriod == null)
            oldClosePeriod = "null";
        String oldCurrentPeriod = chanjetClientCompany.getCurrentPeriod();
        if (oldCurrentPeriod == null)
            oldCurrentPeriod = "null";
        
        if ( !oldClosePeriod.equals(closePeriod) || !oldCurrentPeriod.equals(currentPeriod)) {
            if (closePeriod.equals("null"))
                closePeriod = null;
            if (currentPeriod.equals("null"))
                currentPeriod = null;
            chanjetClientCompany.setClosePeriod(closePeriod);
            chanjetClientCompany.setCurrentPeriod(currentPeriod);
            chanjetClientCompanyRepository.save(chanjetClientCompany);
        }
    }
    
    public GzhClientCompanyReport importAssetReport(GzhClientCompany gzhClientCompany, String period, boolean clearIfExist) throws Exception {
        
        long bookId = gzhClientCompany.getChanjetClientCompany().getBookId();
        Predicate predicate = repoQueryFilter.filterGzhClientCompanyReportByBookIdAndPeriodAndReportType(bookId, period, GzhClientCompanyReportType.ASSET);
        GzhClientCompanyReport gzhClientCompanyAssetReport = gzhClientCompanyReportRepository.findOne(predicate);
        if (gzhClientCompanyAssetReport != null  && clearIfExist == false) {
            //The asset report exists and we do not want to override it.
            //Return the existing report
            return gzhClientCompanyAssetReport;
        }
        
        //In chanjet, an chanjet client company can be imported by its accountant and the accountant's supervisor.
        //However, it seems only the accountant of the chanjet client company can import its reports
        //The ChanjetClientCompany's accountant must exist in our system.
        long accountantUserId = gzhClientCompany.getChanjetClientCompany().getUserId();
        ChanjetUser accountantUser = chanjetUserRepository.findOneByChanjetUserId(accountantUserId);
        if (accountantUser == null) {
            String chanjetClientCompanyName = gzhClientCompany.getChanjetClientCompany().getName();
            String message = messageSource.getMessage("gzh.chanjet.accoutant.not.found", 
                    new Object[] {chanjetClientCompanyName, String.valueOf(accountantUserId), gzhClientCompany.getChanjetClientCompany().getUserNoteName()}, Locale.SIMPLIFIED_CHINESE);
            Exception e = new Exception(message);
            throw e;
        }
        
        //Pass chanjetCompanyName for loging purpose
        String chanjetCompanyName = gzhClientCompany.getChanjetClientCompany().getName();
        //getChanjetCompanyAssetItemList may throw a self generated exception when the max number of retry is reached
        List<ChanjetClientCompanyAssetItem> chanjetClientCompanyAssetItemList = chanjetService.getChanjetCompanyAssetItemList(chanjetCompanyName, accountantUser, period, bookId);
        
        if (chanjetClientCompanyAssetItemList.size() == 0) {
            //The asset report does not exist in Chanjet.
            String message = messageSource.getMessage("gzh.chanjet.report.not.available",
                    new Object[] {gzhClientCompany.getChanjetClientCompany().getName(), period, GzhClientCompanyReportType.ASSET }, Locale.SIMPLIFIED_CHINESE);
            Exception e = new Exception(message);
            throw e;
        }
        
        if (gzhClientCompanyAssetReport != null) {
            //If the code reach here, cleafIfExist must be true and we get a non-empty report from Chanjet
            //clear the existing asset report items
            List<GzhClientCompanyAssetReportItem> gzhClientCompanyAssetReportItemList = gzhClientCompanyAssetReport.getGzhClientCompanyAssetReportItemList();
            for (GzhClientCompanyAssetReportItem gzhClientCompanyAssetReportItem: gzhClientCompanyAssetReportItemList) {
                gzhClientCompanyAssetReportItemRepository.delete(gzhClientCompanyAssetReportItem);
            }
        }
        
        if (gzhClientCompanyAssetReport == null) {
            gzhClientCompanyAssetReport = new GzhClientCompanyReport();
            gzhClientCompanyAssetReport.setBookId(bookId);
            gzhClientCompanyAssetReport.setPeriod(period);
            gzhClientCompanyAssetReport.setReportType(GzhClientCompanyReportType.ASSET);
            gzhClientCompanyAssetReport.setGzhClientCompany(gzhClientCompany);
            gzhClientCompanyReportRepository.save(gzhClientCompanyAssetReport);
        }
        
        //All the ASSET item are level0 for now
        
        //Because chanjet returns all the asset at once, there is no way to get a specific asset from chanjet. 
        //We need to save and override the whole list of asset in the gzhClientCompanyReportItemAsset.
        for (ChanjetClientCompanyAssetItem chanjetClientCompanyAssetItem: chanjetClientCompanyAssetItemList) {
            GzhClientCompanyAssetReportItem gzhClientCompanyAssetReportItem = new GzhClientCompanyAssetReportItem();
            gzhClientCompanyAssetReportItem.setChanjetClientCompanyAssetItem(chanjetClientCompanyAssetItem);
            gzhClientCompanyAssetReportItem.setGzhClientCompanyReport(gzhClientCompanyAssetReport);
            gzhClientCompanyAssetReportItemRepository.save(gzhClientCompanyAssetReportItem);
            //We do not need to update the Java OneToMany relationship in gzhClientCompanyReport 
            //since we are not using gzhClientCompanyAssetReport in this function.
        }
        
        return gzhClientCompanyAssetReport;
    }
    
    public GzhClientCompanyReport importIncomeReport(GzhClientCompany gzhClientCompany, String period, boolean clearIfExist) throws Exception {
        
        long bookId = gzhClientCompany.getChanjetClientCompany().getBookId();
        Predicate predicate = repoQueryFilter.filterGzhClientCompanyReportByBookIdAndPeriodAndReportType(bookId, period, GzhClientCompanyReportType.INCOME);
        GzhClientCompanyReport gzhClientCompanyIncomeReport = gzhClientCompanyReportRepository.findOne(predicate);
        if (gzhClientCompanyIncomeReport != null  && clearIfExist == false) {
            //The income report exists and we do not want to override it.
            return gzhClientCompanyIncomeReport;
        }
        
        long accountantUserId = gzhClientCompany.getChanjetClientCompany().getUserId();
        ChanjetUser accountantUser = chanjetUserRepository.findOneByChanjetUserId(accountantUserId);
        if (accountantUser == null) {
            String chanjetClientCompanyName = gzhClientCompany.getChanjetClientCompany().getName();
            String message = messageSource.getMessage("gzh.chanjet.accoutant.not.found", 
                    new Object[] {chanjetClientCompanyName, String.valueOf(accountantUserId), gzhClientCompany.getChanjetClientCompany().getName()}, Locale.SIMPLIFIED_CHINESE);
            Exception e = new Exception(message);
            throw e;
        }
        
        String chanjetClientCompanyName = gzhClientCompany.getChanjetClientCompany().getName();
        //getChanjetCompanyIncomeItemList  may throw a self generated exception when the max number of retry is reached
        List<ChanjetClientCompanyIncomeItem>  chanjetClientCompanyIncomeItemList = chanjetService.getChanjetCompanyIncomeItemList(chanjetClientCompanyName, accountantUser, period, bookId);

        if (chanjetClientCompanyIncomeItemList.size() == 0) {
            //The income report does not exist in Chanjet.
            String message = messageSource.getMessage("gzh.chanjet.report.not.available", 
                    new Object[] {gzhClientCompany.getChanjetClientCompany().getName(), period, GzhClientCompanyReportType.INCOME}, Locale.SIMPLIFIED_CHINESE);
            Exception e = new Exception(message);
            throw e;
        }
        
        if (gzhClientCompanyIncomeReport != null) {
            //If the code reach here, cleafIfExist must be true and we get a non-empty report from Chanjet
            //clear the existing income report items
            List<GzhClientCompanyIncomeReportItem> gzhClientCompanyIncomeReportItemList = gzhClientCompanyIncomeReport.getGzhClientCompanyIncomeReportItemList();
            for (GzhClientCompanyIncomeReportItem gzhClientCompanyIncomeReportItem: gzhClientCompanyIncomeReportItemList) {
                gzhClientCompanyIncomeReportItemRepository.delete(gzhClientCompanyIncomeReportItem);
            }
        }
        
        if (gzhClientCompanyIncomeReport == null) {
            gzhClientCompanyIncomeReport = new GzhClientCompanyReport();
            gzhClientCompanyIncomeReport.setBookId(bookId);
            gzhClientCompanyIncomeReport.setPeriod(period);
            gzhClientCompanyIncomeReport.setReportType(GzhClientCompanyReportType.INCOME);
            gzhClientCompanyIncomeReport.setGzhClientCompany(gzhClientCompany);
            gzhClientCompanyReportRepository.save(gzhClientCompanyIncomeReport);
        }
        
        //Because chanjet returns all the income at once, there is no way to get a specific income from chanjet. 
        //We need to save and override the whole list of asset in the gzhClientCompanyReportItemAsset.
        for (ChanjetClientCompanyIncomeItem chanjetClientCompanyIncomeItem: chanjetClientCompanyIncomeItemList) {
            GzhClientCompanyIncomeReportItem gzhClientCompanyIncomeReportItem = new GzhClientCompanyIncomeReportItem();
            gzhClientCompanyIncomeReportItem.setChanjetClientCompanyIncomeItem(chanjetClientCompanyIncomeItem);
            gzhClientCompanyIncomeReportItem.setGzhClientCompanyReport(gzhClientCompanyIncomeReport);
            gzhClientCompanyIncomeReportItemRepository.save(gzhClientCompanyIncomeReportItem);
            //We do not need to update the Java OneToMany relationship in gzhClientCompanyIncomeReport 
            //since we are not using gzhClientCompanyIncomeReport in this function.
        }
        
        return gzhClientCompanyIncomeReport;
    }
    
    public GzhClientCompanyReport importCashReport(GzhClientCompany gzhClientCompany, String period, boolean clearIfExist) throws Exception {
        
        long bookId = gzhClientCompany.getChanjetClientCompany().getBookId();
        Predicate predicate = repoQueryFilter.filterGzhClientCompanyReportByBookIdAndPeriodAndReportType(bookId, period, GzhClientCompanyReportType.CASH);
        GzhClientCompanyReport gzhClientCompanyCashReport = gzhClientCompanyReportRepository.findOne(predicate);
        if (gzhClientCompanyCashReport != null  && clearIfExist == false) {
            //The income report exists and we do not want to override it.
            return gzhClientCompanyCashReport;
        }
        
        long accountantUserId = gzhClientCompany.getChanjetClientCompany().getUserId();
        ChanjetUser accountantUser = chanjetUserRepository.findOneByChanjetUserId(accountantUserId);
        if (accountantUser == null) {
            String chanjetClientCompanyName = gzhClientCompany.getChanjetClientCompany().getName();
            String message = messageSource.getMessage("gzh.chanjet.accoutant.not.found", 
                    new Object[] {chanjetClientCompanyName, String.valueOf(accountantUserId), gzhClientCompany.getChanjetClientCompany().getName()}, Locale.SIMPLIFIED_CHINESE);
            Exception e = new Exception(message);
            throw e;
        }
        
        String chanjetClientCompanyName = gzhClientCompany.getChanjetClientCompany().getName();
        //getChanjetCompanyCashItemList  may throw a self generated exception when the max number of retry is reached
        List<ChanjetClientCompanyCashItem>  chanjetClientCompanyCashItemList = 
            chanjetService.getChanjetCompanyCashItemList(chanjetClientCompanyName, accountantUser, period, bookId);

        if (chanjetClientCompanyCashItemList.size() == 0) {
            //The cash report does not exist in Chanjet.
            String message = messageSource.getMessage("gzh.chanjet.report.not.available", 
                    new Object[] {gzhClientCompany.getChanjetClientCompany().getName(), period, GzhClientCompanyReportType.INCOME}, Locale.SIMPLIFIED_CHINESE);
            Exception e = new Exception(message);
            throw e;
        }
        
        if (gzhClientCompanyCashReport != null) {
            //If the code reach here, cleafIfExist must be true and also we get a non-empty report from Chanjet
            //clear the existing cash report items
            List<GzhClientCompanyCashReportItem> cashReportItemList = gzhClientCompanyCashReport.getGzhClientCompanyCashReportItemList();
            for (GzhClientCompanyCashReportItem cashReportItem: cashReportItemList) {
                gzhClientCompanyCashReportItemRepository.delete(cashReportItem);
            }
        }
        
        if (gzhClientCompanyCashReport == null) {
            gzhClientCompanyCashReport = new GzhClientCompanyReport();
            gzhClientCompanyCashReport.setBookId(bookId);
            gzhClientCompanyCashReport.setPeriod(period);
            gzhClientCompanyCashReport.setReportType(GzhClientCompanyReportType.CASH);
            gzhClientCompanyCashReport.setGzhClientCompany(gzhClientCompany);
            gzhClientCompanyReportRepository.save(gzhClientCompanyCashReport);
        }

        //Because chanjet returns all the cash items at once, there is no way to get a specific cash item from chanjet. 
        //We need to save and override the whole list of cash items in the gzhClientCompanyReportItemCash.
        //Add the List<ChanjetClientCompanyCashItem> to DB
        
        for (ChanjetClientCompanyCashItem chanjetClientCompanyCashItem: chanjetClientCompanyCashItemList) {
            //We do not need to update the Java OneToMany relationship in gzhClientCompanyCashReport 
            //since we are not using gzhClientCompanyCashReport in this function.
            
            GzhClientCompanyCashReportItem  gzhClientCompanyCashReportItem = new GzhClientCompanyCashReportItem();
            gzhClientCompanyCashReportItem.setChanjetClientCompanyCashItem(chanjetClientCompanyCashItem);
            gzhClientCompanyCashReportItem.setGzhClientCompanyReport(gzhClientCompanyCashReport);
            gzhClientCompanyCashReportItemRepository.save(gzhClientCompanyCashReportItem);
        }
        
        return gzhClientCompanyCashReport;
    }
}
