package com.cju.gzhOps;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;

import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cju.security.RepositoryQueryFilter;
import com.cju.user.User;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.cju.gzhOps.GzhHtmlPageType;
import com.querydsl.core.types.Predicate;

@Controller
@RequestMapping("/gzhOps")
public class GzhHtmlPageController {
    private Logger log = LoggerFactory.getLogger(GzhHtmlPageController.class);
    
    @Autowired 
    private GzhHtmlPageRepository gzhHtmlPageRepository; 
    
    @Autowired 
    RepositoryQueryFilter repoQueryFilter;
    
    @Autowired
    private MessageSource messageSource;
    
    @Autowired
    private ObjectMapper objectMapper;
    
    //Get the empty summernote editor
    //The content will be initialized by parent webpage when the IFrame is ready.
    @GetMapping("/htmlPage/emptyEditor")
    public String getGzhHtmlPageEmptyEditor(@AuthenticationPrincipal User user, Model model) {
        return "gzhOps/gzhHtmlPageEmptyEditor";
    }
    
    //This is a link used in left side menu. As a result, gzhCompanyId is not known. We use the login user to figure out the gzhCompany
    @PreAuthorize("isAuthenticated()")
    @GetMapping("/htmlPage/create")
    public String getHtmlPageCreateForm(@AuthenticationPrincipal User user,  @RequestParam Map<String, String> params, Model model) { 
        GzhCompany gzhCompany = user.getAdminedGzhCompany();
        if (gzhCompany == null) {
            //Only admin of a GzhCompany can create HtmlPage
            String message = messageSource.getMessage("gzh.gzhcompany.admined.not.bound", null, Locale.SIMPLIFIED_CHINESE);
            model.addAttribute("message", message);
            return "gzhOps/gzhOpsMessage";
        }
        long gzhCompanyId = gzhCompany.getId();
        
        SortedMap<String, GzhHtmlPage> htmlPageMap = new TreeMap<String, GzhHtmlPage>();
        
        //Get all the predefined titles 
        GzhHtmlPageTitle[] htmlPageTitles = GzhHtmlPageTitle.values();
        List<String> titleList = new ArrayList<String>();
        for(GzhHtmlPageTitle htmlPageTitle: htmlPageTitles) {
            htmlPageMap.put(htmlPageTitle.toString(), null);
            if (!htmlPageTitle.equals(GzhHtmlPageTitle.SELFDEFINED_TITLE)) {
                titleList.add(htmlPageTitle.toString());
            }
        }
        
        //Get all the created htmlPage with predefined titles
        Predicate predicate = repoQueryFilter.filterGzhHtmlPageByCompanyIdAndTitleList(gzhCompanyId, titleList);
        Iterable<GzhHtmlPage> htmlPageList = gzhHtmlPageRepository.findAll(predicate);
        for (GzhHtmlPage htmlPage: htmlPageList) {
            htmlPageMap.put(htmlPage.getTitle(), htmlPage);
        }
        
        model.addAttribute("htmlPageMap", htmlPageMap);
        model.addAttribute("gzhCompany", gzhCompany);
        return "gzhOps/gzhHtmlPageCreateList";
    }
    
    //query parameter: pageTitle=公司简介
    @PreAuthorize("isGzhAdmin(#id) || isWebAdmin()")
    @GetMapping("/htmlPage/create/gzhCompany/{id}")
    public String getHtmlPageCreateWithTitleForm(@AuthenticationPrincipal User user,  @PathVariable("id") long id, @RequestParam Map<String, String> params, Model model) { 
        GzhCompany gzhCompany = user.getAdminedGzhCompany();
        GzhHtmlPage gzhHtmlPage = new GzhHtmlPage();
        gzhHtmlPage.setId(0);
        gzhHtmlPage.setPageType(GzhHtmlPageType.SELF_DEFINED);
        String pageTitle = params.get("pageTitle");
        gzhHtmlPage.setTitle(pageTitle);
        gzhHtmlPage.setGzhCompany(gzhCompany);
        model.addAttribute("gzhHtmlPage", gzhHtmlPage);
        
        String message = messageSource.getMessage("gzh.gzhcompany.htmlpage.create", null , Locale.SIMPLIFIED_CHINESE);
        model.addAttribute("pageTitle", message);

        //Create and Edit share the same Html Template
        return "gzhOps/gzhHtmlPageCreateOrEdit";
    }
        
    @PreAuthorize("isGzhAdminForHtmlPage(#id) || isWebAdmin()")
    @GetMapping("/htmlPage/edit/{id}")
    public String getGzhHtmlPageEditForm(@PathVariable("id") long id, Model model) {
        GzhHtmlPage gzhHtmlPage = gzhHtmlPageRepository.findOne(id);
        model.addAttribute("gzhHtmlPage", gzhHtmlPage);
        
        String message = messageSource.getMessage("gzh.gzhcompany.htmlpage.edit", null, Locale.SIMPLIFIED_CHINESE);
        model.addAttribute("pageTitle", message);
        //Create and Edit share the same Html Template
        return "gzhOps/gzhHtmlPageCreateOrEdit";
    }
    
    @PreAuthorize("isAuthenticated()")
    @GetMapping("/htmlPage/selfDefined/list")
    public String getGzhHtmlPageMyList(@AuthenticationPrincipal User user, Pageable pageable, Model model) {
        Predicate predicate = repoQueryFilter.filterGzhHtmlPageSelfDefinedByUser(user);
        Page<GzhHtmlPage> gzhHtmlPagePage = gzhHtmlPageRepository.findAll(predicate, pageable);
        model.addAttribute("gzhHtmlPagePage", gzhHtmlPagePage);
        return "gzhOps/gzhHtmlPageSelfDefinedList";
    }
    
    //Get all htmlPage templates
    @PreAuthorize("isAuthenticated()")
    @GetMapping("/htmlPage/template/list")
    public String getGzhHtmlPageTemplateList(Pageable pageable, Model model) {
        Predicate predicate = repoQueryFilter.filterGzhHtmlPageByType(GzhHtmlPageType.TEMPLATE);
        Page<GzhHtmlPage> gzhHtmlPagePage = gzhHtmlPageRepository.findAll(predicate, pageable);
        model.addAttribute("gzhHtmlPagePage", gzhHtmlPagePage);
        String message = messageSource.getMessage("gzh.htmlpage.template.list", null, Locale.SIMPLIFIED_CHINESE);
        model.addAttribute("pageTitle", message);
        return "gzhOps/gzhHtmlPageTemplateList";
    }
    
    @PreAuthorize("isGzhAdminForHtmlPage(#id) || isGzhHtmlPageTemplate(#id) || isWebAdmin() ")
    @GetMapping("/htmlPage/details/{id}")
    public String getGzhHtmlPageDetails(@PathVariable("id") long id, Model model) {
        GzhHtmlPage gzhHtmlPage = gzhHtmlPageRepository.findOne(id);
        model.addAttribute("gzhHtmlPage", gzhHtmlPage);
        return "gzhOps/gzhHtmlPageDetails";
    }

    //REST API
    
    //No query string or path variable. Form data gzhHtmlPage has all the info.
    //This function handle both create and edit cases.
    //Data is posted in http form xml-url-encoded format.
    @PreAuthorize("isGzhAdminForHtmlPage(#gzhHtmlPage.getId()) || isWebAdmin()")
    @PostMapping("/api/v1/htmlPage/createOrEdit")
    public @ResponseBody String postGzhHtmlPage(@AuthenticationPrincipal User user, @RequestBody GzhHtmlPage gzhHtmlPage) {
        log.info("postGzhHtmlPage GzhHtmlPage=" + gzhHtmlPage);
        
        //isGzhAdminForHtmlPage(#gzhHtmlPage.getId()) will return false is user is not admin.
        GzhCompany gzhCompany = user.getAdminedGzhCompany();
        if (gzhCompany == null && !user.isWebAdmin()) {
            String message = messageSource.getMessage("gzh.gzhcompany.admined.not.bound", null, Locale.SIMPLIFIED_CHINESE);
            return message;
        }
        
        gzhHtmlPage.setGzhCompany(gzhCompany);
        //For create case, gzhHtmlPage.id is 0 and will get a new value after save.
        gzhHtmlPageRepository.save(gzhHtmlPage);
        
        //No need to maintain the OneToMany relationship between GzhCompany and GzhHtmlPage.
        //If this is a create case, the id is a new one.
        String htmlPageId = Long.toString(gzhHtmlPage.getId());
        return htmlPageId;
    }
    
    @PreAuthorize("isAuthenticated()")
    @GetMapping("/api/v1/htmlPage/template/list")
    public @ResponseBody String getGzhHtmlPageTemplateListApi(HttpServletResponse response) {
        Predicate predicate = repoQueryFilter.filterGzhHtmlPageByType(GzhHtmlPageType.TEMPLATE);
        List<GzhHtmlPage> gzhHtmlPageList = gzhHtmlPageRepository.findAll(predicate);
        
        try {
            String gzhHtmlPageListJson = objectMapper.writeValueAsString(gzhHtmlPageList);
            return  gzhHtmlPageListJson;
        } catch (JsonProcessingException e) {
            response.setStatus(HttpStatus.BAD_REQUEST.value());
            return e.getLocalizedMessage();
        }
    }
}
