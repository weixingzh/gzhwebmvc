package com.cju.gzhOps;

import com.fasterxml.jackson.annotation.JsonProperty;

public enum GzhClientCompanyReportType {
    @JsonProperty("资产负债表") ASSET("Asset"),  
    @JsonProperty("现金流量表") CASH("Cash"),  
    @JsonProperty("利润表") INCOME("Income");
    private String reportType;
    private 
    GzhClientCompanyReportType(String reportType) {
        this.reportType = reportType;
    }
    
    public String getValue() {
        return reportType;
    }
    
    //In Thymeleaf, ${reportType} will display the result of toString(), reportType.value will display the result of getValue()

    //For JSON, Jackson2 will use the @JsonProperty value (资产，现金，利润) to map to enum.
    //For @RequestParam GzhClientCompanyReportType gzhClientCompanyReportType, enum name (ASSET, CASH, INCOME) is used to map to enum.
    //For @RequestParam Map<String, String> params, we need to take the String from params and use fromString to map to enum.
    //fromString() will take the enum value as paramenter, i.e. Asset, Cash, Income
    //GzhClientCompanyReportType.valueOf() takes enum name as parameter, i.e. ASSET, CASH, INCOME
    public static GzhClientCompanyReportType fromString(String str) {
        for (GzhClientCompanyReportType type: GzhClientCompanyReportType.values()) {
            if (type.reportType.equals(str)) {
                return type;
            }
        }
        //Default return
        return ASSET;
    }
    
    @Override
    public String toString() {
        switch (reportType) {
            case "Asset": 
                return "资产负债表";
            case "Cash": 
                return "现金流量表";
            case "Income": 
                return "利润表";
            default: 
                return "资产负债表";
        }
    }
}
