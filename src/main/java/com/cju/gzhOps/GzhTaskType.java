package com.cju.gzhOps;

import com.fasterxml.jackson.annotation.JsonProperty;

public enum GzhTaskType {
    @JsonProperty("客户事务") CUSTOMER_TASK("CustomerTask"), 
    @JsonProperty("公共模板") PUBLIC_TEMPLATE("PublicTemplate"), 
    @JsonProperty("私有模板") PRIVATE_TEMPLATE("PrivateTemplate"); 
    private String taskType;
    private GzhTaskType(String taskType) {
        this.taskType = taskType;
    }
    
    public static GzhTaskType fromString(String str) {
        for (GzhTaskType type: GzhTaskType.values()) {
            if (type.taskType.equals(str)) {
                return type;
            }
        }
        
        //Default return
        return CUSTOMER_TASK;
    }
    
    @Override
    public String toString() {
        switch (taskType) {
            case "CustomerTask": 
                return "客户事务";
            case "PublicTemplate": 
                return "公共模板";
            case "PrivateTemplate": 
                return "私有模板";
            default: 
                return "客户事务";
        }
    }
}
