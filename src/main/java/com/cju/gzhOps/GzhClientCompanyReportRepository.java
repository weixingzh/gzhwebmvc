package com.cju.gzhOps;

import java.time.LocalDate;

import org.springframework.data.querydsl.QueryDslPredicateExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.querydsl.core.types.OrderSpecifier;
import com.querydsl.core.types.Predicate;

//QueryDslPredicateExecutor interface has the following query methods:
//T findOne(Predicate predicate);
//Iterable<T> findAll(Predicate predicate);
//Iterable<T> findAll(Predicate predicate, OrderSpecifier<?>... orders);
//Page<T> findAll(Predicate predicate, Pageable pageable);
//long count(Predicate predicate);

@Repository
public interface GzhClientCompanyReportRepository 
    extends PagingAndSortingRepository<GzhClientCompanyReport, Long>, QueryDslPredicateExecutor<GzhClientCompanyReport> {

   // GzhClientCompanyReport findFirstByBookIdAndReportTypeOrderByPeriodDesc(long bookId, String reportType);
}