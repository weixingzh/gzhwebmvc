package com.cju.gzhOps;

import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import com.cju.chanjet.ChanjetClientCompanyIncomeItem;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

@Entity
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id", scope = GzhClientCompanyIncomeReportItem.class)
public class GzhClientCompanyIncomeReportItem {
    @Id    
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    
    @Embedded
    private ChanjetClientCompanyIncomeItem chanjetClientCompanyIncomeItem;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name="gzhClientCompanyReportId", foreignKey=@ForeignKey(name = "GzhClientCompanyIncomeReportItem_FK_GzhClientCompanyReportId"))
    private GzhClientCompanyReport gzhClientCompanyReport;
    
    protected GzhClientCompanyIncomeReportItem() {}
    public GzhClientCompanyIncomeReportItem(long id) {
        this.id = id;
    }
    
    public void setId(long id) {
        this.id = id;
    }
    public long getId() {
        return id;
    }

    public void setChanjetClientCompanyIncomeItem(ChanjetClientCompanyIncomeItem chanjetClientCompanyIncomeItem) {
        this.chanjetClientCompanyIncomeItem = chanjetClientCompanyIncomeItem;
    }
    public ChanjetClientCompanyIncomeItem getChanjetClientCompanyIncomeItem() {
        return chanjetClientCompanyIncomeItem;
    }
    
    public void setGzhClientCompanyReport(GzhClientCompanyReport gzhClientCompanyReport) {
        this.gzhClientCompanyReport = gzhClientCompanyReport;
    }
    public GzhClientCompanyReport getGzhClientCompanyReport() {
        return gzhClientCompanyReport;
    }
    
    @Override
    public String toString() {
        return String.format(
                "GzhClientCompanyIncomeItem[id=%d, chanjetClientCompanyIncomeItem='%s']",
                id, chanjetClientCompanyIncomeItem.toString());
    }
}