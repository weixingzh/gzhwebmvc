package com.cju.gzhOps;

import java.io.IOException;
import java.nio.charset.Charset;
import java.time.OffsetDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.annotation.PostConstruct;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;
import org.springframework.util.StreamUtils;

import com.cju.gzh.WeiXinService;
import com.cju.gzh.WeiXinUser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;

@Service
public class GzhUserService {
    private Logger log = LoggerFactory.getLogger(GzhUserService.class);
    
    @Autowired
    GzhUserRepository gzhUserRepository;
    
    @Autowired 
    private GzhCompanyRepository gzhCompanyRepository; 
    
    @Autowired
    private WeiXinService weiXinService;
      
    public void gzhUserSubscribe(String openId, GzhCompany gzhCompany) {
        //The gzhUser may exist due to subscribe and unsubscribe before
        GzhUser gzhUser = gzhUserRepository.findOneByOpenId(openId);
        if (gzhUser == null) {
            gzhUser = new GzhUser();
            gzhUser.setOpenId(openId);
            gzhUser.setGzhCompany(gzhCompany);
        }
        
        String accessToken = gzhCompany.checkAndGetAccessToken();
        if (accessToken != null) {
            WeiXinUser weiXinUser = weiXinService.loadUserByOpenIdAndAccessToken(openId, accessToken);
            gzhUser.setNickName(weiXinUser.getNickName());
            gzhUser.setCity(weiXinUser.getCity());
            gzhUser.setProvince(weiXinUser.getProvince());
            gzhUser.setCountry(weiXinUser.getCountry());
            gzhUser.setHeadImgUrl(weiXinUser.getHeadimgurl());
            gzhUser.setOpenId(weiXinUser.getOpenId());
        }
        
        gzhUser.setStatus(GzhUserStatus.SUBSCRIBE);
        OffsetDateTime lastStatusChange = OffsetDateTime.now();
        gzhUser.setLastStatusChange(lastStatusChange);
        //Save the user to database.
        gzhUserRepository.save(gzhUser);
    }
    
    //It happens that gzhUser subscribe message is lost. 
    //We may receive a http request to /gzh/company/{id} and could not find openId in our database. 
    //If this happens, we will add the gzhUser to our database.
    public GzhUser addGzhUserIfNotExist(String openId, long gzhCompanyId) {
        GzhUser gzhUser = gzhUserRepository.findOneByOpenId(openId);
        if (gzhUser != null) {
            return gzhUser;
        }
        
        //gzhUser not found in our database. This should not happen a lot.
        GzhCompany gzhCompany = gzhCompanyRepository.findOne(gzhCompanyId);
        log.info("addGzhUserIfNotExist(), gzhUser with openId=" + openId + " not found. gzhCompany=" + gzhCompany.getName());
        
        gzhUser = new GzhUser();
        gzhUser.setOpenId(openId);
        gzhUser.setGzhCompany(gzhCompany);
        
        String accessToken = gzhCompany.checkAndGetAccessToken();
        if (accessToken != null) {
            //If there is an exception in loadUserByOpenIdAndAccessToken, an empty weiXinUser is returned without any details info
            WeiXinUser weiXinUser = weiXinService.loadUserByOpenIdAndAccessToken(openId, accessToken);
            gzhUser.setNickName(weiXinUser.getNickName());
            gzhUser.setCity(weiXinUser.getCity());
            gzhUser.setProvince(weiXinUser.getProvince());
            gzhUser.setCountry(weiXinUser.getCountry());
            gzhUser.setHeadImgUrl(weiXinUser.getHeadimgurl());
        }
        
        gzhUser.setStatus(GzhUserStatus.SUBSCRIBE);
        OffsetDateTime lastStatusChange = OffsetDateTime.now();
        gzhUser.setLastStatusChange(lastStatusChange);
        
        //Save the user to database.
        gzhUserRepository.save(gzhUser);
        return gzhUser;
    }
    
    public void gzhUserUnsubscribe(String openId) {
        GzhUser unsubGzhUser = gzhUserRepository.findOneByOpenId(openId);
        //It is possible that we do not have the gzhUser in database
        if (unsubGzhUser == null)
            return;
        unsubGzhUser.setStatus(GzhUserStatus.UNSUBSCRIBE);
        OffsetDateTime lastStatusChange = OffsetDateTime.now();
        unsubGzhUser.setLastStatusChange(lastStatusChange);
        gzhUserRepository.save(unsubGzhUser);
    }
}
