package com.cju.gzhOps;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;
import javax.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import com.cju.billing.Account;
import com.cju.billing.AccountRepository;
import com.cju.chanjet.ChanjetClientCompany;
import com.cju.chanjet.ChanjetClientCompanyRepository;
import com.cju.chanjet.YesNoType;
import com.cju.gzh.WeiXinService;
import com.cju.marketing.Referal;
import com.cju.marketing.ReferalService;
import com.cju.marketing.ReferalStatus;
import com.cju.security.RepositoryQueryFilter;
import com.cju.user.User;
import com.cju.user.UserRepository;
import com.cju.util.ResultCode;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.querydsl.core.types.Predicate;

@Controller
public class GzhCompanyController {
    private Logger log = LoggerFactory.getLogger(GzhCompanyController.class);
    
    @Autowired 
    private UserRepository userRepository; 
    
    @Autowired 
    private GzhCompanyRepository gzhCompanyRepository; 
    
    @Autowired 
    private GzhCompanyService gzhCompanyService;
    
    @Autowired 
    private AccountRepository accountRepository; 
    
    @Autowired 
    RepositoryQueryFilter repoQueryFilter;
    
    @Autowired
    private MessageSource messageSource;
    
    @Autowired
    private ObjectMapper objectMapper;
    
    @Autowired
    private WeiXinService weiXinService;
    
    @Autowired
    private ReferalService referalService;
    
    
    //Only Web Admin can see the gzhCompany List
    @PreAuthorize("isWebAdmin()")
    @GetMapping("/gzhOps/gzhCompany/list")
    public String getGzhCompanyList(@AuthenticationPrincipal User user, Pageable pageable, Model model) {  
        Predicate predicate = repoQueryFilter.filterGzhCompanyByUser(user);
        //null Predicate will always return true
        Page<GzhCompany> gzhCompanyPage = gzhCompanyRepository.findAll(predicate, pageable);
        model.addAttribute("gzhCompanyPage", gzhCompanyPage);
        log.info("getGzhCompanyList gzhCompanyPage.size=" + gzhCompanyPage.getSize() + ", gzhCompanyPage.Number " + gzhCompanyPage.getNumber());
        return "gzhOps/gzhCompanyList";
    }
    
    //if isGzhAdmin returns true, isWebAdmin will not be called
    @PreAuthorize("isGzhAdmin(#id) || isWebAdmin()")
    @GetMapping("/gzhOps/gzhCompany/details/{id}")
    public String getGzhCompanyDetails(@PathVariable("id") long id, Model model) {    
        GzhCompany gzhCompany = gzhCompanyRepository.findOne(id);
        model.addAttribute("gzhCompany", gzhCompany);
        log.info("getGzhCompanyDetails" + gzhCompany.toString());
        return "gzhOps/gzhCompanyDetails";
    }
    
    @PreAuthorize("isAuthenticated()")
    @GetMapping("/gzhOps/gzhCompany/myGzhCompanyDetails")
    public String getMyGzhCompanyDetails(@AuthenticationPrincipal User user, Model model) {  
        GzhCompany gzhCompany = user.getAdminedGzhCompany();
        
        if (gzhCompany == null) {
            gzhCompany = user.getOwnedGzhCompany();
        }
        
        if (gzhCompany == null) {
            String message = messageSource.getMessage("gzh.gzhcompany.admined.or.owned.not.bound", null, Locale.SIMPLIFIED_CHINESE);
            model.addAttribute("message", message);
            return "gzhOps/gzhOpsMessage";
        }
        
        //When a GzhCompany is just created and the user comes to this page, the adminUserList is empty.
        //That is probably because the adminedGzhCompany in user object does not go through the database loading process.
        //If user logout and login again, the adminUserList of the GzhCompany is populated.
        //So we load the gzhCompany from database refresh here.
        gzhCompany = gzhCompanyRepository.findOne(gzhCompany.getId());
        model.addAttribute("gzhCompany", gzhCompany);
        return "gzhOps/gzhCompanyDetails";
    }
    
    //Any user can create a GzhCompany
    @PreAuthorize("isAuthenticated()")
    @GetMapping("/gzhOps/gzhCompany/create")
    public String getGzhCompanyCreateForm(@AuthenticationPrincipal User user, Model model) { 
        if (user.getOwnedGzhCompany() != null) {
            String message = messageSource.getMessage("gzh.gzhcompany.create.only.once", null, Locale.SIMPLIFIED_CHINESE);
            model.addAttribute("message", message);
            return "gzhOps/gzhOpsMessage";
        }
        GzhCompany gzhCompany = new GzhCompany();
        gzhCompany.setId(0);
        model.addAttribute("gzhCompany", gzhCompany);
        return "gzhOps/gzhCompanyCreate";
    }
    
    //Spring has built-in exception translation mechanism, so that all exceptions thrown by the JPA persistence providers 
    //are converted into Spring's DataAccessException - for all beans annotated with @Repository (or configured).
    //@ModelAttribute clientCompany is bound to incoming form content.
    //You can not inject the repository in the parameter list. Usually controller method arguments are used to pass the 
    //model (parsed http request) to the controller logic
    
    //Any user can create a GzhCompany
    @PreAuthorize("isAuthenticated()")
    @PostMapping("/gzhOps/gzhCompany/create")
    public String submitGzhCompanyCreateForm(@AuthenticationPrincipal User user, @ModelAttribute GzhCompany gzhCompany, Model model) {
        if (user.getOwnedGzhCompany() != null) {
            String message = messageSource.getMessage("gzh.gzhcompany.create.only.once", null, Locale.SIMPLIFIED_CHINESE);
            model.addAttribute("message", message);
            return "gzhOps/gzhOpsMessage";
        }
        
        gzhCompany.setOwnerUser(user);
        
        //create an Account for the GzhCompany
        Account account = new Account();
        account.setInternalAccountOwnerName(user.getUserName());
        accountRepository.save(account);
        
        //Set gzhId and appId to null if they are empty
        //We have unique constrains on gzhId and appId. There can not exist two empty gzhId or appId.
        //But there can exist multiple null gzhId or appId in postgreSQL.
        if (gzhCompany.getAppId().equals("")) {
            gzhCompany.setAppId(null);
        }
        if (gzhCompany.getGzhId().equals("")) {
            gzhCompany.setGzhId(null);
        }
        gzhCompany.setAccount(account);
        //For now, set the default paymentStatus and serviceLevel 
        gzhCompany.setBillingStatus(GzhCompanyBillingStatus.TRIAL_NOT_STARTED);
        gzhCompany.setPaymentStatus(GzhCompanyPaymentStatus.NO_DEBT);
        gzhCompany.setServiceRunningStatus(GzhCompanyServiceRunningStatus.NOT_STARTED);
        gzhCompany.setServiceType(GzhCompanyServiceType.GSSWDL_A);
        gzhCompany.setGzhMenuType(GzhCompanyMenuType.SYSTEM_DEFAULT);
        GzhCompany returnedGzhCompany = gzhCompanyRepository.save(gzhCompany);
        
        account.setGzhCompany(returnedGzhCompany);
        accountRepository.save(account);
        
        user.setOwnedGzhCompany(returnedGzhCompany);
        user.setAdminedGzhCompany(returnedGzhCompany);
        userRepository.save(user);
        
        //If there is an exception in save, the code will not reach here.
        String message = messageSource.getMessage("gzh.gzhcompany.create.success", null, Locale.SIMPLIFIED_CHINESE);
        model.addAttribute("message", message);
        return "gzhOps/gzhOpsMessage";
    }
    
    @PreAuthorize("isGzhOwner(#id) || isGzhAdmin(#id) || isWebAdmin()")
    @GetMapping("/gzhOps/gzhCompany/edit/{id}")
    public String getGzhCompanyEditForm(@PathVariable("id") long id, Model model) {    
        GzhCompany gzhCompany = gzhCompanyRepository.findOne(id);
        if (!gzhCompany.getPaymentStatus().equals(GzhCompanyPaymentStatus.NO_DEBT))
        {
            String message = messageSource.getMessage("gzh.gzhcompany.payment.indebt.cannot.edit", null, Locale.SIMPLIFIED_CHINESE);
            model.addAttribute("message", message);
            return "gzhOps/gzhOpsMessage";
        }

        model.addAttribute("gzhCompany", gzhCompany);
        log.info("getGzhCompanyEditForm" + gzhCompany.toString());
        return "gzhOps/gzhCompanyEdit";
    }
    
    //Here we are using Spring Security Method Security Expressions to do the checking.
    //request object is available in Web Security Expressions, but not available to Method Security Expressions
    //But we can use the method arguments in the Method Security Expressions
    //@PreAuthorize("#gzhCompany.name == #session.getAttribute('CURRENT_USER').userName")
    @PreAuthorize("isGzhOwner(#id) || isGzhAdmin(#id) || isWebAdmin()")
    @PostMapping("/gzhOps/gzhCompany/edit/{id}")
    public String submitGzhCompanyEdit(@AuthenticationPrincipal User user, @PathVariable("id") long id, @ModelAttribute GzhCompany gzhCompany, Model model) {
        log.info("submitGzhCompanyEdit gzhCompany=" + gzhCompany.toString());
        GzhCompany currGzhCompany = gzhCompanyRepository.findOne(gzhCompany.getId());
        List<User> oldAdminUserList = currGzhCompany.getAdminUserList();
        
        List<User> newAdminUserList = new ArrayList<User>();
        List<User> httpAdminUserList = gzhCompany.getAdminUserList();
        log.info("submitGzhCompanyEdit gzhCompany.getAdminUserList()=" + httpAdminUserList);
        gzhCompany.getAdminUserList().forEach(admin -> {
            log.info("adminUserList in http AttributeModel: admin=" + admin.getUserName());
            User adminUser = userRepository.findOneByUserName(admin.getUsername());
            if (adminUser != null) {
                log.info("adminUserList in http AttributeModel: findOneByUserName=" + adminUser.getUserName());
                newAdminUserList.add(adminUser);
            }
        });
        
        //adminUserList points to a new list
        currGzhCompany.setAdminUserList(newAdminUserList);
        currGzhCompany.setName(gzhCompany.getName());
        if (currGzhCompany.getAppIdEditable() == YesNoType.YES) {
            if(gzhCompany.getAppId().equals(""))
                currGzhCompany.setAppId(null);
            else
                currGzhCompany.setAppId(gzhCompany.getAppId());
        }
        if (gzhCompany.getGzhId().equals(""))
            currGzhCompany.setGzhId(null);
        else
            currGzhCompany.setGzhId(gzhCompany.getGzhId());
        currGzhCompany.setAppSecret(gzhCompany.getAppSecret());
        //Only webadmin can edit gzhMenyType from gzhCompanyEdit.html
        if (gzhCompany.getGzhMenuType() != null)
            currGzhCompany.setGzhMenuType(gzhCompany.getGzhMenuType());
        //For now chanjetAppKey is the same for all GzhCompany and this chanjetAppKey of GzhCompany is not used.
        currGzhCompany.setChanjetAppKey(gzhCompany.getChanjetAppKey());
        gzhCompanyRepository.save(currGzhCompany);
        
        //update the gzhComapny in the cached user object
        if (user.getAdminedGzhCompany() != null && user.getAdminedGzhCompany().getId() == id)
            user.setAdminedGzhCompany(currGzhCompany);
        if (user.getOwnedGzhCompany() != null && user.getOwnedGzhCompany().getId() == id)
            user.setOwnedGzhCompany(currGzhCompany);
        
        //ManyToOne User->adminedGzhCompany
        newAdminUserList.forEach(adminUser -> {
            log.info("newAdminUserList adminUser=" + adminUser.getUsername());
            adminUser.setAdminedGzhCompany(currGzhCompany);
            userRepository.save(adminUser);
        });
        
        //clear the adminedGzhCompany of Users in oldAdminUserList but not in newAdminUserList
        oldAdminUserList.removeAll(newAdminUserList);
        oldAdminUserList.forEach(adminUser -> {
            log.info("oldAdminUserList adminUser=" + adminUser.getUsername());
            adminUser.setAdminedGzhCompany(null);
            userRepository.save(adminUser);
        });
        
        //If there is an exception, the code will not reach here.
        String message = messageSource.getMessage("gzh.gzhcompany.edit.success", null, Locale.SIMPLIFIED_CHINESE);
        model.addAttribute("message", message);
        return "gzhOps/gzhOpsMessage";
    }
    
    @PreAuthorize("isAuthenticated()")
    @GetMapping("/gzhOps/gzhCompany/myGzhMenu")
    public String getGzhCompanyGzhMenu(@AuthenticationPrincipal User user, Model model) { 
        GzhCompany gzhCompany = user.getAdminedGzhCompany();
        if (gzhCompany == null) {
            String message = messageSource.getMessage("gzh.gzhcompany.admined.not.bound", null, Locale.SIMPLIFIED_CHINESE);
            model.addAttribute("message", message);
            return "gzhOps/gzhOpsMessage";
        }
        
        //getGzhMenuJson() will call gzhCompany.getGzhHtmlPageList()
        //However, the gzhCompany in the cached user object did not load the gzhHtmlPageList as it is lazy loaded.
        //As a result, there will be an exception if we use the gzhCompany in the cached user since the gzhCompany is loaded in a different hibernate session.
        //So we need to get the gzhCompany refresh here.
        
        gzhCompany = gzhCompanyRepository.findOne(gzhCompany.getId());
        
        String gzhMenuJson = gzhCompanyService.getGzhMenuJson(gzhCompany);
        model.addAttribute("gzhCompany", gzhCompany);
        model.addAttribute("gzhMenuJson", gzhMenuJson);
        
        return "gzhOps/gzhCompanyGzhMenu";
    }
    
    @PreAuthorize("isGzhOwner(#id) || isGzhAdmin(#id) || isWebAdmin()")
    @GetMapping("/gzhOps/gzhCompany/{id}/gzhMenu/edit")
    public String getGzhCompanyGzhMenuEdit(@PathVariable("id") long id,  Model model) { 
        GzhCompany gzhCompany = gzhCompanyRepository.findOne(id);
        
        //getGzhMenuJson() will call gzhCompany.getGzhHtmlPageList()
        //However, the gzhCompany in the cached user object did not load the gzhHtmlPageList as it is lazy loaded.
        //As a result, there will be an exception if we use the gzhCompany in the cached user since the gzhCompany is loaded in a different hibernate session.
        //So we need to get the gzhCompany refresh here.
        
        gzhCompany = gzhCompanyRepository.findOne(gzhCompany.getId());
        
        String selfDefinedGzhMenuJson = gzhCompany.getSelfDefinedGzhMenu();
        if (selfDefinedGzhMenuJson == null)
            selfDefinedGzhMenuJson = "{}";
        String systemDefaultGzhMenuJson = gzhCompanyService.getSystemDefaultGzhMenuJson(gzhCompany);
        
        model.addAttribute("gzhCompany", gzhCompany);
        model.addAttribute("systemDefaultGzhMenuJson", systemDefaultGzhMenuJson);
        model.addAttribute("selfDefinedGzhMenuJson", selfDefinedGzhMenuJson);
        
        return "gzhOps/gzhCompanyGzhMenuEdit";
    }
    

    //REST API
    
    @PreAuthorize("isGzhAdmin(#id) || isWebAdmin()")
    @PostMapping("/gzhOps/api/v1/gzhCompany/startService/{id}")
    public @ResponseBody String gzhCompanyStartServiceApi(@AuthenticationPrincipal User user, @PathVariable("id") long id, HttpServletResponse response) throws IOException  {
        
        String message = messageSource.getMessage("gzh.gzhcompany.service.start.success", null, Locale.SIMPLIFIED_CHINESE);
        GzhCompany gzhCompany = gzhCompanyRepository.findOne(id);
        
        if (!gzhCompany.getPaymentStatus().equals(GzhCompanyPaymentStatus.NO_DEBT))
        {
            message = messageSource.getMessage("gzh.gzhcompany.payment.indebt.cannot.start.service", null, Locale.SIMPLIFIED_CHINESE);
            response.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.value());
            return message;
        }
        
        if (gzhCompany.getBillingStatus().equals(GzhCompanyBillingStatus.TRIAL_NOT_STARTED))
        {
            message = messageSource.getMessage("gzh.gzhcompany.billing.trial.not.started.cannot.start.service", null, Locale.SIMPLIFIED_CHINESE);
            response.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.value());
            return message;
        }
        
        if (gzhCompany.getBillingStatus().equals(GzhCompanyBillingStatus.STOPPED))
        {
            message = messageSource.getMessage("gzh.gzhcompany.billing.stopped.cannot.start.service", null, Locale.SIMPLIFIED_CHINESE);
            response.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.value());
            return message;
        }
        
        String accessToken = gzhCompany.checkAndGetAccessToken();
        if (accessToken != null) {
            ResultCode resultCode = weiXinService.createGzhMenu(gzhCompany, accessToken);
            if (resultCode.getCode() != ResultCode.CODE.OK) {
                message = resultCode.getMessage();
                response.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.value());
            }
            
            //Once the weiXin service is started successfully, the user can not edit the appId any more.
            if (gzhCompany.getAppIdEditable() == YesNoType.YES) 
                gzhCompany.setAppIdEditable(YesNoType.NO);
            //Update serviceRunningStatus
            gzhCompany.setServiceRunningStatus(GzhCompanyServiceRunningStatus.RUNNING);
            gzhCompanyRepository.save(gzhCompany);
            
            //update the gzhComapny in the cached user object
            if (user.getAdminedGzhCompany() != null && user.getAdminedGzhCompany().getId() == id)
                user.setAdminedGzhCompany(gzhCompany);
            if (user.getOwnedGzhCompany() != null && user.getOwnedGzhCompany().getId() == id)
                user.setOwnedGzhCompany(gzhCompany);
            
            //Get the referal of the owner user of gzhCompany
            Referal referal = gzhCompany.getOwnerUser().getReferal();
            if (referal.getReferalStatus() != ReferalStatus.ACTIVE) {
                //set the ReferalStatus to ACTIVE
                String referalStatusNote = messageSource.getMessage("gzh.referal.status.active.by.starting.service", null, Locale.SIMPLIFIED_CHINESE);
                referalService.updateReferalStatus(referal, ReferalStatus.ACTIVE, referalStatusNote);
            }
        }
        else {
            message = messageSource.getMessage("gzh.gzhcompany.get.access.token.failed", null, Locale.SIMPLIFIED_CHINESE);
            response.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.value());
        }

        return  message;
    }
    
    //Delete GzhCompany will remove all the data under this GzhCompany due to CascadeType.REMOVE in GzhCompany.java.
    //But the ownerUser and adminUser are not removed as they are not annotated with CascadeType.REMOVE.
    @PreAuthorize("isGzhOwner(#id) || isWebAdmin()")
    @DeleteMapping("/gzhOps/api/v1/gzhCompany/{id}")
    public @ResponseBody String deleteGzhCompanyApi(@AuthenticationPrincipal User userInSession, @PathVariable("id") long id)  { 
        GzhCompany gzhCompany = gzhCompanyRepository.findOne(id);
        for( User user: gzhCompany.getAdminUserList()) {
            user.setAdminedGzhCompany(null);
            userRepository.save(user);
        }
        
        User user = gzhCompany.getOwnerUser();
        user.setOwnedGzhCompany(null);
        userRepository.save(user);
        
        //Need to update the user object in the http session
        if (userInSession.getAdminedGzhCompany() != null && userInSession.getAdminedGzhCompany().getId() == id)
            userInSession.setAdminedGzhCompany(null);
        if (userInSession.getOwnedGzhCompany() != null && userInSession.getOwnedGzhCompany().getId() == id)
            userInSession.setOwnedGzhCompany(null);
        
        gzhCompanyRepository.delete(id);
        String message = messageSource.getMessage("gzh.gzhcompany.delete.success", null, Locale.SIMPLIFIED_CHINESE);
        return  message;
    }
    
    
    //Get the adminUser list of GzhCompany
    @PreAuthorize("isGzhOwner(#gzhCompanyId) || isGzhAdmin(#gzhCompanyId) || isWebAdmin()")
    @GetMapping("/gzhOps/api/v1/gzhCompany/admin/list/{gzhCompanyId}")
    public @ResponseBody String getAdminUserListByGzhCompanyId(@PathVariable("gzhCompanyId") long gzhCompanyId) throws JsonProcessingException {  
        GzhCompany gzhCompany = gzhCompanyRepository.findOne(gzhCompanyId);
        List<User> adminUserList = gzhCompany.getAdminUserList();
        
        // TBD. Use the following to return simple user info
        //        ObjectNode importedReport = objectMapper.createObjectNode();
        //        importedReport.put("clientCompanyName", gzhClientCompany.getChanjetClientCompany().getName());
        //        importedReport.put("reportType", reportType.toString());
        //        importedReport.put("period", period.toString());
        
        List<User> simpleUserList = adminUserList.stream()
            .map(user -> { 
                 User simpleUser = new User(); 
                 simpleUser.setId(user.getId());
                 simpleUser.setUserName(user.getUserName());
                 simpleUser.setEmail(user.getEmail());
                 return simpleUser;
             })
            .collect(Collectors.toList());
        log.info("getAdminUserListByGzhCompanyId simpleUserList=" + simpleUserList);
        String adminUserListJson = objectMapper.writeValueAsString(simpleUserList);
        return  adminUserListJson;
    }
    
    @PreAuthorize("isGzhAdmin(#gzhCompanyId) || isWebAdmin()")
    @PostMapping("/gzhOps/api/v1/gzhCompany/{gzhCompanyId}/gzhMenu/switch")
    public @ResponseBody String postGzhCompanyGzhMenuSwitchApi(@PathVariable("gzhCompanyId") long gzhCompanyId,  @RequestBody JsonNode json ) {  
        String gzhMenuTypeStr = json.path("gzhMenuType").asText();
        GzhCompanyMenuType gzhMenuType = GzhCompanyMenuType.fromString(gzhMenuTypeStr);
        
        GzhCompany gzhCompany = gzhCompanyRepository.findOne(gzhCompanyId);
        gzhCompany.setGzhMenuType(gzhMenuType);
        gzhCompanyRepository.save(gzhCompany);
        
        return  "ok";
    }
    
    @PreAuthorize("isGzhAdmin(#gzhCompanyId) || isWebAdmin()")
    @PostMapping("/gzhOps/api/v1/gzhCompany/{gzhCompanyId}/gzhMenu/selfDefinedMenu/edit")
    public @ResponseBody String postGzhCompanyGzhMenuEditApi(@PathVariable("gzhCompanyId") long gzhCompanyId,  @RequestBody String gzhMenuJsonStr ) {  
        GzhCompany gzhCompany = gzhCompanyRepository.findOne(gzhCompanyId);
        gzhCompany.setSelfDefinedGzhMenu(gzhMenuJsonStr);
        gzhCompanyRepository.save(gzhCompany);
        
        return  gzhMenuJsonStr;
    }
}
