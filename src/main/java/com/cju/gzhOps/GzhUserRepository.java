package com.cju.gzhOps;

import java.util.List;

import org.springframework.data.querydsl.QueryDslPredicateExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.querydsl.core.types.Predicate;

//All of your application components (@Component, @Service, @Repository, @Controller etc.) 
//will be automatically registered as Spring Beans.
@Repository
public interface GzhUserRepository extends PagingAndSortingRepository<GzhUser, Long>, QueryDslPredicateExecutor<GzhUser> {

    List<GzhUser> findByName(String name);
    List<GzhUser> findAll(Predicate predicate);
    GzhUser findOneByOpenId(String openId);
    GzhUser findOneByName(String name);
}