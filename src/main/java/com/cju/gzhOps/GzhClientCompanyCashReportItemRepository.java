package com.cju.gzhOps;

import org.springframework.data.querydsl.QueryDslPredicateExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface GzhClientCompanyCashReportItemRepository 
    extends PagingAndSortingRepository<GzhClientCompanyCashReportItem, Long>, QueryDslPredicateExecutor<GzhClientCompanyCashReportItem> {
}