package com.cju.errors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.web.ErrorAttributes;
import org.springframework.boot.autoconfigure.web.ErrorController;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.time.OffsetDateTime;
import java.time.ZoneId;
import java.util.Date;
import java.util.Map;

//For exceptions happen in controller, GlobalExceptionHandler will handle them.

//The following exceptions happen outside controller and will be forwarded to URL /error and handled by GlobalErrorController.
//    If a user post to a URI and the post does not have csrf data, the Spring Security will catch this error. 
//    csrf requirement for a specific URI can be turned off in WebMvcSecurityConfig
//    If a user get a URL that does not exist, an error happens and will be forwarded to /error path.
//    If an exception happens in thymeleaf processing which is in view layer, it will be forwarded to /error path.

/**
 * GlobalErrorController is the controller used by Spring MVC to handle errors that happens outside a controller.
 * GlobalErrorController is also the controller for webpages to list global errors stored in database
 */
@Controller
public class GlobalErrorController implements ErrorController{
    private Logger log = LoggerFactory.getLogger(GlobalErrorController.class);
    private ErrorAttributes errorAttributes;
    private final static String ERROR_PATH = "/error";
    
    @Autowired
    GlobalErrorRepository globalErrorRepository;

    public GlobalErrorController(ErrorAttributes errorAttributes) {
        this.errorAttributes = errorAttributes;
    }
    
    private boolean isAjax(HttpServletRequest request) {
        String requestedWithHeader = request.getHeader("X-Requested-With");
        return "XMLHttpRequest".equals(requestedWithHeader);
    }
    
    private GlobalError persistGlobalError(Map<String, Object> errorAttributes) {
        
        //Java.util.date does not have timezone info. 
        //But its toString() will add timezone info in the string which is confusing.
        //timestamp in ErrorAttribute is of Date type. 
        Date timestamp = (Date) errorAttributes.get("timestamp");
        OffsetDateTime offsetDateTime = OffsetDateTime.ofInstant(timestamp.toInstant(), ZoneId.systemDefault());
        
        GlobalError globalError = new GlobalError();
        globalError.setTimestamp(offsetDateTime);
        String xForwardedFor = (String)errorAttributes.get("xForwardedFor");
        globalError.setXForwardedFor(xForwardedFor);
        String path = (String)errorAttributes.get("path");
        globalError.setPath(path);
        globalError.setStatus((int)errorAttributes.get("status"));
        globalError.setError((String)errorAttributes.get("error"));
        String exception = (String)errorAttributes.get("exception");
        globalError.setException(exception);
        String message = (String)errorAttributes.get("message");
        globalError.setMessage(message);
        String trace = (String)errorAttributes.get("trace");
        globalError.setTrace(trace);
        globalErrorRepository.save(globalError);
        
        return globalError;
    }

    //Spring MVC will forward to this url when an error happens outside controller for get request.
    //Spring MVC does not change the request method in forwarding.
    
    //This handles both Ajax and Non-Ajax request
    //This handles both Get and Post request
    @RequestMapping(value="/error")
    public String globalRequestErrorHandler(HttpServletRequest request,  HttpServletResponse response, Model model) {
        
        Map<String, Object> errorAttributes = getErrorAttributes(request, true);
        
        //Someone is trying to probe the web server with malicious url
        int statusCode = (int)errorAttributes.get("status");
        if (statusCode == HttpStatus.NOT_FOUND.value()) {
            response.setStatus(statusCode);
            response.setContentType("application/json; charset=UTF-8");
            model.addAttribute("message", "url not found.");
            return "responseBodyOnly";
        }
        else if (statusCode == HttpStatus.FORBIDDEN.value()) {
            response.setStatus(statusCode);
            response.setContentType("application/json; charset=UTF-8");
            model.addAttribute("message", "url is forbidden.");
            return "responseBodyOnly";
        }
        
        String xForwardedFor = request.getHeader("X-Forwarded-For");
        errorAttributes.put("xForwardedFor", xForwardedFor);
        GlobalError globalError = persistGlobalError(errorAttributes);
        
        String path = globalError.getPath();
        log.info("globalRequestErrorHandler(), original request path==" + path +
                ", status code=" + errorAttributes.get("status") + 
                ", X-Requested-With=" + request.getHeader("X-Requested-With") +
                ", xForwardedFor=" + xForwardedFor);
        
        if (isAjax(request)) {
            //Ajax request from WeiXin client and GzhOps website are handled the same way.
            //This is Ajax request. The Ajax header will be kept when the original request is forwarded to /error.
            String errorMessage = globalError.getMessage();
            response.setContentType("application/json; charset=UTF-8");
            model.addAttribute("message", errorMessage);
            
            return "responseBodyOnly";
        }
        
        if (path.startsWith("/gzh/company/") || path.startsWith("/pub/gzh/company/")) {
            //Just show the error message to weixin client. 
            String errorMessage = globalError.getMessage();
            model.addAttribute("message", errorMessage);
            return "gzh/gzhWeiXinMessage";
        }
        else {
            //request comes from gzhOps 
            String exception = globalError.getException();
            if (exception != null && exception.startsWith("org.thymeleaf.exceptions")) {
                //This exception happens during thymeleaf processing. 
                //In this case, do not use layout since the layout is already in place. Just send back text.
                String errorMessage = globalError.getMessage();
                model.addAttribute("message", errorMessage);
                
                return "responseBodyOnly";
            }
            else {
                model.addAttribute("globalError", globalError);
                //show the detail error with layout
                return "errors/gzhOpsErrorDetails";
            }
        }
    }

    @Override
    public String getErrorPath() {
        return ERROR_PATH;
    }

    private Map<String, Object> getErrorAttributes(HttpServletRequest request, boolean includeStackTrace) {
        RequestAttributes requestAttributes = new ServletRequestAttributes(request);
        Map<String, Object> errorAttributesMap = this.errorAttributes                
            .getErrorAttributes(requestAttributes, includeStackTrace);
        return errorAttributesMap;
    }

    @PreAuthorize("isWebAdmin()")
    @GetMapping("/gzhOps/error/list")
    public String getErrorList(Pageable pageable, Model model) {
        Page<GlobalError> globalErrorPage = globalErrorRepository.findAll(pageable);
        model.addAttribute("globalErrorPage", globalErrorPage);
        log.info("getErrorList globalErrorPage.size=" + globalErrorPage.getSize() + ", globalErrorPage.Number " + globalErrorPage.getNumber());
        return "errors/gzhOpsErrorList";
    }
    
    @PreAuthorize("isWebAdmin()")
    @GetMapping("/gzhOps/error/details/{id}")
    public String getErrorList(@PathVariable("id") long id, Pageable pageable, Model model) {
        GlobalError globalError = globalErrorRepository.findOne(id);
        model.addAttribute("globalError", globalError);
        return "errors/gzhOpsErrorDetails";
    }
}