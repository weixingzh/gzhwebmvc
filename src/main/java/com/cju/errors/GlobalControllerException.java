package com.cju.errors;

import java.time.OffsetDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class GlobalControllerException {
    @Id    
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    
    @Column(columnDefinition= "TIMESTAMP WITH TIME ZONE")
    OffsetDateTime  timestamp; //The time that the errors were extracted
    String     path;
    @Column(columnDefinition = "text")
    String     rootCause;
    String     exception;  //exception class name
    @Column(columnDefinition = "text")
    String     trace;
    
    public void setId(long id) {
        this.id = id;
    }
    public long getId() {
        return id;
    }
    
    public void setTimestamp(OffsetDateTime timestamp) {
        this.timestamp = timestamp;
    }
    public OffsetDateTime getTimestamp() {
        return timestamp;
    }
    
    public void setPath(String path) {
        this.path = path;
    }
    public String getPath() {
        return path;
    }
    
    public void setRootCause(String rootCause) {
        this.rootCause = rootCause;
    }
    public String getRootCause() {
        return rootCause;
    }

    public void setException(String exception) {
        this.exception = exception;
    }
    public String getException() {
        return exception;
    }
    
    public void setTrace(String trace) {
        this.trace = trace;
    }
    public String getTrace() {
        return trace;
    }
}
