package com.cju.errors;

import java.time.OffsetDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class GlobalError {
    @Id    
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    
    @Column(columnDefinition= "TIMESTAMP WITH TIME ZONE")
    OffsetDateTime  timestamp; 
    String     xForwardedFor;
    String     path;
    int        status; 
    String     error;  
    //The class name of the root exception
    String     exception;  
    @Column(columnDefinition = "text")
    String     message;
    @Column(columnDefinition = "text")
    String     trace;
    
    public void setId(long id) {
        this.id = id;
    }
    public long getId() {
        return id;
    }
    
    public void setTimestamp(OffsetDateTime timestamp) {
        this.timestamp = timestamp;
    }
    public OffsetDateTime getTimestamp() {
        return timestamp;
    }
    
    public void setXForwardedFor(String xForwardedFor) {
        this.xForwardedFor = xForwardedFor;
    }
    public String getXForwardedFor() {
        return xForwardedFor;
    }
    
    public void setPath(String path) {
        this.path = path;
    }
    public String getPath() {
        return path;
    }
    
    public void setStatus(int status) {
        this.status = status;
    }
    public int getStatus() {
        return status;
    }
    
    public void setError(String error) {
        this.error = error;
    }
    public String getError() {
        return error;
    }
    
    public void setException(String exception) {
        this.exception = exception;
    }
    public String getException() {
        return exception;
    }
    
    public void setMessage(String message) {
        this.message = message;
    }
    public String getMessage() {
        return message;
    }
    
    public void setTrace(String trace) {
        this.trace = trace;
    }
    public String getTrace() {
        return trace;
    }
}
