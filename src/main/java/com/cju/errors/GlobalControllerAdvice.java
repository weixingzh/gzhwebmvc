package com.cju.errors;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;
import java.time.OffsetDateTime;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.AnnotationUtils;
import org.springframework.http.HttpStatus;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;

//Any class annotated with @ControllerAdvice becomes a controller-advice and three types of method are supported:
//   Exception handling methods annotated with @ExceptionHandler.
//   Model enhancement methods (for adding additional data to the model) annotated with @ModelAttribute. Note that these attributes are not available to the exception handling views.
//   Binder initialization methods (used for configuring form-handling) annotated with @InitBinder.

@ControllerAdvice
class GlobalControllerAdvice {
	private Logger log = LoggerFactory.getLogger(GlobalControllerAdvice.class);
	
	@Autowired
	private GlobalControllerExceptionRepository globalExceptionRepository;
	
	//Handler methods which are annotated with @ExceptionHandler are allowed to have very flexible signatures. 
    //    An exception argument
    //    Request and/or response objects (Servlet API or Portlet API). You may choose any specific request/response type, e.g. ServletRequest / HttpServletRequest or PortletRequest / ActionRequest / RenderRequest. 
    //    Session object (Servlet API or Portlet API): either HttpSession or PortletSession.
    //    WebRequest or NativeWebRequest. 
    //    Locale for the current request locale (determined by the most specific locale resolver available, i.e. the configured LocaleResolver in a Servlet environment and the portal locale in a Portlet environment).
    //    InputStream / Reader for access to the request's content. This will be the raw InputStream/Reader as exposed by the Servlet/Portlet API.
    //    OutputStream / Writer for generating the response's content. This will be the raw OutputStream/Writer as exposed by the Servlet/Portlet API.
    //    Model as an alternative to returning a model map from the handler method. 
	//    Note that the provided model is not pre-populated with regular model attributes and therefore always empty, as a convenience for preparing the model for an exception-specific view.
   
	//The following return types are supported for handler methods:
    //    A ModelAndView object (Servlet MVC or Portlet MVC).
    //    A Model object, with the view name implicitly determined through a RequestToViewNameTranslator.
    //    A Map object for exposing a model, with the view name implicitly determined through a RequestToViewNameTranslator.
    //    A View object.
    //    A String value which is interpreted as view name.
    //    @ResponseBody annotated methods (Servlet-only) to set the response content. The return value will be converted to the response stream using message converters.
    //    An HttpEntity<?> or ResponseEntity<?> object (Servlet-only) to set response headers and content. 
    //    void if the method handles the response itself (by writing the response content directly, declaring an argument of type ServletResponse / HttpServletResponse / RenderResponse for that purpose) 
	//      or if the view name is supposed to be implicitly determined through a RequestToViewNameTranslator (not declaring a response argument in the handler method signature; only applicable in a Servlet environment).
	
	private boolean isAjax(HttpServletRequest request) {
	    String requestedWithHeader = request.getHeader("X-Requested-With");
	    return "XMLHttpRequest".equals(requestedWithHeader);
	}
	
	private GlobalControllerException persistControllerException(HttpServletRequest request, Exception e) {
	    Throwable root = e;
        while (root.getCause() != null) {
            root = root.getCause();
        }
        String rootCause = root.getLocalizedMessage();
        log.error("persistControllerException() requestUrl=" + request.getRequestURL().toString() + ", RootCause=" + rootCause);
        
        StringWriter sw = new StringWriter();
        e.printStackTrace(new PrintWriter(sw));
        
        GlobalControllerException globalException = new GlobalControllerException();
        OffsetDateTime offsetDateTime = OffsetDateTime.now();
        globalException.setTimestamp(offsetDateTime);
        globalException.setPath(request.getRequestURL().toString());
        globalException.setRootCause(root.getLocalizedMessage());
        String exceptionName = e.getClass().getSimpleName();
        globalException.setException(exceptionName);
        String trace = sw.toString();
        log.info("defaultExceptionHandler() Trace=" + trace);
        globalException.setTrace(trace);
        globalExceptionRepository.save(globalException);
        
        return globalException;
	}
	
	private void setResponseStatus(HttpServletResponse response, GlobalControllerException globalException) {
	    String exceptionName = globalException.getException();
	    switch (exceptionName) {
	    case "AccessDeniedException" : 
	        response.setStatus(HttpStatus.UNAUTHORIZED.value());
	        break;
	    case "DataIntegrityViolationException" : 
	        response.setStatus(HttpStatus.CONFLICT.value());
	        break;
	    default:
	        response.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.value());
	        break;
	    }
	}
	
	//This is to handle Non-Ajax request, which expects html view response.
	//This handles both Get and Post request.
    @ExceptionHandler(value = Exception.class)
    public String globalControllerExceptionHandler(HttpServletRequest request, HttpServletResponse response, Exception e, Model model) throws Exception {
        
        GlobalControllerException globalException = persistControllerException(request, e);
        
        //If the exception is annotated with @ResponseStatus re-throw it and let the Spring framework handle it.
        if (AnnotationUtils.findAnnotation(e.getClass(), ResponseStatus.class) != null)
            throw e;
        
        //Otherwise, we handle it.
        setResponseStatus(response,  globalException);
        
        //Ajax request from WeiXin client and GzhOps website are handled the same way.
        if (isAjax(request)) {
            //This is Ajax request
            String errorMessage = globalException.getRootCause();
            if (errorMessage == null)
                errorMessage = globalException.getException();
            response.setContentType("application/json; charset=UTF-8");
            model.addAttribute("message", errorMessage);
            
            return "responseBodyOnly";
        }
        
        String requestURI = request.getRequestURI().toString();
        if (requestURI.startsWith("/gzh/company") || requestURI.startsWith("/pub/gzh/company/")) {
            //Just show the exception rootcause message to weixin client. 
            String errorMessage = globalException.getRootCause();
            if (errorMessage == null)
                errorMessage = globalException.getException();
            model.addAttribute("message", errorMessage);
            return "gzh/gzhWeiXinMessage";
        }
        else {
            //request comes from gzhOps 
            String exception = globalException.getException();
            if (exception != null && exception.startsWith("org.thymeleaf.exceptions")) {
                //This exception happens during thymeleaf processing. 
                //In this case, do not use layout since the layout is alreay in place. Just send back text.
                String errorMessage = globalException.getRootCause();
                if (errorMessage == null)
                    errorMessage = globalException.getException();
                
                //Just put the errorMessage, not converting the errorMessage into JSON
                model.addAttribute("message", errorMessage);
                
                return "responseBodyOnly";
            }
            else if (exception != null && exception.startsWith("com.cju.chanjet.ChanjetException")) {
                String errorMessage = globalException.getRootCause();
                model.addAttribute("message", errorMessage);
                
                return "gzhOps/gzhOpsMessage";
            }
            else {
                model.addAttribute("globalException", globalException);
                //show the detail exception with layout
                return "errors/gzhOpsControllerExceptionDetails";
            }
        }
	}
}
