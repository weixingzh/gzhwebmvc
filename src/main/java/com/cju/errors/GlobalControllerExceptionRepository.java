package com.cju.errors;

import org.springframework.data.querydsl.QueryDslPredicateExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface GlobalControllerExceptionRepository 
    extends PagingAndSortingRepository<GlobalControllerException, Long>, QueryDslPredicateExecutor<GlobalControllerException> {
    
}