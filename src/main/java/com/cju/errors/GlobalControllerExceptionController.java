package com.cju.errors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

/**
 * GlobalControllerExceptionController is the controller for webpages to list global exceptions stored in database
 */
@Controller
public class GlobalControllerExceptionController{
	private Logger log = LoggerFactory.getLogger(GlobalControllerExceptionController.class);
    
    @Autowired
    GlobalControllerExceptionRepository globalControllerExceptionRepository;

    @PreAuthorize("isWebAdmin()")
    @GetMapping("/gzhOps/exception/list")
    public String getExceptionList(Pageable pageable, Model model) {
        Page<GlobalControllerException> globalExceptionPage = globalControllerExceptionRepository.findAll(pageable);
        model.addAttribute("globalExceptionPage", globalExceptionPage);
        log.info("getExceptionList globalExceptionPage.size=" + globalExceptionPage.getSize() + ", globalExceptionPage.Number " + globalExceptionPage.getNumber());
        return "errors/gzhOpsControllerExceptionList";
    }
    
    @PreAuthorize("isWebAdmin()")
    @GetMapping("/gzhOps/exception/details/{id}")
    public String getExceptionList(@PathVariable("id") long id, Pageable pageable, Model model) {
        GlobalControllerException globalException = globalControllerExceptionRepository.findOne(id);
        model.addAttribute("globalException", globalException);
        return "errors/gzhOpsControllerExceptionDetails";
    }
}