package com.cju.mail;

import java.util.Locale;
import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.InputStreamSource;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;
import org.springframework.stereotype.Service;
import org.thymeleaf.context.Context;
import org.thymeleaf.spring4.SpringTemplateEngine;
import com.cju.marketing.Referal;
import com.cju.user.User;

@Service
public class MailService {
    private Logger log = LoggerFactory.getLogger(MailService.class);
    
    @Value("${app.email.verify.url.host}")
    private String appEmailVerifyUrlHost;

    //app.email.support=support@gandongtech.com
    @Value("${app.email.support}")
    private String supportEmail;
    
    @Autowired
    private MessageSource messageSource;
    
    @Autowired
    private JavaMailSender mailSender;
    
    @Autowired
    private ThreadPoolTaskScheduler threadPoolTaskScheduler;
    
    @Autowired
    private SpringTemplateEngine emailTemplateEngine;
    
    public void sendMailEnque(MimeMessage emailMessage)  {
        Runnable emailTask = new MailTask(emailMessage, mailSender, threadPoolTaskScheduler, 0);
        threadPoolTaskScheduler.execute(emailTask);
    }
    
    public void sendResetPassword(User user, String token) throws Exception {
        String to = user.getEmail();
        String url = appEmailVerifyUrlHost + "/pub/user/resetPassword?email=" + to + "&resetToken=" + token;
        String subject = messageSource.getMessage("gzh.email.subject.reset.password", 
                null, Locale.SIMPLIFIED_CHINESE);

        Context ctx = new Context(Locale.SIMPLIFIED_CHINESE);
        ctx.setVariable("user", user);
        ctx.setVariable("resetPasswordLink", url);
        String htmlContent = this.emailTemplateEngine.process("email/accountResetPassword", ctx);
        
        MimeMessage mimeMessage = this.mailSender.createMimeMessage();
        // multipart is set to false
        MimeMessageHelper message = new MimeMessageHelper(mimeMessage, false, "UTF-8"); 
        message.setSubject(subject);
        message.setFrom(supportEmail);
        message.setTo(to);
        // isHtml is set to true
        message.setText(htmlContent, true);
        
        sendMailEnque(mimeMessage);
    }
    
    public void sendNewRegistration(User user, String token) throws Exception {
        String to = user.getEmail();
        String url = appEmailVerifyUrlHost + "/pub/user/activate?email=" + to + "&activation=" + token;
        String subject = messageSource.getMessage("gzh.email.subject.activate.account", 
                null, Locale.SIMPLIFIED_CHINESE);

        Context ctx = new Context(Locale.SIMPLIFIED_CHINESE);
        ctx.setVariable("user", user);
        ctx.setVariable("activationLink", url);
        String htmlContent = this.emailTemplateEngine.process("email/accountActivation", ctx);
        
        MimeMessage mimeMessage = this.mailSender.createMimeMessage();
        // multipart is set to false
        MimeMessageHelper message = new MimeMessageHelper(mimeMessage, false, "UTF-8"); 
        message.setSubject(subject);
        message.setFrom(supportEmail);
        message.setTo(to);
        // isHtml is set to true
        message.setText(htmlContent, true);
        
        //mailSender.send(mimeMessage);
        sendMailEnque(mimeMessage);
    }
    
    public void sendReferalStatusActiveNotification(Referal referal) throws MessagingException {
        String subject = messageSource.getMessage("gzh.email.subject.referal.approved", null, Locale.SIMPLIFIED_CHINESE);
        
        Context ctx = new Context(Locale.SIMPLIFIED_CHINESE);
        ctx.setVariable("referal", referal);
        String homePage = appEmailVerifyUrlHost;
        ctx.setVariable("homePage", homePage);
        String imgResourceName = "referalQrImage";
        ctx.setVariable("imageResourceName", imgResourceName); 
        String htmlContent = this.emailTemplateEngine.process("email/referalStatusActiveNotification", ctx);
        
        MimeMessage mimeMessage = this.mailSender.createMimeMessage();
        // multipart is set to true
        // Inline image is encoded in the multipart section and referenced by cid (Content ID) from html.
        MimeMessageHelper message = new MimeMessageHelper(mimeMessage, true, "UTF-8"); 
        message.setSubject(subject);
        message.setFrom(supportEmail);
        message.setTo(referal.getUser().getEmail());
        // isHtml is set to true
        message.setText(htmlContent, true); 
        
        // Add the inline image, referenced from the HTML code as "cid:${imageResourceName}"
        byte[] qrImageBytes = referal.getQrImage().getData();
        InputStreamSource imageSource = new ByteArrayResource(qrImageBytes);
        message.addInline(imgResourceName, imageSource, "image/jpg");
        
        sendMailEnque(mimeMessage);
    }
}