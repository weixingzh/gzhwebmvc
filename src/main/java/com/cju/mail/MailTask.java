package com.cju.mail;

import java.util.Calendar;
import java.util.Date;

import javax.mail.Address;
import javax.mail.Message.RecipientType;
import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.mail.MailException;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;

public class MailTask implements Runnable {
    private Logger log = LoggerFactory.getLogger(MailTask.class);
    private int MAX_NUM_OF_RETRY = 5;
    private int[] delayMinutes = {10, 60, 120, 300, 640};
    
    private MimeMessage emailMessage;
    private JavaMailSender mailSender;
    private ThreadPoolTaskScheduler threadPoolTaskScheduler;
    private int numOfRun;
    
    public MailTask(MimeMessage emailMessage, JavaMailSender mailSender, ThreadPoolTaskScheduler threadPoolTaskScheduler, int numOfRun) {
        this.emailMessage = emailMessage;
        this.mailSender = mailSender;
        this.threadPoolTaskScheduler = threadPoolTaskScheduler;
        this.numOfRun = numOfRun;
    }
    
    @Override
    public void run() {
        
        String to = "";
        String subject = "";
        try {
            Address[] toRecipients = emailMessage.getRecipients(RecipientType.TO);
            to = toRecipients[0].toString();
            subject = emailMessage.getSubject();
        } catch (MessagingException e2) {
            // TODO Auto-generated catch block
            e2.printStackTrace();
        }
        
        log.info("run(), sending email message. To=" + to + ", subject=" + subject);
        
        try {
            mailSender.send(emailMessage);
        }
        catch (MailException e) {
            e.printStackTrace();
            if (numOfRun < MAX_NUM_OF_RETRY) {
                MailTask retryTask = new MailTask(emailMessage, mailSender, threadPoolTaskScheduler, numOfRun + 1);
                int delay = delayMinutes[numOfRun];
                Calendar calendar = Calendar.getInstance(); // gets a calendar using the default time zone and locale.
                calendar.add(Calendar.MINUTE, delay);
                Date startTime = calendar.getTime();
                log.info("run(), sending email failed. Retry num " + (numOfRun + 1) + " will take place at " + startTime);
                threadPoolTaskScheduler.schedule(retryTask, startTime);
            }
            else {
                log.info("run(), sending email message reaching the max num of retries. To=" + to + ", subject=" + subject);
            }
        }
    }
}
