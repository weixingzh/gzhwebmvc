package com.cju;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.amazonaws.ClientConfiguration;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.auth.InstanceProfileCredentialsProvider;
import com.amazonaws.client.builder.AwsClientBuilder.EndpointConfiguration;
import com.amazonaws.regions.Regions;
import com.amazonaws.retry.PredefinedBackoffStrategies;
import com.amazonaws.retry.RetryPolicy;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;


@Configuration
public class WebMvcAwsConfig {
    
    @Value("${aws.iam.gzhwebmvc.key.id}")
    private String awsIamKeyId;
    
    @Value("${aws.iam.gzhwebmvc.access.key}")
    private String awsIamAccessKey;
    
    @Bean
    public AmazonS3 s3Client() {
    
        //To make requests to Amazon Web Services, you must supply AWS credentials to the AWS SDK for Java. You can do this in the following ways:
        //    Use the default credential provider chain (recommended).
        //    Use a specific credential provider or provider chain (or create your own).
        //    Supply the credentials yourself. These can be root account credentials, IAM credentials, or temporary credentials retrieved from AWS STS.
        //You can also pass an InstanceProfileCredentialsProvider instance directly to the client constructor to get instance profile credentials 
        //without proceeding through the entire default provider chain. When using this approach, the SDK retrieves temporary AWS credentials that have 
        //the same permissions as those associated with the IAM role associated with the Amazon EC2 instance in its instance profile. 
        //refreshCredentialsAsync set to false so that credentials will be refreshed from the instance metadata service synchronously
        
        //Use BasicAWSCredentials since InstanceProfileCredentialsProvider can only be used on EC2 and can not be tested locally.
        BasicAWSCredentials awsCreds = new BasicAWSCredentials(awsIamKeyId, awsIamAccessKey);
        ClientConfiguration s3ClientConfiguration = new ClientConfiguration();
        s3ClientConfiguration.setMaxErrorRetry(10);
        AmazonS3 s3Client = AmazonS3ClientBuilder.standard()
                .withCredentials(new AWSStaticCredentialsProvider(awsCreds))
                .withEndpointConfiguration(new EndpointConfiguration("s3.cn-north-1.amazonaws.com.cn", Regions.CN_NORTH_1.getName()))
                .withClientConfiguration(s3ClientConfiguration)
                .build();
        return s3Client;
    }
}
