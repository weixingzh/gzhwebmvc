package com.cju.chanjet;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;

//@Embeddable and @Entity can not be applied to a class at the same time
//client company asset from chanjet. Embedded in GzhClientCompanyIncomeItem.
//uniqueConstraints = { @UniqueConstraint(columnNames={"FIELD_A", "FIELD_B"}) }
@Embeddable
public class ChanjetClientCompanyIncomeItem {
    //bookId, period, itemId identify an income.
    @Column(name = "book_id")
    private long bookId;
    private String period;
    
    //The following fields are taken from chanjet json reply. 
    //Note the spelling error for formular.
    
    //sequenceId is the id field of an income item from chanjet. It identifies the order of the item on the report.
    int sequenceId;
    //fold field of an income item from chanjet. It identifies the sequenceId under which this item is in.
    private int fold;
    //foldFlag field of an income item from chanjet. 
    //If 1, it means this item contains the following items whose fold field is set to the sequenceId of this item.
    private int foldFlag;
    //itemId, not used
    private long itemId;
    //项目
    private String item;
    //incomeIndentClass is calculated based on item and used as the CSS class for ident
    private String incomeIndentClass;
    //行次
    private int row;
    //本月金额
    private double monthCashBD;
    //本年累计金额
    private double yearCashBD; 
    //上年同期累计金额
    private double lastYearMonthCashBD;
    //fomularDetail
    private String fomularDetail;
    
    protected ChanjetClientCompanyIncomeItem() {}
    public ChanjetClientCompanyIncomeItem(long bookId, String period) {
        this.bookId = bookId;
        this.period = period;
    }

    public void setBookId(long bookId) {
        this.bookId = bookId;
    }
    public long getBookId() {
        return bookId;
    }
    
    public void setPeriod(String period) {
        this.period = period;
    }
    public String period() {
        return period;
    }
    
    public void setSequenceId(int sequenceId) {
        this.sequenceId = sequenceId;
    }
    public int getSequenceId() {
        return sequenceId;
    }
    
    public void setFold(int fold) {
        this.fold = fold;
    }
    public int getFold() {
        return fold;
    }
    
    public void setFoldFlag(int foldFlag) {
        this.foldFlag = foldFlag;
    }
    public int getFoldFlag() {
        return foldFlag;
    }
    
    public void setIncomeIndentClass(String incomeIndentClass) {
        this.incomeIndentClass = incomeIndentClass;
    }
    public String getIncomeIndentClass() {
        return incomeIndentClass;
    }
    
    public void setItemId(long itemId) {
        this.itemId = itemId;
    }
    public long getItemId() {
        return itemId;
    }
    
    public void setItem(String item) {
        this.item = item;
    }
    public String getItem() {
        return item;
    }
    
    public void setRow(int row) {
        this.row = row;
    }
    public int getRow() {
        return row;
    }
    
    public void setMonthCashBD(double monthCashBD) {
        this.monthCashBD = monthCashBD;
    }
    public double getMonthCashBD() {
        return monthCashBD;
    }
    
    public void setYearCashBD(double yearCashBD) {
        this.yearCashBD = yearCashBD;
    }
    public double getYearCashBD() {
        return yearCashBD;
    }
    
    public void setLastYearMonthCashBD(double lastYearMonthCashBD) {
        this.lastYearMonthCashBD = lastYearMonthCashBD;
    }
    public double getLastYearMonthCashBD() {
        return lastYearMonthCashBD;
    }
    
    public void setFomularDetail(String fomularDetail) {
        this.fomularDetail = fomularDetail;
    }
    public String getFomularDetail() {
        return fomularDetail;
    }
    
    @Override
    public String toString() {
        return String.format(
                "ChanjetClientCompanyIncomeItem[bookId='%s', period='%s', item='%s']",
                bookId, period, item);
    }
}