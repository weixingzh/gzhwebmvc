package com.cju.chanjet;

import javax.persistence.Column;
import javax.persistence.Embeddable;

//@Embeddable and @Entity can not be applied to a class at the same time
//client company asset from chanjet. Embedded in GzhClientCompanyAsset.
//uniqueConstraints = { @UniqueConstraint(columnNames={"FIELD_A", "FIELD_B"}) }
@Embeddable
public class ChanjetClientCompanyAssetItem {
    //bookId, period,assetType identify an asset.
    @Column(name = "book_id")
    private long bookId;
    private String period;
    
    //The following fields are taken from chanjet json reply. 
    //sequenceId is the id field of an income item from chanjet. It identifies the order of the item on the report.
    private int sequenceId;
    //fold field of an income item from chanjet. It identifies the sequenceId under which this item is in.
    private int fold;
    //foldFlag field of an income item from chanjet. 
    //If 1, it means this item contains the following items whose fold field is set to the sequenceId of this item.
    private int foldFlag;
    private long assetId;
    private String asset;
    private int assetRow;
    private String assetIndentClass;
    private double assetEndBalance;
    private double assetYearInitBalance;
    private long equityId;
    private String equity;
    private int equityRow;
    private String equityIndentClass;
    private double equityEndBalance;
    private double equityYearInitBalance;
    private String fomularDetailOne;
    private String fomularDetailTwo;
    
    protected ChanjetClientCompanyAssetItem() {}
    public ChanjetClientCompanyAssetItem(long bookId, String period) {
        this.bookId = bookId;
        this.period = period;
    }

    public void setBookId(long bookId) {
        this.bookId = bookId;
    }
    public long getBookId() {
        return bookId;
    }
    
    public void setPeriod(String period) {
        this.period = period;
    }
    public String period() {
        return period;
    }
    
    public void setSequenceId(int sequenceId) {
        this.sequenceId = sequenceId;
    }
    public int getSequenceId() {
        return sequenceId;
    }
    
    public void setFold(int fold) {
        this.fold = fold;
    }
    public int getFold() {
        return fold;
    }
    
    public void setFoldFlag(int foldFlag) {
        this.foldFlag = foldFlag;
    }
    public int getFoldFlag() {
        return foldFlag;
    }
    
    public void setAssetId(long assetId) {
        this.assetId = assetId;
    }
    public long getAssetId() {
        return assetId;
    }
    
    public void setAsset(String asset) {
        this.asset = asset;
    }
    public String getAsset() {
        return asset;
    }
    
    public void setAssetRow(int assetRow) {
        this.assetRow = assetRow;
    }
    public int getAssetRow() {
        return assetRow;
    }
    
    public void setAssetIndentClass(String assetIndentClass) {
        this.assetIndentClass = assetIndentClass;
    }
    public String getAssetIndentClass() {
        return assetIndentClass;
    }
    
    public void setAssetEndBalance(double assetEndBalance) {
        this.assetEndBalance = assetEndBalance;
    }
    public double getAssetEndBalance() {
        return assetEndBalance;
    }
    
    public void setAssetYearInitBalance(double assetYearInitBalance) {
        this.assetYearInitBalance = assetYearInitBalance;
    }
    public double getAssetYearInitBalance() {
        return assetYearInitBalance;
    }
    
    public void setEquityId(long equityId) {
        this.equityId = equityId;
    }
    public long getEquityId() {
        return equityId;
    }
    
    public void setEquity(String equity) {
        this.equity = equity;
    }
    public String getEquity() {
        return equity;
    }
    
    public void setEquityRow(int equityRow) {
        this.equityRow = equityRow;
    }
    public int getEquityRow() {
        return equityRow;
    }
    
    public void setEquityIndentClass(String equityIndentClass) {
        this.equityIndentClass = equityIndentClass;
    }
    public String getEquityIndentClass() {
        return equityIndentClass;
    }
    
    public void setEquityEndBalance(double equityEndBalance) {
        this.equityEndBalance = equityEndBalance;
    }
    public double getEquityEndBalance() {
        return equityEndBalance;
    }
    
    public void setEquityYearInitBalance(double equityYearInitBalance) {
        this.equityYearInitBalance = equityYearInitBalance;
    }
    public double getEquityYearInitBalance() {
        return equityYearInitBalance;
    }
    
    public void setFomularDetailOne(String fomularDetailOne) {
        this.fomularDetailOne = fomularDetailOne;
    }
    public String getFomularDetailOne() {
        return fomularDetailOne;
    }
    
    public void setFomularDetailTwo(String fomularDetailTwo) {
        this.fomularDetailTwo = fomularDetailTwo;
    }
    public String getFomularDetailTwo() {
        return fomularDetailTwo;
    }
    
    @Override
    public String toString() {
        return String.format(
                "ChanjetClientCompanyAssetItem[bookId='%s', period='%s', asset='%s']",
                bookId, period, asset);
    }
}