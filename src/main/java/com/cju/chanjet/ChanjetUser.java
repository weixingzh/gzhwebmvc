package com.cju.chanjet;

import java.time.OffsetDateTime;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import com.cju.user.User;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

@Entity
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id", scope = ChanjetUser.class)
@Table(name = "chanjetUser", 
       uniqueConstraints = {@UniqueConstraint(columnNames = "chanjetUserId", name = "ChanjetUser_UNIQUE_ChanjetUserId")}
)
public class ChanjetUser {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    
    //Need to use Long for nullable. Primitive types are not nullable.
    @Column(nullable = true)
    private Long   chanjetUserId;
    private String chanjetUserName;
    private String chanjetPassword; 
    //The chanjetAppKey of the GzhCompany is required to get ciaToken for each chanjetUser.
    private String chanjetCiaToken;
    @Column(columnDefinition= "TIMESTAMP WITH TIME ZONE")
    private OffsetDateTime  chanjetCiaTokenGetLastTime;
    
    @JsonIgnore
    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name="userId", foreignKey = @ForeignKey(name = "ChanjetUser_FK_UserId"))
    private User user;

    public ChanjetUser() {}
    
    public long getId() {
        return id;
    }
    public void setId(long id) {
        this.id = id;
    }
    
    public Long getChanjetUserId() {
        return chanjetUserId;
    }
    public void setChanjetUserId(Long chanjetUserId) {
        this.chanjetUserId = chanjetUserId;
    }
    
    public String getChanjetUserName() {
        return chanjetUserName;
    }
    public void setChanjetUserName(String chanjetUserName) {
        this.chanjetUserName = chanjetUserName;
    }

    public String getChanjetPassword() {
        return chanjetPassword;
    }
    public void setChanjetPassword(String chanjetPassword) {
        this.chanjetPassword = chanjetPassword;
    }
    
    public String getChanjetCiaToken() {
        return chanjetCiaToken;
    }
    public void setChanjetCiaToken(String chanjetCiaToken) {
        this.chanjetCiaToken = chanjetCiaToken;
    }
    
    public OffsetDateTime getChanjetCiaTokenGetLastTime() {
        return chanjetCiaTokenGetLastTime;
    }
    public void setChanjetCiaTokenGetLastTime(OffsetDateTime chanjetCiaTokenGetLastTime) {
        this.chanjetCiaTokenGetLastTime = chanjetCiaTokenGetLastTime;
    }
    
    public void setUser(User user) {
        this.user = user;
    }
    public User getUser() {
        return user;
    }
}
