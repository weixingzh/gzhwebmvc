package com.cju.chanjet;

import java.io.IOException;
import java.nio.charset.Charset;
import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import javax.annotation.PostConstruct;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.stereotype.Service;
import org.springframework.util.DigestUtils;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;


//易代账APIV1.0, web page http://e.chanjet.com/desc/dev
@Service
public class ChanjetService {
    private Logger log = LoggerFactory.getLogger(ChanjetService.class);
    private final int CHANJET_NOT_LOGIN = 2;
    private final int CHANJET_LOGIN_EXPIRED = 25016;
    //This error code happens a lot.
    private final int CHANJET_SET_DEFAULT_BOOK_FAILED = 1512;
    
    @Value("${gzh.chanjet.appKey}")
    private String chanjetAppKey;
    
    @Autowired 
    private ChanjetClientCompanyRepository chanjetClientCompanyRepository; 
    
    @Autowired
    private ChanjetUserRepository chanjetUserRepository;
    
    @Autowired
    private MessageSource messageSource;
    
    @Autowired
    private ObjectMapper objectMapper;
    
    private Map<String, List<Integer>> assetIndentMap;
    private Map<String, List<Integer>> incomeIndentMap;
    private Map<String, List<Integer>> cashIndentMap;
    
    @PostConstruct
    public void init() {
        assetIndentMap = new HashMap<String, List<Integer>> ();
        List<Integer> assetIndent0 = Arrays.asList(new Integer[] {0, 53});
        assetIndentMap.put("indent-0", assetIndent0);
        List<Integer> assetIndent2 = Arrays.asList(new Integer[] {10, 15, 29, 30, 41, 46, 47, 52});
        assetIndentMap.put("indent-2", assetIndent2);
        List<Integer> assetIndent5 = Arrays.asList(new Integer[] {11, 12, 13});
        assetIndentMap.put("indent-5", assetIndent5);
       
        incomeIndentMap = new HashMap<String, List<Integer>> ();
        List<Integer> incomeIndent0 = Arrays.asList(new Integer[] {1, 2, 20, 21, 22, 24, 30, 31, 32});
        incomeIndentMap.put("indent-0", incomeIndent0);
        List<Integer> incomeIndent2 = Arrays.asList(new Integer[] {3, 11, 14, 18, 23, 25});
        incomeIndentMap.put("indent-2", incomeIndent2);
        List<Integer> incomeIndent3 = Arrays.asList(new Integer[] {4, 12, 15, 19});
        incomeIndentMap.put("indent-3", incomeIndent3);
        List<Integer> incomeIndent5 = Arrays.asList(new Integer[] {26, 27, 28, 29});
        incomeIndentMap.put("indent-5", incomeIndent5);
        List<Integer> incomeIndent6 = Arrays.asList(new Integer[] {5, 6, 7, 8, 9, 10, 13, 16, 17});
        incomeIndentMap.put("indent-6", incomeIndent6);
        
        cashIndentMap = new HashMap<String, List<Integer>> ();
        List<Integer> cashIndent0 = Arrays.asList(new Integer[] {0, 20, 22});
        cashIndentMap.put("indent-0", cashIndent0);
        List<Integer> cashIndent2 = Arrays.asList(new Integer[] {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 21});
        cashIndentMap.put("indent-2", cashIndent2);
    }
    
    private String getReportItemIndent(Map<String, List<Integer>> itemIndentMap, int row) {
        for (Map.Entry<String, List<Integer>> entry : itemIndentMap.entrySet()) {
            if (entry.getValue().contains(row)) 
                return entry.getKey();
        }
        
        //if nothing match, return indent-1 which is the most common case
        return "indent-1";
    };
    
    private JsonNode getJsonNode(String jsonStr) {
        //read JSON like DOM Parser
        JsonNode rootNode = null;
        try {
            rootNode = objectMapper.readTree(jsonStr);
        } catch (JsonProcessingException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return rootNode;
    }
    
    private String getUtf8Str(JsonNode jsonNode) {
        String utf8Str = null;
        try {
            //utf8Str will not contain /uFFFF strings 
            //writerWithDefaultPrettyPrinter() will output the json string with line return and indent
            utf8Str = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(jsonNode);
        } catch (JsonProcessingException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return utf8Str;
    }
    
    private JsonNode checkAndRetryIfFailed(String callerInfo, ChanjetUser chanjetUser, JsonNode rootNode, 
        RestTemplate restTemplate, Map<String, String> vars, String url, int maxNumOfRetry) throws Exception  {
        
        log.info("checkAndRetryIfFailed url= " + url);
        //log.info("checkAndRetryIfFailed rootNode=" + getUtf8Str(rootNode));

        if (rootNode.path("success").asBoolean() == true) {
            log.info("checkAndRetryIfFailed sees a success rootNode. Return the same success rootNode back.");
            return rootNode;
        }
        
        JsonNode errorCode = rootNode.path("errorCode");
        if (errorCode.asInt() == CHANJET_NOT_LOGIN || errorCode.asInt() == CHANJET_LOGIN_EXPIRED) {
            log.info("checkAndRetryIfFailed sees a failed rootNode. callerInfo=" + callerInfo 
                   + "code=" + errorCode.asInt() + ", message=" +  rootNode.path("message").asText() + ", url=" + url);
            ChanjetUser updatedChanjetUser;
            // getCiaToken may throw an self generated exception. We need to catch the exception so that we can fall through for retry.
            try {
                updatedChanjetUser = getCiaToken(chanjetUser.getChanjetUserName(), chanjetUser.getChanjetPassword());
                //we get the cia token
                //check if the chanjet user is currently bound to another user
                ChanjetUser alreadyBoundChanjetUser = chanjetUserRepository.findOneByChanjetUserId(updatedChanjetUser.getChanjetUserId());
                if (alreadyBoundChanjetUser != null) {
                    if (alreadyBoundChanjetUser.getId() != chanjetUser.getId()) {
                        //The existing chanjetUser is bound to another different user. 
                        //Need to break the existing bind by the chanjetUserId of the existing chanjetUser to null
                        //PostgreSQL allows multiple null values on a column with unique constrain.
                        alreadyBoundChanjetUser.setChanjetUserId(null);
                        alreadyBoundChanjetUser.setChanjetCiaToken(null);
                        chanjetUserRepository.save(alreadyBoundChanjetUser);
                    }
                }
                
                //Now update the chanjetUser. 
                chanjetUser.setChanjetUserId(updatedChanjetUser.getChanjetUserId());
                chanjetUser.setChanjetCiaToken(updatedChanjetUser.getChanjetCiaToken());
                chanjetUser.setChanjetCiaTokenGetLastTime(OffsetDateTime.now());
                chanjetUserRepository.save(chanjetUser);
                
            } catch (Exception e) {
                e.printStackTrace();
                //Fall through for retry
            }
            
            //Either we get ciaToken from Chanjet successfully or failed. 
            //In either case, we fall through to retry until the retry number is reached.
        }
        
        if (maxNumOfRetry > 0) {
            // retry one more time
            String ciaToken = chanjetUser.getChanjetCiaToken();
            log.info("checkAndRetryIfFailed sees a failed rootNode. callerInfo=" + callerInfo  + ", Retry with ciaToken=" + ciaToken);
            vars.put("CIATOKEN", ciaToken);
            String jsonStr = restTemplate.getForObject(url, String.class, vars);
            JsonNode retryJsonNode = getJsonNode(jsonStr);
            //call checkAndRetryIfFailed
            JsonNode retryNode = checkAndRetryIfFailed(callerInfo, chanjetUser, retryJsonNode, restTemplate, vars, url, maxNumOfRetry-1);
            return retryNode;
        }
        else {
            log.info("checkAndRetryIfFailed sees a failed rootNode, and runs out of retry. Returning the passed in error rootNode back! "
                   + "callerInfo=" + callerInfo + "url=" + url);
            String message = messageSource.getMessage("gzh.chanjet.service.error", 
                    new Object[] {rootNode.path("errorCode").asText(), rootNode.path("message").asText()}, Locale.SIMPLIFIED_CHINESE);
            Exception chanjetException = new Exception(message);
            throw chanjetException;
        }
    }
    
    //二、 用户认证相关接口
    //1. WEB页面单点登录（账号密码）
    //GET Success. Can login and show the webpage
    //String getCiaTokenUrl = "https://e.chanjet.com/auth/thirdSSOPage?loginName=15916201201&password=a0a475cf454cf9a06979034098167b9e&type=password";
    
    //3. 账号和密码登录（获取访问令牌ciaToken） 
    //This API is used in getCiaToken()
    //ciaToken can expire. If chanjet API call fails due to ciaToken expiration, we will get new ciaToken and retry.
    //String getCiaTokenUrl = "https://e.chanjet.com/auth/login?loginName=15916201201&password=a0a475cf454cf9a06979034098167b9e";
   
    //十一、 平台相关接口
    //2. 授权登录接口 jsonp to get auth_code 
    //GET Success
    //https://e.chanjet.com/internal_api/authorizeByJsonp?client_id=finance&callback=cjucallback
    
    //String loginName = "15916201201";
    //String password = "Ab123456";
    //MD5 for "Ab123456" is a0a475cf454cf9a06979034098167b9e
    public ChanjetUser getCiaToken(String chanjetUserName, String chanjetPassword) throws Exception {
        RestTemplate restTemplate = new RestTemplate();
        //Need to add utf8 message converter to restTemplate. Otherwise, the Chinese character will be turned into garbage.
        restTemplate.getMessageConverters().add(0, new StringHttpMessageConverter(Charset.forName("UTF-8")));
        
        Map<String, String> vars = new HashMap<String, String>();
        String getCiaTokenUrl = "https://e.chanjet.com/auth/login";
        MultiValueMap<String, String> form = new LinkedMultiValueMap<String, String>();
        
        String passwordMD5 = DigestUtils.md5DigestAsHex(chanjetPassword.getBytes());
        log.info("getCiaToken password md5DigestAsHex String =" + passwordMD5);
        form.add("loginName", chanjetUserName);
        form.add("password", passwordMD5);
        form.add("appKey", chanjetAppKey);
        String replyStr = restTemplate.postForObject(getCiaTokenUrl, form, String.class, vars);
        log.info("getCiaToken replyStr=" + replyStr);
        JsonNode rootNode = getJsonNode(replyStr);
        
        //No retry if there is error in getting ciaToken
        if (rootNode.path("success").asBoolean() != true) {
            String message = messageSource.getMessage("gzh.chanjet.service.get.cia.token.error", 
                    new Object[] {rootNode.path("errorCode").asText(), rootNode.path("message").asText()}, Locale.SIMPLIFIED_CHINESE);
            Exception e = new Exception(message);
            throw e;
        }
        
        JsonNode dataNode = rootNode.path("data");
        ChanjetUser chanjetUser = new ChanjetUser();
        chanjetUser.setChanjetCiaToken(dataNode.path("ciaToken").asText());
        chanjetUser.setChanjetUserId(dataNode.path("user").path("user_id").asLong());
        chanjetUser.setChanjetUserName(chanjetUserName);
        chanjetUser.setChanjetPassword(chanjetPassword);
        chanjetUser.setChanjetCiaTokenGetLastTime(OffsetDateTime.now());
        
        return chanjetUser;
    }

    
    //五、 客户相关接口
    //1. 查询单个客户
    //chanjet does not support sort. 
    //https://e.chanjet.com/api/v1/account/getAcc?ciaToken=2d909aaf-432c-435b-90da-fa187815682c&acc_id=1219789
    public ChanjetClientCompany getClientCompany(ChanjetUser chanjetUser, long acc_id) throws Exception {
        String url = "https://e.chanjet.com/api/v1/account/getAcc?ciaToken={CIATOKEN}&acc_id={ACCID}";
        RestTemplate restTemplate = new RestTemplate();
        restTemplate.getMessageConverters().add(0, new StringHttpMessageConverter(Charset.forName("UTF-8")));
        Map<String, String> vars = new HashMap<String, String>();
        
        String ciaToken = chanjetUser.getChanjetCiaToken();
        log.info("getClientCompany ciaToken=" + ciaToken);
        vars.put("CIATOKEN", ciaToken);
        vars.put("ACCID", Long.toString(acc_id));
        String jsonStr = restTemplate.getForObject(url, String.class, vars);
        JsonNode rootNode = getJsonNode(jsonStr);
        //log.info("getClientCompany rootNode=" + getUtf8Str(rootNode));
        
        //checkAndRetryIfFailed may throw an exception
        String callerInfo = "getClientCompany() chanjetUser= " + chanjetUser.getChanjetCiaToken() + ", chanjet acc_id=" + acc_id;
        rootNode = checkAndRetryIfFailed(callerInfo, chanjetUser, rootNode, restTemplate, vars, url, 2);
        
        //Now we got a success rootNode.
        JsonNode dataNode = rootNode.path("data");
        JsonNode accountNode = dataNode.path("account");
        ChanjetClientCompany chanjetClientCompany = new ChanjetClientCompany();
        chanjetClientCompany.setName(accountNode.path("name").asText());
        chanjetClientCompany.setAccId(accountNode.path("acc_id").asLong());
        chanjetClientCompany.setBookId(accountNode.path("book_id").asLong());
        // Only the user who is doing the accounting can get financial data from chanjet. 
        chanjetClientCompany.setUserId(accountNode.path("user_id").asLong());
        chanjetClientCompany.setMaker(accountNode.path("maker").asText());
        chanjetClientCompany.setUserNoteName(dataNode.path("user_note_name").asText());
        ChanjetClientCompany currChanjetClientCompany = chanjetClientCompanyRepository.findOneByAccId(acc_id);
        if (currChanjetClientCompany == null)
            chanjetClientCompany.setImportedToGzh(YesNoType.NO);
        else {
            //check to see if the existing chanjetClientCompany belongs to the gzhCompany of the calling user 
            if (currChanjetClientCompany.getGzhClientCompany().getGzhCompany().getId() == chanjetUser.getUser().getAdminedGzhCompany().getId())
                chanjetClientCompany.setImportedToGzh(YesNoType.YES);
            else {
                //The chanjetClientCompany is in the database, but is linked to another gzhCompany.
                chanjetClientCompany.setImportedToGzh(YesNoType.NO);
            }
        }
        chanjetClientCompany.setCerti(accountNode.path("certi").asText());
        chanjetClientCompany.setCerDue(accountNode.path("cer_due").asText());
        chanjetClientCompany.setAddress(accountNode.path("address").asText());
        chanjetClientCompany.setLegalPerson(accountNode.path("legal_person").asText());
        chanjetClientCompany.setLegalNo(accountNode.path("legal_no").asText());
        chanjetClientCompany.setContact(accountNode.path("contact").asText());
        chanjetClientCompany.setContactNote(accountNode.path("contact_note").asText());
        chanjetClientCompany.setManager(accountNode.path("manager").asText());
        chanjetClientCompany.setManagerNote(accountNode.path("manager_note").asText());
        chanjetClientCompany.setTaxDep(accountNode.path("tax_dep").asText());
        chanjetClientCompany.setTaxCpNo(accountNode.path("tax_cp_no").asText());
        chanjetClientCompany.setTaxNo(accountNode.path("tax_no").asText());
        chanjetClientCompany.setTaxCtrl(accountNode.path("tax_ctrl").asText());
        chanjetClientCompany.setTaxType(accountNode.path("tax_type").asText());
        chanjetClientCompany.setVaTax(accountNode.path("va_tax").asText());
        chanjetClientCompany.setSubjectSys(accountNode.path("subject_sys").asText());
        chanjetClientCompany.setTaxState(accountNode.path("tax_state").asText());
        chanjetClientCompany.setStartPeriod(accountNode.path("start_period").asText());
        chanjetClientCompany.setBkState(accountNode.path("bk_state").asText());
        chanjetClientCompany.setBkMonth(accountNode.path("bk_month").asText());
        chanjetClientCompany.setCreateTime(accountNode.path("create_time").asText());
        chanjetClientCompany.setUpdateTime(accountNode.path("update_time").asText());
        
        return chanjetClientCompany; 
    }
    
    //五、 客户相关接口
    //2. 客户列表
    //chanjet does not support sort. But it supports pagination
    //https://e.chanjet.com/api/v1/account/page?ciaToken=2d909aaf-432c-435b-90da-fa187815682c&start=0&limit=20
    //Without user_id, chanjet returns all the client companies under the user and his subordinate team members.
    public Page<ChanjetClientCompany> getClientCompanyPage(ChanjetUser chanjetUser, Pageable pageable) throws Exception {
        String url = "https://e.chanjet.com/api/v1/account/page?ciaToken={CIATOKEN}&start={START}&limit={LIMIT}";
        RestTemplate restTemplate = new RestTemplate();
        restTemplate.getMessageConverters().add(0, new StringHttpMessageConverter(Charset.forName("UTF-8")));
        Map<String, String> vars = new HashMap<String, String>();
        int start = pageable.getOffset();
        int limit = pageable.getPageSize();
        String ciaToken = chanjetUser.getChanjetCiaToken();
        vars.put("CIATOKEN", ciaToken);
        vars.put("START", Integer.toString(start));
        vars.put("LIMIT", Integer.toString(limit));
        log.info("getClientCompanyList start = " + start);
        log.info("getClientCompanyList limit = " + limit);
        
        //jsonStr may contain string representation of UTF8 character in the form of \uFFFF
        //That is, one UTF8 character (Chinese) will be represented by 6 characters in jsonStr.
        String jsonStr = restTemplate.getForObject(url, String.class, vars);
        JsonNode rootNode = getJsonNode(jsonStr);
        //log.info("getClientCompanyPage rootNode=" + getUtf8Str(rootNode));
        
        //checkAndRetryIfFailed may throw an exception
        String callerInfo = "getClientCompanyPage() chanjetUser=" + chanjetUser.getChanjetUserName();
        rootNode = checkAndRetryIfFailed(callerInfo, chanjetUser, rootNode, restTemplate, vars, url, 2);

        //Now we got a success rootNode.
        JsonNode listNode = rootNode.path("data").path("list");
        Iterator<JsonNode> elements = listNode.elements();
        List<ChanjetClientCompany> chanjetClientCompanyList = new ArrayList<ChanjetClientCompany> ();
        
        while(elements.hasNext()){
            JsonNode companyNode = elements.next();
            ChanjetClientCompany chanjetClientCompany = new ChanjetClientCompany();
            chanjetClientCompany.setAccId(companyNode.get("acc_id").asLong());
            chanjetClientCompany.setName(companyNode.get("name").asText());
            //In this API, there is "note_name", not "user_note_name" as in getClientCompany()
            chanjetClientCompany.setUserNoteName(companyNode.path("note_name").asText());
            //Do not need to set other fields here as we only show a few fields in chanjetClientCompany list page.
            //When a user import a chanjet client company either from chanjetClientCompanyList page or from 
            //chanjetClientCompany details page, the chanjetClientCompany will have all the fields set.
            ChanjetClientCompany currChanjetClientCompany = chanjetClientCompanyRepository.findOneByAccId(chanjetClientCompany.getAccId());
            if (currChanjetClientCompany == null)
                chanjetClientCompany.setImportedToGzh(YesNoType.NO);
            else {
                //check to see if the existing chanjetClientCompany belongs to the gzhCompany of the calling user 
                if (currChanjetClientCompany.getGzhClientCompany().getGzhCompany().getId() == chanjetUser.getUser().getAdminedGzhCompany().getId())
                    chanjetClientCompany.setImportedToGzh(YesNoType.YES);
                else {
                    //The chanjetClientCompany is in the database, but is linked to another gzhCompany.
                    chanjetClientCompany.setImportedToGzh(YesNoType.NO);
                }
            }
            //log.info("getClientCompanyPage company=" + company.toString());
            chanjetClientCompanyList.add(chanjetClientCompany);
        }
        int total = rootNode.get("data").get("count").asInt();

        PageImpl<ChanjetClientCompany> chanjetClientCompanyPage = new PageImpl<ChanjetClientCompany>(chanjetClientCompanyList, pageable, total);
        return chanjetClientCompanyPage; 
    }
    
    //账簿相关接口
    //1. 查询当前账簿
    //   https://ee.chanjet.com/api/v1/ee/appVm
    //   接口参数
    //      ciaToken
    //      url=/services/1.0/appext/AccountBook/getAccountBookById
    //      ACCOUNTBOOK=账簿ID
    //      bookId
    public JsonNode getChanjetCompanyReportCurrentPeriod(String chanjetCompanyName, ChanjetUser chanjetUser, long accId,  long bookId) throws Exception {
        
        String ciaToken = chanjetUser.getChanjetCiaToken();
        log.info("getChanjetCompanyAccountingPeriod() ciaToken=" + ciaToken +
                ", accId=" + accId +
                ", bookId=" + bookId);
        String url = "https://ee.chanjet.com/api/v1/ee/appVm?ciaToken={CIATOKEN}&url={URL}&ACCOUNTBOOK={ACCID}&bookID={BOOKID}";
        RestTemplate restTemplate = new RestTemplate();
        restTemplate.getMessageConverters().add(0, new StringHttpMessageConverter(Charset.forName("UTF-8")));
        Map<String, String> vars = new HashMap<String, String>();
        vars.put("CIATOKEN", ciaToken);
        vars.put("URL", "/services/1.0/appext/AccountBook/getAccountBookById");
        vars.put("ACCID", String.valueOf(accId));
        vars.put("BOOKID", String.valueOf(bookId));
        String jsonStr = restTemplate.getForObject(url, String.class, vars);
        JsonNode rootNode = getJsonNode(jsonStr);
        
        //checkAndRetryIfFailed may throw an exception
        String callerInfo = "getChanjetCompanyAccountingPeriod() chanjetCompanyName=" + chanjetCompanyName;
        rootNode = checkAndRetryIfFailed(callerInfo, chanjetUser, rootNode, restTemplate, vars, url, 5);
        
        //We got a success rootNode.
        return rootNode;
    }
 
    //九、 报表查询相关接口
    //1. 查询资产负债表接口
    //   https://ee.chanjet.com/api/v1/ee/appVm
    //   接口参数
    //      ciaToken
    //      url=/services/1.0/appext/BalanceSheet/query
    //      ACCOUNTBOOK=账簿ID
    //      queryCond=查询期间，如{"period": "201603"}
    //   bookId 100001 belongs to a customer whose account is managed by the chanjet user with ciaToken. Otherwise, it failed.
    //   This api is the same as getChanjetCompanyAssetItemList, not used for now.
    
    public List<ChanjetClientCompanyAssetItem> getChanjetCompanyAssetItemListByApi_9_1(String chanjetCompanyName, ChanjetUser chanjetUser, String period,  long bookId) throws Exception {
        String ciaToken = chanjetUser.getChanjetCiaToken();
        log.info("getChanjetCompanyAssetReport() ciaToken=" + ciaToken +
                ", reportType=debt" +
                ", period=" + period +
                ", bookId=" + bookId);
        String url = "https://ee.chanjet.com/api/v1/ee/appVm?ciaToken={CIATOKEN}&url={QUERYURL}&ACCOUNTBOOK={BOOKID}&queryCond={QUERYCOND}";
        RestTemplate restTemplate = new RestTemplate();
        restTemplate.getMessageConverters().add(0, new StringHttpMessageConverter(Charset.forName("UTF-8")));
        Map<String, String> vars = new HashMap<String, String>();
        vars.put("CIATOKEN", ciaToken);
        vars.put("QUERYURL", "/services/1.0/appext/BalanceSheet/query");
        vars.put("BOOKID", String.valueOf(bookId));
        
        ObjectNode objectNode = objectMapper.createObjectNode();
        objectNode.put("period", period);
        String queryCond = objectMapper.writeValueAsString(objectNode);
        log.debug("getChanjetCompanyAssetReport() queryCond=" + queryCond);
        vars.put("QUERYCOND", queryCond);
        
        String jsonStr = restTemplate.getForObject(url, String.class, vars);
        JsonNode rootNode = getJsonNode(jsonStr);
        
        //checkAndRetryIfFailed may throw an exception
        String callerInfo = "getChanjetCompanyAssetItemListByApi_9_1() chanjetCompanyName=" + chanjetCompanyName;
        rootNode = checkAndRetryIfFailed(callerInfo, chanjetUser, rootNode, restTemplate, vars, url, 5);
        
        //We got a success rootNode.
        List<ChanjetClientCompanyAssetItem> chanjetClientCompanyAssetItemList = new ArrayList<ChanjetClientCompanyAssetItem>();
        JsonNode itemsNode = rootNode.path("data").path("items");
        for (Iterator<JsonNode> itemsItr = itemsNode.elements(); itemsItr.hasNext(); ) {
            JsonNode item = itemsItr.next();
            ChanjetClientCompanyAssetItem chanjetClientCompanyAssetItem = new ChanjetClientCompanyAssetItem(bookId, period);
            
            chanjetClientCompanyAssetItem.setSequenceId(item.path("id").asInt());
            //Copy the rest code from getChanjetCompanyAssetItemList if needed
        }
        
        return chanjetClientCompanyAssetItemList;
    }
    
    //4 查询财务报表接口  
    //Used in getChanjetCompanyAssetReport
    //https://ee.chanjet.com/api/v1/eeIfs/queryReport?ciaToken=6ed505cb-41ef-4058-abc1-fe717ea6a1ba&reportType=debt&period=201702&bookId=100001
    //bookId 100001 belongs to a customer whose account is managed by the chanjet user with ciaToken. Otherwise, it failed.
    public List<ChanjetClientCompanyAssetItem> getChanjetCompanyAssetItemList(String chanjetCompanyName, ChanjetUser chanjetUser, String period,  long bookId) throws Exception {
        
        String ciaToken = chanjetUser.getChanjetCiaToken();
        log.info("getChanjetCompanyAssetItemList() ciaToken=" + ciaToken +
                ", reportType=debt" +
                ", period=" + period +
                ", bookId=" + bookId);
        String url = "https://ee.chanjet.com/api/v1/eeIfs/queryReport?ciaToken={CIATOKEN}&reportType=debt&period={PERIOD}&bookId={BOOKID}";
        RestTemplate restTemplate = new RestTemplate();
        restTemplate.getMessageConverters().add(0, new StringHttpMessageConverter(Charset.forName("UTF-8")));
        Map<String, String> vars = new HashMap<String, String>();
        vars.put("CIATOKEN", ciaToken);
        vars.put("PERIOD", period);
        vars.put("BOOKID", String.valueOf(bookId));
        String jsonStr = restTemplate.getForObject(url, String.class, vars);
        JsonNode rootNode = getJsonNode(jsonStr);
        
        //checkAndRetryIfFailed may throw an exception
        String callerInfo = "getChanjetCompanyAssetItemList() chanjetCompanyName=" + chanjetCompanyName;
        rootNode = checkAndRetryIfFailed(callerInfo, chanjetUser, rootNode, restTemplate, vars, url, 5);
        
        //We got a success rootNode.
        List<ChanjetClientCompanyAssetItem> chanjetClientCompanyAssetItemList = new ArrayList<ChanjetClientCompanyAssetItem>();
        JsonNode itemsNode = rootNode.path("data").path("items");
        for (Iterator<JsonNode> itemsItr = itemsNode.elements(); itemsItr.hasNext(); ) {
            JsonNode item = itemsItr.next();
            ChanjetClientCompanyAssetItem chanjetClientCompanyAssetItem = new ChanjetClientCompanyAssetItem(bookId, period);
            
            chanjetClientCompanyAssetItem.setSequenceId(item.path("id").asInt());
            //if the field is empty, asInt() will return 0. No exception is thrown.
            chanjetClientCompanyAssetItem.setFold(item.path("fold").asInt());
            chanjetClientCompanyAssetItem.setFoldFlag(item.path("foldFlag").asInt());
            chanjetClientCompanyAssetItem.setAssetId(item.path("assetId").asLong());
            chanjetClientCompanyAssetItem.setAsset(item.path("asset").asText());
            chanjetClientCompanyAssetItem.setAssetIndentClass(getReportItemIndent(assetIndentMap, item.path("assetRow").asInt()));
            chanjetClientCompanyAssetItem.setAssetRow(item.path("assetRow").asInt());
            chanjetClientCompanyAssetItem.setAssetEndBalance(item.path("assetEndBalance").asDouble());
            chanjetClientCompanyAssetItem.setAssetYearInitBalance(item.path("assetYearInitBalance").asDouble());
            chanjetClientCompanyAssetItem.setEquityId(item.path("equityId").asInt());
            chanjetClientCompanyAssetItem.setEquity(item.path("equity").asText());
            chanjetClientCompanyAssetItem.setEquityIndentClass(getReportItemIndent(assetIndentMap, item.path("equityRow").asInt()));
            chanjetClientCompanyAssetItem.setEquityRow(item.path("equityRow").asInt());
            chanjetClientCompanyAssetItem.setEquityEndBalance(item.path("equityEndBalance").asDouble());
            chanjetClientCompanyAssetItem.setEquityYearInitBalance(item.path("equityYearInitBalance").asDouble());
            chanjetClientCompanyAssetItem.setFomularDetailOne(item.path("fomularDetailOne").asText());
            chanjetClientCompanyAssetItem.setFomularDetailTwo(item.path("fomularDetailTwo").asText());
            
            chanjetClientCompanyAssetItemList.add(chanjetClientCompanyAssetItem);
        }
        
        return chanjetClientCompanyAssetItemList;
    }
    
    //九、 报表查询相关接口
    //4 查询财务报表接口  
    //Used in getChanjetCompanyAssetReport
    //查询财务报表接, Success https://ee.chanjet.com/api/v1/eeIfs/queryReport?ciaToken=6ed505cb-41ef-4058-abc1-fe717ea6a1ba&reportType=cash&period=201702&bookId=100001
    //bookId 100001 belongs to a customer whose account is managed by the chanjet user with ciaToken. Otherwise, it failed.
    
    public List<ChanjetClientCompanyCashItem> getChanjetCompanyCashItemList(String chanjetCompanyName, ChanjetUser chanjetUser, String period,  long bookId) throws Exception {
        String ciaToken = chanjetUser.getChanjetCiaToken();
        log.info("getChanjetCompanyCashItemList() ciaToken=" + ciaToken +
                ", reportType=cash" +
                ", period=" + period +
                ", bookId=" + bookId);
        String url = "https://ee.chanjet.com/api/v1/eeIfs/queryReport?ciaToken={CIATOKEN}&reportType=cash&period={PERIOD}&bookId={BOOKID}";
        RestTemplate restTemplate = new RestTemplate();
        restTemplate.getMessageConverters().add(0, new StringHttpMessageConverter(Charset.forName("UTF-8")));
        Map<String, String> vars = new HashMap<String, String>();
        vars.put("CIATOKEN", ciaToken);
        vars.put("PERIOD", period);
        vars.put("BOOKID", String.valueOf(bookId));
        String jsonStr = restTemplate.getForObject(url, String.class, vars);
        JsonNode rootNode = getJsonNode(jsonStr);
        
        //checkAndRetryIfFailed may throw an exception
        String callerInfo = "getChanjetCompanyCashItemList() chanjetCompanyName=" + chanjetCompanyName;
        rootNode = checkAndRetryIfFailed(callerInfo, chanjetUser, rootNode, restTemplate, vars, url, 2);
        
        //We got a success rootNode.
        List<ChanjetClientCompanyCashItem> chanjetClientCompanyCashList = new ArrayList<ChanjetClientCompanyCashItem>();
        JsonNode itemsNode = rootNode.path("data");
        for (Iterator<JsonNode> itemsItr = itemsNode.elements(); itemsItr.hasNext(); ) {
            JsonNode item = itemsItr.next();
            ChanjetClientCompanyCashItem chanjetClientCompanyCashItem = new ChanjetClientCompanyCashItem(bookId, period);
            
            chanjetClientCompanyCashItem.setSequenceId(item.path("id").asInt());
            //if the field is empty, asInt() will return 0. No exception is thrown.
            chanjetClientCompanyCashItem.setFold(item.path("fold").asInt());
            chanjetClientCompanyCashItem.setFoldFlag(item.path("foldFlag").asInt());
            chanjetClientCompanyCashItem.setCode(item.path("code").asText());
            chanjetClientCompanyCashItem.setItem(item.path("item").asText());
            chanjetClientCompanyCashItem.setCashIndentClass(getReportItemIndent(cashIndentMap, item.path("row").asInt()));
            chanjetClientCompanyCashItem.setRow(item.path("row").asInt());
            chanjetClientCompanyCashItem.setYearCashBD(item.path("yearCashBD").asDouble());
            chanjetClientCompanyCashItem.setMonthCashBD(item.path("monthCashBD").asDouble());
            chanjetClientCompanyCashItem.setLyearCashBD(item.path("lyearCashBD").asDouble());
            chanjetClientCompanyCashItem.setFormula(item.path("formula").asText());
            
            chanjetClientCompanyCashList.add(chanjetClientCompanyCashItem);
        }
        
        return chanjetClientCompanyCashList;
    }
    
    //九、 报表查询相关接口
    //4 查询财务报表接口  
    //Used in getChanjetCompanyIncomeReport
    //查询财务报表接, Success https://ee.chanjet.com/api/v1/eeIfs/queryReport?ciaToken=6ed505cb-41ef-4058-abc1-fe717ea6a1ba&reportType=cash&period=201702&bookId=100001
    //bookId 100001 belongs to a customer whose account is managed by the chanjet user with ciaToken. Otherwise, it failed.    
    public List<ChanjetClientCompanyIncomeItem>  getChanjetCompanyIncomeItemList(String chanjetCompanyName, ChanjetUser chanjetUser, String period,  long bookId) throws Exception {
        String ciaToken = chanjetUser.getChanjetCiaToken();
        log.info("getChanjetCompanyIncomeItemList() ciaToken=" + ciaToken +
                ", reportType=income" +
                ", period=" + period +
                ", bookId=" + bookId);
        String url = "https://ee.chanjet.com/api/v1/eeIfs/queryReport?ciaToken={CIATOKEN}&reportType=income&period={PERIOD}&bookId={BOOKID}";
        RestTemplate restTemplate = new RestTemplate();
        restTemplate.getMessageConverters().add(0, new StringHttpMessageConverter(Charset.forName("UTF-8")));
        Map<String, String> vars = new HashMap<String, String>();
        vars.put("CIATOKEN", ciaToken);
        vars.put("PERIOD", period);
        vars.put("BOOKID", String.valueOf(bookId));
        String jsonStr = restTemplate.getForObject(url, String.class, vars);
        JsonNode rootNode = getJsonNode(jsonStr);
        
        //checkAndRetryIfFailed may throw an exception
        String callerInfo = "getChanjetCompanyIncomeItemList() chanjetCompanyName=" + chanjetCompanyName;
        rootNode = checkAndRetryIfFailed(callerInfo, chanjetUser, rootNode, restTemplate, vars, url, 2);
        
        //We got a success rootNode.
        List<ChanjetClientCompanyIncomeItem> chanjetClientCompanyIncomeItemList = new ArrayList<ChanjetClientCompanyIncomeItem>();
        JsonNode itemsNode = rootNode.path("data");
        for (Iterator<JsonNode> itemsItr = itemsNode.elements(); itemsItr.hasNext(); ) {
            JsonNode item = itemsItr.next();
            ChanjetClientCompanyIncomeItem chanjetClientCompanyIncomeItem = new ChanjetClientCompanyIncomeItem(bookId, period);
            
            chanjetClientCompanyIncomeItem.setSequenceId(item.path("id").asInt());
            //if the field is empty, asInt() will return 0. No exception is thrown.
            chanjetClientCompanyIncomeItem.setFold(item.path("fold").asInt());
            chanjetClientCompanyIncomeItem.setFoldFlag(item.path("foldFlag").asInt());
            chanjetClientCompanyIncomeItem.setItemId(item.path("itemId").asLong());
            //trim() only remove ascii whitespace, it does not remove Chinese whitespace which is encoded in UTF8 as \u3000
            String itemStr = item.path("item").asText().replaceAll("\u3000", "");
            chanjetClientCompanyIncomeItem.setItem(itemStr);
            chanjetClientCompanyIncomeItem.setIncomeIndentClass(getReportItemIndent(incomeIndentMap, item.path("row").asInt()));
            chanjetClientCompanyIncomeItem.setRow(item.path("row").asInt());
            chanjetClientCompanyIncomeItem.setMonthCashBD(item.path("monthCashBD").asDouble());
            chanjetClientCompanyIncomeItem.setYearCashBD(item.path("yearCashBD").asDouble());
            chanjetClientCompanyIncomeItem.setLastYearMonthCashBD(item.path("lastYearMonthCashBD").asDouble());
            chanjetClientCompanyIncomeItem.setFomularDetail(item.path("fomularDetail").asText());
            chanjetClientCompanyIncomeItemList.add(chanjetClientCompanyIncomeItem);
        }
        
        return chanjetClientCompanyIncomeItemList;
    }
    
    //5. 查询纳税统计表接口
    //查询纳税统计表接, Success GET https://ee.chanjet.com/api/v1/ee/appVm?ciaToken=6ed505cb-41ef-4058-abc1-fe717ea6a1ba&url=/services/1.0/appext/TaxStatistics/query&ACCOUNTBOOK=100028&queryCond={"period": "201702"}
    //bookId 100028 belongs to a customer whose account is managed by the chanjet user with ciaToken
    //Not used for now.
}
