package com.cju.chanjet;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.persistence.UniqueConstraint;
import com.cju.gzhOps.GzhClientCompany;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

//@Embeddable and @Entity can not be applied to a class at the same time
//client company from chanjet. We do not store them locally.
@Entity
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id", scope = ChanjetClientCompany.class)
@Table(name = "ChanjetClientCompany", 
       uniqueConstraints = {@UniqueConstraint(columnNames = "acc_id", name = "ChanjetClientCompany_UNIQUE_AccId"),
                            @UniqueConstraint(columnNames = "name", name = "GzhClientCompany_UNIQUE_Name")}
      )
public class ChanjetClientCompany {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    
    //client company name;
    private String name;
    //chanjet system 客户ID. 
    //Make it Long so we can set null. PostgreSQL allows multiple nulls even with unique constrains.
    @Column(name = "acc_id")
    private Long accId;
    @Column(name = "book_id")
    private long bookId;
    //The chanjet userId who is doing the accounting for this client company
    @Column(name = "user_id")
    private long userId;
    //制单人, the name of userId.
    private String maker;
    //userNoteName seems the same as maker
    private String userNoteName;
    //最大结账月份
    private String closePeriod;
    //最大记账月份
    private String currentPeriod;
    //period to start from when syncing to chanjet currentPeriod
    private String syncStartPeriod;
    @Transient
    private YesNoType importedToGzh = YesNoType.NO;
    
    //营业执照号
    private String certi;
    @Column(name = "cer_due")
    private String cerDue;
    private String address;
    @Column(name = "legal_person")
    private String legalPerson;
    @Column(name = "legal_no")
    private String legalNo;
    private String contact;
    @Column(name = "contact_note")
    private String contactNote;
    private String manager;
    @Column(name = "manager_note")
    private String managerNote;
    
    //税管所
    @Column(name = "tax_dep")
    private String taxDep;
    @Column(name = "tax_cp_no")
    private String taxCpNo;
    @Column(name = "tax_no")
    private String taxNo;
    //税控机
    @Column(name = "tax_ctrl")
    private String taxCtrl;
    //所得税申报
    @Column(name = "tax_type")
    private String taxType;
    //增值税类型, tax17 小规模纳税人, tax3 一般纳税人
    @Column(name = "va_tax")
    private String vaTax;
    //会计制度
    @Column(name = "subject_sys")
    private String subjectSys;
    @Column(name = "tax_state")
    private String taxState;
    
    //建账年月
    @Column(name = "start_period")
    private String startPeriod;
    //记账状态
    @Column(name = "bk_state")
    private String bkState;
    //记账状态上报月份
    @Column(name = "bk_month")
    private String bkMonth;
    @Column(name = "create_time")
    private String createTime;
    @Column(name = "update_time")
    private String updateTime;

    @JsonIgnore
    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name="gzhClientCompanyId", foreignKey = @ForeignKey(name = "ChanjetClientCompany_FK_gzhClientCompanyId"))
    private GzhClientCompany gzhClientCompany;
    
    public ChanjetClientCompany() {}

    public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }
    
    public void setAccId(Long acc_id) {
        this.accId = acc_id;
    }
    public Long getAccId() {
        return accId;
    }
    
    public void setUserId(long userId) {
        this.userId = userId;
    }
    public long getUserId() {
        return userId;
    }
    
    public void setMaker(String maker) {
        this.maker = maker;
    }
    public String getMaker() {
        return maker;
    }
    
    public void setUserNoteName(String userNoteName) {
        this.userNoteName = userNoteName;
    }
    public String getUserNoteName() {
        return userNoteName;
    }
    
    public void setClosePeriod(String closePeriod) {
        this.closePeriod = closePeriod;
    }
    public String getClosePeriod() {
        return closePeriod;
    }
    
    public void setCurrentPeriod(String currentPeriod) {
        this.currentPeriod = currentPeriod;
    }
    public String getCurrentPeriod() {
        return currentPeriod;
    }
    
    public void setSyncStartPeriod(String syncStartPeriod) {
        this.syncStartPeriod = syncStartPeriod;
    }
    public String getSyncStartPeriod() {
        return syncStartPeriod;
    }
    
    public void setBookId(long bookId) {
        this.bookId = bookId;
    }
    public long getBookId() {
        return bookId;
    }
    
    public void setName(String name) {
        this.name = name;
    }
    public String getName() {
        return name;
    }
    
    public void setImportedToGzh(YesNoType importedToGzh) {
        this.importedToGzh = importedToGzh;
    }
    public YesNoType getImportedToGzh() {
        return importedToGzh;
    }
    
    //Fields not used for now, but are in DB
    
    public void setCerti(String certi) {
        this.certi = certi;
    }
    public String getCerti() {
        return certi;
    }
    
    public void setCerDue(String cerDue) {
        this.cerDue = cerDue;
    }
    public String getCerDue() {
        return cerDue;
    }
    
    public void setAddress(String address) {
        this.address = address;
    }
    public String getAddress() {
        return address;
    }
    
    public void setLegalPerson(String legalPerson) {
        this.legalPerson = legalPerson;
    }
    public String getLegalPerson() {
        return legalPerson;
    }
    
    public void setLegalNo(String legalNo) {
        this.legalNo = legalNo;
    }
    public String getLegalNo() {
        return legalNo;
    }
    
    public void setContact(String contact) {
        this.contact = contact;
    }
    public String getContact() {
        return contact;
    }
    
    public void setContactNote(String contactNote) {
        this.contactNote = contactNote;
    }
    public String getContactNote() {
        return contactNote;
    }
    
    public void setManager(String manager) {
        this.manager = manager;
    }
    public String getManager() {
        return manager;
    }
    
    public void setManagerNote(String managerNote) {
        this.managerNote = managerNote;
    }
    public String getManagerNote() {
        return managerNote;
    }
    
    public void setTaxDep(String taxDep) {
        this.taxDep = taxDep;
    }
    public String getTaxDep() {
        return taxDep;
    }
    
    public void setTaxCpNo(String taxCpNo) {
        this.taxCpNo = taxCpNo;
    }
    public String getTaxCpNo() {
        return taxCpNo;
    }
    
    public void setTaxNo(String taxNo) {
        this.taxNo = taxNo;
    }
    public String getTaxNo() {
        return taxNo;
    }
    
    public void setTaxCtrl(String taxCtrl) {
        this.taxCtrl = taxCtrl;
    }
    public String getTaxCtrl() {
        return taxCtrl;
    }
    
    public void setTaxType(String taxType) {
        this.taxType = taxType;
    }
    public String getTaxType() {
        return taxType;
    }
    
    public void setVaTax(String vaTax) {
        this.vaTax = vaTax;
    }
    public String getVaTax() {
        return vaTax;
    }
    
    public void setSubjectSys(String subjectSys) {
        this.subjectSys = subjectSys;
    }
    public String getSubjectSys() {
        return subjectSys;
    }
    
    public void setStartPeriod(String startPeriod) {
        this.startPeriod = startPeriod;
    }
    public String getStartPeriod() {
        return startPeriod;
    }
    
    public void setBkState(String bkState) {
        this.bkState = bkState;
    }
    public String getBkState() {
        return bkState;
    }
    
    public void setBkMonth(String bkMonth) {
        this.bkMonth = bkMonth;
    }
    public String bkMonth() {
        return bkMonth;
    }
    
    public void setTaxState(String taxState) {
        this.taxState = taxState;
    }
    public String getTaxState() {
        return taxState;
    }
    
    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }
    public String getCreateTime() {
        return createTime;
    }
    
    public void setUpdateTime(String updateTime) {
        this.updateTime = updateTime;
    }
    public String getUpdateTime() {
        return updateTime;
    }

    public void setGzhClientCompany(GzhClientCompany gzhClientCompany) {
        this.gzhClientCompany = gzhClientCompany;
    }
    public GzhClientCompany getGzhClientCompany() {
        return gzhClientCompany;
    }
    
    @Override
    public String toString() {
        return String.format(
                "ChanjetClientCompany[id=%d, name='%s']",
                accId, name);
    }
}