package com.cju.chanjet;

import com.fasterxml.jackson.annotation.JsonProperty;

public enum YesNoType {
    @JsonProperty("是")YES("Yes"), @JsonProperty("否") NO("No");
    private String value;
    private YesNoType(String value) {
        this.value = value;
    }
    
    public static YesNoType fromString(String str) {
        for (YesNoType type: YesNoType.values()) {
            if (type.value.equals(str)) {
                return type;
            }
        }
        //Default return
        return YES;
    }
    
    @Override
    public String toString() {
        switch (value) {
            case "Yes": 
                return "是";
            case "No": 
                return "否";
            default: 
                return "否";
        }
    }
}
