package com.cju.chanjet;

import java.util.List;

import org.springframework.data.querydsl.QueryDslPredicateExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.cju.user.User;
import com.querydsl.core.types.Predicate;

//All of your application components (@Component, @Service, @Repository, @Controller etc.) 
//will be automatically registered as Spring Beans.
@Repository
public interface ChanjetUserRepository extends PagingAndSortingRepository<ChanjetUser, Long>, QueryDslPredicateExecutor<ChanjetUser> {
    List<ChanjetUser> findAll(Predicate predicate);
    ChanjetUser findOneByChanjetUserId(long accountantUserId);
}