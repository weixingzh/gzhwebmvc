package com.cju.chanjet;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;

//@Embeddable and @Entity can not be applied to a class at the same time
//client company asset from chanjet. Embedded in GzhClientCompanyAsset.
//uniqueConstraints = { @UniqueConstraint(columnNames={"FIELD_A", "FIELD_B"}) }
@Embeddable
public class ChanjetClientCompanyCashItem {
    //bookId, period,assetType identify an asset.
    @Column(name = "book_id")
    private long bookId;
    private String period;
    
    //The following fields are taken from chanjet json reply. 
    //sequenceId is the rowIndex field of an cash item from chanjet. It identifies the order of the item on the report.
    private int sequenceId;
    //fold field of an cash item from chanjet. It identifies the sequenceId under which this item is in.
    private int fold;
    //foldFlag field of an income item from chanjet. 
    //If 1, it means this item contains the following items whose fold field is set to the sequenceId of this item.
    private int foldFlag;
    //code is not used.
    private String code;
    //项目
    private String item;
    //cashIndentClass is calculated based on item and used as the CSS class for indent
    private String cashIndentClass;
    //行次
    private int row;
    //本年累计金额
    private double yearCashBD; 
    //本月金额
    private double monthCashBD;
    //上年同期累计金额
    private double lyearCashBD;
    //formula
    private String formula;
    
    protected ChanjetClientCompanyCashItem() {}
    public ChanjetClientCompanyCashItem(long bookId, String period) {
        this.bookId = bookId;
        this.period = period;
    }

    public void setBookId(long bookId) {
        this.bookId = bookId;
    }
    public long getBookId() {
        return bookId;
    }
    
    public void setPeriod(String period) {
        this.period = period;
    }
    public String period() {
        return period;
    }
    
    public void setSequenceId(int sequenceId) {
        this.sequenceId = sequenceId;
    }
    public int getSequenceId() {
        return sequenceId;
    }
    
    public void setFold(int fold) {
        this.fold = fold;
    }
    public int getFold() {
        return fold;
    }
    
    public void setFoldFlag(int foldFlag) {
        this.foldFlag = foldFlag;
    }
    public int getFoldFlag() {
        return foldFlag;
    }
    
    public void setCashIndentClass(String cashIndentClass) {
        this.cashIndentClass = cashIndentClass;
    }
    public String getCashIndentClass() {
        return cashIndentClass;
    }

    public void setCode(String code) {
        this.code = code;
    }
    public String getCode() {
        return code;
    }
    
    public void setItem(String item) {
        this.item = item;
    }
    public String getItem() {
        return item;
    }
    
    public void setRow(int row) {
        this.row = row;
    }
    public int getRow() {
        return row;
    }
    
    public void setYearCashBD(double yearCashBD) {
        this.yearCashBD = yearCashBD;
    }
    public double getYearCashBD() {
        return yearCashBD;
    }
    
    public void setMonthCashBD(double monthCashBD) {
        this.monthCashBD = monthCashBD;
    }
    public double getMonthCashBD() {
        return monthCashBD;
    }
    
    public void setLyearCashBD(double lyearCashBD) {
        this.lyearCashBD = lyearCashBD;
    }
    public double getLyearCashBD() {
        return lyearCashBD;
    }
    
    public void setFormula(String formula) {
        this.formula = formula;
    }
    public String getFormula() {
        return formula;
    }
    
    
    @Override
    public String toString() {
        return String.format(
                "ChanjetClientCompanyCashItem[bookId='%s', period='%s', item='%s']",
                bookId, period, item);
    }
}