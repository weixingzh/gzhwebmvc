package com.cju.chanjet;

import java.time.LocalDate;
import java.util.Locale;
import javax.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import com.cju.gzhOps.GzhClientCompany;
import com.cju.gzhOps.GzhClientCompanyRepository;
import com.cju.gzhOps.GzhCompany;
import com.cju.user.User;
import com.cju.user.UserRepository;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;

//OpsController is for providing OPS to accounting Company.
//We need to associate a user and a client company to the user's openId for this gzh. Since the openId is only known 
//when user access the website through his weixin client, the user-creation and client-company-creation functionality
//are provided via weixin gzh UI, not here.

@Controller
public class ChanjetController {
	private Logger log = LoggerFactory.getLogger(ChanjetController.class);
	
    @Autowired
    private UserRepository userRepository;
    
    @Autowired
    private ChanjetUserRepository chanjetUserRepository;
    
    @Autowired 
    private GzhClientCompanyRepository gzhClientCompanyRepository; 
    
    @Autowired 
    private ChanjetClientCompanyRepository chanjetClientCompanyRepository; 
    
    @Autowired
    private ChanjetService chanjetService;
    
    @Autowired
    private ObjectMapper objectMapper;
    
    @Autowired
    private MessageSource messageSource;
    
    @PreAuthorize("isAuthenticated()")
    @GetMapping(value={"/chanjet/chanjetUser/details"})
    public String getChanjetUserDetails(@AuthenticationPrincipal User user, Model model) {
         log.info("getChanjetAccountDetails() called");
         ChanjetUser chanjetUser = user.getChanjetUser();
         if (chanjetUser == null) {
              chanjetUser = new ChanjetUser();
         }
         model.addAttribute("chanjetUser", chanjetUser);
         return "chanjet/chanjetUserDetails";
    }
    
    @PreAuthorize("isAuthenticated()")
    @GetMapping(value={"/chanjet/clientCompany/list"})
    public String chanjetClientCompanyList(@AuthenticationPrincipal User user,Pageable pageable, Model model) {
         log.info("chanjetClientCompanyList() called");
         ChanjetUser chanjetUser = user.getChanjetUser();
         if (chanjetUser == null || chanjetUser.getChanjetUserName() == null || chanjetUser.getChanjetPassword() == null) {
             String message = messageSource.getMessage("gzh.user.chanjet.not.bound", null, Locale.SIMPLIFIED_CHINESE);
             model.addAttribute("message", message); 
             return "chanjet/chanjetMessage";
         }
         Page<ChanjetClientCompany> clientCompanyPage;
         try {
             clientCompanyPage = chanjetService.getClientCompanyPage(user.getChanjetUser(), pageable);
         } catch (Exception e) {
             String message = e.getLocalizedMessage();
             model.addAttribute("message", message); 
             return "chanjet/chanjetMessage";
         }
         
         log.info("chanjetClientCompanyList clientCompanyPage=" + clientCompanyPage);
         model.addAttribute("clientCompanyPage", clientCompanyPage);
         return "chanjet/chanjetClientCompanyList";
    }
    
    @PreAuthorize("isAuthenticated()")
    @GetMapping(value={"/chanjet/clientCompany/details/{accId}"})
    public String chanjetClientCompanyDetails(@AuthenticationPrincipal User user, @PathVariable("accId") long accId, Model model) {
         log.info("chanjetClientCompanyList() called");
         ChanjetUser chanjetUser = user.getChanjetUser();
         if (chanjetUser == null || chanjetUser.getChanjetUserName() == null || chanjetUser.getChanjetPassword() == null) {
             String message = messageSource.getMessage("gzh.user.chanjet.not.bound", null, Locale.SIMPLIFIED_CHINESE);
             model.addAttribute("message", message); 
             return "chanjet/chanjetMessage";
         }
         ChanjetClientCompany clientCompany;
         try {
             clientCompany = chanjetService.getClientCompany(chanjetUser, accId);
         } catch (Exception e) {
             String message = e.getLocalizedMessage();
             model.addAttribute("message", message); 
             return "chanjet/chanjetMessage";
         }
         
         log.info("chanjetClientCompanyDetails clientCompany=" + clientCompany);
         model.addAttribute("clientCompany", clientCompany);
         return "chanjet/chanjetClientCompanyDetails";
    }
    
    //REST API
    
    @PreAuthorize("isAuthenticated()")
    @PostMapping("/gzhOps/api/v1/chanjet/bind")
    public @ResponseBody String postChanjetBindApi(@AuthenticationPrincipal User user, HttpServletResponse response) {
        ChanjetUser chanjetUser = user.getChanjetUser();
        if (chanjetUser == null || chanjetUser.getChanjetUserName() == null || chanjetUser.getChanjetPassword() == null) {
            String message = messageSource.getMessage("gzh.user.chanjet.not.bound", null, Locale.SIMPLIFIED_CHINESE);
            response.setStatus(HttpStatus.BAD_REQUEST.value());
            return message;
        }

        ChanjetUser chanjetUserWithCiaToken;
        try {
            chanjetUserWithCiaToken = chanjetService.getCiaToken(chanjetUser.getChanjetUserName(), chanjetUser.getChanjetPassword());
        } catch (Exception e) {
            response.setStatus(HttpStatus.BAD_REQUEST.value());
            return e.getLocalizedMessage();
        }
        
        //check if a chanjetUser with the chanjetUserId already exists in the db.
        //If another user binds to the same chanjetUser before, the chanjetUser will now bind to the this user.
        //Because of the unique constrain on chanjetUserId, there is at most one chanjetUser with a non-null chanjetUserId.
        //There could be multiple chanjetUser with null chanjetUserId.
        //chanjetUser is of type Long so that we can set null value on it.
        long chanjetUserId = chanjetUserWithCiaToken.getChanjetUserId();
        ChanjetUser alreadyBoundChanjetUser = chanjetUserRepository.findOneByChanjetUserId(chanjetUserId);
        if (alreadyBoundChanjetUser != null) {
            if (alreadyBoundChanjetUser.getId() != chanjetUser.getId()) {
                //The existing chanjetUser is bound to another different user. 
                //Need to break the existing bind by the chanjetUserId of the existing chanjetUser to null
                //PostgreSQL allows multiple null values on a column with unique constrain.
                alreadyBoundChanjetUser.setChanjetUserId(null);
                alreadyBoundChanjetUser.setChanjetCiaToken(null);
                chanjetUserRepository.save(alreadyBoundChanjetUser);
            }
        }
        
        //Now it is safe to update the chanjetUser of user with the chanjetUserId. 
        chanjetUser.setChanjetUserId(chanjetUserWithCiaToken.getChanjetUserId());
        chanjetUser.setChanjetCiaToken(chanjetUserWithCiaToken.getChanjetCiaToken());
        chanjetUser.setChanjetCiaTokenGetLastTime(chanjetUserWithCiaToken.getChanjetCiaTokenGetLastTime());
        chanjetUserRepository.save(chanjetUser);
        
        //update the chanjetUser in the cached user obj.
        user.setChanjetUser(chanjetUser);
        return  "Success";
    }
    
    //save the chanjet username and password. Do not do binding.
    @PreAuthorize("isAuthenticated()")
    @PostMapping("/gzhOps/api/v1/chanjet/user/save")
    public @ResponseBody String postChanjetUserNameAndPasswordSaveApi(@AuthenticationPrincipal User user, @RequestBody JsonNode json) {
        log.info("postChanjetUserSaveApi json=" + json);
        
        String chanjetUserName = json.path("chanjetUserName").asText();
        String chanjetPassword = json.path("chanjetPassword").asText();
        
        ChanjetUser chanjetUser = user.getChanjetUser();
        if (chanjetUser == null) {
            chanjetUser = new ChanjetUser();
        }
        
        chanjetUser.setUser(user);
        chanjetUser.setChanjetUserName(chanjetUserName);
        chanjetUser.setChanjetPassword(chanjetPassword);
        chanjetUserRepository.save(chanjetUser);
        
        user.setChanjetUser(chanjetUser);
        userRepository.save(user);
        
        return  "Success";
    }
    
    //Save the passed in chanjetClientCompany which is retrieved from chanjet directly to DB
    private ChanjetClientCompany persistChanjetClientCompany(User user, ChanjetClientCompany chanjetClientCompany) {
        long accId = chanjetClientCompany.getAccId();
        
        ChanjetClientCompany currChanjetClientCompany = chanjetClientCompanyRepository.findOneByAccId(accId);
        if (currChanjetClientCompany == null) {
            //The chanjetClientCompany is not in database.
            //Do not initialize the syncStartPeriod to the chanjet start period. Otherwise, we will sync all the reports from start period.
            //chanjetClientCompany.setSyncStartPeriod(chanjetClientCompany.getStartPeriod());
            chanjetClientCompany.setImportedToGzh(YesNoType.YES);
            //Save will update the chanjetClientCompany with allocated id.
            log.info("persistChanjetClientCompany chanjetClientCompany.id before save=" + chanjetClientCompany.getId());
            chanjetClientCompanyRepository.save(chanjetClientCompany);
            log.info("persistChanjetClientCompany chanjetClientCompany.id after save=" + chanjetClientCompany.getId());
            
            GzhClientCompany newGzhClientCompany = new GzhClientCompany();
            GzhCompany gzhCompany = user.getAdminedGzhCompany();
            newGzhClientCompany.setGzhCompany(gzhCompany);
            newGzhClientCompany.setChanjetClientCompany(chanjetClientCompany);
            newGzhClientCompany.setImportedByUser(user);
            newGzhClientCompany.setFirstImportDate(LocalDate.now());
            //Save will update the newGzhClientCompany with a non-zero id
            gzhClientCompanyRepository.save(newGzhClientCompany);
            
            chanjetClientCompany.setGzhClientCompany(newGzhClientCompany);
            //update the one-to-one reference from chanjetClientCompany to gzhClientCompany
            chanjetClientCompanyRepository.save(chanjetClientCompany);
            
            return chanjetClientCompany;
        }
        else {
            //The chanjetClientCompany is already in database.
            //Override the fields with the new values from chanjet.
            //Since there are so many fields in chanjetClientCompany, we are going to save the passed-in chanjetClientCompany
            
            GzhClientCompany currGzhClientCompany = currChanjetClientCompany.getGzhClientCompany();
            
            //The passed-in chanjetClientCompany does not have the following fields set.
            chanjetClientCompany.setId(currChanjetClientCompany.getId());
            chanjetClientCompany.setSyncStartPeriod(currChanjetClientCompany.getSyncStartPeriod());
            chanjetClientCompany.setCurrentPeriod(currChanjetClientCompany.getCurrentPeriod());
            chanjetClientCompany.setImportedToGzh(YesNoType.YES);
            chanjetClientCompany.setGzhClientCompany(currGzhClientCompany);
            chanjetClientCompanyRepository.save(chanjetClientCompany);
            
            //Update the gzhCompany of the currGzhClientCompany.
            //It may happen that the same Chanjet Client Company is already imported by an user belongs to a different GzhCompany.
            if (currGzhClientCompany.getGzhCompany().getId() == user.getAdminedGzhCompany().getId()) {
                //The Chanjet Client Company is already imported to the same GzhCompany.
                //This should be the normal case.
                
                if (currGzhClientCompany.getImportedByUser().getId() != user.getId()) {
                    //The Chanjet Client Company is already imported by a different user.
                    currGzhClientCompany.setImportedByUser(user);
                    gzhClientCompanyRepository.save(currGzhClientCompany);
                }
                else {
                    //The Chanjet Client Company is already imported by the same user.
                    //Nothing needs to do here. Save a database trip.
                }
            }
            else {
                //The Chanjet Client Company is already imported to a different GzhCompany.
                //This case should rarely happen. 
                //One legal scenario is when a company starts using a different GzhCompany and imports all the chanjet client company to the new GzhCompany.
                //Another not-so-legal case may happen when an accountant joins another company and use the old chanjet account to 
                //retrieve chanjet client company from previous company. But the accountant should not have benefits of doing this.
                
                //No matter how it happens, we needs to removed the associated gzhUser from the gzhClientCompany and link the gzhClientCompany to the new GzhCompany.
                currGzhClientCompany.setGzhUserList(null);
                currGzhClientCompany.setGzhCompany(user.getAdminedGzhCompany());
                currGzhClientCompany.setImportedByUser(user);
                gzhClientCompanyRepository.save(currGzhClientCompany);
            }
          
            //chanjetClientCompanyRepository.save(chanjetClientCompany) above will update the chanjetClientCompany.
            return chanjetClientCompany;
        }
    }
    
    //import one chanjetClientCompany from the chanjetClientCompanyDetails page.
    //Do not need to do chanjetService.getClientCompany(chanjetUser, accId) again.
    //chanjetClientCompanyFromRequest contains all the data retrieved from chanjet directly.
    //Just need to save the chanjetClientCompanyFromRequest to DB
    @PreAuthorize("isAuthenticated()")
    @PostMapping("/gzhOps/api/v1/chanjetClientCompany/save")
    public @ResponseBody String postChanjetClientCompanyImportFromDetailsPageApi(@AuthenticationPrincipal User user, 
        @RequestBody ChanjetClientCompany chanjetClientCompanyFromRequest, HttpServletResponse response) {
        log.info("postChanjetClientCompanySaveApi() called");
        GzhCompany gzhCompany = user.getAdminedGzhCompany();
        if (gzhCompany == null) {
            response.setStatus(HttpStatus.BAD_REQUEST.value());
            String message = messageSource.getMessage("gzh.gzhcompany.admined.not.bound", null, Locale.SIMPLIFIED_CHINESE);
            return message;
        }
        
        persistChanjetClientCompany(user, chanjetClientCompanyFromRequest);
        return ("Success");
    }
    
    //import one chanjetClientCompany from chanjet company list page
    //Need to chanjetService.getClientCompany(chanjetUser, accId) since the chanjetClientCompany from chanjet company list does not contain all the information.
    @PreAuthorize("isAuthenticated()")
    @PostMapping("/gzhOps/api/v1/chanjetClientCompany/import")
    public @ResponseBody String postChanjettClientCompanyImportFromListPageApi(@AuthenticationPrincipal User user, 
        @RequestBody JsonNode json, HttpServletResponse response) {
        log.info("postChanjettClientCompanyImportApi() called");
        GzhCompany gzhCompany = user.getAdminedGzhCompany();
        if (gzhCompany == null) {
            response.setStatus(HttpStatus.BAD_REQUEST.value());
            String message = messageSource.getMessage("gzh.gzhcompany.admined.not.bound", null, Locale.SIMPLIFIED_CHINESE);
            return message;
        }
        
        ChanjetUser chanjetUser = user.getChanjetUser();
        long accId = json.path("accId").asLong();
        String chanjetClientComanyName = json.path("name").asText();
        ChanjetClientCompany chanjetClientCompany;
        
        ObjectNode objectNode = objectMapper.createObjectNode();
        objectNode.put("name", chanjetClientComanyName);
        objectNode.put("accId", accId);
        
        try {
            chanjetClientCompany = chanjetService.getClientCompany(chanjetUser, accId);
            persistChanjetClientCompany(user, chanjetClientCompany);
        } catch (Exception e) {
            response.setStatus(HttpStatus.BAD_REQUEST.value());
            objectNode.put("errMsg", e.getLocalizedMessage());
        }
        
        try {
            String objectNodeJson = objectMapper.writeValueAsString(objectNode);
            return objectNodeJson;
        } catch (JsonProcessingException e) {
            response.setStatus(HttpStatus.BAD_REQUEST.value());
            return e.getLocalizedMessage();
        }
    }
}
