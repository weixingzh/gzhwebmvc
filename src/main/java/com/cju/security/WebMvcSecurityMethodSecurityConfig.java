package com.cju.security;

import org.springframework.context.annotation.Configuration;
import org.springframework.security.access.expression.method.DefaultMethodSecurityExpressionHandler;
import org.springframework.security.access.expression.method.MethodSecurityExpressionHandler;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.method.configuration.GlobalMethodSecurityConfiguration;


//This configures MethodSecurityExpressionHandler, not WebSecurityExpressionRoot
//#authorization.expression in Thymeleaf template file requires the expression method to be defined in WebSecurityExpressionRoot
@Configuration
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class WebMvcSecurityMethodSecurityConfig extends GlobalMethodSecurityConfiguration {
    @Override
    protected MethodSecurityExpressionHandler createExpressionHandler() {
        WebMvcSecurityMethodSecurityExpressionHandler expressionHandler = 
                new WebMvcSecurityMethodSecurityExpressionHandler();
        return expressionHandler;
    }
}