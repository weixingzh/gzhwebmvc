package com.cju.security;

import org.aopalliance.intercept.MethodInvocation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.expression.method.DefaultMethodSecurityExpressionHandler;
import org.springframework.security.access.expression.method.MethodSecurityExpressionOperations;
import org.springframework.security.authentication.AuthenticationTrustResolver;
import org.springframework.security.authentication.AuthenticationTrustResolverImpl;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;

public class WebMvcSecurityMethodSecurityExpressionHandler extends DefaultMethodSecurityExpressionHandler {
  private AuthenticationTrustResolver trustResolver = 
          new AuthenticationTrustResolverImpl();

  @Override
  protected MethodSecurityExpressionOperations createSecurityExpressionRoot(Authentication authentication, MethodInvocation invocation) {
      WebMvcSecurityMethodSecurityExpressionRoot methodSecurityExpressionRoot = 
              new WebMvcSecurityMethodSecurityExpressionRoot(authentication);
      methodSecurityExpressionRoot.setTrustResolver(this.trustResolver);
      methodSecurityExpressionRoot.setRoleHierarchy(getRoleHierarchy());
      return methodSecurityExpressionRoot;
  }
}