package com.cju.security;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


//Even though this mechanism works in blocking malicious http probes, there are so many such probes that makes it impossible 
//to add all the malicious urls. The HashSet will be very big. 
//So we are going to remove the bean registration for this Filter in WebMvcSecurityConfig.java
public class UrlFilter implements Filter {  
    private static final Logger log = LoggerFactory.getLogger(UrlFilter.class);
    private static HashSet<String> blockingUrlSet = new HashSet<String>();  
  
    @Override  
    public void init(FilterConfig filterConfig) throws ServletException {  
        String[] gzList = {
            "/wwwroot.tar.gz", "/www.tar.gz", "/htdocs.tar.gz", "/backup.tar.gz", "/db.tar.gz", "/database.tar.gz",
            "/data.tar.gz", "/admin.tar.gz", "/mysql.tar.gz", "/config.tar.gz"};     

        blockingUrlSet.add("/hedwig.cgi");
        blockingUrlSet.add("/board.cgi");
        blockingUrlSet.add("/upgrade_handle.php");
        blockingUrlSet.add("/azenv.php");
        blockingUrlSet.add("/system.ini");
        blockingUrlSet.add("/shell");
        
        blockingUrlSet.addAll(Arrays.asList(gzList));
    }  
  
    @Override  
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {  
        String uri = ((HttpServletRequest)request).getRequestURI();
        if (blockingUrlSet.contains(uri)) {
            String xForwardedFor = ((HttpServletRequest)request).getHeader("X-Forwarded-For");
            log.warn("doFilter() blocking malicious url=" + uri + ", X-Forwarded-For=" + xForwardedFor);
            HttpServletResponse httpResponse = (HttpServletResponse) response;
            //httpResponse.setStatus will return the http response
            //httpResponse.sendError(HttpServletResponse.SC_BAD_REQUEST) will forward the request to /error and trigger the error handler controller.
            httpResponse.setStatus(HttpServletResponse.SC_BAD_REQUEST);
        }
        else {
            chain.doFilter(request,response);  
        }  
    }  
  
    @Override  
    public void destroy() {  
  
    }  
}  