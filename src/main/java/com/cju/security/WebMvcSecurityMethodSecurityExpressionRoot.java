package com.cju.security;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.access.expression.SecurityExpressionRoot;
import org.springframework.security.access.expression.method.MethodSecurityExpressionOperations;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import com.cju.BeanProvider;
import com.cju.billing.Bill;
import com.cju.billing.BillRepository;
import com.cju.billing.BillingStatusChange;
import com.cju.billing.BillingStatusChangeRepository;
import com.cju.billing.Transaction;
import com.cju.billing.TransactionRepository;
import com.cju.file.QrImage;
import com.cju.file.QrImageRepository;
import com.cju.gzhOps.GzhClientCompany;
import com.cju.gzhOps.GzhClientCompanyRepository;
import com.cju.gzhOps.GzhCompany;
import com.cju.gzhOps.GzhHtmlPage;
import com.cju.gzhOps.GzhHtmlPageRepository;
import com.cju.gzhOps.GzhHtmlPageType;
import com.cju.gzhOps.GzhTask;
import com.cju.gzhOps.GzhTaskRepository;
import com.cju.gzhOps.GzhTaskType;
import com.cju.gzhOps.GzhUser;
import com.cju.gzhOps.GzhUserRepository;
import com.cju.marketing.Referal;
import com.cju.marketing.ReferalCashOut;
import com.cju.marketing.ReferalTransaction;
import com.cju.order.Order;
import com.cju.order.OrderRepository;
import com.cju.user.User;

//This can not be a bean as for each authentication, a MethodSecurityExpressionRoot is required.
public class WebMvcSecurityMethodSecurityExpressionRoot extends SecurityExpressionRoot implements MethodSecurityExpressionOperations {
    private static final Logger log = LoggerFactory.getLogger(WebMvcSecurityMethodSecurityExpressionRoot.class);
    private Object filterObject;
    private Object returnObject;
    private Object target;

    public WebMvcSecurityMethodSecurityExpressionRoot(Authentication authentication) {
        super(authentication);
    }
    
    public boolean isSelf(long userId) {
        User user = (User) this.getAuthentication().getPrincipal();
        if (user.getId() == userId)
            return true;
        return false;
    }
    
    public boolean isWebAdmin() {
        User user = (User) this.getPrincipal();
        GrantedAuthority webAdmin = new SimpleGrantedAuthority("ROLE_WEBADMIN");
        if (user.getAuthorities().contains(webAdmin))
            return true;
        else
            return false;
    }
    

    public boolean isGzhOwner(long gzhCompanyId) {
        User user = (User) this.getAuthentication().getPrincipal();
        GzhCompany gzhCompany = user.getOwnedGzhCompany();
        if (gzhCompany == null) 
            return false;
        if (gzhCompany.getId() == gzhCompanyId)
            return true;
        else
            return false;
    }
    
    public boolean isGzhAdmin(long gzhCompanyId) {
        User user = (User) this.getAuthentication().getPrincipal();
        GzhCompany gzhCompany = user.getAdminedGzhCompany();
        if (gzhCompany == null) 
            return false;
        if (gzhCompany.getId() == gzhCompanyId)
            return true;
        else
            return false;
    }
    
    public boolean isGzhAdminForGzhUser(long gzhUserId) {
        User user = (User) this.getPrincipal();
        GzhCompany gzhCompany = user.getAdminedGzhCompany();
        if (gzhCompany == null) 
            return false;
        GzhUserRepository gzhUserRepository = BeanProvider.getGzhUserRepository();
        GzhUser gzhUser = gzhUserRepository.findOne(gzhUserId);
        if (gzhUser == null)
            return false;
        GzhCompany gzhCompanyByGzhUser = gzhUser.getGzhCompany();
        if (gzhCompanyByGzhUser == null)
            return false;
        if (gzhCompany.getId() == gzhCompanyByGzhUser.getId())
            return true;
        else
            return false;
    }
    
    public boolean isGzhTaskPublicTemplate(long gzhTaskId) {
        GzhTaskRepository gzhTaskRepository = BeanProvider.getGzhTaskRepository();
        GzhTask gzhTask = gzhTaskRepository.findOne(gzhTaskId);
        GzhTaskType gzhTaskType = gzhTask.getType();
        if (gzhTaskType == GzhTaskType.PUBLIC_TEMPLATE)
            return true;
        else
            return false;
    }
    
    public boolean isGzhAdminForGzhTask(long gzhTaskId) {
        User user = (User) this.getPrincipal();
        GzhCompany gzhCompany = user.getAdminedGzhCompany();
        if (gzhCompany == null) 
            return false;
        
        GzhTaskRepository gzhTaskRepository = BeanProvider.getGzhTaskRepository();
        GzhTask gzhTask = gzhTaskRepository.findOne(gzhTaskId);
        
        GzhTaskType gzhTaskType = gzhTask.getType();
        if (gzhTaskType == GzhTaskType.PUBLIC_TEMPLATE) {
            return false;
        }
        else if (gzhTaskType == GzhTaskType.PRIVATE_TEMPLATE) {
            if (gzhTask.getGzhCompany().getId() == gzhCompany.getId())            
                return true;
            else
                return false;
        }
        else {
            GzhUser gzhUser = gzhTask.getGzhUser();
            if (gzhUser == null)
                return false;
            GzhCompany gzhCompanyByGzhUser = gzhUser.getGzhCompany();
            if (gzhCompanyByGzhUser == null)
                return false;
            if (gzhCompany.getId() == gzhCompanyByGzhUser.getId())
                return true;
            else
                return false;
        }
    }
    
    public boolean isGzhAdminForGzhClientCompany(long gzhClientCompanyId) {
        User user = (User) this.getPrincipal();
        GzhCompany gzhCompany = user.getAdminedGzhCompany();
        if (gzhCompany == null) 
            return false;
        GzhClientCompanyRepository gzhClientCompanyRepository = BeanProvider.getGzhClientCompanyRepository();
        GzhClientCompany gzhClientCompany = gzhClientCompanyRepository.findOne(gzhClientCompanyId);
        if (gzhClientCompany == null)
            return false;
        GzhCompany gzhCompanyByGzhClientCompany = gzhClientCompany.getGzhCompany();
        if (gzhCompanyByGzhClientCompany == null)
            return false;
        if (gzhCompany.getId() == gzhCompanyByGzhClientCompany.getId())
            return true;
        else
            return false;
    }
    
    public boolean isGzhAdminForHtmlPage(long htmlPageId) {
        User user = (User) this.getPrincipal();
        GzhCompany gzhCompany = user.getAdminedGzhCompany();
        if (gzhCompany == null) 
            return false;
        if (htmlPageId == 0)
            return true;
        
        GzhHtmlPageRepository gzhHtmlPageRepository = BeanProvider.getGzhHtmlPageRepository();
        GzhHtmlPage gzhHtmlPage = gzhHtmlPageRepository.findOne(htmlPageId);
        GzhCompany gzhCompanyByHtmlPageId = gzhHtmlPage.getGzhCompany();
        if (gzhCompanyByHtmlPageId == null)
            return false;
        if (gzhCompany.getId() == gzhCompanyByHtmlPageId.getId())
            return true;
        else
            return false;
    }
    
    public boolean isGzhHtmlPageTemplate(long htmlPageId) {
        
        GzhHtmlPageRepository gzhHtmlPageRepository = BeanProvider.getGzhHtmlPageRepository();
        GzhHtmlPage gzhHtmlPage = gzhHtmlPageRepository.findOne(htmlPageId);
        if (gzhHtmlPage == null)
            return false;
        if (gzhHtmlPage.getPageType() == GzhHtmlPageType.TEMPLATE)
            return true;
        else
            return false;
    }
    
    public boolean isGzhAdminForBill(long id) {
        User user = (User) this.getPrincipal();
        GzhCompany gzhCompany = user.getAdminedGzhCompany();
        if (gzhCompany == null) 
            return false;
        BillRepository billRepository = BeanProvider.getBillRepository();
        Bill bill = billRepository.findOne(id);
        GzhCompany gzhCompanyByBill = bill.getAccount().getGzhCompany();
        if (gzhCompanyByBill == null)
            return false;
        if (gzhCompany.getId() == gzhCompanyByBill.getId())
            return true;
        else
            return false;
    }
    
    public boolean isGzhAdminForTransaction(long id) {
        User user = (User) this.getPrincipal();
        GzhCompany gzhCompany = user.getAdminedGzhCompany();
        if (gzhCompany == null) 
            return false;
        TransactionRepository transactionRepository = BeanProvider.getTransactionRepository();
        Transaction transaction = transactionRepository.findOne(id);
        GzhCompany gzhCompanyByTransaction = transaction.getAccount().getGzhCompany();
        if (gzhCompanyByTransaction == null)
            return false;
        if (gzhCompany.getId() == gzhCompanyByTransaction.getId())
            return true;
        else
            return false;
    }
    
    public boolean isGzhAdminForBillingStatusChange(long id) {
        User user = (User) this.getPrincipal();
        GzhCompany gzhCompany = user.getAdminedGzhCompany();
        if (gzhCompany == null) 
            return false;
        BillingStatusChangeRepository billingStatusChangeRepository = BeanProvider.getBillingStatusChangeRepository();
        BillingStatusChange billingStatusChange = billingStatusChangeRepository.findOne(id);
        GzhCompany gzhCompanyByStatusChange = billingStatusChange.getGzhCompany();
        if (gzhCompanyByStatusChange == null)
            return false;
        if (gzhCompany.getId() == gzhCompanyByStatusChange.getId())
            return true;
        else
            return false;
    }
    
    //Order
    public boolean isGzhAdminForGzhCompanyRechargeOrder(long id) {
        User user = (User) this.getPrincipal();
        GzhCompany gzhCompany = user.getAdminedGzhCompany();
        if (gzhCompany == null) 
            return false;
        OrderRepository orderRepository = BeanProvider.getOrderRepository();
        Order order = orderRepository.findOne(id);
        GzhCompany gzhCompanyByOrder = order.getGzhCompany();
        if (gzhCompanyByOrder == null)
            return false;
        if (gzhCompany.getId() == gzhCompanyByOrder.getId())
            return true;
        else
            return false;
    }
    
    public boolean isReferalOwnerForReferalVerifyOrder(long id) {
        User user = (User) this.getPrincipal();
        Referal selfReferal = user.getReferal();

        OrderRepository orderRepository = BeanProvider.getOrderRepository();
        Order order = orderRepository.findOne(id);

        Referal referal = order.getReferal();
        if (referal == null) {
            return false;
        }
        if (referal.getId() == selfReferal.getId()) {
            return true;
        }
        
        return false;
    }
    
    //QrImage
    public boolean isGzhAdminForGzhCompanyRechargeOrderQrImage(long id) {
        User user = (User) this.getPrincipal();
        GzhCompany gzhCompany = user.getAdminedGzhCompany();
        if (gzhCompany == null) 
            return false;
        QrImageRepository qrImageRepository = BeanProvider.getQrImageRepository();
        QrImage qrImage = qrImageRepository.findOne(id);
        Order order = qrImage.getOrder();
        if (order == null) {
            //qrImage is not generated for a weixin payorder for GzhCompany recharge
            return false;
        }
        GzhCompany gzhCompanyByQrImage = order.getGzhCompany();
        if (gzhCompanyByQrImage == null)
            return false;
        if (gzhCompany.getId() == gzhCompanyByQrImage.getId())
            return true;
        
        return false;
    }
    
    public boolean isReferalOwnerForReferalVerifyOrderQrImage(long id) {
        User user = (User) this.getPrincipal();
        Referal selfReferal = user.getReferal();
        QrImageRepository qrImageRepository = BeanProvider.getQrImageRepository();
        QrImage qrImage = qrImageRepository.findOne(id);
        Order order = qrImage.getOrder();
        if (order == null) {
            //qrImage is not generated for a weixin payorder for referal verification
            return false;
        }
        Referal referal = order.getReferal();
        if (referal != null) {
            if (referal.getId() == selfReferal.getId())
                return true;
        }
        
        return false;
    }
    
    public boolean isReferalOwnerForReferalLinkQrImage(long id) {
        User user = (User) this.getPrincipal();
        Referal selfReferal = user.getReferal();
        
        QrImage qrImage = selfReferal.getQrImage();
        if (qrImage == null) {
            return false;
        }
        
        if (qrImage.getId() == id) {
            return true;
        }
        
        return false;
    }
    
    //Referal
    public boolean isSelfReferal(long id) {
        User user = (User) this.getPrincipal();
        long selfReferalId = user.getReferal().getId();
        if (selfReferalId == id)
            return true;
        
        return false;
    }
    
    public boolean isLevel1Referal(long id) {
        User user = (User) this.getPrincipal();
        long selfReferalId = user.getReferal().getId();
        Referal referal = BeanProvider.getReferalRepository().findOne(id);
        Referal parentReferal = referal.getParentReferal();
        if (parentReferal != null && parentReferal.getId() == selfReferalId)
            return true;
        
        return false;
    }
    
    public boolean isLevel2Referal(long id) {
        User user = (User) this.getPrincipal();
        long selfReferalId = user.getReferal().getId();
        
        Referal referal = BeanProvider.getReferalRepository().findOne(id);
        Referal parentReferal = referal.getParentReferal();
        if (parentReferal != null) {
            Referal grandParentReferal = parentReferal.getParentReferal();
            if (grandParentReferal != null && grandParentReferal.getId() == selfReferalId)
                return true;
        }
        
        return false;
    }
    
    public boolean isReferalOwnerForTransaction(long referalTransactionId) {
        User user = (User) this.getPrincipal();
        long selfReferalId = user.getReferal().getId();
        
        ReferalTransaction referalTransaction = BeanProvider.getReferalTransactionRepository().findOne(referalTransactionId);
        if (referalTransaction.getReferalAccount().getReferal().getId() == selfReferalId)
            return true;
        
        return false;
    }
    
    public boolean isReferalOwnerForCashOut(long cashOutId) {
        User user = (User) this.getPrincipal();
        long selfReferalId = user.getReferal().getId();
        
        ReferalCashOut referalCashOut = BeanProvider.getReferalCashOutRepository().findOne(cashOutId);
        if (referalCashOut.getReferal().getId() == selfReferalId)
            return true;
        
        return false;
    }

    
    //Implementing MethodSecurityExpressionOperations
    public void setFilterObject(Object filterObject) {
        this.filterObject = filterObject;
    }
    public Object getFilterObject() {
        return filterObject;
    }

    public void setReturnObject(Object returnObject) {
        this.returnObject = returnObject;
    }
    public Object getReturnObject() {
        return returnObject;
    }

    /**
     * Sets the "this" property for use in expressions. Typically this will be the "this"
     * property of the {@code JoinPoint} representing the method invocation which is being
     * protected.
     *
     * @param target the target object on which the method in is being invoked.
     */
    void setThis(Object target) {
        this.target = target;
    }
    public Object getThis() {
        return target;
    }
}
