package com.cju.security;

import java.io.IOException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

import com.cju.user.UserDetailsServiceImpl;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;

//@EnableWebSecurity
//Add this annotation to an @Configuration class to have the Spring Security configuration defined in any WebSecurityConfigurer 
//or more likely by extending the WebSecurityConfigurerAdapter base class and overriding individual methods.

//As of Spring Security 4.0, @EnableWebMvcSecurity is deprecated. The replacement is @EnableWebSecurity which will determine adding 
//the Spring MVC features based upon the classpath

//@EnableGlobalMethodSecurity options:
//    prePostEnabled :Determines if Spring Security’s pre post annotations [@PreAuthorize,@PostAuthorize,..] should be enabled.
//    secureEnabled : Determines if Spring Security’s secured annotation [@Secured] should be enabled.
//    jsr250Enabled : Determines if JSR-250 annotations [@RolesAllowed..] should be enabled.
@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class WebMvcSecurityConfig extends WebSecurityConfigurerAdapter {
	private Logger log = LoggerFactory.getLogger(WebMvcSecurityConfig.class);
	
	@Autowired
	UserDetailsServiceImpl userDetailsServiceImpl;
	
    @Override
    protected void configure(HttpSecurity http) throws Exception {
    	//By default, Spring Secutiry will enable csrf protection on all URLs.
    	//When a user send a message to WeiXin, WeiXin will post the message to the development server at the "/gzh" URL
    	//We need to disable csrf on "/gzh". Otherwise, Spring Security will deny the request before it reaches the controller.
    	
    	//The mapping matches URLs using the following rules:

		// ? matches one character
		// * matches zero or more characters
		// ** matches zero or more 'directories' in a path
		// Some examples:
		//
		//    com/t?st.jsp - matches com/test.jsp but also com/tast.jsp or com/txst.jsp
		//    com/*.jsp - matches all .jsp files in the com directory
		//    com/**/test.jsp - matches all test.jsp files underneath the com path
		//    org/springframework/**/*.jsp - matches all .jsp files underneath the org/springframework path
		//    org/**/servlet/bla.jsp - matches org/springframework/servlet/bla.jsp 
		//    but also org/springframework/testing/servlet/bla.jsp and org/servlet/bla.jsp
    	
    	// It is usually enough to secure only URLs in simple cases. 
    	// Think about method level security (@PreAuthorize)as an addition to URL level security. It takes more resources.
    	
    	//When using the WebSecurityConfigurerAdapter, logout capabilities are automatically applied. 
    	//The default is that accessing the URL /logout will log the user out by:
		//	  Invalidating the HTTP Session
		//	  Cleaning up any RememberMe authentication that was configured
		//	  Clearing the SecurityContextHolder
		//	  Redirect to /login?logout
    	//Adding CSRF will update the LogoutFilter to only use HTTP POST.
    	//If you really want to use HTTP GET with logout you can do so by logoutRequestMatcher, but remember this is generally not recommended. 
        
        //antMatcher - * means the direct subfolder. ** means all the recursively subfolders.
        
        //The following urls do not require authentication.
        //    /, 
        //    /pub/user/** (including register, login, logout, reset)
        //    /pub/home/**
        //The following urls require authentication.
        //    /gzhOps/**, /user/**, /chanjet/**, 
        //POST to /gzh does not require csrf protection
        
        //sameOrigin frame option allows the http response to be rendered in a frame. 
        //Used in GzhCompany IntroPage details to render localHtml introPage in a frame.
        
        //After the Web Server restarted, all the old sessions are invalid. 
        //However a browser may send a login request with old session id in http header JSESSIONID. 
        //By default, Spring Security with SessionCreationPolicy.IF_REQUIRED will not create a new session if JSESSIONID is present.
        //However the old session will cause thymeleaf to throw an error 
        //    - java.lang.IllegalStateException: Cannot create a session after the response has been committed.
        //To solve this, use SessionCreationPolicy.ALWAYS. 
        
        http
            .sessionManagement()
                .sessionCreationPolicy(SessionCreationPolicy.ALWAYS)
                .and()
            .exceptionHandling()
                .authenticationEntryPoint(new WebMvcSecurityAuthEntryPoint("/pub/user/login"))
                .and()
            .headers()
                .frameOptions().sameOrigin()
                .and()
            .csrf()
            	.ignoringAntMatchers("/gzh")
            	.ignoringAntMatchers("/gzh/wxpay/**")
            	.and()
            .authorizeRequests()
                 .antMatchers("/gzhOps/**").authenticated()
                 .antMatchers("/user/**").authenticated()
                 .antMatchers("/chanjet/**").authenticated()
                 .anyRequest().permitAll()
                 .and()
            .formLogin()
                .loginPage("/pub/user/login")
                .successHandler(successHandler())
                .failureHandler(failureHandler())
                .permitAll()
                .and()
            .logout()
                .logoutSuccessUrl("/pub/user/logoutSuccess")
                .logoutRequestMatcher(new AntPathRequestMatcher("/pub/user/logout"))
                .permitAll();
    }

    @Bean
    public AuthenticationSuccessHandler successHandler() {
    	AuthenticationSuccessHandler  handler = new AuthenticationSuccessHandler () {
        	@Override
        	public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response,
        	                                    Authentication authentication) throws IOException {
        		//If user just paste http://hostname/login to the browser address bar, 
        	    //then the login POST URL will not have redirectUrl parameter set.
        		String redirectUrl = request.getParameter("redirectUrl");
        		if (redirectUrl == null)
        			redirectUrl = request.getScheme() + "://" + request.getHeader("Host") + request.getContextPath() + "/gzhOps";
        		log.info("AuthenticationSuccessHandler.onAuthenticationSuccess(), redirecting to " + redirectUrl +
        		         ", request url=" + request.getRequestURL() + 
        		         ", request parameter redirectUrl=" + redirectUrl);
        	    response.sendRedirect(redirectUrl);
        	}
        };
        return handler;
    }
    
    @Bean
    public AuthenticationFailureHandler failureHandler() {
    	AuthenticationFailureHandler  handler = new AuthenticationFailureHandler() {
        	@Override
        	public void onAuthenticationFailure(HttpServletRequest request, HttpServletResponse response,
        			AuthenticationException ae) throws IOException {
        	    //We need get the redirectUrl parameter from current request and add it to the redirect
        	    String redirectUrl = "";
        	    if (ae instanceof UsernameNotFoundException) {
        	        redirectUrl = "/pub/user/login?error=usernameNotFound&redirectUrl=" + request.getParameter("redirectUrl");
        	    }
        	    else if (ae instanceof BadCredentialsException) {
        	        redirectUrl = "/pub/user/login?error=badCredentials&redirectUrl=" + request.getParameter("redirectUrl");
        	    }
        	    else if (ae instanceof DisabledException) {
        	        redirectUrl = "/pub/user/login?error=disabled&redirectUrl=" + request.getParameter("redirectUrl");
        	    }
        	    log.info("AuthenticationFailureHandler AuthenticationException=" + ae.getLocalizedMessage() +
        	             ", redirectUrl=" + redirectUrl);
        	    response.sendRedirect(redirectUrl);
        	}
        };
        return handler;
    }

    //springSecurityFilterChain defaults to order 0
    //@Bean
    //public FilterRegistrationBean urlFilterBean() {
    //    final FilterRegistrationBean filterRegBean = new FilterRegistrationBean();
    //    filterRegBean.setFilter(new UrlFilter());
    //    filterRegBean.addUrlPatterns("/*");
    //    filterRegBean.setEnabled(Boolean.TRUE);
    //    filterRegBean.setName("Block-Non-Existing-Url Filter");
    //    //Set order -5 so it comes before springSecurityFilterChain
    //    filterRegBean.setOrder(-5);
    //    return filterRegBean;
    //}
    
    //Some endpoints (over http) of spring-boot-starter-actuator are protected by web secutiry. 
    //To be able to access those endpoints, spring-boot-starter-security is required with an authenticated user with ACTUATOR role.
    //If the AuthenticationManagerBuilder has not been populated and no AuthenticationProviderBean is defined, then the UserService bean 
    //in UserService.java implementing UserDetailsService will be used.
    //@Autowired means AuthenticationManagerBuilder is a bean to be autowired
    @Autowired
    public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
        // auth.inMemoryAuthentication().withUser("apple").password("apple").roles("WEB_ADMIN", "ACTUATOR");
        // auth.inMemoryAuthentication().withUser("shengyi").password("shengyi").roles("COMPANY_ADMIN");
        //userDetailsService and passwordEncoder are configured in the customized DaoAuthenticationProvider in WebMvcBeansConfig.java
        //auth.userDetailsService(userDetailsServiceImpl).passwordEncoder(new BCryptPasswordEncoder());
    }
}

