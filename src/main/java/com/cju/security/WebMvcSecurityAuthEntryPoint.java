package com.cju.security;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.LoginUrlAuthenticationEntryPoint;
import org.springframework.stereotype.Component;

import com.cju.gzh.GzhController;

public class WebMvcSecurityAuthEntryPoint extends LoginUrlAuthenticationEntryPoint{
	private Logger log = LoggerFactory.getLogger(WebMvcSecurityAuthEntryPoint.class);
	
	public WebMvcSecurityAuthEntryPoint(String loginFormUrl) {
	   //LoginFormUrl URL where the login page can be found.
       super(loginFormUrl);
	}
	
	@Override
	public String determineUrlToUseForThisRequest(HttpServletRequest request,
			HttpServletResponse response, AuthenticationException exception) {
	    
	    String requestedWithHeader = request.getHeader("X-Requested-With");
	    if ("XMLHttpRequest".equals(requestedWithHeader)) {
	        //This is an Ajax request to an url that requires authentication
	        //No need to append redirectUrl.
	        //log.info("determineUrlToUseForThisRequest(), received an ajax login request. Url to use after login=/pub/user/ajaxLogin");
	        return "/pub/user/ajaxLogin";
	    }
	    
	    StringBuffer requestUrl = request.getRequestURL();
        String queryString = request.getQueryString();
        if (queryString != null) {
            try {
                queryString = URLEncoder.encode(queryString, "utf-8");
            } catch (UnsupportedEncodingException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            requestUrl = requestUrl.append("?").append(queryString);
        }
        
        String redirectUrl = "/?redirectUrl=" + requestUrl;
        String urlToUse = getLoginFormUrl() + redirectUrl;
        //log.info("determineUrlToUseForThisRequest() Url to use after login=" + urlToUse);
        
		return urlToUse;
	}
}
