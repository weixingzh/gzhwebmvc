package com.cju.security;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.OffsetDateTime;
import java.util.List;
import java.util.UUID;
import org.springframework.data.querydsl.QSort;
import org.springframework.stereotype.Component;
import com.cju.BeanProvider;
import com.cju.billing.QAccount;
import com.cju.billing.QBill;
import com.cju.billing.QBillingStatusChange;
import com.cju.billing.QTransaction;
import com.cju.file.ImageSourceType;
import com.cju.file.QImage;
import com.cju.gzhOps.GzhAutoReplyType;
import com.cju.gzhOps.GzhClientCompany;
import com.cju.gzhOps.GzhClientCompanyReportType;
import com.cju.gzhOps.GzhClientCompanyRepository;
import com.cju.gzhOps.GzhCompany;
import com.cju.gzhOps.GzhCompanyBillingStatus;
import com.cju.gzhOps.GzhCompanyServiceRunningStatus;
import com.cju.gzhOps.GzhHtmlPageTitle;
import com.cju.gzhOps.GzhHtmlPageType;
import com.cju.gzhOps.GzhTaskType;
import com.cju.gzhOps.GzhUser;
import com.cju.gzhOps.GzhUserRepository;
import com.cju.gzhOps.QGzhAutoReply;
import com.cju.gzhOps.QGzhClientCompany;
import com.cju.gzhOps.QGzhClientCompanyReport;
import com.cju.gzhOps.QGzhCompany;
import com.cju.gzhOps.QGzhContact;
import com.cju.gzhOps.QGzhHtmlPage;
import com.cju.gzhOps.QGzhTask;
import com.cju.gzhOps.QGzhUser;
import com.cju.marketing.QReferal;
import com.cju.marketing.QReferalCashOut;
import com.cju.marketing.QReferalTransaction;
import com.cju.marketing.Referal;
import com.cju.marketing.ReferalCashOutStatus;
import com.cju.marketing.ReferalFeeStatus;
import com.cju.order.QOrder;
import com.cju.user.User;
import com.querydsl.core.types.OrderSpecifier;
import com.querydsl.core.types.Predicate;
import com.querydsl.core.types.dsl.StringPath;

@Component
public class RepositoryQueryFilter {
    //GzhCompany filters
    public Predicate filterGzhCompanyByUser(User user) {
        QGzhCompany qGzhCompany = QGzhCompany.gzhCompany;
        
        //WebAdmin can see all. null predicate does not filter out anything 
        if (user.isWebAdmin())
           return null;
        
        Long gzhCompanyId = -1L;
        GzhCompany gzhCompany = user.getAdminedGzhCompany();
        if (gzhCompany != null)
            gzhCompanyId = gzhCompany.getId();
            
        Predicate predicate = qGzhCompany.id.eq(gzhCompanyId);
        return predicate;
    }
    
    public Predicate filterGzhCompanyToUpdateFromTrialToActive(LocalDate today) {
        QGzhCompany qGzhCompany = QGzhCompany.gzhCompany;
        Predicate predicate = qGzhCompany.billingStatus.eq(GzhCompanyBillingStatus.TRIAL)
                .and(qGzhCompany.activeBillingStartDate.loe(today));
        return predicate;
    }
    
    public Predicate filterGzhCompanyToUpdateReferalFeeStatusAccumulating(LocalDate today) {
        QGzhCompany qGzhCompany = QGzhCompany.gzhCompany;
        Predicate predicate = qGzhCompany.ownerUser.referal.referalFeeStatus.eq(ReferalFeeStatus.NOT_STARTED)
                .and(qGzhCompany.activeBillingStartDate.loe(today));
        return predicate;
    }
    
    public Predicate filterGzhCompanyToImportReport(long totalGroups, long filterGroupNum) {
        QGzhCompany qGzhCompany = QGzhCompany.gzhCompany;
        Predicate predicate = qGzhCompany.serviceRunningStatus.eq(GzhCompanyServiceRunningStatus.RUNNING)
                .and(qGzhCompany.id.mod(totalGroups).eq(filterGroupNum));
        return predicate;
    }
    
    //GzhClientCompany filters
    public Predicate filterGzhClientCompanyByUser(User user) {
        QGzhClientCompany qGzhClientCompany = QGzhClientCompany.gzhClientCompany;
        //WebAdmin can see all. null predicate does not filter out anything 
        if (user.isWebAdmin())
           return null;
        
        Long gzhCompanyId = -1L;
        GzhCompany gzhCompany = user.getAdminedGzhCompany();
        if (gzhCompany != null)
            gzhCompanyId = gzhCompany.getId();
        
        Predicate predicate = qGzhClientCompany.gzhCompany.id.eq(gzhCompanyId);
        return predicate;
    }
    
    public Predicate filterGzhClientCompanyByOpenId(String openId) {
        QGzhClientCompany qGzhClientCompany = QGzhClientCompany.gzhClientCompany;
        
        Predicate predicate = qGzhClientCompany.gzhUserList.any().openId.eq(openId);
        return predicate;
    }
    
    public Predicate filterGzhClientCompanyToUpdateCurrentPeriod(String lastMonth) {
        QGzhClientCompany qGzhClientCompany = QGzhClientCompany.gzhClientCompany;
        
        Predicate predicate = qGzhClientCompany.chanjetClientCompany.currentPeriod.isNull()
                .or(qGzhClientCompany.chanjetClientCompany.currentPeriod.ne(lastMonth));
        return predicate;
    }
    
    public Predicate filterGzhClientCompanyToSyncReport() {
        QGzhClientCompany qGzhClientCompany = QGzhClientCompany.gzhClientCompany;
        
        StringPath currentPeriod = qGzhClientCompany.chanjetClientCompany.currentPeriod;
        StringPath syncStartPeriod = qGzhClientCompany.chanjetClientCompany.syncStartPeriod;
        Predicate predicate = currentPeriod.isNotNull().and(syncStartPeriod.isNull().or(currentPeriod.ne(syncStartPeriod)));
        return predicate;
    }
    
    public Predicate filterFakeGzhClientCompany() {
        QGzhClientCompany qGzhClientCompany = QGzhClientCompany.gzhClientCompany;
        
        Predicate predicate = qGzhClientCompany.chanjetClientCompany.accId.isNull()
                .and(qGzhClientCompany.importedByUser.isNull());
        return predicate;
    }
    
    //GzhUser filter
    public Predicate filterGzhUserByUser(User user) {
        QGzhUser qGzhUser = QGzhUser.gzhUser;
        //WebAdmin can see all. null predicate does not filter out anything 
        if (user.isWebAdmin())
           return null;
        
        Long gzhCompanyId = -1L;
        GzhCompany gzhCompany = user.getAdminedGzhCompany();
        if (gzhCompany != null)
            gzhCompanyId = gzhCompany.getId();
        
        Predicate predicate = qGzhUser.gzhCompany.id.eq(gzhCompanyId);
        return predicate;
    }
    
    public Predicate filterGzhUserByGzhClientCompanyId(long gzhClientCompanyId) {
        GzhClientCompanyRepository gzhClientCompanyRepository = BeanProvider.getGzhClientCompanyRepository();
        GzhClientCompany gzhClientCompany = gzhClientCompanyRepository.findOne(gzhClientCompanyId);
        QGzhUser qGzhUser = QGzhUser.gzhUser;
        Predicate predicate = qGzhUser.gzhClientCompanyList.contains(gzhClientCompany);
        return predicate;
    }
    
    public Predicate filterGzhUserByGzhCompanyId(long gzhCompanyId) {
        QGzhUser qGzhUser = QGzhUser.gzhUser;
        Predicate predicate = qGzhUser.gzhCompany.id.eq(gzhCompanyId);
        return predicate;
    }
    
    
    //GzhClientCompanyReport
    public Predicate filterGzhClientCompanyReportByUser(User user) {
        QGzhClientCompanyReport qGzhClientCompanyReport = QGzhClientCompanyReport.gzhClientCompanyReport;
        //WebAdmin can see all. null predicate does not filter out anything 
        if (user.isWebAdmin())
           return null;
        
        Long gzhCompanyId = -1L;
        GzhCompany gzhCompany = user.getAdminedGzhCompany();
        if (gzhCompany != null)
            gzhCompanyId = gzhCompany.getId();
        
        Predicate predicate = qGzhClientCompanyReport.gzhClientCompany.gzhCompany.id.eq(gzhCompanyId);
        return predicate;
    }
    
    public Predicate filterGzhClientCompanyReportByClientCompanyId(long id) {
        QGzhClientCompanyReport qGzhClientCompanyReport = QGzhClientCompanyReport.gzhClientCompanyReport;
        
        Predicate predicate = qGzhClientCompanyReport.gzhClientCompany.id.eq(id);
        return predicate;
    }
    
    public Predicate filterGzhClientCompanyReportByBookIdAndPeriodAndReportType(long bookId, String period, GzhClientCompanyReportType reportType) {
        QGzhClientCompanyReport qGzhClientCompanyReport = QGzhClientCompanyReport.gzhClientCompanyReport;
        Predicate predicate = qGzhClientCompanyReport.bookId.eq(bookId)
             .and(qGzhClientCompanyReport.period.eq(period))
             .and(qGzhClientCompanyReport.reportType.eq(reportType));
        return predicate;
    }
    
    public Predicate filterGzhClientCompanyReportByBookIdAndReportType(long bookId, GzhClientCompanyReportType reportType) {
        QGzhClientCompanyReport qGzhClientCompanyReport = QGzhClientCompanyReport.gzhClientCompanyReport;
        Predicate predicate = qGzhClientCompanyReport.bookId.eq(bookId)
             .and(qGzhClientCompanyReport.reportType.eq(reportType));
        return predicate;
    }
    
    public Predicate filterFakeGzhClientCompanyReport(long gzhClientCompanyId, String period, GzhClientCompanyReportType reportType) {
        QGzhClientCompanyReport qGzhClientCompanyReport = QGzhClientCompanyReport.gzhClientCompanyReport;
        Predicate predicate = qGzhClientCompanyReport.gzhClientCompany.id.eq(gzhClientCompanyId)
                .and(qGzhClientCompanyReport.bookId.eq(0L))
                .and(qGzhClientCompanyReport.period.eq(period))
                .and(qGzhClientCompanyReport.reportType.eq(reportType));
        return predicate;
    }
    
    //GZH Task predicates
    
    public Predicate filterGzhTaskPublicTemplate() {
        QGzhTask qGzhTask = QGzhTask.gzhTask;
        Predicate predicate = qGzhTask.type.eq(GzhTaskType.PUBLIC_TEMPLATE);
        return predicate;
    }
    
    //queryDsl has a bug with the following predicate
    //qGzhTask.templateType.eq(GzhTaskTemplateType.PUBLIC_TEMPLATE)).
    //    or(qGzhTask.gzhUser.gzhCompany.id.eq(gzhCompanyId))
    //So we seperate the filter for public template and private template
    
    public Predicate filterGzhTaskPrivateTemplateByUser(User user) {
        QGzhTask qGzhTask = QGzhTask.gzhTask;
        Predicate predicate = null;
        if (user.isWebAdmin()) {
            predicate = qGzhTask.type.eq(GzhTaskType.PRIVATE_TEMPLATE);
            return predicate;
        }
        
        Long gzhCompanyId = -1L;
        GzhCompany gzhCompany = user.getAdminedGzhCompany();
        if (gzhCompany != null)
            gzhCompanyId = gzhCompany.getId();
        
        predicate = qGzhTask.type.eq(GzhTaskType.PRIVATE_TEMPLATE)
            .and(qGzhTask.gzhCompany.id.eq(gzhCompanyId));
        return predicate;
    }
    
    public Predicate filterGzhTaskPrivateTemplateByGzhCompanyId(long gzhCompanyId) {
        QGzhTask qGzhTask = QGzhTask.gzhTask;

        Predicate predicate = qGzhTask.type.eq(GzhTaskType.PRIVATE_TEMPLATE)
                .and(qGzhTask.gzhCompany.id.eq(gzhCompanyId));
        return predicate;
    }
    
    public Predicate filterGzhTaskByUser(User user) {
        QGzhTask qGzhTask = QGzhTask.gzhTask;
        Predicate predicate = null;
        if (user.isWebAdmin()) {
            predicate = qGzhTask.type.eq(GzhTaskType.CUSTOMER_TASK);
            return predicate;
        }
        
        Long gzhCompanyId = -1L;
        GzhCompany gzhCompany = user.getAdminedGzhCompany();
        if (gzhCompany != null)
            gzhCompanyId = gzhCompany.getId();
        
        predicate = qGzhTask.type.eq(GzhTaskType.CUSTOMER_TASK)
            .and(qGzhTask.gzhUser.gzhCompany.id.eq(gzhCompanyId));
        return predicate;
    }
    
    public Predicate filterGzhTaskByGzhUserId(long gzhUserId) {
        QGzhTask qGzhTask = QGzhTask.gzhTask;
        Predicate predicate = qGzhTask.type.eq(GzhTaskType.CUSTOMER_TASK)
            .and(qGzhTask.gzhUser.id.eq(gzhUserId));
        return predicate;
    }
    
    public Predicate filterGzhTaskByOpenId(String openId) {
        QGzhTask qGzhTask = QGzhTask.gzhTask;
        Predicate predicate = qGzhTask.type.eq(GzhTaskType.CUSTOMER_TASK)
                .and(qGzhTask.gzhUser.openId.eq(openId));
        return predicate;
    }
    
    public QSort sortGzhTaskByLastStatusChangeDesc() {
        QGzhTask qGzhTask = QGzhTask.gzhTask;
        OrderSpecifier<OffsetDateTime> orderSpec = qGzhTask.lastStatusChange.desc();
        QSort qSort = new QSort(orderSpec);
        return qSort;
    }
        
    //GZH Company HTML Page predicates
    public Predicate filterGzhHtmlPageSelfDefinedByUser(User user) {
        QGzhHtmlPage qGzhHtmlPage = QGzhHtmlPage.gzhHtmlPage;
        if (user.isWebAdmin()) {
            Predicate predicate = qGzhHtmlPage.pageType.eq(GzhHtmlPageType.SELF_DEFINED);
            return predicate;
        }
        
        Long gzhCompanyId = -1L;
        GzhCompany gzhCompany = user.getAdminedGzhCompany();
        if (gzhCompany != null)
            gzhCompanyId = gzhCompany.getId();
        
        Predicate predicate = qGzhHtmlPage.gzhCompany.id.eq(gzhCompanyId);
        return predicate;
    }
    
    public Predicate filterGzhHtmlPageByCompanyIdAndTitle(long gzhCompanyId, GzhHtmlPageTitle pageTitle) {
        QGzhHtmlPage qGzhHtmlPage = QGzhHtmlPage.gzhHtmlPage;
        Predicate predicate = qGzhHtmlPage.gzhCompany.id.eq(gzhCompanyId)
            .and(qGzhHtmlPage.title.eq(pageTitle.toString()));
        return predicate;
    }
    
    public Predicate filterGzhHtmlPageByCompanyIdAndTitleList(long gzhCompanyId, List<String> titleList) {
        QGzhHtmlPage qGzhHtmlPage = QGzhHtmlPage.gzhHtmlPage;
        Predicate predicate = qGzhHtmlPage.gzhCompany.id.eq(gzhCompanyId)
                .and(qGzhHtmlPage.title.in(titleList));
        return predicate;
    }
    
    public Predicate filterGzhHtmlPageByType(GzhHtmlPageType gzhHtmlPageType) {
        QGzhHtmlPage qGzhHtmlPage = QGzhHtmlPage.gzhHtmlPage;
        Predicate predicate = qGzhHtmlPage.pageType.eq(gzhHtmlPageType);
        return predicate;
    }
    
    //GZH Company Auto Reply predicates
    public Predicate filterGzhAutoReplyByPublic() {
        QGzhAutoReply qGzhAutoReply = QGzhAutoReply.gzhAutoReply;
        
        Predicate predicate = qGzhAutoReply.type.eq(GzhAutoReplyType.PUBLIC);
        return predicate;
    }
    
    public Predicate filterGzhAutoReplyByPublicAndKey(String key) {
        QGzhAutoReply qGzhAutoReply = QGzhAutoReply.gzhAutoReply;
        
        Predicate predicate = qGzhAutoReply.type.eq(GzhAutoReplyType.PUBLIC)
            .and(qGzhAutoReply.key.eq(key));
        return predicate;
    }
    
    public Predicate filterGzhAutoReplyByPublicOrUser(User user) {
        QGzhAutoReply qGzhAutoReply = QGzhAutoReply.gzhAutoReply;
        
        //WebAdmin can see all. null predicate does not filter out anything 
        if (user.isWebAdmin())
            return null;
        
        Long gzhCompanyId = -1L;
        GzhCompany gzhCompany = user.getAdminedGzhCompany();
        if (gzhCompany != null)
            gzhCompanyId = gzhCompany.getId();
        
        Predicate predicate = qGzhAutoReply.type.eq(GzhAutoReplyType.PUBLIC)
            .or(qGzhAutoReply.gzhCompany.id.eq(gzhCompanyId));
        return predicate;
    }
    
    public Predicate filterGzhAutoReplyByGzhCompanyIdAndKey(long gzhCompanyId, String key) {
        QGzhAutoReply qGzhAutoReply = QGzhAutoReply.gzhAutoReply;
        
        Predicate predicate = qGzhAutoReply.gzhCompany.id.eq(gzhCompanyId)
                .and(qGzhAutoReply.key.eq(key));
        return predicate;
    }
    
    public Predicate filterGzhAutoReplyByGzhIdAndKey(String gzhId, String key) {
        QGzhAutoReply qGzhAutoReply = QGzhAutoReply.gzhAutoReply;
        
        Predicate predicate = qGzhAutoReply.gzhCompany.gzhId.eq(gzhId)
            .and(qGzhAutoReply.key.eq(key));
        return predicate;
    }
    
    //GZH Company Contact predicates
    public Predicate filterGzhContactByUser(User user) {
        QGzhContact qGzhContact = QGzhContact.gzhContact;
        
        //WebAdmin can see all. null predicate does not filter out anything 
        if (user.isWebAdmin())
            return null;
        
        Long gzhCompanyId = -1L;
        GzhCompany gzhCompany = user.getAdminedGzhCompany();
        if (gzhCompany != null)
            gzhCompanyId = gzhCompany.getId();
        
        Predicate predicate = qGzhContact.gzhCompany.id.eq(gzhCompanyId);
        return predicate;
    }
    
    public Predicate filterGzhContactByGzhCompanyId(long gzhCompanyId) {
        QGzhContact qGzhContact = QGzhContact.gzhContact;
        
        Predicate predicate = qGzhContact.gzhCompany.id.eq(gzhCompanyId);
        return predicate;
    }
    
    //Image predicates
    public Predicate filterImageByGzhCompanyIdAndDigest(long gzhCompanyId, String digest) {
        QImage qImage = QImage.image;
        Predicate predicate = qImage.gzhCompany.id.eq(gzhCompanyId)
            .and(qImage.digest.eq(digest));
        return predicate;
    }
    
    public Predicate filterImageByNullGzhCompanyAndDigest(String digest) {
        QImage qImage = QImage.image;
        Predicate predicate = qImage.gzhCompany.isNull()
            .and(qImage.digest.eq(digest));
        return predicate;
    }
    
    public Predicate filterImageByDigest(String digest) {
        QImage qImage = QImage.image;
        Predicate predicate = qImage.digest.eq(digest);
        return predicate;
    }
    
    public Predicate filterImageBySourceType(ImageSourceType imageSourceType) {
        QImage qImage = QImage.image;
        Predicate predicate = qImage.imageSourceType.eq(imageSourceType);
        return predicate;
    }
    
    //Transaction
    public Predicate filterTransactionByUser(User user) {
        QTransaction qTransaction = QTransaction.transaction;
        //WebAdmin can see all. null predicate does not filter out anything 
        if (user.isWebAdmin())
            return null;
        
        Long gzhCompanyId = -1L;
        GzhCompany gzhCompany = user.getAdminedGzhCompany();
        if (gzhCompany != null)
            gzhCompanyId = gzhCompany.getId();
        
        Predicate predicate = qTransaction.account.gzhCompany.id.eq(gzhCompanyId);
        return predicate;
    }
    
    public Predicate filterTransactionByAccountId(long accountId) {
        QTransaction qTransaction = QTransaction.transaction;
        Predicate predicate = qTransaction.account.id.eq(accountId);
        return predicate;
    }
    
    //bill
    public Predicate filterBillByAccountIdAndBillMonth(long accountId, String billMonth) {
        QBill qBill = QBill.bill;
        Predicate predicate = qBill.account.id.eq(accountId)
            .and(qBill.billMonth.eq(billMonth));
        return predicate;
    }
    public Predicate filterBillByBillId(String billId) {
        UUID uuid = UUID.fromString(billId);
        QBill qBill = QBill.bill;
        Predicate predicate = qBill.billId.eq(uuid);
        return predicate;
    }
    public Predicate filterBillByUser(User user) {
        QBill qBill = QBill.bill;
        //WebAdmin can see all. null predicate does not filter out anything 
        if (user.isWebAdmin())
            return null;
        
        Long gzhCompanyId = -1L;
        GzhCompany gzhCompany = user.getAdminedGzhCompany();
        if (gzhCompany != null)
            gzhCompanyId = gzhCompany.getId();
        
        Predicate predicate = qBill.account.gzhCompany.id.eq(gzhCompanyId);
        return predicate;
    }
    
    public Predicate filterBillByGzhCompanyIdAndUntilDate(long gzhCompanyId, LocalDate untilDate) {
        QBill qBill = QBill.bill;
        Predicate predicate = qBill.account.gzhCompany.id.eq(gzhCompanyId)
                .and(qBill.periodEnd.loe(untilDate));
        return predicate;
    }
    
    public Predicate filterBillByGzhCompanyId(long gzhCompanyId) {
        QBill qBill = QBill.bill;
        Predicate predicate = qBill.account.gzhCompany.id.eq(gzhCompanyId);
        return predicate;
    }
    
    public Predicate fillterAccountByNegativeBalance() {
        QAccount qAccount = QAccount.account;
        Predicate predicate = qAccount.balance.lt(0);
        return predicate;
    }
    
    //BillingStatusChange filters
    public Predicate filterBillingStatusChangeByUser(User user) {
        //WebAdmin can see all. null predicate does not filter out anything 
        if (user.isWebAdmin())
            return null;
        
        Long gzhCompanyId = -1L;
        GzhCompany gzhCompany = user.getAdminedGzhCompany();
        if (gzhCompany != null)
            gzhCompanyId = gzhCompany.getId();
        
        QBillingStatusChange qBillingStatusChange = QBillingStatusChange.billingStatusChange;
        Predicate predicate = qBillingStatusChange.gzhCompany.id.eq(gzhCompanyId);
        return predicate;
    }
    
    public Predicate filterBillingStatusChangeByBillignStatusChangeId(String billignStatusChangeId) {
        UUID uuid = UUID.fromString(billignStatusChangeId);
        QBillingStatusChange qBillingStatusChange = QBillingStatusChange.billingStatusChange;
        Predicate predicate = qBillingStatusChange.billingStatusChangeId.eq(uuid);
        return predicate;
    }
    
    //Excluding billingBeginDate
    public Predicate filterBillingStatusChangeBeforeDate(long gzhCompanyId, LocalDate billBeginDate) {
        QBillingStatusChange qBillingStatusChange = QBillingStatusChange.billingStatusChange;
        Predicate predicate = qBillingStatusChange.gzhCompany.id.eq(gzhCompanyId)
            .and(qBillingStatusChange.timeStamp.lt(billBeginDate.atStartOfDay()));
        return predicate;
    }
    
    //Including both billBeginDate and billEndDate
    public Predicate filterBillingStatusChangeBetweenDates(long gzhCompanyId, LocalDate billBeginDate, LocalDate billEndDate) {
        QBillingStatusChange qBillingStatusChange = QBillingStatusChange.billingStatusChange;
        Predicate predicate = qBillingStatusChange.gzhCompany.id.eq(gzhCompanyId)
                .and(qBillingStatusChange.timeStamp.goe(billBeginDate.atStartOfDay()))
                .and(qBillingStatusChange.timeStamp.lt(billEndDate.plusDays(1).atStartOfDay()));
        return predicate;
    }
    
    public QSort sortBillingStatusChangeByTimeStampDesc() {
        QBillingStatusChange qBillingStatusChange = QBillingStatusChange.billingStatusChange;
        OrderSpecifier<LocalDateTime> orderSpec = qBillingStatusChange.timeStamp.desc();
        QSort qSort = new QSort(orderSpec);
        return qSort;
    }
    
    public QSort sortBillingStatusChangeByTimeStampAsc() {
        QBillingStatusChange qBillingStatusChange = QBillingStatusChange.billingStatusChange;
        OrderSpecifier<LocalDateTime> orderSpec = qBillingStatusChange.timeStamp.asc();
        QSort qSort = new QSort(orderSpec);
        return qSort;
    }
    
    //Order
    public Predicate filterRechargeOrderByUser(User user) {
        QOrder qOrder = QOrder.order;
        Predicate predicate = null;
        
        //order can be associated to either referal or gzhCompany.
        //Only get the orders associated with gzhCompany.
        
        //WebAdmin can see all. Get all the order associated to a gzhCompany.
        if (user.isWebAdmin()) {
            predicate = qOrder.gzhCompany.id.gt(0L);
        }
        
        Long gzhCompanyId = -1L;
        GzhCompany gzhCompany = user.getAdminedGzhCompany();
        if (gzhCompany != null)
            gzhCompanyId = gzhCompany.getId();
        
        predicate = qOrder.gzhCompany.id.eq(gzhCompanyId);
        return predicate;
    }
    
    public Predicate filterReferalOrderAll() {
        QOrder qOrder = QOrder.order;
        
        //order can be associated to either referal or gzhCompany.
        //Get all the orders associated with referal.
        
        Predicate predicate = qOrder.referal.id.gt(0L);
        
        return predicate;
    }
    
    //Referal
    public Predicate filterReferalWithGzhCompany() {
        QReferal qReferal = QReferal.referal;
        Predicate predicate = qReferal.user.ownedGzhCompany.isNotNull();
        return predicate;
    }
    public Predicate filterTwoLevelReferalByUser(User user) {
        QReferal qReferal = QReferal.referal;
        //WebAdmin can see all. null predicate does not filter out anything 
        if (user.isWebAdmin())
            return null;
        
        Long referalId = -1L;
        Referal referal = user.getReferal();
        if (referal != null)
            referalId = referal.getId();
        
        Predicate predicate = qReferal.parentReferal.id.eq(referalId)
            .or(qReferal.parentReferal.parentReferal.id.eq(referalId));
        return predicate;
    }
    
    public Predicate filterOneLevelReferalByUser(User user) {
        QReferal qReferal = QReferal.referal;
        //WebAdmin can see all. null predicate does not filter out anything 
        if (user.isWebAdmin())
            return null;
        
        Long referalId = -1L;
        Referal referal = user.getReferal();
        if (referal != null)
            referalId = referal.getId();
            
        Predicate predicate = qReferal.parentReferal.id.eq(referalId);
        return predicate;
    }
    
    public Predicate filterTwoLevelReferalByReferalId(long referalId) {
        QReferal qReferal = QReferal.referal;
        
        Predicate predicate = qReferal.parentReferal.id.eq(referalId)
            .or(qReferal.parentReferal.parentReferal.id.eq(referalId));
        return predicate;
    }
    
    public Predicate filterOneLevelReferalByReferalId(long referalId) {
        QReferal qReferal = QReferal.referal;
        
        Predicate predicate = qReferal.parentReferal.id.eq(referalId);
        return predicate;
    }
    
    public Predicate filterMatchNoneReferal() {
        QReferal qReferal = QReferal.referal;
        Long matchNoneId = -1L;
        
        Predicate predicate = qReferal.id.eq(matchNoneId);

        return predicate;
    }
    
    public Predicate filterReferalToUpdateReferalFeeStatusAccumulationEnded(LocalDate today) {
        QReferal qReferal = QReferal.referal;
        Predicate predicate = qReferal.referalFeeStatus.eq(ReferalFeeStatus.ACCUMULATING)
                .and(qReferal.referalFeeEndDate.loe(today));
        return predicate;
    }
    
    public Predicate filterReferalOfAccumulatingOrAccumulationEnded() {
        QReferal qReferal = QReferal.referal;
        Predicate predicate = qReferal.referalFeeStatus.eq(ReferalFeeStatus.ACCUMULATING)
                .or(qReferal.referalFeeStatus.eq(ReferalFeeStatus.ACCUMULATION_ENDED));
        return predicate;
    }
    
    public Predicate filterReferalOfTotalCalculated() {
        QReferal qReferal = QReferal.referal;
        Predicate predicate = qReferal.referalFeeStatus.eq(ReferalFeeStatus.TOTAL_CALCULATED);
        return predicate;
    }
    
    //Referal transaction
    public Predicate filterReferalTransactionByReferalAccountId(long referalAccountId) {
        QReferalTransaction qReferalTransaction = QReferalTransaction.referalTransaction;
        
        Predicate predicate = qReferalTransaction.referalAccount.id.eq(referalAccountId);
        return predicate;
    }
    
    //ReferalCashOut
    public Predicate filterReferalCashOutInProcessingByUser(User user) {
        QReferalCashOut qReferalCashOut = QReferalCashOut.referalCashOut;
        
        Long referalId = -1L;
        Referal referal = user.getReferal();
        if (referal != null)
            referalId = referal.getId();
        
        Predicate predicate = qReferalCashOut.referal.id.eq(referalId)
                .and(qReferalCashOut.referalCashOutStatus.eq(ReferalCashOutStatus.PROCESSING));
        return predicate;
    }
    
    public Predicate filterReferalCashOutByUser(User user) {
        //WebAdmin can see all. null predicate does not filter out anything 
        if (user.isWebAdmin())
            return null;
        
        QReferalCashOut qReferalCashOut = QReferalCashOut.referalCashOut;
        
        Long referalId = -1L;
        Referal referal = user.getReferal();
        if (referal != null)
            referalId = referal.getId();
        
        Predicate predicate = qReferalCashOut.referal.id.eq(referalId);
        return predicate;
    }
}
