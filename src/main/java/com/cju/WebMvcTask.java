package com.cju;

import java.time.LocalDate;
import java.time.YearMonth;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.orm.jpa.EntityManagerHolder;
import org.springframework.scheduling.Trigger;
import org.springframework.scheduling.TriggerContext;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;
import org.springframework.stereotype.Component;
import org.springframework.transaction.support.TransactionSynchronizationManager;
import com.cju.billing.Bill;
import com.cju.billing.BillingService;
import com.cju.chanjet.ChanjetClientCompany;
import com.cju.chanjet.ChanjetClientCompanyRepository;
import com.cju.gzhOps.GzhClientCompany;
import com.cju.gzhOps.GzhClientCompanyRepository;
import com.cju.gzhOps.GzhClientCompanyService;
import com.cju.gzhOps.GzhCompany;
import com.cju.marketing.Referal;
import com.cju.marketing.ReferalService;
import com.cju.security.RepositoryQueryFilter;
import com.querydsl.core.types.Predicate;


//When a container of the application(be it a Java EE container or any other custom container like Spring) manages the lifecycle of the Entity Manager, 
//the Entity Manager is said to be Container Managed. 
//The most common way of acquiring a Container Managed EntityManager is to use @PersistenceContext annotation on an EntityManager attribute.
//A Transactional scoped Entity Manager creates a Transaction scoped Persistence Context. An Extended Scope Entity Manager uses the Extended Persistence Context.
//A Transaction Scoped Entity Manager is returned whenever a reference created by @PersistenceContext is resolved.

//Guess: Each repository method call such as find(), save() runs in its own transaction.
//Only CRUD methods (CrudRepository methods) are by default marked as transactional. 
//If you are using custom query methods you should explicitly mark it with @Transactional annotation.

//Spring Boot sets property for JPA configuration spring.jpa.open-in-view=true
//This property will register an OpenEntityManagerInViewInterceptor, which registers an EntityManager to the current thread, 
//so you will have the same EntityManager until the web request is finished.

//WebMvcTask runs outside controller. There is no thread local entity manager injected by OpenEntityManagerInViewInterceptor.
//The CRUD methods will create a new entity manager (hibernate session) for each method call and close the hibernate session after the method call.
//Thus hibernate session is closed after each repository call which is transactional.
//When the hibernate session is closed and a lazily loaded field is accessed, org.hibernate.LazyInitializationException will be thrown.

@Component
public class WebMvcTask {
    private Logger log = LoggerFactory.getLogger(WebMvcTask.class);
    
    private final int GZH_CLIENT_COMPANY_GROUP_SIZE = 100;
    private final int RANDOM_DELAY_SECONDS = 300;
    
    @Autowired
    private BillingService billingService;
    
    @Autowired 
    private GzhClientCompanyRepository gzhClientCompanyRepository;
    
    @Autowired 
    private GzhClientCompanyService gzhClientCompanyService;
    
    @Autowired 
    private ChanjetClientCompanyRepository chanjetClientCompanyRepository;
    
    @Autowired
    RepositoryQueryFilter repoQueryFilter;
    
    @Autowired
    private ReferalService referalService;
    
    //Inject the entityManagerFactory, only one in the whole application
    @PersistenceUnit
    private EntityManagerFactory entityManagerFactory;
    
    @Autowired
    private ThreadPoolTaskScheduler threadPoolTaskScheduler;
    
    @Autowired
    private MessageSource messageSource;
    
    //Billing related tasks
    //0:30 every day
    public Map<String, String>  updateBillingStatusFromTrialToActive() {
        Map<String, String> taskResult = new HashMap<String, String>();
        
        String today = LocalDate.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd"));
        String operator = messageSource.getMessage("gzh.gzhcompany.operator.system.auto", null, Locale.SIMPLIFIED_CHINESE);
        List<GzhCompany> gzhCompanyList = billingService.updateBillingStatusFromTrialToActive(operator);
        
        log.info("updateBillingStatusFromTrialToActive, total " + gzhCompanyList.size() + " gzhCompany billingStatus changed from TRIAL to ACTIVE.");
        //gzhCompanyList.forEach(gzhCompany -> {
        //    log.info("GzhCompany name=" + gzhCompany.getName() + ", billingStatus=" + gzhCompany.getBillingStatus());
        //});
        
        String taskName = messageSource.getMessage("gzh.scheduled.task.update.billing.status.tastName", new Object[] {today}, Locale.SIMPLIFIED_CHINESE);
        int itemCount = gzhCompanyList.size();
        String note = messageSource.getMessage("gzh.scheduled.task.update.billing.status.note", new Object[] {today}, Locale.SIMPLIFIED_CHINESE);
        note = note + "\n" + "Success count: " + gzhCompanyList.size();
        
        taskResult.put("taskName", taskName);
        taskResult.put("note", note);
        taskResult.put("itemCount", String.valueOf(itemCount));
        
        return taskResult;
    }
    
    //2:00 of day 1 of every month
    public Map<String, String> generateBillForAllGzhCompany() {
        String today = LocalDate.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd"));
        Map<String, String> taskResult = new HashMap<String, String>();
        
        String operator = messageSource.getMessage("gzh.gzhcompany.operator.system.auto", null, Locale.SIMPLIFIED_CHINESE);
        List<Bill> billList = billingService.generateBillForAllGzhCompany(operator);
        
        log.info("generateBillForAllAccounts, total " + billList.size() + " bills are generated.");
        //billList.forEach(bill -> {
        //    log.info("Bill: GzhCompany name=" + bill.getAccount().getGzhCompany().getName() +
        //            ", billMonth=" + bill.getBillMonth() +
        //            ", amount=" + bill.getAmount() +
        //            ", numOfGzhClientCompany=" + bill.getNumOfGzhClientCompany());
        //});
        
        String taskName = messageSource.getMessage("gzh.scheduled.task.generate.bill.taskName", new Object[] {today}, Locale.SIMPLIFIED_CHINESE);
        int itemCount = billList.size();
        String note = messageSource.getMessage("gzh.scheduled.task.generate.bill.note", new Object[] {today}, Locale.SIMPLIFIED_CHINESE);
        note = note + "\n" + "Success count: " + itemCount;
        
        taskResult.put("taskName", taskName);
        taskResult.put("note", note);
        taskResult.put("itemCount", String.valueOf(itemCount));
        
        return taskResult;
        
    }
    
    //2:00 of day 11 of every month
    public Map<String, String> updateGzhCompanyInDebt() {
        String today = LocalDate.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd"));
        Map<String, String> taskResult = new HashMap<String, String>();
        
        List<GzhCompany> newInDebtList = billingService.updateGzhCompanyInDebt();
        
        log.info("updateGzhCompanyInDebt, total " + newInDebtList.size() + " GzhCompany changed to InDebt status.");
        //newInDebtList.forEach(gzhCompany -> {
        //    log.info("InGzhCompany name=" + gzhCompany.getName() +", balance=" + gzhCompany.getAccount().getBalance());
        //});
        
        String taskName = messageSource.getMessage("gzh.scheduled.task.update.in.debt.status.taskName", new Object[] {today}, Locale.SIMPLIFIED_CHINESE);
        int itemCount = newInDebtList.size();
        String note = messageSource.getMessage("gzh.scheduled.task.update.in.debt.status.note", new Object[] {today}, Locale.SIMPLIFIED_CHINESE);
        note = note + "\n" + "Success Count: " + itemCount;
        
        taskResult.put("taskName", taskName);
        taskResult.put("note", note);
        taskResult.put("itemCount", String.valueOf(itemCount));
        
        return taskResult;
    }
    
    //2:00 of day 21 of every month
    public Map<String, String> updateGzhCompanyOverDebt() {
        String today = LocalDate.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd"));
        Map<String, String> taskResult = new HashMap<String, String>();
        
        List<GzhCompany> newOverDebtList = billingService.updateGzhCompanyOverDebt();
        
        log.info("updateGzhCompanyInDebt, total " + newOverDebtList.size() + " GzhCompany changed to OverDebt status.");
        //newOverDebtList.forEach(gzhCompany -> {
        //    log.info("OverDebt GzhCompany name=" + gzhCompany.getName() +
        //             ", balance=" + gzhCompany.getAccount().getBalance() +
        //             ", paymentInDebtDate=" + gzhCompany.getPaymentInDebtDate());
        //});
        
        String taskName = messageSource.getMessage("gzh.scheduled.task.update.over.debt.status.taskName", new Object[] {today}, Locale.SIMPLIFIED_CHINESE);
        int itemCount = newOverDebtList.size();
        String note = messageSource.getMessage("gzh.scheduled.task.update.over.debt.status.note", new Object[] {today}, Locale.SIMPLIFIED_CHINESE);
        note = note + "\n" + "Success Count: " + itemCount;
        
        taskResult.put("taskName", taskName);
        taskResult.put("note", note);
        taskResult.put("itemCount", String.valueOf(itemCount));
        
        return taskResult;
    }
    
    
    //GzhClienCompanyReport related scheduled tasks
    
    private Trigger randomDelayTrigger(int delaySeconds) {
        long randomDelay = Math.round(Math.random() * delaySeconds);
        Trigger trigger = new Trigger () {
            private boolean fired = false;
            @Override
            public Date nextExecutionTime(TriggerContext triggerContext) {
                if (fired == true)
                    return null;
                log.info("randomDelayTrigger nextExecutionTime randomDelay=" + randomDelay);
                Date nextExecution = new Date(System.currentTimeMillis() + randomDelay * 1000);
                fired = true;
                return nextExecution;
            }
        };
        
        return trigger;
    }
    
    //At 2:00 in the morning day 5, 15, 25 of a month, import chanjet report of last month
    public Map<String, String> setupTasksToUpdateGzhClientComanyCurrentPeriod(WebMvcTask webMvcTaskBean) {
        String today = LocalDate.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd"));
        Map<String, String> taskResult = new HashMap<String, String>();
        
        YearMonth yearMonth = YearMonth.now();
        String lastMonth = yearMonth.minusMonths(1).format(DateTimeFormatter.ofPattern("yyyyMM"));
        
        //Count all the gzhClientCompany whose currentPeriod is not updated to lastMonth yet
        //filterGzhClientCompanyToUpdateCurrentPeriod will include the gzhClientCompany whose currentPeroid is null
        Predicate predicate = repoQueryFilter.filterGzhClientCompanyToUpdateCurrentPeriod(lastMonth);
        long totalNonCurrent = gzhClientCompanyRepository.count(predicate);
        log.info("setupTasksToUpdateGzhClientComanyCurrentPeriod totalNonCurrent=" + totalNonCurrent);
        
        //Divide the total into smaller groups, each group is scheduled as a task
        int totalGroups = (int) Math.ceil(totalNonCurrent*1.0/GZH_CLIENT_COMPANY_GROUP_SIZE);
        
        for (int taskNum = 0; taskNum < totalGroups; taskNum++) {
            final int pageNum = taskNum;
            Trigger randomDelayTrigger = randomDelayTrigger(RANDOM_DELAY_SECONDS);
            //Schedule a task for each group
            threadPoolTaskScheduler.schedule(
                new Runnable() {
                    public void run() {
                        //Only when we call updateGzhClientComanyCurrentPeriod through a bean, will the logTaskTime AOP kick in.
                        webMvcTaskBean.updateGzhClientComanyCurrentPeriod(lastMonth, pageNum);
                    }
                },
                randomDelayTrigger
            );
        }
        
        String taskName = messageSource.getMessage("gzh.scheduled.task.setup.task.to.update.current.period.taskName", new Object[] {today}, Locale.SIMPLIFIED_CHINESE);
        String note = messageSource.getMessage("gzh.scheduled.task.setup.task.to.update.current.period.note", new Object[] {today}, Locale.SIMPLIFIED_CHINESE);
        note = note + "\n" + "Total tasks scheduled: " + totalGroups;
        int itemCount = totalGroups;
        taskResult.put("taskName", taskName);
        taskResult.put("note", note);
        taskResult.put("itemCount", String.valueOf(itemCount));
        
        return taskResult;
    }
    
    public Map<String, String> updateGzhClientComanyCurrentPeriod(String lastMonth, int pageNum) {
        String today = LocalDate.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd"));
        int successCount = 0;
        int failCount = 0;
        int skipCount = 0;
        Map<String, String> taskResult = new HashMap<String, String>();
        
        log.info("updateGzhClientComanyCurrentPeriod pageNum=" + pageNum);
        PageRequest pageRequest = new PageRequest(pageNum, GZH_CLIENT_COMPANY_GROUP_SIZE, Sort.Direction.ASC, "id");
        //filterGzhClientCompanyToUpdateCurrentPeriod will include the gzhClientCompany whose currentPeroid is null
        Predicate predicate = repoQueryFilter.filterGzhClientCompanyToUpdateCurrentPeriod(lastMonth);
        Iterable<GzhClientCompany> gzhClientCompanyItr = gzhClientCompanyRepository.findAll(predicate, pageRequest);
        for (GzhClientCompany gzhClientCompany: gzhClientCompanyItr) {
            if (gzhClientCompany.getImportedByUser() == null && gzhClientCompany.getChanjetClientCompany().getAccId() == null) {
                //This gzhClientCompany is not imported from chanjet. This is a fake gzhClientCompany
                skipCount++;
                continue;
            }
            log.info("updateGzhClientComanyCurrentPeriod gzhClientCompany=" + gzhClientCompany.getChanjetClientCompany().getName());
            try {
                gzhClientCompanyService.updateReportCurrentPeriod(gzhClientCompany);
                successCount ++;
            } catch (Exception e) {
                failCount ++;
                log.info("updateGzhClientComanyCurrentPeriod received an exception from gzhClientCompanyService on gzhClientCompany=" 
                        + gzhClientCompany.getChanjetClientCompany().getName() + ", " + e.getLocalizedMessage());
            }
        }

        String taskName = messageSource.getMessage("gzh.scheduled.task.update.current.period.taskName", new Object[] {today, pageNum}, Locale.SIMPLIFIED_CHINESE);
        String note = messageSource.getMessage("gzh.scheduled.task.update.current.period.note", new Object[] {today, pageNum}, Locale.SIMPLIFIED_CHINESE);
        note = note + "\n" + "Success count: " + successCount + "\n" + "Fail count: " + failCount + "\n" + "Skil count: " + skipCount;
        int itemCount = successCount + failCount + skipCount;
        taskResult.put("taskName", taskName);
        taskResult.put("note", note);
        taskResult.put("itemCount", String.valueOf(itemCount));
        
        return taskResult;
    }
    
    
    public Map<String, String> setupTasksToSyncGzhClientComanyReport(WebMvcTask webMvcTaskBean) {
        String today = LocalDate.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd"));
        Map<String, String> taskResult = new HashMap<String, String>();
        
        YearMonth yearMonth = YearMonth.now();
        String lastMonth = yearMonth.minusMonths(1).format(DateTimeFormatter.ofPattern("yyyyMM"));
        
        //Count all the gzhClientCompany whose syncStartPeriod does not catch up with currentPeriod
        //filterGzhClientCompanyToSyncReport will exclude the gzhClientCompany whose currentPeriod is null
        Predicate predicate = repoQueryFilter.filterGzhClientCompanyToSyncReport();
        long totalToSync = gzhClientCompanyRepository.count(predicate);
        log.info("setupTasksToSyncGzhClientComanyReport totalToSync=" + totalToSync);
        
        //Divide the total into smaller groups, each group is scheduled as a task
        int totalGroups = (int) Math.ceil(totalToSync*1.0/GZH_CLIENT_COMPANY_GROUP_SIZE);
        
        for (int taskNum = 0; taskNum < totalGroups; taskNum++) {
            final int pageNum = taskNum;
            Trigger randomDelayTrigger = randomDelayTrigger(RANDOM_DELAY_SECONDS);
            //Schedule a task for each group
            threadPoolTaskScheduler.schedule(
                new Runnable() {
                    public void run() {
                        //Only when we call syncGzhClientComanyReport through a bean, will the logTaskTime AOP kick in.
                        webMvcTaskBean.syncGzhClientComanyReport(lastMonth, pageNum);
                    }
                },
                randomDelayTrigger
            );
        }
        
        String taskName = messageSource.getMessage("gzh.scheduled.task.setup.task.to.sync.report.taskName", new Object[] {today}, Locale.SIMPLIFIED_CHINESE);
        String note = messageSource.getMessage("gzh.scheduled.task.setup.task.to.sync.report.note", new Object[] {today}, Locale.SIMPLIFIED_CHINESE);
        note = note + "\n" + "Total tasks scheduled: " + totalGroups;
        int itemCount = totalGroups;
        taskResult.put("taskName", taskName);
        taskResult.put("note", note);
        taskResult.put("itemCount", String.valueOf(itemCount));
        
        return taskResult;
    }
    
    //2:00 of day 5, 15, 25 of every month
    public Map<String, String> syncGzhClientComanyReport(String lastMonth, int pageNum) {
        String today = LocalDate.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd"));
        int successCount = 0;
        int failCount = 0;
        int skipCount = 0;
        Map<String, String> taskResult = new HashMap<String, String>();
        
        //entityManager is not thread safe. 
        //So we need to create a real application-managed entityManager which has an PersistenceContextType.EXTENDED scope.
        //We can not use entityManager injected by @PersistenceContext(type=PersistenceContextType.EXTENDED), 
        //as it is just a proxy and there is no real entityManager for the thread yet.
        if(!TransactionSynchronizationManager.hasResource(entityManagerFactory)) {
            log.info("syncGzhClientComanyReport entityManager not bound to thread!");
            EntityManager entityManager = entityManagerFactory.createEntityManager();
            TransactionSynchronizationManager.bindResource(entityManagerFactory, new EntityManagerHolder(entityManager));
        };
        
        log.info("syncGzhClientComanyReport pageNum=" + pageNum);
        PageRequest pageRequest = new PageRequest(pageNum, GZH_CLIENT_COMPANY_GROUP_SIZE, Sort.Direction.ASC, "id");
        //filterGzhClientCompanyToSyncReport will exclude the gzhClientCompany whose currentPeriod is null
        Predicate predicate = repoQueryFilter.filterGzhClientCompanyToSyncReport();
        Iterable<GzhClientCompany> gzhClientCompanyItr = gzhClientCompanyRepository.findAll(predicate, pageRequest);
        
        for (GzhClientCompany gzhClientCompany: gzhClientCompanyItr) {
            ChanjetClientCompany chanjetClientCompany = gzhClientCompany.getChanjetClientCompany();
            if (gzhClientCompany.getImportedByUser() == null && chanjetClientCompany.getAccId() == null) {
                //This gzhClientCompany is not imported from chanjet. This is a fake gzhClientCompany
                skipCount++;
                continue;
            }
            
            log.info("syncGzhClientComanyReport gzhClientCompany=" + chanjetClientCompany.getName());
            Map<String, String>  importResult = importGzhClientCompanyReport(gzhClientCompany);
            if (importResult.get("exception") == null) {
                //All reports are imported successfully.
                successCount ++;
            }
            else {
                //This may be partial fail. That is, some reports for the gzhClientCompany are imported successfully, but not all.
                failCount ++;
            }
            
            String updatedSyncStartPeriod = importResult.get("updatedSyncStartPeriod");
            if (updatedSyncStartPeriod != null) {
                chanjetClientCompany.setSyncStartPeriod(updatedSyncStartPeriod);
                chanjetClientCompanyRepository.save(chanjetClientCompany);
            }
        }
        
        if(TransactionSynchronizationManager.hasResource(entityManagerFactory)) {
            log.info("syncGzhClientComanyReport finished importing reports. Unbind entityManager from thread!");
            TransactionSynchronizationManager.unbindResource(entityManagerFactory);
        };
        
        String taskName = messageSource.getMessage("gzh.scheduled.task.sync.report.taskName", new Object[] {today, pageNum}, Locale.SIMPLIFIED_CHINESE);
        String note = messageSource.getMessage("gzh.scheduled.task.sync.report.note", new Object[] {today, pageNum}, Locale.SIMPLIFIED_CHINESE);
        note = note + "\n" + "Success count: " + successCount + "\n" + "Fail count: " + failCount + "\n" + "Skil count: " + skipCount;
        int itemCount = skipCount + successCount + failCount;
        taskResult.put("taskName", taskName);
        taskResult.put("note", note);
        taskResult.put("itemCount", String.valueOf(itemCount));
        
        return taskResult;
    }
    
    private Map<String, String> importGzhClientCompanyReport(GzhClientCompany gzhClientCompany) {
        Map<String, String> importResult = new HashMap<String, String>();
        boolean clearIfExist = false;
        String updatedSyncStartPeriod = null;
        
        //It is assume the currentPeriod of the passed in gzhClientCompany is not null.
        //Otherwise, we can not decide the syncStopYearMonth.
        String currentPeriod = gzhClientCompany.getChanjetClientCompany().getCurrentPeriod();
        YearMonth currentPeriodMonth = YearMonth.parse(currentPeriod, DateTimeFormatter.ofPattern("yyyyMM"));
        YearMonth syncStopYearMonth = currentPeriodMonth.plusMonths(1);
        
        YearMonth syncStartPeriodMonth;
        String syncStartPeriod = gzhClientCompany.getChanjetClientCompany().getSyncStartPeriod();
        if (syncStartPeriod == null) 
            syncStartPeriodMonth = currentPeriodMonth;
        else
            syncStartPeriodMonth = YearMonth.parse(syncStartPeriod, DateTimeFormatter.ofPattern("yyyyMM")).plusMonths(1);
        
        //Iterate starting from syncStartPeriod(Exclusive) to currentPeriod(inclusive)
        for (YearMonth yearMonth = syncStartPeriodMonth; yearMonth.isBefore(syncStopYearMonth); yearMonth = yearMonth.plusMonths(1)) {
            String reportMonth = yearMonth.format(DateTimeFormatter.ofPattern("yyyyMM"));
            try {
                gzhClientCompanyService.importAssetReport(gzhClientCompany, reportMonth, clearIfExist);
                gzhClientCompanyService.importIncomeReport(gzhClientCompany, reportMonth, clearIfExist);
                gzhClientCompanyService.importCashReport(gzhClientCompany, reportMonth, clearIfExist);
                updatedSyncStartPeriod = reportMonth;
            } catch (Exception e) {
                log.info("importGzhClientCompanyReport received an exception from gzhClientCompanyService on gzhClientCompany=" 
                        + gzhClientCompany.getChanjetClientCompany().getName() + ", " + e.getLocalizedMessage());
                importResult.put("exception", e.getLocalizedMessage());
            }
        }
        
        //syncStartMonth is the last success month.
        //Unfortunately there is no other better way to set the syncStartMonth considering the following possible cases:
        //    1. When the gzhClientCompany is first imported, both syncStartPeriod and currentPeriod are null
        //    2. If there is an exception when importing a report, and there is a success importing a report for a later month, we have to move forward 
        //       the syncStartMonth forward. Otherwise, we may stuck to the exception for each following attempt and never update the syncStartPeriod.
        
        importResult.put("updatedSyncStartPeriod", updatedSyncStartPeriod);
        return importResult;
    }
    
    //Referal fee scheduled tasks
    //1:30 of every day
    public Map<String, String> updateReferalFeeStatus() {
        String today = LocalDate.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd"));
        Map<String, String> taskResult = new HashMap<String, String>();
        
        List<Referal> referalAccumulatingList = referalService.updateReferalFeeAccumulatingStatus();
        log.info("updateReferalFeeStatus, total " + referalAccumulatingList.size() + " referalFeeStatus changed from NOT_STARTED to ACCUMULATING.");
        //referalAccumulatingList.forEach(referal -> {
        //    GzhCompany gzhCompany = referal.getUser().getOwnedGzhCompany();
        //    log.info("Referal gzhCompany name=" + gzhCompany.getName() + ", activeBillingStartDate=" + gzhCompany.getActiveBillingStartDate());
        //});
        
        List<Referal> referalFinishedList = referalService.updateReferalFeeAccumulationEndedStatus();
        log.info("updateReferalFeeStatus, total " + referalFinishedList.size() + " referalFeeStatus changed from ACCUMULATING to ACCUMULATION_ENDED.");
        //referalFinishedList.forEach(referal -> {
        //    GzhCompany gzhCompany = referal.getUser().getOwnedGzhCompany();
        //    log.info("Referal gzhCompany name=" + gzhCompany.getName() + ", activeBillingStartDate=" + gzhCompany.getActiveBillingStartDate());
        //});
        
        String taskName = messageSource.getMessage("gzh.scheduled.task.update.referal.fee.status.taskName", new Object[] {today}, Locale.SIMPLIFIED_CHINESE);
        String note = messageSource.getMessage("gzh.scheduled.task.update.referal.fee.status.note", new Object[] {today}, Locale.SIMPLIFIED_CHINESE);
        String accumulating = messageSource.getMessage("gzh.scheduled.task.update.referal.fee.status.to.accumulating", null, Locale.SIMPLIFIED_CHINESE);
        accumulating = accumulating + ", Success count: " + referalAccumulatingList.size();
        String accumulationEnd = messageSource.getMessage("gzh.scheduled.task.update.referal.fee.status.to.accumulation.ended", null, Locale.SIMPLIFIED_CHINESE);
        accumulationEnd = accumulationEnd + ", Success count: " + referalFinishedList.size();
        note = note + "\n" + accumulating + "\n" + accumulationEnd;
        int itemCount = referalAccumulatingList.size() + referalFinishedList.size();
        taskResult.put("taskName", taskName);
        taskResult.put("note", note);
        taskResult.put("itemCount", String.valueOf(itemCount));
        
        return taskResult;
    }
    
    //2:00 of day 2 of every month
    public Map<String, String> generateAccumulatedReferalFee() {
        String today = LocalDate.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd"));
        Map<String, String> taskResult = new HashMap<String, String>();
        
        List<Referal> referalList = referalService.generateAccumulatedReferalFeeByEndOfLastMonth();
        LocalDate now = LocalDate.now();
        LocalDate endOfLastMonth = YearMonth.of(now.getYear(), now.getMonth()).minusMonths(1).atEndOfMonth();
        
        log.info("generateAccumulatedReferalFee, total " + referalList.size() + " accumulated referal fee are calcuated by end of " + endOfLastMonth);
        //referalList.forEach(referal -> {
        //    GzhCompany gzhCompany = referal.getUser().getOwnedGzhCompany();
        //    log.info("Referal gzhCompany name=" + gzhCompany.getName() + 
        //             ", referalFeeForParent=" + referal.getReferalFeeForParent() +
        //             ", referalFeeForGrandParent=" + referal.getReferalFeeForGrandParent() +
        //             ", referalFeeCurrentEndDate=" + referal.getReferalFeeCurrentEndDate() +
        //             ", referalFeeStatus=" + referal.getReferalFeeStatus());
        //});
        
        String taskName = messageSource.getMessage("gzh.scheduled.task.generate.accumulated.referal.fee.taskName", new Object[] {today}, Locale.SIMPLIFIED_CHINESE);
        int itemCount = referalList.size();
        String note = messageSource.getMessage("gzh.scheduled.task.generate.accumulated.referal.fee.note", new Object[] {today}, Locale.SIMPLIFIED_CHINESE);
        note = note + "\n" + "Success count: " + referalList.size();
        taskResult.put("taskName", taskName);
        taskResult.put("note", note);
        taskResult.put("itemCount", String.valueOf(itemCount));
        
        return taskResult;
    }
    
    //3:00 of day 2 of every month
    public Map<String, String> updateReferalFeeAccountingCompletedStatus() {
        String today = LocalDate.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd"));
        Map<String, String> taskResult = new HashMap<String, String>();
        
        //updateReferalFeeAccountingCompletedStatus will access LAZY loaded parentReferal
        if(!TransactionSynchronizationManager.hasResource(entityManagerFactory)) {
            log.info("updateReferalFeeAccountingCompletedStatus entityManager not bound to thread!");
            EntityManager entityManager = entityManagerFactory.createEntityManager();
            TransactionSynchronizationManager.bindResource(entityManagerFactory, new EntityManagerHolder(entityManager));
        };
        
        String operator = messageSource.getMessage("gzh.gzhcompany.operator.system.auto", null, Locale.SIMPLIFIED_CHINESE);
        List<Referal> referalList = referalService.updateReferalFeeAccountingCompletedStatus(operator);
        
        log.info("updateReferalFeeAccountingCompletedStatus, total " + referalList.size() + " referal fee are accounted to parent referal and grandparent referal.");
        //referalList.forEach(referal -> {
        //    GzhCompany gzhCompany = referal.getUser().getOwnedGzhCompany();
        //    log.info("Referal gzhCompany name=" + gzhCompany.getName() + 
        //            ", referalFeeForParent=" + referal.getReferalFeeForParent() +
        //            ", referalFeeForGrandParent=" + referal.getReferalFeeForGrandParent() +
        //            ", referalFeeCurrentEndDate=" + referal.getReferalFeeCurrentEndDate() +
        //            ", referalFeeStatus=" + referal.getReferalFeeStatus());
        //});
        
        if(TransactionSynchronizationManager.hasResource(entityManagerFactory)) {
            log.info("updateReferalFeeAccountingCompletedStatus finished updating referal fee accounting completed status. Unbind entityManager from thread!");
            TransactionSynchronizationManager.unbindResource(entityManagerFactory);
        };
        
        String taskName = messageSource.getMessage("gzh.scheduled.task.update.referal.fee.accounting.status.taskName", new Object[] {today}, Locale.SIMPLIFIED_CHINESE);
        String note = messageSource.getMessage("gzh.scheduled.task.update.referal.fee.accounting.status.note", new Object[] {today}, Locale.SIMPLIFIED_CHINESE);
        int itemCount = referalList.size();
        note = note + "\n" + "Success count: " + referalList.size();
        taskResult.put("taskName", taskName);
        taskResult.put("note", note);
        taskResult.put("itemCount", String.valueOf(itemCount));
        
        return taskResult;
    }
}
