package com.cju.billing;

import org.springframework.data.querydsl.QueryDslPredicateExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface BillingStatusChangeRepository extends PagingAndSortingRepository<BillingStatusChange, Long>, QueryDslPredicateExecutor<BillingStatusChange> {
}