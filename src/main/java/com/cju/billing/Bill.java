package com.cju.billing;

import java.time.LocalDate;
import java.time.OffsetDateTime;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.persistence.UniqueConstraint;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

@Entity
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id", scope = Bill.class)
@Table(name = "Bill",
       uniqueConstraints = {@UniqueConstraint(columnNames={"accountId", "billMonth"}, name="Bill_UNIQUE_AccountAndBillMonth")})
public class Bill {
    @Id    
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    private UUID billId;
    @Column(columnDefinition= "TIMESTAMP WITH TIME ZONE")
    private OffsetDateTime timeStamp;
    private String billMonth;
    private LocalDate periodStart;
    private LocalDate periodEnd;
    @Enumerated(EnumType.STRING) 
    private BillingRate billingRate;
    private long  numOfGzhClientCompany;
    private double billingDays;
    private double amount;
    private String operator;
    @Transient
    private String errMsg = null;
    
    //Bill should reference to Account rather than GzhCompany since bill is related to account more closely.
    //When a bill is generated the corresponding account balance is adjusted.
    //@JsonIgnore means do not serialize the field into Json.
    @JsonIgnore
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="accountId", foreignKey = @ForeignKey(name = "Bill_FK_AccountId"))
    private Account account;
    
    public Bill() {}
    
    public void setId(long id) {
        this.id = id;
    }
    public long getId() {
        return id;
    }
    
    public void setBillId(UUID billId) {
        this.billId = billId;
    }
    public UUID getBillId() {
        return billId;
    }
    
    public void setTimeStamp(OffsetDateTime timeStamp) {
        this.timeStamp = timeStamp;
    }
    public OffsetDateTime getTimeStamp() {
        return timeStamp;
    }
    
    public void setBillMonth(String billMonth) {
        this.billMonth = billMonth;
    }
    public String getBillMonth() {
        return billMonth;
    }
    
    public void setPeriodStart(LocalDate periodStart) {
        this.periodStart = periodStart;
    }
    public LocalDate getPeriodStart() {
        return periodStart;
    }
    
    public void setPeriodEnd(LocalDate periodEnd) {
        this.periodEnd = periodEnd;
    }
    public LocalDate getPeriodEnd() {
        return periodEnd;
    }
    
    public void setBillingRate(BillingRate billingRate) {
        this.billingRate = billingRate;
    }
    public BillingRate getBillingRate() {
        return billingRate;
    }
    
    public void setNumOfGzhClientCompany(long numOfGzhClientCompany) {
        this.numOfGzhClientCompany = numOfGzhClientCompany;
    }
    public long getNumOfGzhClientCompany() {
        return numOfGzhClientCompany;
    }
    
    public void setBillingDays(double billingDays) {
        this.billingDays = billingDays;
    }
    public double getBillingDays() {
        return billingDays;
    }
    
    public void setAmount(double amount) {
        this.amount = amount;
    }
    public double getAmount() {
        return amount;
    }
    
    public void setOperator(String operator) {
        this.operator = operator;
    }
    public String getOperator() {
        return operator;
    }
    
    public void setErrMsg(String errMsg) {
        this.errMsg = errMsg;
    }
    public String getErrMsg() {
        return errMsg;
    }
    
    public void setAccount(Account account) {
        this.account = account;
    }
    public Account getAccount() {
        return account;
    }
    
    @Override
    public String toString() {
        return String.format(
                "Bill[id=%s, periodStart='%s', periodEnd='%s']",
                id, periodStart, periodStart);
    }

}
