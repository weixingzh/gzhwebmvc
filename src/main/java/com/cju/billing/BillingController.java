package com.cju.billing;

import java.io.IOException;
import java.time.LocalDate;
import java.time.OffsetDateTime;
import java.util.List;
import java.util.Locale;
import javax.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import com.cju.gzhOps.GzhCompany;
import com.cju.gzhOps.GzhCompanyBillingStatus;
import com.cju.gzhOps.GzhCompanyPaymentStatus;
import com.cju.gzhOps.GzhCompanyRepository;
import com.cju.gzhOps.GzhCompanyServiceRunningStatus;
import com.cju.security.RepositoryQueryFilter;
import com.cju.user.User;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.querydsl.core.types.Predicate;

//The controller can be made @Transactional, but indeed it's a common recommendation to only make the service layer transactional (the persistence layer should not be transactional either).
//The reason for this is not technical feasibility, but separation of concerns. 
//The controller responsibility is to get the parameter requests, and then call one or more service methods and combine the results in a response that is then sent back to the client.

//'CRUD methods on repository instances are transactional by default' means that by default they run in propagation REQUIRED, 
//meaning they join an ongoing transaction and if none is present run in their own transaction.

@Controller
public class BillingController {
    private Logger log = LoggerFactory.getLogger(BillingController.class);
    
    @Autowired
    private BillingService billingService;
    
    @Autowired 
    private GzhCompanyRepository gzhCompanyRepository; 
    
    @Autowired 
    private AccountRepository accountRepository; 
    
    @Autowired 
    private TransactionRepository transactionRepository; 
    
    @Autowired 
    private BillRepository billRepository; 
    
    @Autowired 
    private BillingStatusChangeRepository billingStatusChangeRepository; 
    
    @Autowired 
    RepositoryQueryFilter repoQueryFilter;
    
    @Autowired
    private MessageSource messageSource;
    
    @Autowired
    private ObjectMapper objectMapper;
    
    @PreAuthorize("isAuthenticated()")
    @GetMapping("/gzhOps/billing/myAccount/details")
    public String getGzhCompanyMyAccountDetails(@AuthenticationPrincipal User user, Pageable pageable, Model model) {  
        GzhCompany gzhCompany = user.getAdminedGzhCompany();
        if (gzhCompany == null) {
            String message = messageSource.getMessage("gzh.gzhcompany.admined.not.bound", null, Locale.SIMPLIFIED_CHINESE);
            model.addAttribute("message", message);
            return "gzhOps/gzhOpsMessage";
        }
        model.addAttribute("account", gzhCompany.getAccount());
        
        Predicate predicate = repoQueryFilter.filterTransactionByAccountId(gzhCompany.getAccount().getId());
        Page<Transaction> transactionPage = transactionRepository.findAll(predicate, pageable);
        model.addAttribute("transactionPage", transactionPage);
        log.info("getGzhCompanyAccountBalance transactionPage.size=" + transactionPage.getSize() + ", transactionPage.Number " + transactionPage.getNumber());
        
        return "billing/accountDetails";
    }
    
    @PreAuthorize("isAuthenticated()")
    @GetMapping("/gzhOps/billing/bill/list")
    public String getGzhCompanyBillList(@AuthenticationPrincipal User user, Pageable pageable, Model model) { 
        Predicate predicate = repoQueryFilter.filterBillByUser(user);
        Page<Bill> billPage = billRepository.findAll(predicate, pageable);
        model.addAttribute("billPage", billPage);
        log.info("getGzhCompanyBillList billPage.size=" + billPage.getSize() + ", billPage.Number " + billPage.getNumber());
        
        return "billing/billList";
    }
    
    @PreAuthorize("isWebAdmin()")
    @GetMapping("/gzhOps/billing/gzhCompany/{id}/bill/list")
    public String getGzhCompanyBillListForOneGzhCompany(@PathVariable("id") long id, Pageable pageable, Model model) { 
        Predicate predicate = repoQueryFilter.filterBillByGzhCompanyId(id);
        Page<Bill> billPage = billRepository.findAll(predicate, pageable);
        model.addAttribute("billPage", billPage);
        log.info("getGzhCompanyBillListForOneGzhCompany billPage.size=" + billPage.getSize() + ", billPage.Number " + billPage.getNumber());
        
        return "billing/billList";
    }
    
    @PreAuthorize("isWebAdmin() || isGzhAdminForBill(#id)")
    @GetMapping("/gzhOps/billing/bill/{id}/details")
    public String getGzhCompanyBillDetails(@PathVariable("id") long id, Model model) { 
        Bill bill = billRepository.findOne(id);
        model.addAttribute("bill", bill);
        
        return "billing/billDetails";
    }
    
    @PreAuthorize("isWebAdmin()")
    @GetMapping("/gzhOps/billing/transaction/list")
    public String getGzhCompanyTransactionList(@AuthenticationPrincipal User user,  Pageable pageable, Model model) { 
        Predicate predicate = repoQueryFilter.filterTransactionByUser(user);
        Page<Transaction> transactionPage = transactionRepository.findAll(predicate, pageable);
        model.addAttribute("transactionPage", transactionPage);
        log.info("getGzhCompanyTransactionList transactionPage.size=" + transactionPage.getSize() + ", transactionPage.Number " + transactionPage.getNumber());

        return "billing/transactionList";
    }
    
    @PreAuthorize("isWebAdmin() || isGzhAdminForTransaction(#id)")
    @GetMapping("/gzhOps/billing/transaction/{id}/details")
    public String getGzhCompanyTransactionDetails(@PathVariable("id") long id, Model model) { 
        Transaction transaction = transactionRepository.findOne(id);
        model.addAttribute("transaction", transaction);
        
        String href = null;
        if (transaction.getTransactionType() == TransactionType.BILL_AUTO_DEBIT) {
            //billId is UUID
            String billId = transaction.getTransactionRefNum();
            Predicate predicate = repoQueryFilter.filterBillByBillId(billId);
            Bill bill = billRepository.findOne(predicate);
            href ="/gzhOps/billing/bill/" + bill.getId() + "/details";
        }
        else if (transaction.getTransactionType() == TransactionType.BILLING_ACTIVATION_CHARGE) {
            //statusChangeId is UUID
            String statusChangeId = transaction.getTransactionRefNum();
            Predicate predicate = repoQueryFilter.filterBillingStatusChangeByBillignStatusChangeId(statusChangeId);
            BillingStatusChange billingStatusChange = billingStatusChangeRepository.findOne(predicate);
            href ="/gzhOps/billing/statusChange/" + billingStatusChange.getId() + "/details";
        }
        
        //empty href in browser will reload the current page 
        model.addAttribute("link", href);
        return "billing/transactionDetails";
    }
    
    
    @PreAuthorize("isAuthenticated()")
    @GetMapping("/gzhOps/billing/statusChange/list")
    public String getBillingStatusChangeList(@AuthenticationPrincipal User user, Pageable pageable, Model model) { 
        Predicate predicate = repoQueryFilter.filterBillingStatusChangeByUser(user);
        
        Page<BillingStatusChange> billingStatusChangePage = billingStatusChangeRepository.findAll(predicate, pageable);
        model.addAttribute("billingStatusChangePage", billingStatusChangePage);
        log.info("getBillingStatusChangeList getBillingStatusChangeList.size=" + billingStatusChangePage.getSize() 
               + ", billingStatusChangePage.Number " + billingStatusChangePage.getNumber());
        
        return "billing/billingStatusChangeList";
    }
    
    @PreAuthorize("isWebAdmin() || isGzhAdminForBillingStatusChange(#id)")
    @GetMapping("/gzhOps/billing/statusChange/{id}/details")
    public String getBillingStatusChangeDetails(@PathVariable("id") long id, Pageable pageable, Model model) {  
        BillingStatusChange billingStatusChange = billingStatusChangeRepository.findOne(id);
        model.addAttribute("billingStatusChange", billingStatusChange);
        
        return "billing/billingStatusChangeDetails";
    }
    
    //WebAdmin Only Operations
    @PreAuthorize("isWebAdmin()")
    @GetMapping("/gzhOps/billing/account/list")
    public String getGzhCompanyAccountList(Pageable pageable, Model model) {  
        Page<Account> accountPage = accountRepository.findAll(pageable);
        model.addAttribute("accountPage", accountPage);
        log.info("getGzhCompanyAccountList accountPage.size=" + accountPage.getSize() + ", accountPage.Number " + accountPage.getNumber());
        
        return "billing/accountList";
    }
    
    @PreAuthorize("isWebAdmin()")
    @GetMapping("/gzhOps/billing/account/{id}/details")
    public String getGzhCompanyAccountDetails(@PathVariable("id") long id, Pageable pageable, Model model) {  
        Account account = accountRepository.findOne(id);
        model.addAttribute("account", account);
        
        Predicate predicate = repoQueryFilter.filterTransactionByAccountId(id);
        Page<Transaction> transactionPage = transactionRepository.findAll(predicate, pageable);
        model.addAttribute("transactionPage", transactionPage);
        log.info("getGzhCompanyAccountBalance transactionPage.size=" + transactionPage.getSize() + ", transactionPage.Number " + transactionPage.getNumber());
        
        return "billing/accountDetails";
    }
    
    @PreAuthorize("isWebAdmin()")
    @GetMapping("/gzhOps/billing/gzhCompany/{id}/bill/generate")
    public String getGzhCompanyBillGenerateForm(@PathVariable("id") long id, Model model) {  
        GzhCompany gzhCompany = gzhCompanyRepository.findOne(id);
        model.addAttribute("gzhCompany", gzhCompany);
        
        return "billing/billGenerate";
    }
    
    //We use HTTP Get to generate bills for all accounts
    //So it is not guaranteed to get the same result over two calls on this URL
    @PreAuthorize("isWebAdmin()")
    @GetMapping("/gzhOps/billing/gzhCompany/bill/generateAll")
    public String getGzhCompanyBillGenerateAll(@AuthenticationPrincipal User user, Model model) {  
        List<Bill> thisMonthBillList = billingService.generateBillForAllGzhCompany(user.getUserName());
        model.addAttribute("billList", thisMonthBillList);
        
        int size = thisMonthBillList.size();
        String message = messageSource.getMessage("gzh.gzhcompany.billing.generate.all.summary", 
                new Object[] {size}, Locale.SIMPLIFIED_CHINESE);
        model.addAttribute("message", message);
        
        return "billing/billGenerateAllList";
    }

    
    @PreAuthorize("isWebAdmin()")
    @GetMapping("/gzhOps/billing/updateInDebt")
    public String updateGzhCompanyAccountInDebtStatus(Model model) {  
        List<GzhCompany> gzhCompanyList = billingService.updateGzhCompanyInDebt();
        int size = gzhCompanyList.size();
        GzhCompanyPaymentStatus paymentStatus = GzhCompanyPaymentStatus.IN_DEBT;
        
        model.addAttribute("gzhCompanyList", gzhCompanyList);
        String message = messageSource.getMessage("gzh.gzhcompany.payment.status.change.summary", 
                new Object[] {size, paymentStatus}, Locale.SIMPLIFIED_CHINESE);
        model.addAttribute("message", message);
        
        return "billing/updateStatusList";
    }
    
    @PreAuthorize("isWebAdmin()")
    @GetMapping("/gzhOps/billing/updateOverDebt")
    public String updateGzhCompanyAccountOverDebt(Model model) {  
        List<GzhCompany> gzhCompanyList = billingService.updateGzhCompanyOverDebt();
        int size = gzhCompanyList.size();
        GzhCompanyPaymentStatus paymentStatus = GzhCompanyPaymentStatus.OVER_DEBT;
        
        model.addAttribute("gzhCompanyList", gzhCompanyList);
        String message = messageSource.getMessage("gzh.gzhcompany.payment.status.change.summary", 
                new Object[] {size, paymentStatus}, Locale.SIMPLIFIED_CHINESE);
        model.addAttribute("message", message);
        
        return "billing/updateStatusList";
    }
    
    @PreAuthorize("isWebAdmin()")
    @GetMapping("/gzhOps/billing/account/{id}/balance/adjust")
    public String getGzhCompanyAccountBalanceAdjustForm(@PathVariable("id") long id, Model model) {  
        Account account = accountRepository.findOne(id);
        model.addAttribute("account", account);
        
        return "billing/accountBalanceAdjust";
    }


    //REST API
    @PreAuthorize("isWebAdmin()")
    @PostMapping("/gzhOps/api/v1/billing/bill/generate")
    public @ResponseBody String postGzhCompanyBillGenerateApi(@AuthenticationPrincipal User user, @RequestBody JsonNode json ) throws IOException  {
        long id = json.path("id").asLong();
        GzhCompany gzhCompany = gzhCompanyRepository.findOne(id);
        String billMonth = json.path("billMonth").asText();
        
        //generateBill will store the bill in DB, adjust account balance according to bill, and add a transaction to record the account balance change
        //If the bill already exist, generateBill will simply return the existing bill
        Bill bill = billingService.generateBillForGzhCompanyForBillMonth(gzhCompany, billMonth, user.getUserName());
        
        if (bill.getErrMsg() != null) 
            return bill.getErrMsg();
        
        String billJson = objectMapper.writeValueAsString(bill);
        return billJson;
    }
    
    @PreAuthorize("isWebAdmin()")
    @PostMapping("/gzhOps/api/v1/account/balance/adjust")
    public @ResponseBody String postGzhCompanyAccountBalanceAdjustApi(@AuthenticationPrincipal User user, @RequestBody JsonNode json ) throws IOException  {
        long id = json.path("id").asLong();
        Account account = accountRepository.findOne(id);
        double adjustAmount = json.path("adjustAmount").asDouble();
        String adjustNote = json.path("adjustNote").asText();
        TransactionType txType = TransactionType.MANUAL_BALANCE_ADJUST;
        String txRefNum = "";
        
        //For manual balance adjust, the txRefNum is empty. The adjustNote field is expected to have information on what triggers the adjust and 
        //corresponding txRefNum if there is any.
        
        //generateBill will store the bill in DB, adjust account balance according to bill, and add a transaction to record the account balance change
        //If the bill already exist, generateBill will simply return the existing bill
        account = billingService.adjustAccountBalance(account, adjustAmount, adjustNote, txType, txRefNum, user.getUserName());
        
        String responseJson = objectMapper.writeValueAsString(json);
        return  responseJson;
    }
    
    //We use HTTP Get to perform a check and update the state of GzhCompany
    //So it is not guaranteed to get the same result over two calls on this URL
    @PreAuthorize("isWebAdmin()")
    @GetMapping("/gzhOps/billing/updateTrial")
    public String updateBillingStatusFromTrialToActive(@AuthenticationPrincipal User user, Model model) {  
        
        List<GzhCompany> gzhCompanyList = billingService.updateBillingStatusFromTrialToActive(user.getUserName());
        int size = gzhCompanyList.size();
        
        model.addAttribute("gzhCompanyList", gzhCompanyList);
        String message = messageSource.getMessage("gzh.gzhcompany.billing.status.change.summary", 
                new Object[] {size, GzhCompanyBillingStatus.ACTIVE}, Locale.SIMPLIFIED_CHINESE);
        model.addAttribute("message", message);
        
        return "billing/updateStatusList";
    }
    
    @PreAuthorize("isGzhAdmin(#id) || isWebAdmin()")
    @PostMapping("/gzhOps/api/v1/gzhCompany/startTrial/{id}")
    public @ResponseBody String gzhCompanyStartTrialApi(@AuthenticationPrincipal User user, @PathVariable("id") long id, HttpServletResponse response) throws IOException  {
        
        String message = messageSource.getMessage("gzh.gzhcompany.trial.start.success", null, Locale.SIMPLIFIED_CHINESE);
        GzhCompany gzhCompany = gzhCompanyRepository.findOne(id);
        
        if (!gzhCompany.getBillingStatus().equals(GzhCompanyBillingStatus.TRIAL_NOT_STARTED))
        {
            message = messageSource.getMessage("gzh.gzhcompany.billing.trial.already.started.cannot.start.trial", null, Locale.SIMPLIFIED_CHINESE);
            response.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.value());
            return message;
        }

        gzhCompany.setBillingStatus(GzhCompanyBillingStatus.TRIAL);
        gzhCompany.setTrialStartDate(LocalDate.now());
        gzhCompany.setActiveBillingStartDate(LocalDate.now().plusMonths(1));
        gzhCompanyRepository.save(gzhCompany);
        
        //update the gzhComapny in the cached user object
        if (user.getAdminedGzhCompany() != null && user.getAdminedGzhCompany().getId() == id)
            user.setAdminedGzhCompany(gzhCompany);
        if (user.getOwnedGzhCompany() != null && user.getOwnedGzhCompany().getId() == id)
            user.setOwnedGzhCompany(gzhCompany);
        
        return  message;
    }
    
    @PreAuthorize("isGzhOwner(#id) || isWebAdmin()")
    @PostMapping("/gzhOps/api/v1/gzhCompany/stopBilling/{id}")
    public @ResponseBody String gzhCompanyStopBillingApi(@AuthenticationPrincipal User user, @PathVariable("id") long id, HttpServletResponse response) throws IOException  {
        
        String message = messageSource.getMessage("gzh.gzhcompany.billing.stop.success", null, Locale.SIMPLIFIED_CHINESE);
        GzhCompany gzhCompany = gzhCompanyRepository.findOne(id);
        
        GzhCompanyBillingStatus billingStatusBefore = gzhCompany.getBillingStatus();
        
        if (billingStatusBefore.equals(GzhCompanyBillingStatus.TRIAL_NOT_STARTED))
        {
            message = messageSource.getMessage("gzh.gzhcompany.billing.trial.not.started.cannot.stop.billing", null, Locale.SIMPLIFIED_CHINESE);
            response.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.value());
            return message;
        }
        
        if (billingStatusBefore.equals(GzhCompanyBillingStatus.STOPPED))
        {
            message = messageSource.getMessage("gzh.gzhcompany.billing.already.stopped.cannot.stop.billing", null, Locale.SIMPLIFIED_CHINESE);
            response.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.value());
            return message;
        }
        
        //A user can stop billing in either trial or activeBilling state
        GzhCompanyBillingStatus billingStatusAfter = GzhCompanyBillingStatus.STOPPED;
        gzhCompany.setBillingStatus(billingStatusAfter);
        gzhCompany.setServiceRunningStatus(GzhCompanyServiceRunningStatus.STOPPED);
        gzhCompanyRepository.save(gzhCompany);
        
        OffsetDateTime now = OffsetDateTime.now();
        billingService.recordBillingStatusChange(gzhCompany, billingStatusBefore, billingStatusAfter, user.getUserName(), now);
        
        //Do not forget to update the gzhComapny in the cached user object
        if (user.getAdminedGzhCompany() != null && user.getAdminedGzhCompany().getId() == id)
            user.setAdminedGzhCompany(gzhCompany);
        if (user.getOwnedGzhCompany() != null && user.getOwnedGzhCompany().getId() == id)
            user.setOwnedGzhCompany(gzhCompany);
        
        return  message;
    }
    
    @PreAuthorize("isGzhOwner(#id) || isWebAdmin()")
    @PostMapping("/gzhOps/api/v1/gzhCompany/restartBilling/{id}")
    public @ResponseBody String gzhCompanyRestartBillingApi(@AuthenticationPrincipal User user, @PathVariable("id") long id, HttpServletResponse response) throws IOException  {
        
        String message = messageSource.getMessage("gzh.gzhcompany.billing.restart.success", null, Locale.SIMPLIFIED_CHINESE);
        GzhCompany gzhCompany = gzhCompanyRepository.findOne(id);
        GzhCompanyBillingStatus billingStatusBefore = gzhCompany.getBillingStatus();
        
        //Only if the billingStatus is STOPPED, can a user do billing restart
        if (!billingStatusBefore.equals(GzhCompanyBillingStatus.STOPPED))
        {
            message = messageSource.getMessage("gzh.gzhcompany.billing.not.stopped.cannot.restart.billing", null, Locale.SIMPLIFIED_CHINESE);
            response.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.value());
            return message;
        }
        
        //Only if the account balance is >= 0, can a user do billing restart
        if (gzhCompany.getAccount().getBalance() < 0)
        {
            message = messageSource.getMessage("gzh.gzhcompany.billing.negtive.balance.cannot.restart.billing", null, Locale.SIMPLIFIED_CHINESE);
            response.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.value());
            return message;
        }
        
        LocalDate billingRestartDate = LocalDate.now();
        GzhCompanyBillingStatus billingStatusAfter = null;
        if (billingRestartDate.isAfter(gzhCompany.getActiveBillingStartDate())) 
            billingStatusAfter = GzhCompanyBillingStatus.ACTIVE;
        else 
            billingStatusAfter = GzhCompanyBillingStatus.TRIAL;

        gzhCompany.setBillingStatus(billingStatusAfter);
        gzhCompanyRepository.save(gzhCompany);
        
        //Keep a record for all billing status changes
        OffsetDateTime now = OffsetDateTime.now();
        BillingStatusChange billingStatusChange = billingService.recordBillingStatusChange(gzhCompany, billingStatusBefore, billingStatusAfter, user.getUserName(), now);
        
        //Charge fee for restart billing, even after restart the billing status is TRIAL
        billingService.chargeAccountForBillingActivationFromStoppedStatus(gzhCompany.getAccount(), billingStatusChange, user.getUserName());
        
        //Do not forget to update the gzhComapny in the cached user object
        if (user.getAdminedGzhCompany() != null && user.getAdminedGzhCompany().getId() == id)
            user.setAdminedGzhCompany(gzhCompany);
        if (user.getOwnedGzhCompany() != null && user.getOwnedGzhCompany().getId() == id)
            user.setOwnedGzhCompany(gzhCompany);
        
        return  message;
    }
    
}
    