package com.cju.billing;

import java.time.OffsetDateTime;
import java.util.UUID;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

@Entity
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id", scope = Transaction.class)
@Table(name = "Transaction",
       uniqueConstraints = {@UniqueConstraint(columnNames={"transactionRefNum"}, name="Transaction_UNIQUE_TransactionRefNum")})
public class Transaction {
    @Id    
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    
    private UUID txId;
    @Column(columnDefinition= "TIMESTAMP WITH TIME ZONE")
    private OffsetDateTime timeStamp;
    @Enumerated(EnumType.STRING) 
    private TransactionType transactionType;
    private String transactionRefNum;
    private double transactionAmount;
    private double accountBalanceBefore;
    private double accountBalanceAfter;
    private String operator;
    //This is set to the internalAccountName of the assocationed account when the transaction record is create.
    //This information looks duplicate and is kept in DB for internal use only.
    private String internalAccountOwnerName;
    
    //text has no size limit
    @Column(columnDefinition = "text")
    private String note;
        
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="accountId", foreignKey = @ForeignKey(name = "Transaction_FK_AccountId"))
    private Account account;
    
    public Transaction() {}
    
    public void setId(long id) {
        this.id = id;
    }
    public long getId() {
        return id;
    }
    
    public void setTxId(UUID txId) {
        this.txId = txId;
    }
    public UUID getTxId() {
        return txId;
    }
    
    public void setTimeStamp(OffsetDateTime timeStamp) {
        this.timeStamp = timeStamp;
    }
    public OffsetDateTime getTimeStamp() {
        return timeStamp;
    }
    
    public void setTransactionType(TransactionType transactionType) {
        this.transactionType = transactionType;
    }
    public TransactionType getTransactionType() {
        return transactionType;
    }

    public void setTransactionRefNum(String transactionRefNum) {
        this.transactionRefNum = transactionRefNum;
    }
    public String getTransactionRefNum() {
        return transactionRefNum;
    }
    
    public void setTransactionAmount(double transactionAmount) {
        this.transactionAmount = transactionAmount;
    }
    public double getTransactionAmount() {
        return transactionAmount;
    }
    
    public void setAccountBalanceBefore(double accountBalanceBefore) {
        this.accountBalanceBefore = accountBalanceBefore;
    }
    public double getAccountBalanceBefore() {
        return accountBalanceBefore;
    }
    
    public void setAccountBalanceAfter(double accountBalanceAfter) {
        this.accountBalanceAfter = accountBalanceAfter;
    }
    public double getAccountBalanceAfter() {
        return accountBalanceAfter;
    }
    
    public void setOperator(String operator) {
        this.operator = operator;
    }
    public String getOperator() {
        return operator;
    }
    
    public void setInternalAccountOwnerName(String internalAccountOwnerName) {
        this.internalAccountOwnerName = internalAccountOwnerName;
    }
    public String getInternalAccountOwnerName() {
        return internalAccountOwnerName;
    }
    
    public void setNote(String note) {
        this.note = note;
    }
    public String getNote() {
        return note;
    }
    
    public void setAccount(Account account) {
        this.account = account;
    }
    public Account getAccount() {
        return account;
    }
    
    @Override
    public String toString() {
        return String.format(
                "Transaction[id=%s, timeStamp='%s', transactionType='%s', transactionAmount='%f', accountBalanceBefore='%f', accountBalanceAfter='%f', txId='%s']",
                id, timeStamp, transactionType, transactionAmount, accountBalanceBefore, accountBalanceAfter, txId);
    }

}
