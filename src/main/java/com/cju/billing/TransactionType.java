package com.cju.billing;

import com.fasterxml.jackson.annotation.JsonProperty;

public enum TransactionType {
    @JsonProperty("微信充值") WEIXIN_RECHARGE("winXinRecharge"),  
    @JsonProperty("账单自动扣费") BILL_AUTO_DEBIT ("BillAutoDebit"),
    @JsonProperty("重启计费收费") BILLING_ACTIVATION_CHARGE ("BillingActivationCharge"),
    @JsonProperty("手工余额调整") MANUAL_BALANCE_ADJUST ("ManualBalanceAdjust");
    private String type;
    
    private TransactionType(String type) {
        this.type = type;
    }
    
    @Override
    public String toString() {
        switch (type) {
            case "winXinRecharge": 
                return "微信充值";
            case "BillAutoDebit": 
                return "账单自动扣费";
            case "BillingActivationCharge": 
                return "重启计费收费";
            case "ManualBalanceAdjust": 
                return "手工余额调整";
            default: 
                return "未知交易类型";
        }
    }
}