package com.cju.billing;


import com.fasterxml.jackson.annotation.JsonProperty;

public enum BillingRate {
    @JsonProperty("基本费率") BASIC("Basic"),  @JsonProperty("基本费率+客户公司包") BASIC_PLUS_CLIENTCOMPANY("BasicPlusClientCompany");
    private String type;
    
    private BillingRate(String type) {
        this.type = type;
    }
    
    @Override
    public String toString() {
        switch (type) {
            case "Basic": 
                return "基本费率";
            case "BasicPlusClientCompany": 
                return "基本费率+客户公司包";
            default: 
                return "基本费率";
        }
    }
}
