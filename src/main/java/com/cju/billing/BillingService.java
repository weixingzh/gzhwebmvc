package com.cju.billing;

import java.time.LocalDate;
import java.time.OffsetDateTime;
import java.time.YearMonth;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.UUID;
import org.apache.commons.math3.util.Precision;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.data.querydsl.QSort;
import org.springframework.stereotype.Service;
import com.cju.gzhOps.GzhClientCompanyRepository;
import com.cju.gzhOps.GzhCompany;
import com.cju.gzhOps.GzhCompanyBillingStatus;
import com.cju.gzhOps.GzhCompanyPaymentStatus;
import com.cju.gzhOps.GzhCompanyRepository;
import com.cju.gzhOps.GzhCompanyServiceRunningStatus;
import com.cju.gzhOps.GzhCompanyServiceType;
import com.cju.security.RepositoryQueryFilter;
import com.querydsl.core.types.Predicate;

@Service
public class BillingService {
    private Logger log = LoggerFactory.getLogger(BillingService.class);
    
    final double BASE_AMOUNT = 500.00;
    final double BASE_AMOUNT_WITHOUT_GZH_CLIENT_COMPANY = 200.00;
    final double PER_GZHCLIENTCOMPANY = 5.00;
    final double BILLING_ACTIVATION_FEE = 200.00;
    final int INDEBT_UPDATE_DAY_OF_MONTH = 1;
    final int OVERDEBT_UPDATE_DAYS_AFTER_INDEBT = 1;
    
    @Autowired 
    private GzhCompanyRepository gzhCompanyRepository;
    @Autowired 
    private GzhClientCompanyRepository gzhClientCompanyRepository;
    @Autowired 
    private AccountRepository accountRepository; 
    @Autowired 
    private BillRepository billRepository;  
    @Autowired 
    private BillingStatusChangeRepository billingStatusChangeRepository;  
    @Autowired 
    private TransactionRepository transactionRepository; 
    @Autowired 
    RepositoryQueryFilter repoQueryFilter;
    @Autowired
    private MessageSource messageSource;
    
    private BillingRate calculateBillingRate(GzhCompany gzhCompany) {
        if (gzhCompany.getServiceType() == GzhCompanyServiceType.GSSWDL_A)
            return BillingRate.BASIC_PLUS_CLIENTCOMPANY;
        else {
            //Right now, only gandongtech does not have ServiceType of GSSWDL_A.
            return BillingRate.BASIC;
        }
    }
    
    private long getNumOfGzhClientCompanyByImportedDate(long gzhCompanyId, LocalDate cutoffDate) {
        long count = gzhClientCompanyRepository.countByGzhCompanyIdAndCutoffDate(gzhCompanyId, cutoffDate);
        return count;
    }
    
    private double calculateAdjustAmount(GzhCompany gzhCompany) {
        //marketing discount is calculated here.
        return 0;
    }
    
    private double calculateBillAmount(GzhCompany gzhCompany, BillingRate billingRate, long numOfGzhClientCompany) {
        double amount = 0;
        if (billingRate == BillingRate.BASIC) {
            //Right now, only gandongtech is in this case.
            amount = BASE_AMOUNT;
        }
        else if (billingRate == BillingRate.BASIC_PLUS_CLIENTCOMPANY) {
            if (numOfGzhClientCompany == 0) {
                //For those general purpose GzhCompany who does not have GzhClientCompany.
                amount = BASE_AMOUNT_WITHOUT_GZH_CLIENT_COMPANY;
            }
            else if (numOfGzhClientCompany <= 100)
                amount = BASE_AMOUNT;
            else 
                amount = BASE_AMOUNT + (numOfGzhClientCompany - 100) * PER_GZHCLIENTCOMPANY;
        }
        
        double adjust = calculateAdjustAmount(gzhCompany);
        
        return amount + adjust;
    }
    
    public List<Bill> generateBillForAllGzhCompany(String operator) {
        List<Bill> billList = new ArrayList<Bill>();
        LocalDate now = LocalDate.now();
        String billMonthStr = YearMonth.of(now.getYear(), now.getMonth()).minusMonths(1).toString();
        
        Iterable<GzhCompany> gzhCompanyItr = gzhCompanyRepository.findAll();
        gzhCompanyItr.forEach(gzhCompany -> {
            Bill bill = generateBillForGzhCompanyForBillMonth(gzhCompany, billMonthStr, operator); 
            if (bill.getErrMsg() == null)
                billList.add(bill);
        });
        
        return billList;
    }
    
    public Bill generateBillForGzhCompanyForBillMonth(GzhCompany gzhCompany, String billMonthStr, String operator) {
        Account account = gzhCompany.getAccount();
        Predicate predicate = repoQueryFilter.filterBillByAccountIdAndBillMonth(account.getId(), billMonthStr);
        //Only one bill for each month
        Bill bill = billRepository.findOne(predicate);
        if (bill != null) {
            log.info("generateBill gzhCompany=" + account.getGzhCompany().getName() + ", billMonth=" + billMonthStr + ". Bill already exists!");
            String message = messageSource.getMessage("gzh.billing.bill.already.generated", 
                    new Object[] {account.getGzhCompany().getName(), billMonthStr}, Locale.SIMPLIFIED_CHINESE);
            bill.setErrMsg(message);
            return bill;
        } 
        
        //generate a empty bill first
        bill = new Bill();
        
        YearMonth billMonth = YearMonth.parse(billMonthStr);
        LocalDate firstDayOfMonth = billMonth.atDay(1);
        LocalDate lastDayOfMonth = billMonth.atEndOfMonth();
        
        //Bill month is not finished yet, no bills
        //        if (lastDayOfMonth.isAfter(LocalDate.now())) {
        //            log.info("generateBill gzhCompany=" + gzhCompany.getName() + ", billMonth=" + billMonthStr + " is not finished yet!");
        //            String message = messageSource.getMessage("gzh.billing.bill.month.not.finished", new Object[] {billMonthStr}, Locale.SIMPLIFIED_CHINESE);
        //            bill.setErrMsg(message);
        //            return bill;
        //        }
        // 
        
        //if gzhCompany never started trial, no bills.
        if (gzhCompany.getBillingStatus() == GzhCompanyBillingStatus.TRIAL_NOT_STARTED) {
            String message = messageSource.getMessage("gzh.billing.bill.trial.not.started", 
                    new Object[] {gzhCompany.getName()}, Locale.SIMPLIFIED_CHINESE);
            bill.setErrMsg(message);
            return bill;
        }
            
        //Trial is started. Both trialStartDate and activeBillingStartDate are set.       
        LocalDate activeBillingStartDate = gzhCompany.getActiveBillingStartDate();
        //lastDayOfMonth is strictly before the active billing start date, no bills
        if(lastDayOfMonth.isBefore(activeBillingStartDate)) {
            String message = messageSource.getMessage("gzh.billing.bill.last.day.of.billmonth.is.before.active.billing.start.date", 
                    new Object[] {lastDayOfMonth, gzhCompany.getName(), gzhCompany.getActiveBillingStartDate()}, 
                    Locale.SIMPLIFIED_CHINESE);
            bill.setErrMsg(message);
            return bill;
        }

        //At this point, activeBillingStartDate is equal to or before lastDayOfMonth
        LocalDate billingStatusChangeSplitDate = null;
        if (firstDayOfMonth.isAfter(activeBillingStartDate)) {
            billingStatusChangeSplitDate = firstDayOfMonth;
        }
        else {
            billingStatusChangeSplitDate = activeBillingStartDate;
        }
        
        //First, we need to get all the billingStatusChange records starting from billingStatusChangeSplitDate to lastDayOfMonth
        Predicate billingChangesBetweenPredicate = repoQueryFilter.filterBillingStatusChangeBetweenDates(gzhCompany.getId(), billingStatusChangeSplitDate, lastDayOfMonth);
        QSort sortByTimeStampAsc = repoQueryFilter.sortBillingStatusChangeByTimeStampAsc();
        Iterable<BillingStatusChange> billingChangesBetweenItr = billingStatusChangeRepository.findAll(billingChangesBetweenPredicate, sortByTimeStampAsc);
        
        //Second, we need to get the last billingStatusChange record before billingStatusChangeSplitDate
        Predicate billingChangesBeforePredicate = repoQueryFilter.filterBillingStatusChangeBeforeDate(gzhCompany.getId(), billingStatusChangeSplitDate);
        QSort sortByTimeStampDesc = repoQueryFilter.sortBillingStatusChangeByTimeStampDesc();
        Iterator<BillingStatusChange> billingChangesBeforeItr = billingStatusChangeRepository.findAll(billingChangesBeforePredicate, sortByTimeStampDesc).iterator();
        
        OffsetDateTime billingPeriodBeginTime = null;
        GzhCompanyBillingStatus previousBillingStatus = null;
        
        if (billingChangesBeforeItr.hasNext()) {
            //The first one in the itr is the latest status change according to time 
            BillingStatusChange lastChangeBeforeBillSplit = billingChangesBeforeItr.next();
            previousBillingStatus = lastChangeBeforeBillSplit.getStatusAfter();
        }
        else {
            //Note billingStatusChangeSplitDate is guaranteed to be equal or after activeBillingStartDate
            //And there is no billingStatusChange record before the billingStatusChangeSplitDate
            //So we set the following:
            previousBillingStatus = GzhCompanyBillingStatus.TRIAL;
        }
        billingPeriodBeginTime = OffsetDateTime.of(billingStatusChangeSplitDate.atStartOfDay(), OffsetDateTime.now().getOffset());

        //Now we need to calculate the accumulated ACTIVE billing time
        OffsetDateTime billingPeriodEndTime = null;
        BillingStatusChange currBillingStatusChange = null;
        long periodMinutes = 0;
        long totalMinutes = 0;
        Iterator<BillingStatusChange> billingChangeItr = billingChangesBetweenItr.iterator();
        while (billingChangeItr.hasNext()) {
            currBillingStatusChange = billingChangeItr.next();
            if(currBillingStatusChange.getStatusBefore() != previousBillingStatus) {
                String message = messageSource.getMessage("gzh.billing.next.billing.status.change.not.expected", 
                        new Object[] {currBillingStatusChange.getId(), currBillingStatusChange.getStatusBefore(), previousBillingStatus}, 
                        Locale.SIMPLIFIED_CHINESE);
                bill.setErrMsg(message);
                return bill;
            }
            
            if (previousBillingStatus == GzhCompanyBillingStatus.TRIAL || previousBillingStatus == GzhCompanyBillingStatus.ACTIVE) {
                if (currBillingStatusChange.getStatusAfter() == GzhCompanyBillingStatus.STOPPED) {
                    billingPeriodEndTime = currBillingStatusChange.getTimeStamp();
                    periodMinutes = billingPeriodBeginTime.until(billingPeriodEndTime, ChronoUnit.MINUTES);
                    totalMinutes = totalMinutes + periodMinutes;
                }
            }
            else if (previousBillingStatus == GzhCompanyBillingStatus.STOPPED) {
                if (currBillingStatusChange.getStatusAfter() == GzhCompanyBillingStatus.ACTIVE) {
                    billingPeriodBeginTime = currBillingStatusChange.getTimeStamp();
                }
            }
            
            previousBillingStatus = currBillingStatusChange.getStatusAfter();
        }
        
        //Now we iterated all the billingStatusChange records in the billmonth
        if (previousBillingStatus == GzhCompanyBillingStatus.TRIAL || previousBillingStatus == GzhCompanyBillingStatus.ACTIVE) {
            //close the last billing period
            billingPeriodEndTime = OffsetDateTime.of(lastDayOfMonth.plusDays(1).atStartOfDay(), OffsetDateTime.now().getOffset());
            periodMinutes = billingPeriodBeginTime.until(billingPeriodEndTime, ChronoUnit.MINUTES);
            totalMinutes = totalMinutes + periodMinutes;
        }

        if (totalMinutes == 0) {
            String message = messageSource.getMessage("gzh.billing.total.billing.minutes.is.zero", 
                new Object[] {gzhCompany.getName(), billMonth}, Locale.SIMPLIFIED_CHINESE);
            bill.setErrMsg(message);
            return bill;
        }
        
        bill = generateBillEntry(account, firstDayOfMonth, lastDayOfMonth, billMonth, totalMinutes, operator);
        
        return bill;
    }
    
    //billBeginDate and billEndDate are in the same month
    //Assume all the checking are done before calling this function, just generate the bill.
    private Bill generateBillEntry(Account account, LocalDate billBeginDate, LocalDate billEndDate, YearMonth billMonth, long billingMinutes, String operator) {
        //generate a empty bill first
        Bill bill = new Bill();
        GzhCompany gzhCompany = account.getGzhCompany();
        
        bill.setAccount(account);
        bill.setBillMonth(billMonth.toString());
        bill.setPeriodStart(billBeginDate);
        bill.setPeriodEnd(billEndDate);
        long numOfGzhClientCompany = getNumOfGzhClientCompanyByImportedDate(gzhCompany.getId(), billBeginDate);
        bill.setNumOfGzhClientCompany(numOfGzhClientCompany);
        BillingRate billingRate = calculateBillingRate(gzhCompany);
        bill.setBillingRate(billingRate);
        double amount = calculateBillAmount(gzhCompany, billingRate, numOfGzhClientCompany);
        double billingDays = Precision.round((1.0*billingMinutes)/(24*60), 3);
        int lengthOfMonth = billMonth.lengthOfMonth();
        amount = amount * billingDays / lengthOfMonth;
        amount = Precision.round(amount, 2);
        bill.setAmount(amount);
        bill.setBillingDays(billingDays);
        bill.setTimeStamp(OffsetDateTime.now());
        UUID billId = UUID.randomUUID(); 
        bill.setBillId(billId);
        bill.setOperator(operator);
        billRepository.save(bill);
        log.info("generateBillForPeriodInOneMonth gzhCompany=" + gzhCompany.getName() 
               + ", billBeginDate=" + billBeginDate 
               + ", billEndDate=" + billEndDate
               + ", totalBillingMinutes=" + billingMinutes 
               + ", billingDays=" + billingDays);
        log.info("generateBillForPeriodInOneMonth numOfGzhClientCompany=" + numOfGzhClientCompany);
        
        //If there is an exception in generating bill entry, the code will not reach here
        //adjust account balance if bill entry is generated successfully.
        account.setLastBillMonth(billMonth.toString());
        String adjustNote = messageSource.getMessage("gzh.billing.bill.auto.deduction", null, Locale.SIMPLIFIED_CHINESE);
        TransactionType txType = TransactionType.BILL_AUTO_DEBIT;
        String txRefNum = billId.toString();
        adjustAccountBalance(account, -amount, adjustNote, txType, txRefNum, operator);
        
        return bill;
    }
    
    
    
    public List<GzhCompany> updateGzhCompanyInDebt() {
        List<GzhCompany> newInDebtList = new ArrayList<GzhCompany>();
        Predicate predicate = repoQueryFilter.fillterAccountByNegativeBalance();
        Iterable<Account> accountItr = accountRepository.findAll(predicate);
        accountItr.forEach(account -> {
            GzhCompany gzhCompany = updateOneGzhCompanyInDebt(account); 
            if (gzhCompany != null)
                newInDebtList.add(gzhCompany);
        });
        
        return newInDebtList;
    }
    
    //When the balance of account goes above 0, we update the GzhCompanyPaymentStatus to ACTIVE immediately.
    //We delay marking GzhCompanyPaymentStatus to InDebt/OverDebt to specific dates in each month.
    public GzhCompany updateOneGzhCompanyInDebt(Account account) {
        GzhCompany gzhCompany = account.getGzhCompany();
        if (account.getBalance() < 0) {
            if (gzhCompany.getPaymentStatus() == GzhCompanyPaymentStatus.NO_DEBT) {
                LocalDate now = LocalDate.now();
                LocalDate day10 = YearMonth.of(now.getYear(), now.getMonth()).atDay(INDEBT_UPDATE_DAY_OF_MONTH);
                if (now.isAfter(day10)) {
                    gzhCompany.setPaymentStatus(GzhCompanyPaymentStatus.IN_DEBT);
                    gzhCompany.setPaymentInDebtDate(day10);
                    gzhCompanyRepository.save(gzhCompany);
                    return gzhCompany;
                }
            }
        }

        return null;
    }
    
    public List<GzhCompany> updateGzhCompanyOverDebt() {
        List<GzhCompany> newOverDebtList = new ArrayList<GzhCompany>();
        Predicate predicate = repoQueryFilter.fillterAccountByNegativeBalance();
        Iterable<Account> accountItr = accountRepository.findAll(predicate);
        accountItr.forEach(account -> {
            GzhCompany gzhCompany = updateOneGzhCompanyOverDebt(account); 
            if (gzhCompany != null)
                newOverDebtList.add(gzhCompany);
        });
        
        return newOverDebtList;
    }
    
    //When the balance of account goes above 0, we update the GzhCompanyPaymentStatus to ACTIVE immediately.
    //We delay marking GzhCompanyPaymentStatus to InDebt/OverDebt to specific dates in each month.
    public GzhCompany updateOneGzhCompanyOverDebt(Account account) {
        GzhCompany gzhCompany = account.getGzhCompany();
        if (account.getBalance() < 0) {
            if (gzhCompany.getPaymentStatus() == GzhCompanyPaymentStatus.IN_DEBT) {
                LocalDate inDebitDate = gzhCompany.getPaymentInDebtDate();
                LocalDate now = LocalDate.now();
                if (now.isAfter(inDebitDate.plusDays(OVERDEBT_UPDATE_DAYS_AFTER_INDEBT))) {
                    //10 days after INDEBT, change to OVERDEBT
                    gzhCompany.setPaymentStatus(GzhCompanyPaymentStatus.OVER_DEBT);
                    gzhCompany.setServiceRunningStatus(GzhCompanyServiceRunningStatus.STOPPED);
                    gzhCompanyRepository.save(gzhCompany);
                    return gzhCompany;
                }
            }
        }
        
        return null;
    }
    
    public List<GzhCompany> updateBillingStatusFromTrialToActive(String operator) {
        List<GzhCompany> newActiveBillingList = new ArrayList<GzhCompany>();
        LocalDate today = LocalDate.now();
        
        //Filter gzhCompany with TRIAL billing status and activeBillingStartDate is loe today
        Predicate predicate = repoQueryFilter.filterGzhCompanyToUpdateFromTrialToActive(today);
        Iterable<GzhCompany> gzhCompanyItr = gzhCompanyRepository.findAll(predicate);
        gzhCompanyItr.forEach(gzhCompany -> {
            gzhCompany.setBillingStatus(GzhCompanyBillingStatus.ACTIVE);
            gzhCompanyRepository.save(gzhCompany);
            newActiveBillingList.add(gzhCompany);
            
            //Insert an entry to record the billing status change
            recordBillingStatusChange(gzhCompany, GzhCompanyBillingStatus.TRIAL, GzhCompanyBillingStatus.ACTIVE, operator, null);
            
            //Do not charge for going from TRIAL to ACTIVE.
        });
        
        return newActiveBillingList;
    }
    
    //Valid billingStatusChange are:
    //    TRIAL->ACTIVE
    //    TRIAL->STOPPED
    //    ACTIVE->STOPPED
    //    STOPPED->TRIAL or ACTIVE depending on whether the TRIAL expires or not.
    public BillingStatusChange recordBillingStatusChange(GzhCompany gzhCompany, GzhCompanyBillingStatus billingStatusBefore, 
            GzhCompanyBillingStatus billingStatusAfter, String operator, OffsetDateTime statusChangeTime) {
        
        BillingStatusChange billingStatusChange = new BillingStatusChange();
        billingStatusChange.setBillingStatusChangeId(UUID.randomUUID());
        billingStatusChange.setGzhCompany(gzhCompany);
        billingStatusChange.setOperator(operator);
        billingStatusChange.setStatusBefore(billingStatusBefore);
        billingStatusChange.setStatusAfter(billingStatusAfter);
        if (billingStatusBefore == GzhCompanyBillingStatus.TRIAL && billingStatusAfter == GzhCompanyBillingStatus.ACTIVE){
            //This is a special case, we need to set the billingStatusChange to the activeBillingStartDate for billing purpose
            billingStatusChange.setTimeStamp(OffsetDateTime.of(gzhCompany.getActiveBillingStartDate().atStartOfDay(), OffsetDateTime.now().getOffset()));
        }
        else {
            //For all other cases, use the current time.
            billingStatusChange.setTimeStamp(statusChangeTime);
        }
        
        if (billingStatusBefore == GzhCompanyBillingStatus.STOPPED && billingStatusAfter == GzhCompanyBillingStatus.ACTIVE) {
            //Only record charged fee for going from STOPPED to ACTIVE.
            //The actual charging on the account is done in chargeAccountForBillingActivation
            billingStatusChange.setChargedFee(BILLING_ACTIVATION_FEE);;
        }
        
        billingStatusChangeRepository.save(billingStatusChange);
        return billingStatusChange;
    }
    
    public Account chargeAccountForBillingActivationFromStoppedStatus(Account account, BillingStatusChange billingStatusChange, String operator) {
        double adjustAmount = -BILLING_ACTIVATION_FEE;
        String adjustNote = messageSource.getMessage("gzh.billing.activation.charge", null, Locale.SIMPLIFIED_CHINESE);
        TransactionType txType = TransactionType.BILLING_ACTIVATION_CHARGE;
        String txRefNum = billingStatusChange.getBillingStatusChangeId().toString();
        account = adjustAccountBalance(account, adjustAmount, adjustNote, txType, txRefNum, operator);
        
        return account;
    }
    
    public Account adjustAccountBalance(Account account, double adjustAmount, String adjustNote, TransactionType txType, String txRefNum, String operator) {
        //create a transaction to record the account balance change
        double oldBalance = account.getBalance();
        //adjustAmount can be negative 
        double newBalance = Precision.round(oldBalance + adjustAmount, 2);
        Transaction transaction = new Transaction();
        transaction.setAccount(account);
        transaction.setAccountBalanceBefore(oldBalance);
        transaction.setAccountBalanceAfter(newBalance);
        transaction.setTransactionAmount(adjustAmount);
        transaction.setNote(adjustNote);
        transaction.setTimeStamp(OffsetDateTime.now());
        UUID txId = UUID.randomUUID(); 
        transaction.setTxId(txId);
        transaction.setTransactionType(txType);

        transaction.setTransactionRefNum(txRefNum);
        transaction.setInternalAccountOwnerName(account.getInternalAccountOwnerName());
        transaction.setOperator(operator);
        transactionRepository.save(transaction);
        log.info("adjustAccountBalance generated transaction=" + transaction);
        
        //If there is an exception in saving the transaction, such as duplicate txRefNum, the account is not updated.
        //This helped avoid adjusting the account more than once with the same txRefNum
        
        //Now we need to adjust the account balance
        account.setBalance(newBalance);
        //Do not need to update the Many-To-One relationship to transaction
        //Many-To-One is maintained from the Many side
        account = accountRepository.save(account);
        log.info("adjustAccountBalance Adjusted account balance for account=" + account);
        
        //Set GzhCompanyPaymentStatus to ACTIVE if accountBalance is >= 0
        if (account.getBalance() >= 0) {
            GzhCompany gzhCompany = account.getGzhCompany();
            if (gzhCompany.getPaymentStatus() != GzhCompanyPaymentStatus.NO_DEBT) {
                gzhCompany.setPaymentStatus(GzhCompanyPaymentStatus.NO_DEBT);
                gzhCompany.setServiceRunningStatus(GzhCompanyServiceRunningStatus.RUNNING);
                gzhCompanyRepository.save(gzhCompany);
            }
        }
        
        //if accountBalance is < 0, we do not update GzhCompanyPaymentStatus right away.
        //We have tasks scheduled on specific date to check whether GzhCompany is InDebt or OverDebt.
        return account;
    }

}
