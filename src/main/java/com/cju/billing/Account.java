package com.cju.billing;

import java.util.ArrayList;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import com.cju.gzhOps.GzhCompany;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

@Entity
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id", scope = Account.class)
@Table(name = "Account")
public class Account {
    @Id    
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    
    private double balance = 0;
    
    private String lastBillMonth;
    
    //internalAccountOwnerName is set to the userName of gzhCompany owner when the account is created.
    //This information looks duplicate and is kept in DB for internal use only.
    private String internalAccountOwnerName;
    
    //The gzhcompany associated with this account
    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name="gzhCompanyId", foreignKey = @ForeignKey(name = "Account_FK_GzhCompanyId"))
    private GzhCompany gzhCompany;
    
    @OneToMany(mappedBy = "account", fetch = FetchType.LAZY)
    private List<Transaction> transactionList = new ArrayList<Transaction>();
        
    public Account() {}
    
    public void setId(long id) {
        this.id = id;
    }
    public long getId() {
        return id;
    }
    
    public void setBalance(double balance) {
        this.balance = balance;
    }
    public double getBalance() {
        return balance;
    }
    
    public void setLastBillMonth(String lastBillMonth) {
        this.lastBillMonth = lastBillMonth;
    }
    public String getLastBillMonth() {
        return lastBillMonth;
    }
    
    public void setInternalAccountOwnerName(String internalAccountOwnerName) {
        this.internalAccountOwnerName = internalAccountOwnerName;
    }
    public String getInternalAccountOwnerName() {
        return internalAccountOwnerName;
    }
    
    public void setGzhCompany(GzhCompany gzhCompany) {
        this.gzhCompany = gzhCompany;
    }
    public GzhCompany getGzhCompany() {
        return gzhCompany;
    }

    public void setTransactionList(List<Transaction> transactionList) {
        this.transactionList = transactionList;
    }
    public List<Transaction> getTransactionList() {
        return transactionList;
    }
    
    @Override
    public String toString() {
        return String.format(
                "Account[id=%s, internalAccountName='%s', accountBalance='%f']",
                id, internalAccountOwnerName, balance);
    }
}
