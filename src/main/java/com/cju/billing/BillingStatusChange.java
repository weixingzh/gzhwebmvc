package com.cju.billing;

import java.time.OffsetDateTime;
import java.util.UUID;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.cju.gzhOps.GzhCompany;
import com.cju.gzhOps.GzhCompanyBillingStatus;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

@Entity
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id", scope = BillingStatusChange.class)
@Table(name = "BillingStatusChange")

//Not needed if database table name is the same as the class name.
public class BillingStatusChange {
    @Transient
    private Logger log = LoggerFactory.getLogger(BillingStatusChange.class);

    @Id    
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    
    private UUID billingStatusChangeId;
    @Column(columnDefinition= "TIMESTAMP WITH TIME ZONE")
    private OffsetDateTime timeStamp;
    @Enumerated(EnumType.STRING)
    private GzhCompanyBillingStatus statusBefore;
    @Enumerated(EnumType.STRING)
    private GzhCompanyBillingStatus statusAfter;
    private String operator;
    private double chargedFee;
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="gzhCompanyId", foreignKey = @ForeignKey(name = "BillingStatusChange_FK_GzhCompanyId"))
    private GzhCompany gzhCompany;
    
    public void setId(long id) {
        this.id = id;
    }
    public long getId() {
        return id;
    }
    
    public void setBillingStatusChangeId(UUID billingStatusChangeId) {
        this.billingStatusChangeId = billingStatusChangeId;
    }
    public UUID getBillingStatusChangeId() {
        return billingStatusChangeId;
    }
    
    public void setTimeStamp(OffsetDateTime timeStamp) {
        this.timeStamp = timeStamp;
    }
    public OffsetDateTime getTimeStamp() {
        return timeStamp;
    }
    
    public void setStatusBefore(GzhCompanyBillingStatus statusBefore) {
        this.statusBefore = statusBefore;
    }
    public GzhCompanyBillingStatus getStatusBefore() {
        return statusBefore;
    }
    
    public void setStatusAfter(GzhCompanyBillingStatus statusAfter) {
        this.statusAfter = statusAfter;
    }
    public GzhCompanyBillingStatus getStatusAfter() {
        return statusAfter;
    }
    
    public void setOperator(String operator) {
        this.operator = operator;
    }
    public String getOperator() {
        return operator;
    }
    
    public void setChargedFee(double chargedFee) {
        this.chargedFee = chargedFee;
    }
    public double getChargedFee() {
        return chargedFee;
    }
    
    public void setGzhCompany(GzhCompany gzhCompany) {
        this.gzhCompany = gzhCompany;
    }
    public GzhCompany getGzhCompany() {
        return gzhCompany;
    }
}