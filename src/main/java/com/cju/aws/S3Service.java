package com.cju.aws;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;

import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.S3Object;

@Service
public class S3Service {
    
    @Autowired
    private AmazonS3 s3Client;
    
    public void putFile(String bucketName, String key, byte[] data, String fileName, long size) {
        ObjectMetadata metadata = new ObjectMetadata();
        metadata.addUserMetadata("fileName", fileName);
        metadata.addUserMetadata("size", String.valueOf(size));
        InputStream input = new ByteArrayInputStream(data);
        s3Client.putObject(bucketName, key, input, metadata);
    }
    
    public byte[] getFile(String bucketName, String key) {
        S3Object s3Object = s3Client.getObject(bucketName, key);
        InputStream inputStream = s3Object.getObjectContent();
        byte[] bytes = new byte[0];
        try {
            bytes = IOUtils.toByteArray(inputStream);
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } finally {
            IOUtils.closeQuietly(inputStream);
        }
        
        return bytes;
    }
}
