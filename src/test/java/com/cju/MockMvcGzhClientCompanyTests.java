package com.cju;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.security.test.context.support.WithSecurityContextTestExecutionListener;
import org.springframework.security.test.context.support.WithUserDetails;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.json.JSONObject;

import static org.hamcrest.Matchers.containsString;
import org.testng.annotations.*;

//@RunWith(SpringRunner.class) tells JUnit to run using Spring’s testing support. SpringRunner is the new name for SpringJUnit4ClassRunner.
//@SpringBootTest is saying “bootstrap with Spring Boot’s support” (e.g. load application.properties and give me all the Spring Boot goodness)
//If we want to load a specific configuration, we can use the classes attribute of @SpringBootTest. 
//In this example, we’ve omitted classes which means that the test will first attempt to load @Configuration from any inner-classes, 
//and if that fails, it will search for your primary @SpringBootApplication class.

//Without (webEnvironment = WebEnvironment.RANDOM_PORT), there is no real container (Tomcat) setup and running.
//@WebMvcTest is only going to scan the controller you've defined and the MVC infrastructure.
//We are not using @AutoConfigureMockMvc since we build the mockMvc with spring security.

//Spring MVC Test provides a convenient interface called a RequestPostProcessor that can be used to modify a request.

@SpringBootTest
@TestExecutionListeners(listeners={WithSecurityContextTestExecutionListener.class})
@ActiveProfiles("dev, dev-use-testdb, dev-clear-and-load-json-false")
public class MockMvcGzhClientCompanyTests extends AbstractTestNGSpringContextTests {
    
    @Autowired
    private WebApplicationContext context;
    private MockMvc mockMvc;
    @Autowired
    private MockHttpSession session;
    
    //SecurityMockMvcConfigurers.springSecurity() will integrate Spring Security with Spring MVC Test
    //Set alwaysRun=true so that setup() will be run even though when a group of tests in this class is picked to run in testng.xml file.
    @BeforeClass(alwaysRun=true)
    public void setup() {
        mockMvc = MockMvcBuilders.webAppContextSetup(context).apply(springSecurity()).build();
    }
        
    @Test
    @WithUserDetails("admin")
    public void gzhOpsAdminGzhCompanyList() throws Exception {
        MockHttpServletRequestBuilder requestBuilder = get("/gzhOps/gzhClientCompany/list").session(session);
        mockMvc.perform(requestBuilder)
            .andExpect(status().isOk());
    }
    
    @Test
    @WithUserDetails("user1")
    public void gzhOpsUserGzhCompanyList() throws Exception {
        MockHttpServletRequestBuilder requestBuilder = get("/gzhOps/gzhClientCompany/list").session(session);
        mockMvc.perform(requestBuilder)
            .andExpect(status().isOk());
    }

    @Test
    @WithUserDetails("admin")
    public void gzhOpsAdminGzhCompanyDetails() throws Exception {
        MockHttpServletRequestBuilder requestBuilder = get("/gzhOps/gzhClientCompany/details/1").session(session);
        mockMvc.perform(requestBuilder)
            .andExpect(status().isOk())
            .andExpect(content().string(containsString("chanjetClientCompany1")));
    }
    
    @Test
    @WithUserDetails("user1")
    public void gzhOpsUserGzhCompanyDetails() throws Exception {
        MockHttpServletRequestBuilder requestBuilder = get("/gzhOps/gzhClientCompany/details/1").session(session);
        mockMvc.perform(requestBuilder)
            .andExpect(status().isOk())
            .andExpect(content().string(containsString("chanjetClientCompany1")));
    }
    
    @Test
    @WithUserDetails("user51")
    public void gzhOpsUserGzhCompanyDetailsNotAllowed() throws Exception {
        MockHttpServletRequestBuilder requestBuilder = get("/gzhOps/gzhClientCompany/details/1").session(session);
        mockMvc.perform(requestBuilder)
            .andExpect(status().is4xxClientError())
            .andExpect(content().string(containsString("不允许访问")));
    }
    
    // Report Import Testing. URL: /gzhOps/gzhClientCompany/{id}/report/import
    
    //Pre-condition: 
    //    gzhClientCompany needs to be imported from chanjet.
    //    user1 needs to be admin for the gzhClientCompany
    @Test
    @WithUserDetails("user1")
    public void gzhOpsUserGetGzhClientCompanyReportImportForm() throws Exception {
        //InitTestDatabase created 4 gzhClientCompany.
        //gzhClientCompany 5 is the first client company imported from chanjet.
        MockHttpServletRequestBuilder requestBuilder = get("/gzhOps/gzhClientCompany/5/report/import").session(session);
        mockMvc.perform(requestBuilder)
            .andExpect(status().isOk())
            .andExpect(content().string(containsString("珠海市恒嘉土石方工程有限公司")));
    }
    
    @Test
    @WithUserDetails("user51")
    public void gzhOpsUserSubmitGzhClientCompanyReportImportFormNotAllowed() throws Exception  {
        JSONObject reportImport = new JSONObject();
        
        reportImport.put("period", "201701");
        reportImport.put("reportTypeValue", "Asset");
        
        String reportImportJson = reportImport.toString();
        
        MockHttpServletRequestBuilder requestBuilder = post("/gzhOps/api/v1/gzhClientCompany/5/report/import")
            .session(session)
            .contentType("application/json")
            .content(reportImportJson)
            .with(csrf());

        mockMvc.perform(requestBuilder)
            .andDo(print())
            .andExpect(status().is4xxClientError())
            .andExpect(content().string(containsString("不允许访问")));
    }
    
    // Report List and Report details testing
    // URL: /gzhClientCompany/report/list
    // To get report list of gzhClientCompanies admined by user
    @Test
    @WithUserDetails("user1")
    public void gzhOpsUserGzhClientCompanyReportListByUser() throws Exception {
        MockHttpServletRequestBuilder requestBuilder = get("/gzhOps/gzhClientCompany/report/list").session(session);
        mockMvc.perform(requestBuilder)
            .andExpect(status().isOk());
    }

    // URL: /gzhClientCompany/{id}/report/list
    // To get report list of a gzhClientCompany
    @Test
    @WithUserDetails("admin")
    public void gzhOpsAdminGzhClientCompanyReportListById() throws Exception {
        MockHttpServletRequestBuilder requestBuilder = get("/gzhOps/gzhClientCompany/5/report/list").session(session);
        mockMvc.perform(requestBuilder)
            .andDo(print())
            .andExpect(status().isOk())
            .andExpect(content().string(containsString("珠海市恒嘉土石方工程有限公司")))
            .andExpect(content().string(containsString("201701")))
            .andExpect(content().string(containsString("Asset")));
    }
    
    @Test
    @WithUserDetails("user1")
    public void gzhOpsUserGzhClientCompanyReportListById() throws Exception {
        MockHttpServletRequestBuilder requestBuilder = get("/gzhOps/gzhClientCompany/5/report/list").session(session);
        mockMvc.perform(requestBuilder)
            .andExpect(status().isOk())
            .andExpect(content().string(containsString("珠海市恒嘉土石方工程有限公司")))
            .andExpect(content().string(containsString("201701")))
            .andExpect(content().string(containsString("Asset")));
    }
    
    @Test
    @WithUserDetails("user51")
    public void gzhOpsUserGzhClientCompanyReportListByIdNotAllowed() throws Exception {
        MockHttpServletRequestBuilder requestBuilder = get("/gzhOps/gzhClientCompany/5/report/list").session(session);
        mockMvc.perform(requestBuilder)
            .andExpect(status().is4xxClientError())
            .andExpect(content().string(containsString("不允许访问")));
    }
      
    // URL: /gzhOps/gzhClientCompany/{id}/report/details?period=period&reportType=reportType
    @Test
    @WithUserDetails("admin")
    public void gzhOpsAdminGzhClientCompanyReportDetailsById() throws Exception {
        MockHttpServletRequestBuilder requestBuilder = get("/gzhOps/gzhClientCompany/5/report/details")
            .session(session)
            .param("period", "201701")
            .param("reportType", "Asset");
        mockMvc.perform(requestBuilder)
            .andExpect(status().isOk())
            .andExpect(content().string(containsString("珠海市恒嘉土石方工程有限公司")))
            .andExpect(content().string(containsString("201701")))
            .andExpect(content().string(containsString("流动资产")));
    }
      
    @Test
    @WithUserDetails("user1")
    public void gzhOpsUserGzhClientCompanyReportDetailsById() throws Exception {
        MockHttpServletRequestBuilder requestBuilder = get("/gzhOps/gzhClientCompany/5/report/details")
            .session(session)
            .param("period", "201701")
            .param("reportType", "Asset");
        mockMvc.perform(requestBuilder)
            .andExpect(status().isOk())
            .andExpect(content().string(containsString("珠海市恒嘉土石方工程有限公司")))
            .andExpect(content().string(containsString("201701")))
            .andExpect(content().string(containsString("流动资产")));
    }

    @Test
    @WithUserDetails("user51")
    public void gzhOpsUserGzhClientCompanyReportDetailsByIdNotAllowed() throws Exception {
        MockHttpServletRequestBuilder requestBuilder = get("/gzhOps/gzhClientCompany/5/report/details")
            .session(session)
            .param("period", "201701")
            .param("reportType", "Asset");
        mockMvc.perform(requestBuilder)
            .andExpect(status().is4xxClientError())
            .andExpect(content().string(containsString("不允许访问")));
    }
}
