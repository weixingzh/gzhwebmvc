package com.cju;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.security.test.context.support.WithSecurityContextTestExecutionListener;
import org.springframework.security.test.context.support.WithUserDetails;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.*;
import static org.hamcrest.Matchers.containsString;
import org.testng.annotations.*;

import com.cju.errors.GlobalControllerExceptionController;

//@RunWith(SpringRunner.class) tells JUnit to run using Spring’s testing support. SpringRunner is the new name for SpringJUnit4ClassRunner.
//@SpringBootTest is saying “bootstrap with Spring Boot’s support” (e.g. load application.properties and give me all the Spring Boot goodness)
//If we want to load a specific configuration, we can use the classes attribute of @SpringBootTest. 
//In this example, we’ve omitted classes which means that the test will first attempt to load @Configuration from any inner-classes, 
//and if that fails, it will search for your primary @SpringBootApplication class.

//Without (webEnvironment = WebEnvironment.RANDOM_PORT), there is no real container (Tomcat) setup and running.
//@WebMvcTest is only going to scan the controller you've defined and the MVC infrastructure.
//We are not using @AutoConfigureMockMvc since we build the mockMvc with spring security.

//Spring MVC Test provides a convenient interface called a RequestPostProcessor that can be used to modify a request.

@SpringBootTest
@TestExecutionListeners(listeners={WithSecurityContextTestExecutionListener.class})
@ActiveProfiles("dev, dev-use-testdb, dev-clear-and-load-json-false")
public class MockMvcChanjetTests extends AbstractTestNGSpringContextTests {
    private Logger log = LoggerFactory.getLogger(MockMvcChanjetTests.class);
    
    @Autowired
    private WebApplicationContext context;
    private MockMvc mockMvc;
    
    @Autowired
    private MockHttpSession session;

    //SecurityMockMvcConfigurers.springSecurity() will integrate Spring Security with Spring MVC Test
    //Set alwaysRun=true so that setup() will be run even though when a group of tests in this class is picked to run in testng.xml file.
    @BeforeClass(alwaysRun = true)
    public void setup() {
        log.info("@BeforeClass setup() called.");
        mockMvc = MockMvcBuilders.webAppContextSetup(context).apply(springSecurity()).build();
    }
    
    @Test
    @WithUserDetails("user1")
    public void gzhOpsUserGzhClientCompanyList() throws Exception {
        MockHttpServletRequestBuilder requestBuilder = get("/gzhOps/gzhClientCompany/list").session(session);
        mockMvc.perform(requestBuilder)
            .andDo(print())
            .andExpect(status().isOk())
            .andExpect(content().string(containsString("chanjetClientCompany1")))
            .andExpect(content().string(containsString("珠海市恒嘉土石方工程有限公司")));
    }
}
