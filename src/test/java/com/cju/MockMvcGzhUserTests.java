package com.cju;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.security.test.context.support.WithSecurityContextTestExecutionListener;
import org.springframework.security.test.context.support.WithUserDetails;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import org.json.JSONObject;
import org.json.JSONArray;
import org.json.JSONException;

import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.*;
import static org.hamcrest.Matchers.containsString;
import org.testng.annotations.*;

//@RunWith(SpringRunner.class) tells JUnit to run using Spring’s testing support. SpringRunner is the new name for SpringJUnit4ClassRunner.
//@SpringBootTest is saying “bootstrap with Spring Boot’s support” (e.g. load application.properties and give me all the Spring Boot goodness)
//If we want to load a specific configuration, we can use the classes attribute of @SpringBootTest. 
//In this example, we’ve omitted classes which means that the test will first attempt to load @Configuration from any inner-classes, 
//and if that fails, it will search for your primary @SpringBootApplication class.

//Without (webEnvironment = WebEnvironment.RANDOM_PORT), there is no real container (Tomcat) setup and running.
//@WebMvcTest is only going to scan the controller you've defined and the MVC infrastructure.
//We are not using @AutoConfigureMockMvc since we build the mockMvc with spring security.

//Spring MVC Test provides a convenient interface called a RequestPostProcessor that can be used to modify a request.

@SpringBootTest
@TestExecutionListeners(listeners={WithSecurityContextTestExecutionListener.class})
@ActiveProfiles("dev, dev-use-testdb, dev-clear-and-load-json-false")
public class MockMvcGzhUserTests extends AbstractTestNGSpringContextTests {
    private Logger log = LoggerFactory.getLogger(MockMvcGzhUserTests.class);
    
    @Autowired
    private WebApplicationContext context;
    private MockMvc mockMvc;
    
    @Autowired
    private MockHttpSession session;

    //SecurityMockMvcConfigurers.springSecurity() will integrate Spring Security with Spring MVC Test
    @BeforeClass
    public void setup() {
        mockMvc = MockMvcBuilders.webAppContextSetup(context).apply(springSecurity()).build();
    }
    
    @Test
    @WithUserDetails("admin")
    public void gzhOpsAdminGzhUserList() throws Exception {
        MockHttpServletRequestBuilder requestBuilder = get("/gzhOps/gzhUser/list").session(session);
        mockMvc.perform(requestBuilder)
            .andExpect(status().isOk())
            .andExpect(content().string(containsString("gzhUser16")));
    }
    
    @Test
    @WithUserDetails("user1")
    public void gzhOpsUserGzhUserList() throws Exception {
        MockHttpServletRequestBuilder requestBuilder = get("/gzhOps/gzhUser/list").session(session);
        mockMvc.perform(requestBuilder)
            .andExpect(status().isOk())
            .andExpect(content().string(containsString("gzhUser16")));
    }
    
    @Test
    @WithUserDetails("admin")
    public void gzhOpsAdminGzhUserDetails() throws Exception {
        MockHttpServletRequestBuilder requestBuilder = get("/gzhOps/gzhUser/details/16").session(session);
        mockMvc.perform(requestBuilder)
            .andExpect(status().isOk())
            .andExpect(content().string(containsString("gzhUser16")));
    }
    
    @Test
    @WithUserDetails("user1")
    public void gzhOpsUserGzhUserDetails() throws Exception {
        MockHttpServletRequestBuilder requestBuilder = get("/gzhOps/gzhUser/details/16").session(session);
        mockMvc.perform(requestBuilder)
            .andExpect(status().isOk())
            .andExpect(content().string(containsString("gzhUser16")));
    }
    
    @Test
    @WithUserDetails("user51")
    public void gzhOpsUserGzhUserDetailsNotAllowed() throws Exception {
        MockHttpServletRequestBuilder requestBuilder = get("/gzhOps/gzhUser/details/16").session(session);
        mockMvc.perform(requestBuilder)
            .andExpect(status().is4xxClientError())
            .andExpect(content().string(containsString("不允许访问")));
    }
    
    //REST API URL: /gzhOps/api/v1/gzhUser/list/gzhCompany/{gzhCompanyId}
    @Test
    @WithUserDetails("admin")
    public void gzhOpsAdminGzhUserListByGzhCompanyId() throws Exception {
        MockHttpServletRequestBuilder requestBuilder = get("/gzhOps/api/v1/gzhUser/list/gzhCompany/1");
        mockMvc.perform(requestBuilder)
            .andExpect(status().isOk())
            .andExpect(content().string(containsString("gzhUser1")));
    }
    
    @Test
    @WithUserDetails("user1")
    public void gzhOpsUserGzhUserListByGzhCompanyId() throws Exception {
        MockHttpServletRequestBuilder requestBuilder = get("/gzhOps/api/v1/gzhUser/list/gzhCompany/1");
        mockMvc.perform(requestBuilder)
            .andExpect(status().isOk())
            .andExpect(content().string(containsString("gzhUser1")));
    }
    
    @Test
    @WithUserDetails("user51")
    public void gzhOpsUserGzhUserListByGzhCompanyIdNotAllowed() throws Exception {
        MockHttpServletRequestBuilder requestBuilder = get("/gzhOps/api/v1/gzhUser/list/gzhCompany/1").session(session);
        mockMvc.perform(requestBuilder)
            .andExpect(status().is4xxClientError())
            .andExpect(content().string(containsString("不允许访问")));
    }
    
    //URL: /gzhUser/list/gzhClientCompany/{gzhClientCompanyId}
    @WithUserDetails("admin")
    public void gzhOpsAdminGzhUserListByGzhClientCompanyId() throws Exception {
        MockHttpServletRequestBuilder requestBuilder = get("/gzhOps/api/v1/gzhUser/list/gzhClientCompany/1");
        mockMvc.perform(requestBuilder)
            .andExpect(status().isOk())
            .andExpect(content().string(containsString("gzhUser1")));
    }
    
    @Test
    @WithUserDetails("user1")
    //This is REST API. Does not use Thymeleaf template.
    public void gzhOpsUserGzhUserListByGzhClientCompanyId() throws Exception {
        MockHttpServletRequestBuilder requestBuilder = get("/gzhOps/api/v1/gzhUser/list/gzhClientCompany/1");
        mockMvc.perform(requestBuilder)
            .andExpect(status().isOk())
            .andExpect(content().string(containsString("gzhUser1")));
    }
    
    //Even though this is a REST API url, a security checking failure will trigger an exception handling to 
    //return a html page using Thyemleaf template.
    @Test
    @WithUserDetails("user51")
    public void gzhOpsUserGzhUserListByGzhClientCompanyIdNotAllowed() throws Exception {
        MockHttpServletRequestBuilder requestBuilder = get("/gzhOps/api/v1/gzhUser/list/gzhClientCompany/1").session(session);
        mockMvc.perform(requestBuilder)
            .andExpect(status().is4xxClientError())
            .andExpect(content().string(containsString("不允许访问")));
    }
    
    //POST REST APIs
    private MockHttpServletRequestBuilder getPostGzhUserListByGzhClientCompanyIdBuilder(long gzhClientCompanyId) throws JSONException {
        JSONArray gzhUserList = new JSONArray();
        JSONObject gzhUser1 = new JSONObject();
        gzhUser1.put("id", "1");
        gzhUser1.put("name", "gzhUser1");
        gzhUserList.put(gzhUser1);
        JSONObject gzhUser3 = new JSONObject();
        gzhUser3.put("id", "3");
        gzhUser3.put("name", "gzhUser3");
        gzhUserList.put(gzhUser3);

        String simpleGzhUserListJson = gzhUserList.toString();
        log.info("simpleGzhUserListJson=" + simpleGzhUserListJson);
        MockHttpServletRequestBuilder requestBuilder = post("/gzhOps/api/v1/gzhUser/list/gzhClientCompany/" + gzhClientCompanyId)
                .contentType("application/json")
                .content(simpleGzhUserListJson)
                .with(csrf());
        return requestBuilder;
    }
    
    @Test
    @WithUserDetails("user51")
    public void gzhOpsUserPostGzhUserListByGzhClientCompanyIdNotAllowed() throws Exception {
        long gzhClientCompanyId = 1L;
        MockHttpServletRequestBuilder requestBuilder = getPostGzhUserListByGzhClientCompanyIdBuilder(gzhClientCompanyId).session(session);
            
        mockMvc.perform(requestBuilder)
           .andExpect(status().is4xxClientError())
           .andExpect(content().string(containsString("不允许访问")));
    }

}
