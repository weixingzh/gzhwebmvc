package com.cju;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.security.test.context.support.WithSecurityContextTestExecutionListener;
import org.springframework.security.test.context.support.WithUserDetails;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.hamcrest.Matchers.containsString;
import org.testng.annotations.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.redirectedUrl;

//@RunWith(SpringRunner.class) tells JUnit to run using Spring’s testing support. SpringRunner is the new name for SpringJUnit4ClassRunner.
//@SpringBootTest is saying “bootstrap with Spring Boot’s support” (e.g. load application.properties and give me all the Spring Boot goodness)
//If we want to load a specific configuration, we can use the classes attribute of @SpringBootTest. 
//In this example, we’ve omitted classes which means that the test will first attempt to load @Configuration from any inner-classes, 
//and if that fails, it will search for your primary @SpringBootApplication class.

//Without (webEnvironment = WebEnvironment.RANDOM_PORT), there is no real container (Tomcat) setup and running.
//@WebMvcTest is only going to scan the controller you've defined and the MVC infrastructure.
//We are not using @AutoConfigureMockMvc since we build the mockMvc with spring security.

//Spring MVC Test provides a convenient interface called a RequestPostProcessor that can be used to modify a request.

@SpringBootTest
@TestExecutionListeners(listeners={WithSecurityContextTestExecutionListener.class})
@ActiveProfiles("dev, dev-use-testdb, dev-clear-and-load-json-false")
public class MockMvcUserTests extends AbstractTestNGSpringContextTests {
    
    @Autowired
    private WebApplicationContext context;
    private MockMvc mockMvc;
    
    //mockMvc creates a HttpSession. This httpSession is used in Spring Security Filter Chain. 
    //@WithUserDetails will trigger Spring Security Filter Chain, which calls UserDetailsService().
    //UserDetailsService() will cache CURRENT_USER in httpSession.
    //In mockMvc.perform() call, we pass in the same httpSession. The Thymeleaf templates use the httpSession 
    //to get the cached CURRENT_USER and GzhCompany name.
    //If we do not pass the httpSession, it seems mockMvc.perform() will use a different httpSession, which does not CURRENT_USER in it.
    @Autowired
    private MockHttpSession session;

    //SecurityMockMvcConfigurers.springSecurity() will integrate Spring Security with Spring MVC Test
    @BeforeClass(alwaysRun=true)
    public void setup() {
        mockMvc = MockMvcBuilders.webAppContextSetup(context).apply(springSecurity()).build();
    }

    //@WithMockUser does not call UserDetailsService and the principal just contains username, password, authorities.
    //@WithUserDetails calls UserDetailsService and returns customized principal.
    //The custom principal is often times returned by a custom UserDetailsService that returns an object that implements both UserDetails and the custom type.
    //For situations like this, it is useful to create the test user using the custom UserDetailsService. That is exactly what @WithUserDetails does.

    @Test
    public void gzhOpsLogin() throws Exception {
        //formLogin does not set the host header in the httprequest. 
        //Host header is needed in processing the /use/login to get the redirectUrl
        //Therefore, we cannot use mockMvc.perform(formLogin("/user/login").user("admin").password("123")) to test /user/login
        
        MockHttpServletRequestBuilder requestBuilder = post("/pub/user/login")
            .param("username", "admin")
            .param("password", "123")
            .header("Host", "localhost")
            .with(csrf());
        
        mockMvc.perform(requestBuilder)
            .andDo(print())
            .andExpect(status().is3xxRedirection())
            .andExpect(redirectedUrl("http://localhost/gzhOps"));
    }
    
    //Since we are using mockMvc, logout does not depend on login. 
    //We can logout without first login. Also, we do not need to set the username in logout.
    @Test
    public void gzhOpsLogout() throws Exception {
        MockHttpServletRequestBuilder requestBuilder = post("/pub/user/logout")
            .param("username", "admin")
            .with(csrf());
        
        mockMvc.perform(requestBuilder)
            .andExpect(status().is3xxRedirection())
            .andExpect(redirectedUrl("/pub/user/logoutSuccess"));
    }
    
    @Test
    @WithUserDetails("user1")
    public void gzhOpsUserHomePage() throws Exception {
        mockMvc.perform(get("/gzhOps").session(session))
            .andExpect(status().isOk());
    }
    
    @Test
    @WithUserDetails("user1")
    public void gzhOpsUserMyProfile() throws Exception {
        MockHttpServletRequestBuilder requestBuilder = get("/user/myProfile").session(session);
        mockMvc.perform(requestBuilder)
            .andExpect(status().isOk());
    }
  
    @Test
    @WithUserDetails("admin")
    public void gzhOpsAdminUserList() throws Exception {
        MockHttpServletRequestBuilder requestBuilder = get("/user/list").session(session);
        mockMvc.perform(requestBuilder)
            .andExpect(status().isOk())
            .andExpect(content().string(containsString("admin@gandongtech.com")));
    }
    
    @Test
    @WithUserDetails("user1")
    public void gzhOpsUserUserListNotAllowed() throws Exception {
        MockHttpServletRequestBuilder requestBuilder = get("/user/list").session(session);
        mockMvc.perform(requestBuilder)
            .andDo(print())
            .andExpect(status().is4xxClientError())
            .andExpect(content().string(containsString("不允许访问")));
    }
    
    @Test
    @WithUserDetails("admin")
    public void gzhOpsAdminUserDetails() throws Exception {
        MockHttpServletRequestBuilder requestBuilder = get("/user/details/1").session(session);
        mockMvc.perform(requestBuilder)
            .andExpect(status().isOk())
            .andExpect(content().string(containsString("user1@company1.com")));
    }
    
    @Test
    @WithUserDetails("user1")
    public void gzhOpsUserUserDetails() throws Exception {
        MockHttpServletRequestBuilder requestBuilder = get("/user/details/1").session(session);
        mockMvc.perform(requestBuilder)
        .andDo(print())
            .andExpect(status().isOk())
            .andExpect(content().string(containsString("user1@company1.com")));
    }
    
    @Test
    @WithUserDetails("user2")
    public void gzhOpsUserUserDetailsNotAllowed() throws Exception {
        MockHttpServletRequestBuilder requestBuilder = get("/user/details/1").session(session);
        mockMvc.perform(requestBuilder)
            .andExpect(status().is4xxClientError())
            .andExpect(content().string(containsString("不允许访问")));
    }
}
