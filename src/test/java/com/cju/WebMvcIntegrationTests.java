package com.cju;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.ResponseEntity;
import static org.assertj.core.api.Assertions.assertThat;
import org.testng.annotations.*;

//@RunWith(SpringRunner.class) tells JUnit to run using Spring’s testing support. SpringRunner is the new name for SpringJUnit4ClassRunner.
//@SpringBootTest is saying “bootstrap with Spring Boot’s support” (e.g. load application.properties and give me all the Spring Boot goodness)
//If we want to load a specific configuration, we can use the classes attribute of @SpringBootTest. 
//In this example, we’ve omitted classes which means that the test will first attempt to load @Configuration from any inner-classes, 
//and if that fails, it will search for your primary @SpringBootApplication class.
//@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT) will starts a real web container (Tomcat).

//Because we are starting a real web container, we can not use MockMvc and MockUser etc.
//We need to test with htmlunit or webdriver or restTemplate.

//TBD. We should use htmlunit or webdriver to test html pages.

//Do not need @RunWith for TestNG
//@RunWith(SpringRunner.class)

@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
public class WebMvcIntegrationTests extends AbstractTestNGSpringContextTests{
    //TestRestTemplate is only auto-configured when @SpringBootTest has been configured with a webEnvironment that means 
    //it starts the web container and listens for HTTP requests.
    //It’s pre-configured to resolve relative paths to http://localhost:${local.server.port}.
    
    @Autowired
    private TestRestTemplate restTemplate;
    
    @Test
    public void contextLoads() {
    }

    @Test
    public void gzhOpsAdminGzhOpsPage() {
        ResponseEntity<String> response = this.restTemplate.getForEntity("/gzhOps", String.class);
        System.out.println(response.getHeaders());
        String loginUrl = response.getHeaders().get("Location").get(0);
        assertThat(loginUrl.contains("/login?redirectUrl="));
    }
}
