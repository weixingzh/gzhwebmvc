package com.cju;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.security.test.context.support.WithSecurityContextTestExecutionListener;
import org.springframework.security.test.context.support.WithUserDetails;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.hamcrest.Matchers.containsString;
import org.testng.annotations.*;

//@RunWith(SpringRunner.class) tells JUnit to run using Spring’s testing support. SpringRunner is the new name for SpringJUnit4ClassRunner.
//To use this class, simply annotate a JUnit 4 based test class with @RunWith(SpringRunner.class).
//It is not needed when using testNG

//@SpringBootTest is saying “bootstrap with Spring Boot’s support” (e.g. load application.properties and give me all the Spring Boot goodness)
//If we want to load a specific configuration, we can use the classes attribute of @SpringBootTest. 
//In this example, we’ve omitted classes which means that the test will first attempt to load @Configuration from any inner-classes, 
//and if that fails, it will search for your primary @SpringBootApplication class.

//@Target(value=TYPE)
//@Retention(value=RUNTIME)
//@Documented
//@Inherited
//@BootstrapWith(value=SpringBootTestContextBootstrapper.class)
//public @interface SpringBootTest
//Annotation that can be specified on a test class that runs Spring Boot based tests. 
//Provides the following features over and above the regular Spring TestContext Framework:
//    Uses SpringBootContextLoader as the default ContextLoader when no specific @ContextConfiguration(loader=...) is defined.
//    Automatically searches for a @SpringBootConfiguration when nested @Configuration is not used, and no explicit classes are specified.
//    Allows custom Environment properties to be defined using the properties attribute.
//    Provides support for different webEnvironment modes, including the ability to start a fully running container listening on a defined or random port.
//    Registers a TestRestTemplate bean for use in web tests that are using a fully running container.

//@ContextConfiguration can be used to declare either path-based resource locations (via the locations() or value() attribute) or annotated classes (via the classes() attribute). 
//    The term annotated class can refer to any of the following.
//    A class annotated with @Configuration
//    A component (i.e., a class annotated with @Component, @Service, @Repository, etc.)
//    A JSR-330 compliant class that is annotated with javax.inject annotations
//    Any other class that contains @Bean-methods

//Without (webEnvironment = WebEnvironment.RANDOM_PORT), there is no real container (Tomcat) setup and running.
//@WebMvcTest is only going to scan the controller you've defined and the MVC infrastructure.
//We are not using @AutoConfigureMockMvc since we build the mockMvc with spring security.

//Spring MVC Test provides a convenient interface called a RequestPostProcessor that can be used to modify a request.

//@Target(value={METHOD,TYPE})
//@Retention(value=RUNTIME)
//@Inherited
//@Documented
//@WithSecurityContext(factory=org.springframework.security.test.context.support.WithUserDetailsSecurityContextFactory.class)
//public @interface WithUserDetails
//When used with WithSecurityContextTestExecutionListener this annotation can be added to a test method to emulate running with 
//a UserDetails returned from the UserDetailsService. 

//@TestExecutionListeners(listeners={WithSecurityContextTestExecutionListener.class}) is needed to use @WithUserDetails in TestNG
//@TestExecutionListeners(listeners={WithSecurityContextTestExecutionListener.class}) is NOT needed to use @WithUserDetails in JUnit

//@ActiveProfiles() will override the spring.profiles.active=dev in application.properties

@SpringBootTest
@TestExecutionListeners(listeners={WithSecurityContextTestExecutionListener.class})
@ActiveProfiles("dev, dev-use-testdb, dev-clear-and-load-json-false")
public class MockMvcGzhHtmlPageTests extends AbstractTestNGSpringContextTests {
    
    @Autowired
    private WebApplicationContext context;
    private MockMvc mockMvc;
    
    @Autowired
    private MockHttpSession session;

    //SecurityMockMvcConfigurers.springSecurity() will integrate Spring Security with Spring MVC Test
    @BeforeClass
    public void setup() {
        mockMvc = MockMvcBuilders.webAppContextSetup(context).apply(springSecurity()).build();
    }
    
    @Test
    @WithUserDetails("user1")
    public void gzhOpsUserGzhCompanyMyIntroPage() throws Exception {
        MockHttpServletRequestBuilder requestBuilder = get("/gzhOps/htmlPage/details/3")
            .session(session);
        mockMvc.perform(requestBuilder)
            .andExpect(status().isOk())
            .andDo(print())
            .andExpect(content().string(containsString("公司简介")));
    }
}
