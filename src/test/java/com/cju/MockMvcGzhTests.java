package com.cju;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.security.test.context.support.WithSecurityContextTestExecutionListener;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.hamcrest.Matchers.containsString;
import org.testng.annotations.*;

//@RunWith(SpringRunner.class) tells JUnit to run using Spring’s testing support. SpringRunner is the new name for SpringJUnit4ClassRunner.
//@SpringBootTest is saying “bootstrap with Spring Boot’s support” (e.g. load application.properties and give me all the Spring Boot goodness)
//If we want to load a specific configuration, we can use the classes attribute of @SpringBootTest. 
//In this example, we’ve omitted classes which means that the test will first attempt to load @Configuration from any inner-classes, 
//and if that fails, it will search for your primary @SpringBootApplication class.

//Without (webEnvironment = WebEnvironment.RANDOM_PORT), there is no real container (Tomcat) setup and running.
//@WebMvcTest is only going to scan the controller you've defined and the MVC infrastructure.
//We are not using @AutoConfigureMockMvc since we build the mockMvc with spring security.

//Spring MVC Test provides a convenient interface called a RequestPostProcessor that can be used to modify a request.

@SpringBootTest
@TestExecutionListeners(listeners={WithSecurityContextTestExecutionListener.class})
@ActiveProfiles("dev, dev-use-testdb, dev-clear-and-load-json-false")
public class MockMvcGzhTests extends AbstractTestNGSpringContextTests {
    private Logger log = LoggerFactory.getLogger(MockMvcGzhTests.class);
    
    @Autowired
    private WebApplicationContext context;
    private MockMvc mockMvc;
    
    //SecurityMockMvcConfigurers.springSecurity() will integrate Spring Security with Spring MVC Test
    @BeforeClass
    public void setup() {
        mockMvc = MockMvcBuilders.webAppContextSetup(context).apply(springSecurity()).build();
    }
    
    @Test
    public void gzhCompanyLocalHtmlIntroPage() throws Exception {
        MockHttpServletRequestBuilder requestBuilder = get("/pub/gzh/company/1/localHtml?pageType=IntroPage");
        mockMvc.perform(requestBuilder)
        .andExpect(status().isOk())
        .andExpect(content().string(containsString("NGTest HtmlPage for GzhCompany1")));
    }
    
    @Test
    public void gzhCompanyContactList() throws Exception {
        MockHttpServletRequestBuilder requestBuilder = get("/pub/gzh/company/1/contact/list");
        mockMvc.perform(requestBuilder)
        .andExpect(status().isOk())
        .andExpect(content().string(containsString("DuWeiXin")));
    }
   
    //The following tests need to have openId on the server side to identify the gzhUser
    //authorizationInterceptor() in WebMvcConfigure.java can detect request starting with /gzh/company coming from local and
    //hard code the openId in HttpSession with value olrli1bueeEdDGJJBPEINhXk_Gaw
    @Test
    public void gzhCompanyTaskList() throws Exception {
        MockHttpServletRequestBuilder requestBuilder = get("/gzh/company/1/task/list");
        mockMvc.perform(requestBuilder)
        .andExpect(status().isOk())
        .andExpect(content().string(containsString("NGTest Task")));
    }
    
    @Test
    public void gzhClientCompanyList() throws Exception {
        MockHttpServletRequestBuilder requestBuilder = get("/gzh/company/1/clientCompany/list");
        mockMvc.perform(requestBuilder)
            .andExpect(status().isOk())
            .andExpect(content().string(containsString("珠海市恒嘉土石方工程有限公司")));
    }
    
    @Test
    public void gzhClientCompanyReportList() throws Exception {
        MockHttpServletRequestBuilder requestBuilder = get("/gzh/company/1/clientCompany/1/report/list");
        mockMvc.perform(requestBuilder)
            .andExpect(status().isOk());
    }

    
}
