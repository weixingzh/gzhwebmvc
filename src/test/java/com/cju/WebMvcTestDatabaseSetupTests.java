package com.cju;

import static org.hamcrest.Matchers.containsString;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.security.authentication.encoding.Md5PasswordEncoder;
import org.springframework.security.test.context.support.WithSecurityContextTestExecutionListener;
import org.springframework.security.test.context.support.WithUserDetails;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import org.testng.annotations.*;
import com.cju.gzhOps.GzhAutoReplyType;
import com.cju.gzhOps.GzhTaskStatus;
import com.cju.gzhOps.GzhTaskType;
import com.cju.user.User;
import com.cju.user.UserRepository;

//@RunWith(SpringRunner.class) tells JUnit to run using Spring’s testing support. SpringRunner is the new name for SpringJUnit4ClassRunner.
//@SpringBootTest is saying “bootstrap with Spring Boot’s support” (e.g. load application.properties and give me all the Spring Boot goodness)
//If we want to load a specific configuration, we can use the classes attribute of @SpringBootTest. 
//In this example, we’ve omitted classes which means that the test will first attempt to load @Configuration from any inner-classes, 
//and if that fails, it will search for your primary @SpringBootApplication class.

//Without (webEnvironment = WebEnvironment.RANDOM_PORT), there is no real container (Tomcat) setup and running.
//@WebMvcTest is only going to scan the controller you've defined and the MVC infrastructure.
//We are not using @AutoConfigureMockMvc since we build the mockMvc with spring security.

//Spring MVC Test provides a convenient interface called a RequestPostProcessor that can be used to modify a request.

//By default, TestNG will run your tests in the order they are found in the XML file. 
//The order of invocation for methods that belong in the same group is not guaranteed to be the same across test run

@SpringBootTest
@TestExecutionListeners(listeners={WithSecurityContextTestExecutionListener.class})
@ActiveProfiles("dev, dev-use-testdb, dev-createdb, dev-insert-data, dev-clear-and-load-json-true")
public class WebMvcTestDatabaseSetupTests extends AbstractTestNGSpringContextTests {
    private Logger log = LoggerFactory.getLogger(WebMvcTestDatabaseSetupTests.class);
    
    @Autowired
    private UserRepository userRepository;
    
    @Autowired
    private WebApplicationContext context;
    private MockMvc mockMvc;
   
    //mockMvc creates a HttpSession. This httpSession is used in Spring Security Filter Chain. 
    //@WithUserDetails will trigger Spring Security Filter Chain, which calls UserDetailsService().
    //UserDetailsService() will cache CURRENT_USER in httpSession.
    //In mockMvc.perform() call, we pass in the same httpSession. The Thymeleaf templates use the httpSession 
    //to get the cached CURRENT_USER and GzhCompany name.
    //If we do not pass the httpSession, it seems mockMvc.perform() will use a different httpSession, which does not CURRENT_USER in it.
    @Autowired
    private MockHttpSession session;

    //SecurityMockMvcConfigurers.springSecurity() will integrate Spring Security with Spring MVC Test
    //Set alwaysRun=true so that setup() will be run even though when a group of tests in this class is picked to run in testng.xml file.
    @BeforeClass(alwaysRun = true)
    public void setup() {
        log.info("@BeforeClass setup() called.");
        mockMvc = MockMvcBuilders.webAppContextSetup(context).apply(springSecurity()).build();
    }
    
    //By setting dev-createdb profile, this will trigger Hibernate to drop all the tables and create new tables.
    //By setting dev-insert-data profile, this will trigger the WebMvcInitTestDatabase.java to run to insert data into database.
    @Test(groups="init-testdb")
    public void initTestDb() {
        //This test will return success always. It will trigger WebMvcInitTestDatabase.java.
        //WebMvcInitTestDatabase.java creates User, GzhCompany, GzhUser, GzhClientCompany
    }
    
    @Test(groups="user-registration")
    public void gzhOpsUserRegister() throws Exception {
        MockHttpServletRequestBuilder requestBuilder = post("/pub/user/register")
            .param("userName", "aabbcc")
            .param("password", "123")
            .param("email", "aabbcc@b.com")
            .with(csrf());
      
        mockMvc.perform(requestBuilder)
            .andDo(print())
            .andExpect(status().isOk())
            .andExpect(content().string(containsString("注册成功。请查看邮件并激活账号")));
    }
    
    @Test(groups="user-registration", dependsOnMethods = { "gzhOpsUserRegister" })
    public void gzhOpsUserActivate() throws Exception {
        String userName = "aabbcc";
        User user = userRepository.findOneByUserName(userName);
        String activationToken = user.getActivationToken();
        String url = "/pub/user/activate?email=aabbcc@b.com&activation=" + activationToken;
        MockHttpServletRequestBuilder requestBuilder = get(url);
        mockMvc.perform(requestBuilder)
          .andExpect(status().isOk())
          .andExpect(content().string(containsString("账号激活成功")));;
    }
    
    
    @Test(groups="gzhClientCompany-group", dependsOnGroups = { "init-testdb" })
    @WithUserDetails("admin")
    public void gzhOpsAdminPostGzhUserListByGzhClientCompanyId() throws Exception {
        long gzhClientCompanyId = 1L;
        MockHttpServletRequestBuilder requestBuilder = getPostGzhUserListByGzhClientCompanyIdBuilder(gzhClientCompanyId);
        mockMvc.perform(requestBuilder)
        .andExpect(status().isOk())
        .andExpect(content().string(containsString("gzhUser1")))
        .andExpect(content().string(containsString("gzhUser3")));
    }
    
    @Test(groups="chanjet-bind-and-import", dependsOnGroups = { "init-testdb" })
    @WithUserDetails("user1")
    public void gzhOpsUserBindChanjet() throws Exception {
        JSONObject chanjetUser = new JSONObject();
        
        //chanjetUser is created in webMvcTestDatabaseInit.java
        chanjetUser.put("id", "1");
        chanjetUser.put("chanjetUserName", "15916201201");
        chanjetUser.put("chanjetPassword", "Ab123456");
        
        String chanjetUserJson = chanjetUser.toString();
        log.info("chanjetUserJson=" + chanjetUserJson);
        
        MockHttpServletRequestBuilder requestBuilder = post("/gzhOps/api/v1/chanjet/bind")
            .session(session)
            .contentType("application/json")
            .content(chanjetUserJson)
            .with(csrf());
        
        mockMvc.perform(requestBuilder)
            .andDo(print())
            .andExpect(status().isOk())
            .andExpect(content().string(containsString("chanjetCiaToken")));
    }
    
    @Test(groups="chanjet-bind-and-import", dependsOnGroups = { "init-testdb" }, dependsOnMethods = { "gzhOpsUserBindChanjet" })
    @WithUserDetails("user1")
    public void gzhOpsUserChanjetClientCompanyImport() throws Exception  {
        JSONObject chanjetClientCompany = new JSONObject();
        
        //chanjetUser is created in webMvcTestDatabaseInit.java
        chanjetClientCompany.put("accId", "1219869");
        
        String chanjetClientCompanyJson = chanjetClientCompany.toString();
        log.info("chanjetClientCompanyJson=" + chanjetClientCompanyJson);
        
        MockHttpServletRequestBuilder requestBuilder = post("/gzhOps/api/v1/chanjetClientCompany/import")
            .session(session)
            .contentType("application/json")
            .content(chanjetClientCompanyJson)
            .with(csrf());
        
        mockMvc.perform(requestBuilder)
            .andDo(print())
            .andExpect(status().isOk())
            .andExpect(content().string(containsString("珠海市恒嘉土石方工程有限公司")));
    }
    
    //gzhClientCompany 5 is 珠海市恒嘉土石方工程有限公司
    //This test case fails sometime due to Chanjet server not returning data.
    @Test(groups="gzhClientCompany-group", dependsOnGroups = { "chanjet-bind-and-import" })
    @WithUserDetails("user1")
    public void gzhOpsUserSubmitGzhClientCompanyReportImportForm() throws Exception  {
        JSONObject reportImport = new JSONObject();
        
        reportImport.put("period", "201701");
        reportImport.put("reportTypeValue", "Asset");
        
        String reportImportJson = reportImport.toString();
        log.info("reportImportJson=" + reportImportJson);
        
        MockHttpServletRequestBuilder requestBuilder = post("/gzhOps/api/v1/gzhClientCompany/5/report/import")
            .session(session)
            .contentType("application/json")
            .content(reportImportJson)
            .with(csrf());
        
        mockMvc.perform(requestBuilder)
            .andDo(print())
            .andExpect(status().isOk())
            .andExpect(content().string(containsString("珠海市恒嘉土石方工程有限公司")));
    }
    
    @Test(groups="gzhClientCompany-group", dependsOnGroups = { "chanjet-bind-and-import" })
    @WithUserDetails("user1")
    public void gzhOpsUserPostGzhUserListByGzhClientCompanyId() throws Exception {
        long gzhClientCompanyId = 5L;
        MockHttpServletRequestBuilder requestBuilder = getPostGzhUserListByGzhClientCompanyIdBuilder(gzhClientCompanyId);
        mockMvc.perform(requestBuilder)
            .andExpect(status().isOk())
            .andExpect(content().string(containsString("gzhUser1")))
            .andExpect(content().string(containsString("gzhUser3")));
    }
    
    //Create IntroPage for GzhCompany 1 
    @Test(groups="gzhCompany-group", dependsOnGroups = { "init-testdb" })
    @WithUserDetails("user1")
    public void gzhOpsUserPostGzhIntroPageByUser() throws Exception {
        JSONObject htmlPage = new JSONObject();
        
        //chanjetUser is created in webMvcTestDatabaseInit.java
        htmlPage.put("pageType", "公司简介");
        htmlPage.put("linkType", "本地网页");
        htmlPage.put("localHtml", "NGTest HtmlPage for GzhCompany1");
        
        String htmlPageJson = htmlPage.toString();
        log.info("htmlPageJson=" + htmlPageJson);

        MockHttpServletRequestBuilder requestBuilder = post("/gzhOps/htmlPage/createOrEdit")
           .session(session)
           .contentType("application/json")
           .content(htmlPageJson)
           .with(csrf());

        mockMvc.perform(requestBuilder)
           .andExpect(status().isOk());
    }
    
    //Create AutoReply for GzhCompany 1 
    @Test(groups="gzhCompany-group", dependsOnGroups = { "init-testdb" })
    @WithUserDetails("user1")
    public void gzhOpsUserPostGzhAutoReplyByUser() throws Exception {
        MockHttpServletRequestBuilder requestBuilder = getPostGzhAutoReplyListByUserBuilder();
        
        mockMvc.perform(requestBuilder)
           .andExpect(status().isOk())
           .andExpect(content().string(containsString("NGTest Message2")));
    }
    
    //Create a task for gzhUser1
    @Test(groups="gzhCompany-group", dependsOnGroups = { "init-testdb" })
    @WithUserDetails("user1")
    public void gzhOpsUserPostGzhTaskForGzhUser() throws Exception {
        MockHttpServletRequestBuilder requestBuilder = getPostGzhTaskForGzhUserBuilder();
        
        mockMvc.perform(requestBuilder)
           .andExpect(status().isOk())
           .andDo(print())
           .andExpect(content().string(containsString("NGTest Task Name")));
    }
    
    //Create a contact for gzhCompany1
    @Test(groups="gzhCompany-group", dependsOnGroups = { "init-testdb" })
    @WithUserDetails("user1")
    public void gzhOpsUserPostGzhCompanyContact() throws Exception {
        JSONObject gzhContactHttpPostData = new JSONObject();
        JSONArray gzhContactList = new JSONArray();
        
        JSONObject gzhContact1 = new JSONObject();
        gzhContact1.put("name", "Du");
        gzhContact1.put("jobTitle", "会计");
        gzhContact1.put("mobile", "123456789");
        gzhContact1.put("weixin", "DuWeiXin");
        
        gzhContactList.put(gzhContact1);
        
        gzhContactHttpPostData.put("createOrEditList", gzhContactList);
        gzhContactHttpPostData.put("removeList", new JSONArray());
        
        String gzhContactHttpPostDataJson = gzhContactHttpPostData.toString();
        log.info("gzhContactHttpPostDataJson=" + gzhContactHttpPostDataJson);
        MockHttpServletRequestBuilder requestBuilder = post("/gzhOps/api/v1/contact/myList")
            .session(session)
            .contentType("application/json")
            .content(gzhContactHttpPostDataJson)
            .with(csrf());
        
        mockMvc.perform(requestBuilder)
            .andExpect(status().isOk())
            .andExpect(content().string(containsString("DuWeiXin")));
    }
    
    
    private MockHttpServletRequestBuilder getPostGzhUserListByGzhClientCompanyIdBuilder(long gzhClientCompanyId) throws JSONException {
        JSONArray gzhUserList = new JSONArray();
        JSONObject gzhUser1 = new JSONObject();
        gzhUser1.put("id", "1");
        gzhUser1.put("name", "gzhUser1");
        gzhUserList.put(gzhUser1);
        JSONObject gzhUser3 = new JSONObject();
        gzhUser3.put("id", "3");
        gzhUser3.put("name", "gzhUser3");
        gzhUserList.put(gzhUser3);

        String simpleGzhUserListJson = gzhUserList.toString();
        log.info("simpleGzhUserListJson=" + simpleGzhUserListJson);
        MockHttpServletRequestBuilder requestBuilder = post("/gzhOps/api/v1/gzhUser/list/gzhClientCompany/" + gzhClientCompanyId)
                .contentType("application/json")
                .content(simpleGzhUserListJson)
                .with(csrf());
        return requestBuilder;
    }
    
    private MockHttpServletRequestBuilder getPostGzhAutoReplyListByUserBuilder() throws JSONException {
        JSONObject gzhAutoReplyPostData = new JSONObject();
        JSONArray createOrEditList = new JSONArray();
        JSONArray removeList = new JSONArray();
        
        JSONObject gzhAutoReply1 = new JSONObject();
        gzhAutoReply1.put("key", "NGTest Key1");
        gzhAutoReply1.put("message", "NGTest Message1");
        gzhAutoReply1.put("type", GzhAutoReplyType.PRIVATE);
        createOrEditList.put(gzhAutoReply1);
        JSONObject gzhAutoReply2 = new JSONObject();
        gzhAutoReply2.put("key", "NGTest Key2");
        gzhAutoReply2.put("message", "NGTest Message2");
        gzhAutoReply2.put("type", GzhAutoReplyType.PRIVATE);
        createOrEditList.put(gzhAutoReply2);
        
        gzhAutoReplyPostData.put("createOrEditList", createOrEditList);
        gzhAutoReplyPostData.put("removeList", removeList);
        
        String gzhAutoReplyPostDataJson = gzhAutoReplyPostData.toString();
        log.info("gzhAutoReplyPostDataJson=" + gzhAutoReplyPostDataJson);
        MockHttpServletRequestBuilder requestBuilder = post("/gzhOps/api/v1/autoReply")
                .session(session)
                .contentType("application/json")
                .content(gzhAutoReplyPostDataJson)
                .with(csrf());
        return requestBuilder;
    }
    
    private MockHttpServletRequestBuilder getPostGzhTaskForGzhUserBuilder() throws JSONException {
        JSONObject gzhTask = new JSONObject();
        JSONArray gzhTaskStepList = new JSONArray();
        
        JSONObject gzhTaskStep1 = new JSONObject();
        gzhTaskStep1.put("order", "1");
        gzhTaskStep1.put("name", "NGTest Task Step 1");
        gzhTaskStep1.put("status", GzhTaskStatus.IN_PROCESS);
        
        JSONObject gzhTaskStep2 = new JSONObject();
        gzhTaskStep2.put("order", "2");
        gzhTaskStep2.put("name", "NGTest Task Step 2");
        gzhTaskStep2.put("status", GzhTaskStatus.NOT_STARTED);
        
        gzhTaskStepList.put(gzhTaskStep1);
        gzhTaskStepList.put(gzhTaskStep2);
        
        gzhTask.put("type", GzhTaskType.CUSTOMER_TASK);
        gzhTask.put("name", "NGTest Task Name");
        gzhTask.put("status", GzhTaskStatus.NOT_STARTED);
        gzhTask.put("steps", gzhTaskStepList);
        
        String gzhTaskJson = gzhTask.toString();
        log.info("gzhTaskJson=" + gzhTaskJson);
        MockHttpServletRequestBuilder requestBuilder = post("/gzhOps/api/v1/task/gzhUser/1")
                .session(session)
                .contentType("application/json")
                .content(gzhTaskJson)
                .with(csrf());
        return requestBuilder;
    }

}
